/// @DnDAction : YoYo Games.Random.Randomize
/// @DnDVersion : 1
/// @DnDHash : 2927427E
randomize();

/// @DnDAction : YoYo Games.Drawing.Set_Font
/// @DnDVersion : 1
/// @DnDHash : 64A167B8
/// @DnDArgument : "font" "fnt_main"
/// @DnDSaveInfo : "font" "6058b07f-930e-4758-9f4d-1b3a2c3a1cf7"
draw_set_font(fnt_main);

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 528B5655
/// @DnDArgument : "code" "global.remainingspawns = 10$(13_10)global.currentlevel = 1$(13_10)global.current_tower = 0$(13_10)instance_create_layer(2400,1300,"Instances",obj_next_level)$(13_10)global.path = forest$(13_10)$(13_10)"
global.remainingspawns = 10
global.currentlevel = 1
global.current_tower = 0
instance_create_layer(2400,1300,"Instances",obj_next_level)
global.path = forest