/// @DnDAction : YoYo Games.Random.Randomize
/// @DnDVersion : 1
/// @DnDHash : 50DCE793
randomize();

/// @DnDAction : YoYo Games.Drawing.Set_Font
/// @DnDVersion : 1
/// @DnDHash : 4B1A62F2
/// @DnDArgument : "font" "fnt_main"
/// @DnDSaveInfo : "font" "6058b07f-930e-4758-9f4d-1b3a2c3a1cf7"
draw_set_font(fnt_main);

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 20E145F6
/// @DnDArgument : "code" "global.muted = false$(13_10)global.speedup = false$(13_10)global.ui = true$(13_10)global.can_tab = true$(13_10)global.tooltip = " "$(13_10)global.currentlevel = 1$(13_10)global.tower_healed = 0$(13_10)global.killed_by_poison = 0$(13_10)global.encrypted = true$(13_10)$(13_10)$(13_10)if !file_exists("info.sav")$(13_10)then$(13_10){$(13_10)global.map = ds_map_create();$(13_10)ds_map_add(global.map,"currency", 0)$(13_10)ds_map_add(global.map,"Player Name", "new_player")$(13_10)$(13_10)}$(13_10)else$(13_10){$(13_10)	global.map = ds_map_secure_load("info.sav")$(13_10)}$(13_10)global.premium_currency = ds_map_find_value(global.map,"currency")$(13_10)log("[PLAYER INFO]   Player has a valid name: " + string(ds_map_find_value(global.map,"Player Name")))$(13_10)log("[PLAYER INFO]   Player has: " + string(global.premium_currency) + " shields")$(13_10)global.not_enough_gold = false$(13_10)$(13_10)if os_is_network_connected() then$(13_10){$(13_10)var _promise = undefined;$(13_10)var apiKey = "6eb3990f-6db6-4440-be33-b6879e6a236d";$(13_10)if gameon_util_session_data_exists()$(13_10)    {$(13_10)$(13_10)		{$(13_10)	fast_file_key_crypt("_amazon_gameon_session.dat","_amazon_gameon_session.dat",false,"work")$(13_10)		}$(13_10)		_promise = gameon_init(apiKey, eGameOnAuth.LOAD_EXISTING);$(13_10)    }$(13_10)else$(13_10)    {$(13_10)    _promise = gameon_init(apiKey, eGameOnAuth.REGISTER_NEW);$(13_10)    }$(13_10)if(gameon_util_object_exists(_promise))$(13_10)    {$(13_10)    _promise.delegate_instance = id;$(13_10)    _promise.delegate_event_success = 0;$(13_10)    _promise.delegate_event_failure = 1;$(13_10)    }$(13_10)else gameon_util_log("GameOn authentication request could not be sent");$(13_10)}$(13_10)"
global.muted = false
global.speedup = false
global.ui = true
global.can_tab = true
global.tooltip = " "
global.currentlevel = 1
global.tower_healed = 0
global.killed_by_poison = 0
global.encrypted = true


if !file_exists("info.sav")
then
{
global.map = ds_map_create();
ds_map_add(global.map,"currency", 0)
ds_map_add(global.map,"Player Name", "new_player")

}
else
{
	global.map = ds_map_secure_load("info.sav")
}
global.premium_currency = ds_map_find_value(global.map,"currency")
log("[PLAYER INFO]   Player has a valid name: " + string(ds_map_find_value(global.map,"Player Name")))
log("[PLAYER INFO]   Player has: " + string(global.premium_currency) + " shields")
global.not_enough_gold = false

if os_is_network_connected() then
{
var _promise = undefined;
var apiKey = "6eb3990f-6db6-4440-be33-b6879e6a236d";
if gameon_util_session_data_exists()
    {

		{
	fast_file_key_crypt("_amazon_gameon_session.dat","_amazon_gameon_session.dat",false,"work")
		}
		_promise = gameon_init(apiKey, eGameOnAuth.LOAD_EXISTING);
    }
else
    {
    _promise = gameon_init(apiKey, eGameOnAuth.REGISTER_NEW);
    }
if(gameon_util_object_exists(_promise))
    {
    _promise.delegate_instance = id;
    _promise.delegate_event_success = 0;
    _promise.delegate_event_failure = 1;
    }
else gameon_util_log("GameOn authentication request could not be sent");
}