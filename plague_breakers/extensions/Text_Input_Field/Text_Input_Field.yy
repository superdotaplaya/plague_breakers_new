{
    "id": "d5eedad0-c05b-42d3-9668-bc556fd8cfe7",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "Text_Input_Field",
    "IncludedResources": [
        "Sprites\\spr_textbox",
        "Scripts\\textbox_text_selected_get",
        "Scripts\\textbox_text_selected_remove",
        "Scripts\\textbox_text_selected_set",
        "Scripts\\textbox_text_insert",
        "Scripts\\textbox_get_cursor_at",
        "Scripts\\string_get_highest_index",
        "Scripts\\string_get_lowest_index",
        "Objects\\obj_textbox"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": -1,
    "date": "2019-40-20 03:02:14",
    "description": "",
    "extensionName": "",
    "files": [
        
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": "",
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "com.kanelbull3games.textinputfield",
    "productID": "F79C97955FD1AF4686EB4EF4EF90F0DB",
    "sourcedir": "",
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": "",
    "tvosdelegatename": "",
    "tvosmaccompilerflags": "",
    "tvosmaclinkerflags": "",
    "tvosplistinject": "",
    "version": "1.1.1"
}