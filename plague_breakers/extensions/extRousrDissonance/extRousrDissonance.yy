{
    "id": "5b35a2f7-9bf1-42a2-8269-0c79a1314630",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "extRousrDissonance",
    "IncludedResources": [
        "Sprites\\bg",
        "Sprites\\sCheck",
        "Sprites\\sButton",
        "Sprites\\sPixel",
        "Scripts\\Dissonance\\rousr_dissonance_create",
        "Scripts\\Dissonance\\rousr_dissonance_dummy_function",
        "Scripts\\Dissonance\\rousr_dissonance_handler_on_ready",
        "Scripts\\Dissonance\\rousr_dissonance_handler_on_disconnected",
        "Scripts\\Dissonance\\rousr_dissonance_handler_on_error",
        "Scripts\\Dissonance\\rousr_dissonance_handler_on_join",
        "Scripts\\Dissonance\\rousr_dissonance_handler_on_spectate",
        "Scripts\\Dissonance\\rousr_dissonance_handler_on_join_request",
        "Scripts\\Dissonance\\rousr_dissonance_respond_to_join",
        "Scripts\\Dissonance\\rousr_dissonance_set_details",
        "Scripts\\Dissonance\\rousr_dissonance_set_large_image",
        "Scripts\\Dissonance\\rousr_dissonance_set_small_image",
        "Scripts\\Dissonance\\rousr_dissonance_set_timestamps",
        "Scripts\\Dissonance\\rousr_dissonance_set_state",
        "Scripts\\Dissonance\\rousr_dissonance_set_party",
        "Scripts\\Dissonance\\rousr_dissonance_set_join_secret",
        "Scripts\\Dissonance\\rousr_dissonance_set_spectate_secret",
        "Scripts\\Dissonance\\rousr_dissonance_set_match_secret",
        "Scripts\\Dissonance\\internal\\rousrDissonance_event_create",
        "Scripts\\Dissonance\\internal\\rousrDissonance_event_step",
        "Scripts\\Dissonance\\internal\\rousrDissonance_event_end_step",
        "Scripts\\Dissonance\\internal\\rousrDissonance_event_async_social",
        "Scripts\\Dissonance\\internal\\gmlscripts_unix_timestamp",
        "Scripts\\Example\\button_can_click",
        "Scripts\\Example\\example_click_accept",
        "Scripts\\Example\\example_click_ignore",
        "Scripts\\Example\\example_click_reject",
        "Scripts\\Example\\example_on_disconnected",
        "Scripts\\Example\\example_on_error",
        "Scripts\\Example\\example_on_join",
        "Scripts\\Example\\example_on_join_request",
        "Scripts\\Example\\example_on_ready",
        "Scripts\\Example\\example_on_spectate",
        "Scripts\\Example\\example_text_field",
        "Scripts\\Example\\example_toggle",
        "Scripts\\Example\\random_key",
        "Scripts\\ReadMe",
        "Fonts\\fnt_game",
        "Objects\\Dissonance\\rousrDissonance",
        "Objects\\Example\\Example",
        "Objects\\Example\\Button",
        "Objects\\Example\\CheckBox",
        "Objects\\Example\\TextField",
        "Objects\\Example\\logo",
        "Rooms\\rm_demo",
        "Included Files\\Example_Assets__App_Dashboard_\\dissonance_large.png",
        "Included Files\\Example_Assets__App_Dashboard_\\dissonance_small.png",
        "Included Files\\04b24.TTF",
        "Included Files\\README.md"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 105553859707074,
    "date": "2019-40-31 01:01:24",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "68c0fbc2-a033-4a8b-9520-749dd4fe5132",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 105553859707074,
            "filename": "rousrDissonance.dll",
            "final": "discord_shutdown",
            "functions": [
                {
                    "id": "153eef83-f147-4e65-8f3a-e0497386fad6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        1,
                        1
                    ],
                    "externalName": "Init",
                    "help": "initialize Discord with the application ID and Steam ID (_application_id, [_steam_id])",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_init",
                    "returnType": 2
                },
                {
                    "id": "8e97c450-1af8-4636-88b0-84e7df304ddf",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "UpdatePresence",
                    "help": "send the presences updates to Discord, call after sets.",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_update_presence",
                    "returnType": 2
                },
                {
                    "id": "57f42b96-bcbe-4d2d-91b5-271e1df6e048",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "RunCallbacks",
                    "help": "run any pending discord call backs - call once per step",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_run_callbacks",
                    "returnType": 2
                },
                {
                    "id": "962d2c08-d4bd-4bbe-8520-d3d06f00464c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "ResetPresence",
                    "help": "clears all presence data currently set this session",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_reset_presence",
                    "returnType": 2
                },
                {
                    "id": "f20a214e-1915-4fb5-bf03-3bf9302943b4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "SetState",
                    "help": "set the state string (_state)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_state",
                    "returnType": 2
                },
                {
                    "id": "4463e855-0216-448b-89c1-f6d6ebcb4ff9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "SetDetails",
                    "help": "set the details string (_details)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_details",
                    "returnType": 2
                },
                {
                    "id": "5bbc5cb9-404d-4b80-8fd6-08c64275803d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 4,
                    "args": [
                        2,
                        2,
                        2,
                        2
                    ],
                    "externalName": "SetTimeStamps",
                    "help": "set the start and end timestamp, in unix time format (_start_lo, _start_hi, _end_lo, _end_hi)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_timestamps",
                    "returnType": 2
                },
                {
                    "id": "6e8c3cbb-6112-4c8c-9071-d24e3368abfa",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        1,
                        1
                    ],
                    "externalName": "SetLargeImage",
                    "help": "set the large image detail and caption  (_large_image_key, _large_image_text)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_large_image",
                    "returnType": 2
                },
                {
                    "id": "dddc1324-e203-4de5-aebb-f4683b435e79",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        1,
                        1
                    ],
                    "externalName": "SetSmallImage",
                    "help": "set the small image detail and caption (_small_image_key, _small_image_text)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_small_image",
                    "returnType": 2
                },
                {
                    "id": "71d971a9-3127-4cb5-b0e7-48b69849e288",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 3,
                    "args": [
                        1,
                        2,
                        2
                    ],
                    "externalName": "SetPartyData",
                    "help": "set the party id and member count data (_party_id, _party_count, _party_max)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_party",
                    "returnType": 2
                },
                {
                    "id": "7c3f0646-af9c-4c8a-9b37-b9a225d7a2dd",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        1,
                        2
                    ],
                    "externalName": "SetMatchSecret",
                    "help": "set the match secret, and whether or not the match represents a finite \"match\" (_match_secret, _instance)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_match_secret",
                    "returnType": 2
                },
                {
                    "id": "519f8bd5-9cd3-4fb8-a944-3fe83bd63d96",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "SetJoinSecret",
                    "help": "set the unique, encrypted join secret token (_join_secret)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_join_secret",
                    "returnType": 2
                },
                {
                    "id": "05550a6d-01b2-4ba3-b832-d227dbbdaab8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 1,
                    "args": [
                        1
                    ],
                    "externalName": "SetSpectateSecret",
                    "help": "set the unique, encrypted spectate secret token (_spectate_secret)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_set_spectate_secret",
                    "returnType": 2
                },
                {
                    "id": "02f3b9a7-c172-46fb-a0a8-399c1efa4e81",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        1,
                        2
                    ],
                    "externalName": "Respond",
                    "help": "send a reply to `ask to join` request (_user_id, _reply)",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_respond",
                    "returnType": 2
                },
                {
                    "id": "9b6280e9-b8a6-417d-8e24-9c35150abf04",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 4,
                    "args": [
                        1,
                        1,
                        1,
                        1
                    ],
                    "externalName": "RegisterCallbacks",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "RegisterCallbacks",
                    "returnType": 2
                },
                {
                    "id": "f5348259-f3e1-4780-8af5-66ebbccaa623",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "Shutdown",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "discord_shutdown",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 1,
            "order": [
                
            ],
            "origname": "extensions\\rousrDissonance.dll",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": "",
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "com.rousr.dissonance",
    "productID": "1E83191CE77E300EE0DC1270C217654D",
    "sourcedir": "",
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": "",
    "tvosdelegatename": "",
    "tvosmaccompilerflags": "",
    "tvosmaclinkerflags": "",
    "tvosplistinject": "",
    "version": "1.0.3"
}