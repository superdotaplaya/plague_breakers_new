{
    "id": "95122751-823f-4d4a-b75b-6ca3608e015e",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "Sprite_Exploder",
    "IncludedResources": [
        "Sprites\\spr_demo_player",
        "Sprites\\spr_rectangle",
        "Scripts\\scr_sprite_explode",
        "Scripts\\scr_sprite_dissolve",
        "Scripts\\scr_sprite_radial",
        "Scripts\\scr_sprite_asymble",
        "Objects\\rm_explosion\\obj_demo_controller",
        "Objects\\rm_explosion\\obj_demo_player",
        "Objects\\rm_explosion\\obj_demo_particle",
        "Objects\\rm_dissolve\\obj_dissolve_controller",
        "Objects\\rm_dissolve\\obj_dissolve_particle",
        "Objects\\rm_dissolve\\obj_dissolve_player",
        "Objects\\rm_radial\\obj_radial_player",
        "Objects\\rm_radial\\obj_radial_particle",
        "Objects\\rm_radial\\obj_radial_controller",
        "Objects\\rm_asymble\\obj_asymble_player",
        "Objects\\rm_asymble\\obj_asymble_particle",
        "Objects\\rm_asymble\\obj_asymble_controller",
        "Rooms\\rm_demo_asymble",
        "Rooms\\rm_demo_explosion",
        "Rooms\\rm_demo_dissolve",
        "Rooms\\rm_demo_radial"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": -1,
    "date": "2019-41-30 01:01:43",
    "description": "",
    "extensionName": "",
    "files": [
        
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": "",
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "com.mfzz99.spriteexploder",
    "productID": "F79C97955FD1AF4686EB4EF4EF90F0DB",
    "sourcedir": "",
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": "",
    "tvosdelegatename": "",
    "tvosmaccompilerflags": "",
    "tvosmaclinkerflags": "",
    "tvosplistinject": "",
    "version": "1.0.4"
}