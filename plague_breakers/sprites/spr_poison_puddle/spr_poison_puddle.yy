{
    "id": "7e33d25b-48a8-4127-8cc6-b0829cb4b5ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_poison_puddle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 6,
    "bbox_right": 59,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "150ff05d-d17e-4f40-8b7e-4bca5e17328c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e33d25b-48a8-4127-8cc6-b0829cb4b5ac",
            "compositeImage": {
                "id": "a2b36b65-4c98-4b85-b049-045bdca84de1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "150ff05d-d17e-4f40-8b7e-4bca5e17328c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48f1a8a9-81ba-4994-abe1-b60324e793c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "150ff05d-d17e-4f40-8b7e-4bca5e17328c",
                    "LayerId": "34e80778-f7b3-43e6-9515-102ada9a9fe1"
                }
            ]
        },
        {
            "id": "44215ada-ed6e-4106-a111-c35e69921999",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e33d25b-48a8-4127-8cc6-b0829cb4b5ac",
            "compositeImage": {
                "id": "c160b784-438f-4b8e-b609-165a2c5a1c5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44215ada-ed6e-4106-a111-c35e69921999",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c150e4ff-c6bf-4502-89ae-5bf290ae5be1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44215ada-ed6e-4106-a111-c35e69921999",
                    "LayerId": "34e80778-f7b3-43e6-9515-102ada9a9fe1"
                }
            ]
        },
        {
            "id": "ce75aa2f-f9b5-45d0-9703-cd42235cb3a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e33d25b-48a8-4127-8cc6-b0829cb4b5ac",
            "compositeImage": {
                "id": "87ea4d1e-a44e-4012-8a41-0229b162cef0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce75aa2f-f9b5-45d0-9703-cd42235cb3a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b48aeb3-ecd2-4145-935e-870db3ffbfc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce75aa2f-f9b5-45d0-9703-cd42235cb3a6",
                    "LayerId": "34e80778-f7b3-43e6-9515-102ada9a9fe1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "34e80778-f7b3-43e6-9515-102ada9a9fe1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e33d25b-48a8-4127-8cc6-b0829cb4b5ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}