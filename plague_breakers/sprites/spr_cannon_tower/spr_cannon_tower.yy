{
    "id": "22b2a54a-e74f-4bcc-b8e1-19fa267438ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cannon_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 21,
    "bbox_right": 44,
    "bbox_top": 19,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "025ef821-e407-4192-9075-42223ed54d55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22b2a54a-e74f-4bcc-b8e1-19fa267438ec",
            "compositeImage": {
                "id": "04a348fb-96b0-4c3a-bf9c-bf076ccb2fb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "025ef821-e407-4192-9075-42223ed54d55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8323f033-78ad-472c-8cc1-5a966241ab58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "025ef821-e407-4192-9075-42223ed54d55",
                    "LayerId": "7f940aa6-2671-4e4a-8ea2-559112153f2e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7f940aa6-2671-4e4a-8ea2-559112153f2e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22b2a54a-e74f-4bcc-b8e1-19fa267438ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}