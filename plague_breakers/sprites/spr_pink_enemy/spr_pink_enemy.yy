{
    "id": "fbc26842-b94e-445f-85cb-da759c3b0bef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pink_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4552afe8-f90e-4cd5-b291-59f56eb02a3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbc26842-b94e-445f-85cb-da759c3b0bef",
            "compositeImage": {
                "id": "90f6b018-fc39-4ee9-85f0-a4b33002dcfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4552afe8-f90e-4cd5-b291-59f56eb02a3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd79756d-0426-439d-989e-18b601151129",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4552afe8-f90e-4cd5-b291-59f56eb02a3e",
                    "LayerId": "50beae88-15b0-46f8-a3bf-72bafaf6f4d0"
                }
            ]
        },
        {
            "id": "c2737559-1424-4363-b5c4-24afddce3f20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbc26842-b94e-445f-85cb-da759c3b0bef",
            "compositeImage": {
                "id": "399818f7-5e44-4ff0-9fd1-727134629c69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2737559-1424-4363-b5c4-24afddce3f20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49f82909-5752-4106-8f54-1abc3695f979",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2737559-1424-4363-b5c4-24afddce3f20",
                    "LayerId": "50beae88-15b0-46f8-a3bf-72bafaf6f4d0"
                }
            ]
        },
        {
            "id": "df09490c-80b5-4640-8a10-75d99137b06f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbc26842-b94e-445f-85cb-da759c3b0bef",
            "compositeImage": {
                "id": "42df61f0-7d88-46e8-858f-d59b09225217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df09490c-80b5-4640-8a10-75d99137b06f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8df15d7-7c6d-4c28-949b-451823a701db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df09490c-80b5-4640-8a10-75d99137b06f",
                    "LayerId": "50beae88-15b0-46f8-a3bf-72bafaf6f4d0"
                }
            ]
        },
        {
            "id": "372ec8bb-6cfc-49ed-85b7-9ee2a5e96620",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbc26842-b94e-445f-85cb-da759c3b0bef",
            "compositeImage": {
                "id": "6707a70f-6a2d-49b3-b4f2-7ac1a6dac7a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "372ec8bb-6cfc-49ed-85b7-9ee2a5e96620",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bcb8311-c6f1-42da-8428-ef701ede0d0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "372ec8bb-6cfc-49ed-85b7-9ee2a5e96620",
                    "LayerId": "50beae88-15b0-46f8-a3bf-72bafaf6f4d0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "50beae88-15b0-46f8-a3bf-72bafaf6f4d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbc26842-b94e-445f-85cb-da759c3b0bef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}