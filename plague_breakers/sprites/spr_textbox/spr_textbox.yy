{
    "id": "60027976-6d41-437e-9ab6-de7be070a220",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_textbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2e74f11-4b89-4ce4-a68e-e331de562082",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60027976-6d41-437e-9ab6-de7be070a220",
            "compositeImage": {
                "id": "f9fdc087-d6ab-403f-aebe-a80da2174af7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2e74f11-4b89-4ce4-a68e-e331de562082",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d4ee0e2-cbaa-451d-938d-71bb8d2f9962",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2e74f11-4b89-4ce4-a68e-e331de562082",
                    "LayerId": "c179d1c7-ee68-43b3-9aef-5ebc27f2defc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c179d1c7-ee68-43b3-9aef-5ebc27f2defc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60027976-6d41-437e-9ab6-de7be070a220",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}