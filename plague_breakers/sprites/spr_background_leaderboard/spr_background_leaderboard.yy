{
    "id": "746b3f6c-d760-489d-8688-b545c377396e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background_leaderboard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1415,
    "bbox_left": 12,
    "bbox_right": 2547,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53eeff86-a3d6-4c71-bd60-ac18037cd17d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "746b3f6c-d760-489d-8688-b545c377396e",
            "compositeImage": {
                "id": "ef349a09-a006-44e6-8f28-4ae9aa8682a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53eeff86-a3d6-4c71-bd60-ac18037cd17d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8e524f7-32d7-415e-a767-8a7b470fe889",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53eeff86-a3d6-4c71-bd60-ac18037cd17d",
                    "LayerId": "816a4e2f-a134-4d8a-bd1a-362362de2662"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1440,
    "layers": [
        {
            "id": "816a4e2f-a134-4d8a-bd1a-362362de2662",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "746b3f6c-d760-489d-8688-b545c377396e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2560,
    "xorig": 0,
    "yorig": 0
}