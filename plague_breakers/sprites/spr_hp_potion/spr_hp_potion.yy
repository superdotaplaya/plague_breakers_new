{
    "id": "7c783422-a340-4215-9a7e-fb978e7a3e3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hp_potion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 25,
    "bbox_right": 39,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9abb2b53-2e1a-4c86-9b92-9df698976e6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c783422-a340-4215-9a7e-fb978e7a3e3b",
            "compositeImage": {
                "id": "9a104b75-d84c-44da-8602-d0a645e7c578",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9abb2b53-2e1a-4c86-9b92-9df698976e6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f66537dd-b7a2-44f1-bc4d-9f8976a48006",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9abb2b53-2e1a-4c86-9b92-9df698976e6b",
                    "LayerId": "8eb712a8-cd8c-4265-9454-64e9da66cd62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8eb712a8-cd8c-4265-9454-64e9da66cd62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c783422-a340-4215-9a7e-fb978e7a3e3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}