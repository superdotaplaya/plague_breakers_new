{
    "id": "d8d1cf77-18a1-4e11-be66-1a9006f6bc4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_info",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 286,
    "bbox_left": 3,
    "bbox_right": 578,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e397a4ed-b746-4170-b8c6-40259e629be3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8d1cf77-18a1-4e11-be66-1a9006f6bc4d",
            "compositeImage": {
                "id": "b8a0ec9c-f5ca-46bb-bc39-f6c207130553",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e397a4ed-b746-4170-b8c6-40259e629be3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d5f60a6-352c-467b-95c9-2c489d3fcff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e397a4ed-b746-4170-b8c6-40259e629be3",
                    "LayerId": "59681215-4a41-459b-8608-8610d922ccd2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "59681215-4a41-459b-8608-8610d922ccd2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8d1cf77-18a1-4e11-be66-1a9006f6bc4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 0,
    "yorig": 0
}