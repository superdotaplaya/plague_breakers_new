{
    "id": "db6a0503-0957-430e-bc35-3fa42a1d7f8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yellow_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 12,
    "bbox_right": 22,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2496296f-ed31-46d0-8334-8cee2dba7efc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6a0503-0957-430e-bc35-3fa42a1d7f8c",
            "compositeImage": {
                "id": "1d7cecfb-18f0-4b43-b43e-a7fd3ff419f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2496296f-ed31-46d0-8334-8cee2dba7efc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7175bef-4bff-4d56-ba64-48dece610dc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2496296f-ed31-46d0-8334-8cee2dba7efc",
                    "LayerId": "f4013ba8-5595-4f32-9a6a-4046c161487c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f4013ba8-5595-4f32-9a6a-4046c161487c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db6a0503-0957-430e-bc35-3fa42a1d7f8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}