{
    "id": "16a63efe-3aca-4109-9ef6-95e57da6e530",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_credits_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 347,
    "bbox_left": 45,
    "bbox_right": 603,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0159a4cc-60c0-4c29-a2bc-3e386514840c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16a63efe-3aca-4109-9ef6-95e57da6e530",
            "compositeImage": {
                "id": "2349b16f-3b9b-49df-b464-e03010ae7313",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0159a4cc-60c0-4c29-a2bc-3e386514840c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d46bf761-827a-46a3-b0ef-5ec558c81cba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0159a4cc-60c0-4c29-a2bc-3e386514840c",
                    "LayerId": "dab7085b-f4f1-4c48-a223-cb6136c5728d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "dab7085b-f4f1-4c48-a223-cb6136c5728d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16a63efe-3aca-4109-9ef6-95e57da6e530",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 200
}