{
    "id": "f3f49e62-29bf-4389-aae4-b22d528fccb0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_orange_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 341,
    "bbox_left": 52,
    "bbox_right": 341,
    "bbox_top": 58,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d65308a-6c60-4ee4-ad83-f0497f94c7f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f49e62-29bf-4389-aae4-b22d528fccb0",
            "compositeImage": {
                "id": "bc1ab83f-8c1e-4710-a40f-0969175a8e1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d65308a-6c60-4ee4-ad83-f0497f94c7f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80277c05-c462-4813-b6e1-f162459529c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d65308a-6c60-4ee4-ad83-f0497f94c7f8",
                    "LayerId": "cfbbe0b1-0cc7-4f07-a3aa-96096107b563"
                }
            ]
        },
        {
            "id": "b174275b-95c1-428a-b97d-f409256e8d94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f49e62-29bf-4389-aae4-b22d528fccb0",
            "compositeImage": {
                "id": "cec8f925-771a-4134-bb73-7eec4cc6adb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b174275b-95c1-428a-b97d-f409256e8d94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8764024b-064c-4c07-b0a2-4320806773e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b174275b-95c1-428a-b97d-f409256e8d94",
                    "LayerId": "cfbbe0b1-0cc7-4f07-a3aa-96096107b563"
                }
            ]
        },
        {
            "id": "d4c5250e-4098-43e8-ab2e-f072dcbb2bad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f49e62-29bf-4389-aae4-b22d528fccb0",
            "compositeImage": {
                "id": "2b7c74a2-8a77-4e13-b4f3-5c666e52403f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4c5250e-4098-43e8-ab2e-f072dcbb2bad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0121daa-a313-438f-a5b8-311ca5a9fad3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4c5250e-4098-43e8-ab2e-f072dcbb2bad",
                    "LayerId": "cfbbe0b1-0cc7-4f07-a3aa-96096107b563"
                }
            ]
        },
        {
            "id": "a67ed810-c549-4ed0-81fe-71108c234eae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f49e62-29bf-4389-aae4-b22d528fccb0",
            "compositeImage": {
                "id": "acab569e-f152-49cd-8841-b145764645d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a67ed810-c549-4ed0-81fe-71108c234eae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df7cd333-3bc0-498b-99c6-17cbcfefcc32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a67ed810-c549-4ed0-81fe-71108c234eae",
                    "LayerId": "cfbbe0b1-0cc7-4f07-a3aa-96096107b563"
                }
            ]
        },
        {
            "id": "b7ba49c2-23c0-420a-acc4-a88873427c23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f49e62-29bf-4389-aae4-b22d528fccb0",
            "compositeImage": {
                "id": "24aefb0b-2d86-465e-9337-54e58a402a50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7ba49c2-23c0-420a-acc4-a88873427c23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c46045a-07d3-4682-b1bb-6b3638a634b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7ba49c2-23c0-420a-acc4-a88873427c23",
                    "LayerId": "cfbbe0b1-0cc7-4f07-a3aa-96096107b563"
                }
            ]
        },
        {
            "id": "fdeb5a8d-cc74-4e05-8362-ec7f5439febe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f49e62-29bf-4389-aae4-b22d528fccb0",
            "compositeImage": {
                "id": "d7a54f29-1f59-4fd1-bd2d-eb1d9032087f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdeb5a8d-cc74-4e05-8362-ec7f5439febe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8b9d8d0-102f-4230-b20e-7de152c6ef8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdeb5a8d-cc74-4e05-8362-ec7f5439febe",
                    "LayerId": "cfbbe0b1-0cc7-4f07-a3aa-96096107b563"
                }
            ]
        },
        {
            "id": "1fb60864-eabd-4a31-a9a0-783d727555a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f49e62-29bf-4389-aae4-b22d528fccb0",
            "compositeImage": {
                "id": "f60091a0-eb94-4d72-b010-0ba562edc498",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fb60864-eabd-4a31-a9a0-783d727555a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86300255-f3c7-4b5f-9d72-012a34ce44fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fb60864-eabd-4a31-a9a0-783d727555a9",
                    "LayerId": "cfbbe0b1-0cc7-4f07-a3aa-96096107b563"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "cfbbe0b1-0cc7-4f07-a3aa-96096107b563",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3f49e62-29bf-4389-aae4-b22d528fccb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 200,
    "yorig": 200
}