{
    "id": "11b8face-6f2b-4171-be46-d041141fcb35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_continue_hovered",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 347,
    "bbox_left": 45,
    "bbox_right": 603,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eddc66b9-c37b-40c4-ab20-5966206a28b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11b8face-6f2b-4171-be46-d041141fcb35",
            "compositeImage": {
                "id": "2b35d15f-d1e1-44b4-b299-c19278366baf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eddc66b9-c37b-40c4-ab20-5966206a28b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e08f7a57-75df-4aea-a0bd-59cc46fd9f73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eddc66b9-c37b-40c4-ab20-5966206a28b1",
                    "LayerId": "dab4b69c-f69e-4ef4-8bcc-22b86b1e22a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "dab4b69c-f69e-4ef4-8bcc-22b86b1e22a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11b8face-6f2b-4171-be46-d041141fcb35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 200
}