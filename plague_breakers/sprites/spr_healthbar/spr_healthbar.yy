{
    "id": "03c96bdc-9644-4018-a189-f4a69e0b3987",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_healthbar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 85,
    "bbox_left": -105,
    "bbox_right": -102,
    "bbox_top": 74,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d6a0ad9-ad3f-4ee7-9ed3-cb6ab1943ef1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c96bdc-9644-4018-a189-f4a69e0b3987",
            "compositeImage": {
                "id": "6b183200-5542-411d-9368-7c8455765581",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d6a0ad9-ad3f-4ee7-9ed3-cb6ab1943ef1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "103b5f2b-a509-45de-a0bb-c5ba676d8bd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d6a0ad9-ad3f-4ee7-9ed3-cb6ab1943ef1",
                    "LayerId": "736bf823-bb9a-4d88-80e6-855479d6aefa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "736bf823-bb9a-4d88-80e6-855479d6aefa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03c96bdc-9644-4018-a189-f4a69e0b3987",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}