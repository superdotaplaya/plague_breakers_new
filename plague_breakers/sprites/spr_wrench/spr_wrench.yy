{
    "id": "d4feecde-dd78-4d82-b0e6-b3075e17c982",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wrench",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 2,
    "bbox_right": 28,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c7afb96-c589-41d8-8f68-d6fa34013805",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4feecde-dd78-4d82-b0e6-b3075e17c982",
            "compositeImage": {
                "id": "7e156eef-9b4c-4ac6-b46f-a41c912db719",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c7afb96-c589-41d8-8f68-d6fa34013805",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebf88098-a290-422d-8dcc-f433f6ac3c1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c7afb96-c589-41d8-8f68-d6fa34013805",
                    "LayerId": "35fcd23a-6768-4d3d-aaae-c27e7d41de26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "35fcd23a-6768-4d3d-aaae-c27e7d41de26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4feecde-dd78-4d82-b0e6-b3075e17c982",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}