{
    "id": "f8636494-beb1-4902-9739-ccc4fd8e31f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_red_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 70,
    "bbox_left": 36,
    "bbox_right": 72,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72c58f9c-c620-407c-8796-4ac49d007cca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8636494-beb1-4902-9739-ccc4fd8e31f0",
            "compositeImage": {
                "id": "8c812580-a32d-481b-bc4a-64f5f8aec4d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72c58f9c-c620-407c-8796-4ac49d007cca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca64c416-d5ab-4e99-a0f9-6831a4696a82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72c58f9c-c620-407c-8796-4ac49d007cca",
                    "LayerId": "79cc112f-1e5c-436d-af48-526d2934ac46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "79cc112f-1e5c-436d-af48-526d2934ac46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8636494-beb1-4902-9739-ccc4fd8e31f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 55,
    "yorig": 49
}