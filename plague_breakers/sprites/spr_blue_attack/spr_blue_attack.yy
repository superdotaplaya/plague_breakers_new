{
    "id": "5db1d80e-97e3-46da-9b27-cf9052d48797",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blue_attack",
    "For3D": false,
    "HTile": true,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "059a0e06-0c7b-4e16-a8a9-c0e0d7ef9715",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5db1d80e-97e3-46da-9b27-cf9052d48797",
            "compositeImage": {
                "id": "f0b76455-086e-464a-b95a-71b8fdfdee98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "059a0e06-0c7b-4e16-a8a9-c0e0d7ef9715",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad15d61c-c347-4dea-8132-910108c4b803",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "059a0e06-0c7b-4e16-a8a9-c0e0d7ef9715",
                    "LayerId": "69d2e2e0-c659-4cd0-a3c0-d40730bc7d47"
                }
            ]
        },
        {
            "id": "1327c0a8-075d-4251-bb4c-809911384f5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5db1d80e-97e3-46da-9b27-cf9052d48797",
            "compositeImage": {
                "id": "1644a9ea-e00a-48ee-8bc2-95b4621882c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1327c0a8-075d-4251-bb4c-809911384f5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53187b1a-aa10-4ce8-9228-4b5cb192da71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1327c0a8-075d-4251-bb4c-809911384f5f",
                    "LayerId": "69d2e2e0-c659-4cd0-a3c0-d40730bc7d47"
                }
            ]
        },
        {
            "id": "cad157b6-a4d1-42cd-af66-b40ea4f8f88b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5db1d80e-97e3-46da-9b27-cf9052d48797",
            "compositeImage": {
                "id": "46545540-f60e-42f6-884e-25f6dce17ce4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cad157b6-a4d1-42cd-af66-b40ea4f8f88b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c1a4604-9a2c-4cfb-982a-141765ae306f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cad157b6-a4d1-42cd-af66-b40ea4f8f88b",
                    "LayerId": "69d2e2e0-c659-4cd0-a3c0-d40730bc7d47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 66,
    "layers": [
        {
            "id": "69d2e2e0-c659-4cd0-a3c0-d40730bc7d47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5db1d80e-97e3-46da-9b27-cf9052d48797",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 33
}