{
    "id": "45cfa927-ae76-40bc-80ed-369d8ccc3501",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tesla_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 18,
    "bbox_right": 45,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed477200-b03e-4382-a519-916aa1519592",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45cfa927-ae76-40bc-80ed-369d8ccc3501",
            "compositeImage": {
                "id": "600c37a8-4370-455f-a179-03b3c9bb7852",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed477200-b03e-4382-a519-916aa1519592",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc7cfe80-860c-4eaf-96fc-85f41aef8b84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed477200-b03e-4382-a519-916aa1519592",
                    "LayerId": "b77076b7-2329-49c2-836c-1f4bcdc3f0c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b77076b7-2329-49c2-836c-1f4bcdc3f0c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45cfa927-ae76-40bc-80ed-369d8ccc3501",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}