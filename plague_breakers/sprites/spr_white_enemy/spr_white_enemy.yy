{
    "id": "d76a1cb6-60f9-4d7b-a963-046dc809a89a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_white_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62b4ce65-c928-463a-b30e-d253b3fd83e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d76a1cb6-60f9-4d7b-a963-046dc809a89a",
            "compositeImage": {
                "id": "fb968b8c-5ae0-47d7-a618-5ce27259efcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62b4ce65-c928-463a-b30e-d253b3fd83e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4ac517c-d6c2-48a7-af4d-8f2bf02cf3f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62b4ce65-c928-463a-b30e-d253b3fd83e3",
                    "LayerId": "3ee6bb0f-b5eb-485a-b9f6-26f491cb45c4"
                }
            ]
        },
        {
            "id": "5c6fa061-b572-4ead-825b-fa140791fc68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d76a1cb6-60f9-4d7b-a963-046dc809a89a",
            "compositeImage": {
                "id": "c0ce31c1-2f16-45c9-b167-e8bede7d6b63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c6fa061-b572-4ead-825b-fa140791fc68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3286b2d4-6411-4687-b5ee-e081230ed41b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c6fa061-b572-4ead-825b-fa140791fc68",
                    "LayerId": "3ee6bb0f-b5eb-485a-b9f6-26f491cb45c4"
                }
            ]
        },
        {
            "id": "7a958fbe-d71c-4d70-ae54-e7096092f406",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d76a1cb6-60f9-4d7b-a963-046dc809a89a",
            "compositeImage": {
                "id": "2666ebf3-6fb6-4f30-8941-e273c50785a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a958fbe-d71c-4d70-ae54-e7096092f406",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3fb0755-e234-4333-b2ce-d8abe970c459",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a958fbe-d71c-4d70-ae54-e7096092f406",
                    "LayerId": "3ee6bb0f-b5eb-485a-b9f6-26f491cb45c4"
                }
            ]
        },
        {
            "id": "c4562c71-d6e7-4049-a10b-1c252cc19adf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d76a1cb6-60f9-4d7b-a963-046dc809a89a",
            "compositeImage": {
                "id": "233f50ad-2fab-4f4d-aae7-bc77d567133a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4562c71-d6e7-4049-a10b-1c252cc19adf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0d7a89a-c161-438c-a9fc-253686a2e1ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4562c71-d6e7-4049-a10b-1c252cc19adf",
                    "LayerId": "3ee6bb0f-b5eb-485a-b9f6-26f491cb45c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "3ee6bb0f-b5eb-485a-b9f6-26f491cb45c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d76a1cb6-60f9-4d7b-a963-046dc809a89a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}