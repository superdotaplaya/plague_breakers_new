{
    "id": "ff17cdf8-f43a-45c6-8068-c71366134d73",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_green_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 8,
    "bbox_right": 56,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68f96db9-63c7-4d54-907c-19986c69de97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff17cdf8-f43a-45c6-8068-c71366134d73",
            "compositeImage": {
                "id": "95e551ec-9b79-4199-b502-bf2b990c4f75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68f96db9-63c7-4d54-907c-19986c69de97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f863f95-2ad3-43ad-b4f9-1e47de58185d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68f96db9-63c7-4d54-907c-19986c69de97",
                    "LayerId": "d5829dbf-3e98-4842-b309-c557dd57abf6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d5829dbf-3e98-4842-b309-c557dd57abf6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff17cdf8-f43a-45c6-8068-c71366134d73",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}