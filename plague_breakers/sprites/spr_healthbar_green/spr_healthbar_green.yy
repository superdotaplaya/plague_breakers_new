{
    "id": "56e0e675-9422-45e2-bfce-a169100fb4ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_healthbar_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": -64,
    "bbox_right": -53,
    "bbox_top": 38,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd51fcac-5330-4277-a801-ff22c9400f65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56e0e675-9422-45e2-bfce-a169100fb4ea",
            "compositeImage": {
                "id": "12a02d60-0580-4680-87d9-810e99914782",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd51fcac-5330-4277-a801-ff22c9400f65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59c62c28-7c1b-472b-b4b2-c77fad1c6af4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd51fcac-5330-4277-a801-ff22c9400f65",
                    "LayerId": "7a7489e7-7782-489b-a5bd-b6d2e460d823"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "7a7489e7-7782-489b-a5bd-b6d2e460d823",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56e0e675-9422-45e2-bfce-a169100fb4ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}