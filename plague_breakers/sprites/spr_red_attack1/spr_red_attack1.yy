{
    "id": "ad00aabc-3afc-4848-8f18-53f3606a4cc4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_red_attack1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 12,
    "bbox_right": 22,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "745425e4-91d7-4952-8275-cf5963436cb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad00aabc-3afc-4848-8f18-53f3606a4cc4",
            "compositeImage": {
                "id": "aee7a096-1eeb-4608-a4d5-7509b6bda350",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "745425e4-91d7-4952-8275-cf5963436cb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b435cc4-fecd-40c6-bc1d-7b1e85926cca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "745425e4-91d7-4952-8275-cf5963436cb8",
                    "LayerId": "b63b8562-48e6-4492-ab0f-791a7743c951"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b63b8562-48e6-4492-ab0f-791a7743c951",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad00aabc-3afc-4848-8f18-53f3606a4cc4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 55,
    "yorig": 49
}