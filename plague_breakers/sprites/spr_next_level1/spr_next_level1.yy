{
    "id": "1795b121-2cd7-4e63-a7bf-3fc0cde4db8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_next_level1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 347,
    "bbox_left": 45,
    "bbox_right": 603,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "351f6aef-6911-418b-b8bb-55a7028f4c06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1795b121-2cd7-4e63-a7bf-3fc0cde4db8c",
            "compositeImage": {
                "id": "91cfe5b7-3201-4a98-a848-52496d5430b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "351f6aef-6911-418b-b8bb-55a7028f4c06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84f05ecb-f093-407e-b6fa-536adff91ed1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "351f6aef-6911-418b-b8bb-55a7028f4c06",
                    "LayerId": "b19af59a-6253-446b-abaf-adb2d8dc4424"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "b19af59a-6253-446b-abaf-adb2d8dc4424",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1795b121-2cd7-4e63-a7bf-3fc0cde4db8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 200
}