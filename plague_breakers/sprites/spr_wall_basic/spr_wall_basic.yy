{
    "id": "8790704c-191e-4222-95ed-89cd5354d33d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall_basic",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 12,
    "bbox_right": 50,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4d8b768-8073-48aa-97f7-a4ee998e7241",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8790704c-191e-4222-95ed-89cd5354d33d",
            "compositeImage": {
                "id": "cd572730-f268-4efe-956f-3cf864c51835",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4d8b768-8073-48aa-97f7-a4ee998e7241",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1b390d2-a91e-4c27-95dd-802d1234f184",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4d8b768-8073-48aa-97f7-a4ee998e7241",
                    "LayerId": "096e0ada-52c8-4e52-88c3-601f7b72ee33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "096e0ada-52c8-4e52-88c3-601f7b72ee33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8790704c-191e-4222-95ed-89cd5354d33d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}