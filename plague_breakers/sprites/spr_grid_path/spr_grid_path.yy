{
    "id": "59da29b9-0888-4671-90c0-5d7f78cf715a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grid_path",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b155001-c12c-4d72-9bd1-f4d39b6e2f04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59da29b9-0888-4671-90c0-5d7f78cf715a",
            "compositeImage": {
                "id": "d3feca67-a8d3-4dee-831c-f0e1ccdc3959",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b155001-c12c-4d72-9bd1-f4d39b6e2f04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daa60c4e-11d8-49f5-a810-0f12ec3d7dd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b155001-c12c-4d72-9bd1-f4d39b6e2f04",
                    "LayerId": "55243b21-6326-4832-ac6c-12ed1d751e15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "55243b21-6326-4832-ac6c-12ed1d751e15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59da29b9-0888-4671-90c0-5d7f78cf715a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}