{
    "id": "cd280ec0-cafa-4c27-a6f0-46d60d1e9c3a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 85,
    "bbox_left": -31,
    "bbox_right": 114,
    "bbox_top": -18,
    "bboxmode": 2,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7ecbc0d-180d-480f-b4e7-101496f5911e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd280ec0-cafa-4c27-a6f0-46d60d1e9c3a",
            "compositeImage": {
                "id": "4a34f0ce-f6dd-4463-a52a-3a985533c488",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7ecbc0d-180d-480f-b4e7-101496f5911e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "363c621c-e7e0-4239-9440-1bd744af7866",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7ecbc0d-180d-480f-b4e7-101496f5911e",
                    "LayerId": "171ca657-3cfb-44ac-9350-714136c044d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "171ca657-3cfb-44ac-9350-714136c044d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd280ec0-cafa-4c27-a6f0-46d60d1e9c3a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}