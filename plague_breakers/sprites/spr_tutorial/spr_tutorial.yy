{
    "id": "44d0938e-a44c-42ae-a9ec-78fd21aecb46",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tutorial",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 6,
    "bbox_right": 639,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "32003bba-de11-49bc-9cb8-7518df137c32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44d0938e-a44c-42ae-a9ec-78fd21aecb46",
            "compositeImage": {
                "id": "8e9d117c-a5b3-40b4-b1e0-300cb3897fad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32003bba-de11-49bc-9cb8-7518df137c32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1641425f-149b-454b-a90e-074d81324082",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32003bba-de11-49bc-9cb8-7518df137c32",
                    "LayerId": "ab0a6c56-112c-4e73-8ede-58cfc1ae2576"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 118,
    "layers": [
        {
            "id": "ab0a6c56-112c-4e73-8ede-58cfc1ae2576",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44d0938e-a44c-42ae-a9ec-78fd21aecb46",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 647,
    "xorig": 0,
    "yorig": 0
}