{
    "id": "33a3fd3d-dd23-4893-871f-9e7ffacc4088",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_options",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 278,
    "bbox_left": 0,
    "bbox_right": 558,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88dbb74a-89a4-4962-a2cb-6c4024b76652",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33a3fd3d-dd23-4893-871f-9e7ffacc4088",
            "compositeImage": {
                "id": "a65c4ff9-88a8-4c40-8629-44eb64a0dbad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88dbb74a-89a4-4962-a2cb-6c4024b76652",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1abdb4f-3a2b-4c26-b212-ed2e2571f32b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88dbb74a-89a4-4962-a2cb-6c4024b76652",
                    "LayerId": "f1e0c50f-d93a-4b86-a817-31c9e9596252"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 279,
    "layers": [
        {
            "id": "f1e0c50f-d93a-4b86-a817-31c9e9596252",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33a3fd3d-dd23-4893-871f-9e7ffacc4088",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 559,
    "xorig": 279,
    "yorig": 139
}