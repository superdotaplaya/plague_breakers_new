{
    "id": "efb1303d-2cc2-424f-a98e-a6685adf6805",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_start_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 347,
    "bbox_left": 45,
    "bbox_right": 603,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b0ec750-516d-4ddc-9680-b48fe8428506",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efb1303d-2cc2-424f-a98e-a6685adf6805",
            "compositeImage": {
                "id": "f905e854-41de-4b9e-bbda-cba7c7c59ac1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b0ec750-516d-4ddc-9680-b48fe8428506",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9d9730e-b385-4c2b-bab4-f7731ff44aa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b0ec750-516d-4ddc-9680-b48fe8428506",
                    "LayerId": "0d02ee1b-1b6a-435f-824b-2a5a72e65e38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "0d02ee1b-1b6a-435f-824b-2a5a72e65e38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efb1303d-2cc2-424f-a98e-a6685adf6805",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 200
}