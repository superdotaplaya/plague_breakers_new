{
    "id": "d061de22-4133-425d-bffd-7cdf744a1f0b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "studio_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 120,
    "bbox_right": 1079,
    "bbox_top": 120,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9667035a-f75d-4c73-8dce-74231f246ace",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d061de22-4133-425d-bffd-7cdf744a1f0b",
            "compositeImage": {
                "id": "dbf05d57-9604-483f-9aa7-76fd51b2eedc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9667035a-f75d-4c73-8dce-74231f246ace",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ad6ec13-b589-471b-a4bc-883fa55fccc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9667035a-f75d-4c73-8dce-74231f246ace",
                    "LayerId": "8c55be4e-5f89-470b-88ab-05ce38bc4554"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1200,
    "layers": [
        {
            "id": "8c55be4e-5f89-470b-88ab-05ce38bc4554",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d061de22-4133-425d-bffd-7cdf744a1f0b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1200,
    "xorig": 0,
    "yorig": 0
}