{
    "id": "1a832f2b-2273-4cc3-b553-8c06b7dc617f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tooltip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 392,
    "bbox_left": 22,
    "bbox_right": 381,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f2ed564-1d08-4b29-b688-3918d7afb57b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a832f2b-2273-4cc3-b553-8c06b7dc617f",
            "compositeImage": {
                "id": "c9178cb4-61d1-4ce0-b855-b89bf5f454ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f2ed564-1d08-4b29-b688-3918d7afb57b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89b35dad-ca4b-44a7-bc6a-dad73a84b5b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f2ed564-1d08-4b29-b688-3918d7afb57b",
                    "LayerId": "1008c971-5f8f-4a39-b963-991e2c5f29e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "1008c971-5f8f-4a39-b963-991e2c5f29e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a832f2b-2273-4cc3-b553-8c06b7dc617f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}