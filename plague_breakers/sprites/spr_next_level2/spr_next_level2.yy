{
    "id": "eb005f81-346c-4c47-b622-d181bdad986a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_next_level2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 347,
    "bbox_left": 45,
    "bbox_right": 603,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9355fcb-bc1d-4c3d-817d-0a2359d59a7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb005f81-346c-4c47-b622-d181bdad986a",
            "compositeImage": {
                "id": "2dc7eeaf-7a2f-4fcc-a904-2e97c38e6a7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9355fcb-bc1d-4c3d-817d-0a2359d59a7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc19e4c5-8261-4446-a19e-2f768ba711c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9355fcb-bc1d-4c3d-817d-0a2359d59a7f",
                    "LayerId": "84c17774-bcad-41b8-9ffe-78c67c2aaf2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "84c17774-bcad-41b8-9ffe-78c67c2aaf2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb005f81-346c-4c47-b622-d181bdad986a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 200
}