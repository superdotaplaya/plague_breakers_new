{
    "id": "6826cbeb-67f8-4f65-b8bd-2950128ae748",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_info_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 601,
    "bbox_left": 0,
    "bbox_right": 601,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93b791b4-0922-41db-83f8-ca97db7a24ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6826cbeb-67f8-4f65-b8bd-2950128ae748",
            "compositeImage": {
                "id": "ed800202-a25c-4f1a-aa5f-a9c5f03362a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93b791b4-0922-41db-83f8-ca97db7a24ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf22231f-1fbf-4bfd-a148-1c74c9c5d536",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93b791b4-0922-41db-83f8-ca97db7a24ec",
                    "LayerId": "bcf99942-5e51-405a-a2e2-98621abbe570"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 602,
    "layers": [
        {
            "id": "bcf99942-5e51-405a-a2e2-98621abbe570",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6826cbeb-67f8-4f65-b8bd-2950128ae748",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 602,
    "xorig": 0,
    "yorig": 0
}