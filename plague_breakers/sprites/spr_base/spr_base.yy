{
    "id": "f9e6372f-8462-4770-81ae-ada1befb6e45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_base",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad6938e0-d405-4b3e-8b13-e40b171a9668",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9e6372f-8462-4770-81ae-ada1befb6e45",
            "compositeImage": {
                "id": "3d0c1917-c07f-491b-9f44-705880e3e265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad6938e0-d405-4b3e-8b13-e40b171a9668",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ad429a0-e62c-48c2-9170-79c6577fecbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad6938e0-d405-4b3e-8b13-e40b171a9668",
                    "LayerId": "05591288-2ae9-41f6-8bd6-dd19e1bcdc6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "05591288-2ae9-41f6-8bd6-dd19e1bcdc6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9e6372f-8462-4770-81ae-ada1befb6e45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}