{
    "id": "b1ac8a00-419f-4e13-b41d-288459b9a5ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gold",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 225,
    "bbox_left": -24,
    "bbox_right": 225,
    "bbox_top": -22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59fd5c1e-d0da-413c-b761-bfdfce721564",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1ac8a00-419f-4e13-b41d-288459b9a5ce",
            "compositeImage": {
                "id": "58d1637f-d242-4bbc-9abe-845096ec6ebe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59fd5c1e-d0da-413c-b761-bfdfce721564",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d4c5f19-d4bf-4900-82f0-a767cb3cecc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59fd5c1e-d0da-413c-b761-bfdfce721564",
                    "LayerId": "fe8fdf40-4462-46db-88ee-e70b2f94c46d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "fe8fdf40-4462-46db-88ee-e70b2f94c46d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1ac8a00-419f-4e13-b41d-288459b9a5ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}