{
    "id": "7e2d22d2-2ee3-47d6-b267-f07eb76b5fe7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bug",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 147,
    "bbox_left": -80,
    "bbox_right": 143,
    "bbox_top": -45,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 4,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba0d39c2-7279-4491-bd54-f159115e5fb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e2d22d2-2ee3-47d6-b267-f07eb76b5fe7",
            "compositeImage": {
                "id": "fa2b4020-c8d2-466b-9693-bfb450d09a85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba0d39c2-7279-4491-bd54-f159115e5fb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e58fb5d9-7fd7-4b87-800e-fe634a599ea5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba0d39c2-7279-4491-bd54-f159115e5fb8",
                    "LayerId": "fd61a340-8039-40ae-a418-f1ba696039ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "fd61a340-8039-40ae-a418-f1ba696039ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e2d22d2-2ee3-47d6-b267-f07eb76b5fe7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 35
}