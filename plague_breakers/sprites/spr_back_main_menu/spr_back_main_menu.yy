{
    "id": "a70d20f5-51b5-47ae-b5aa-c0d83a06280c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_back_main_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 0,
    "bbox_right": 152,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28a60191-52f9-498c-bb7d-33a3185a6a16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a70d20f5-51b5-47ae-b5aa-c0d83a06280c",
            "compositeImage": {
                "id": "c0d3cdb2-221d-47e7-973c-a9a9ab08d016",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28a60191-52f9-498c-bb7d-33a3185a6a16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95dda5b1-446e-4b92-a1d7-bfee6e738a5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28a60191-52f9-498c-bb7d-33a3185a6a16",
                    "LayerId": "55d9343f-7adc-4b69-9d9e-8e7ab9a1489e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 76,
    "layers": [
        {
            "id": "55d9343f-7adc-4b69-9d9e-8e7ab9a1489e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a70d20f5-51b5-47ae-b5aa-c0d83a06280c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 153,
    "xorig": 0,
    "yorig": 0
}