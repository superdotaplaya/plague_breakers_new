{
    "id": "e6f5584b-c13d-48a9-9d69-ec6c75a74a54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_continue_basic",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 347,
    "bbox_left": 45,
    "bbox_right": 603,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e00d4716-d2cf-4a75-bb89-9391e69bc652",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6f5584b-c13d-48a9-9d69-ec6c75a74a54",
            "compositeImage": {
                "id": "c624f1f3-851e-4ab3-8829-7a1624cb95f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e00d4716-d2cf-4a75-bb89-9391e69bc652",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9564239-434f-4f71-a352-a321bec2b66b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e00d4716-d2cf-4a75-bb89-9391e69bc652",
                    "LayerId": "159909b9-7547-4518-8e1b-186e48a382ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "159909b9-7547-4518-8e1b-186e48a382ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6f5584b-c13d-48a9-9d69-ec6c75a74a54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 200
}