{
    "id": "51d95f11-61b4-46fa-8ebf-1779ada7b4e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite66",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 192,
    "bbox_left": 0,
    "bbox_right": 192,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab53de62-24dc-4330-aa95-3058f2d1a70c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51d95f11-61b4-46fa-8ebf-1779ada7b4e4",
            "compositeImage": {
                "id": "6cb7c8b4-b612-4066-a485-217f59a5a879",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab53de62-24dc-4330-aa95-3058f2d1a70c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9682795f-1c13-45ea-a71e-900cd416cf85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab53de62-24dc-4330-aa95-3058f2d1a70c",
                    "LayerId": "75589d2d-3392-4436-9dcc-f0fc146eee0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "75589d2d-3392-4436-9dcc-f0fc146eee0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51d95f11-61b4-46fa-8ebf-1779ada7b4e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}