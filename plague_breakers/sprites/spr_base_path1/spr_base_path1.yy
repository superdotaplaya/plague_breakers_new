{
    "id": "091a6854-2e67-4b58-ac8d-8e69fb2f3816",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_base_path1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1439,
    "bbox_left": 0,
    "bbox_right": 2559,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c27a3999-1cc7-4c47-8376-eccad57d64b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "091a6854-2e67-4b58-ac8d-8e69fb2f3816",
            "compositeImage": {
                "id": "66ff7d8d-3bd9-4559-bf8c-46ecfc16a33a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c27a3999-1cc7-4c47-8376-eccad57d64b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "989b1ede-8eb7-4b28-8321-9115dbb8e359",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c27a3999-1cc7-4c47-8376-eccad57d64b4",
                    "LayerId": "e4745a5d-402c-498a-852e-e3286facde5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1440,
    "layers": [
        {
            "id": "e4745a5d-402c-498a-852e-e3286facde5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "091a6854-2e67-4b58-ac8d-8e69fb2f3816",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2560,
    "xorig": 0,
    "yorig": 0
}