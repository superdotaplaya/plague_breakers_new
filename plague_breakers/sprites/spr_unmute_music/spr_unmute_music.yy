{
    "id": "e33b44f4-0c2d-40e1-a300-2d89b0ef4fa2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_unmute_music",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 7,
    "bbox_right": 190,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3f5f69d-ec5b-4a30-bc35-ca70a9bec0ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e33b44f4-0c2d-40e1-a300-2d89b0ef4fa2",
            "compositeImage": {
                "id": "653da5b4-0390-4d8a-9dfa-0f8f56fb7a60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3f5f69d-ec5b-4a30-bc35-ca70a9bec0ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8600a63e-264c-486a-b1b8-eb64b94a40db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3f5f69d-ec5b-4a30-bc35-ca70a9bec0ed",
                    "LayerId": "2310d2bf-c622-4a19-93f7-7996ab0f39ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "2310d2bf-c622-4a19-93f7-7996ab0f39ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e33b44f4-0c2d-40e1-a300-2d89b0ef4fa2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}