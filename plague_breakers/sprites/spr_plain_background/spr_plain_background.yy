{
    "id": "de61de29-884c-41e3-aac1-ca215bc74c25",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_plain_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1439,
    "bbox_left": 0,
    "bbox_right": 2559,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d626e06a-ea3b-43fa-b530-84556dec5515",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de61de29-884c-41e3-aac1-ca215bc74c25",
            "compositeImage": {
                "id": "640eb36e-a1a5-4752-8d18-86ce0fe0fda7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d626e06a-ea3b-43fa-b530-84556dec5515",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfaf54f7-981e-4820-8212-2ab88df94152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d626e06a-ea3b-43fa-b530-84556dec5515",
                    "LayerId": "f0067d54-1bb7-49e6-8f24-b0b33b0fb7f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1440,
    "layers": [
        {
            "id": "f0067d54-1bb7-49e6-8f24-b0b33b0fb7f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de61de29-884c-41e3-aac1-ca215bc74c25",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2560,
    "xorig": 0,
    "yorig": 0
}