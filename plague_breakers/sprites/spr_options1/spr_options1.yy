{
    "id": "a1d75ba7-b0b2-491f-b4de-157f8c64524b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_options1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 347,
    "bbox_left": 45,
    "bbox_right": 603,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f46cc369-08a8-41a2-9ac3-71148d1000b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1d75ba7-b0b2-491f-b4de-157f8c64524b",
            "compositeImage": {
                "id": "294042cd-0db9-438b-bfb6-bbc12417be3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f46cc369-08a8-41a2-9ac3-71148d1000b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bb1c49e-2d2e-46a8-a0e1-8287c5f9d892",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f46cc369-08a8-41a2-9ac3-71148d1000b3",
                    "LayerId": "97a6e26e-c42e-49ca-abd1-9d89be3d1d5b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "97a6e26e-c42e-49ca-abd1-9d89be3d1d5b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1d75ba7-b0b2-491f-b4de-157f8c64524b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 200
}