{
    "id": "b0ded13b-636c-4074-a05a-9df3ee3a67d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_help",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9192a67c-14c3-4282-9b26-61a99e283508",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0ded13b-636c-4074-a05a-9df3ee3a67d1",
            "compositeImage": {
                "id": "7a75a062-3287-4439-b71a-c5538e3a59fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9192a67c-14c3-4282-9b26-61a99e283508",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d50f4a7-bfd5-4271-89c7-742478938854",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9192a67c-14c3-4282-9b26-61a99e283508",
                    "LayerId": "4acab36a-a9ec-4fdb-be3f-72cb03c8c839"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4acab36a-a9ec-4fdb-be3f-72cb03c8c839",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0ded13b-636c-4074-a05a-9df3ee3a67d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}