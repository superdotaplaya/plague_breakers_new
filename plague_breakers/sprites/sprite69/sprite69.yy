{
    "id": "7678a4ee-0764-4ee9-9088-91163a6175af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite69",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a140d15d-4895-4b7d-a47a-12bab292db94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7678a4ee-0764-4ee9-9088-91163a6175af",
            "compositeImage": {
                "id": "4434840b-f14e-4cd6-9f4f-35d6e163d7fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a140d15d-4895-4b7d-a47a-12bab292db94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bae196ba-5790-41b6-9530-0d89dfd6824c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a140d15d-4895-4b7d-a47a-12bab292db94",
                    "LayerId": "3a13b6e6-6156-4586-9f1e-87c71ae9d906"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3a13b6e6-6156-4586-9f1e-87c71ae9d906",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7678a4ee-0764-4ee9-9088-91163a6175af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}