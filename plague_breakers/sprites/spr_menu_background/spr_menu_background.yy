{
    "id": "b2d93003-ac3b-4801-8997-e69644e064d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1439,
    "bbox_left": 0,
    "bbox_right": 2559,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12aec7b1-18c6-4c1d-8beb-fa2b7a0ab0a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2d93003-ac3b-4801-8997-e69644e064d9",
            "compositeImage": {
                "id": "f9bca89f-705d-4688-b6f3-ab768e148cc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12aec7b1-18c6-4c1d-8beb-fa2b7a0ab0a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84bc1b7e-a4de-433f-a91d-ae423a7ef5d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12aec7b1-18c6-4c1d-8beb-fa2b7a0ab0a9",
                    "LayerId": "2678a7e1-01ae-40a3-b9c3-93fd35b92487"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1440,
    "layers": [
        {
            "id": "2678a7e1-01ae-40a3-b9c3-93fd35b92487",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2d93003-ac3b-4801-8997-e69644e064d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2560,
    "xorig": 0,
    "yorig": 0
}