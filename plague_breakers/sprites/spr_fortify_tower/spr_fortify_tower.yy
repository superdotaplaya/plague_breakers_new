{
    "id": "decb777e-e907-450e-a61d-9f6972cc4a0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fortify_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf613820-9fa2-4611-bc72-2e39aff36ed2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "d6599e0a-ce5a-4238-84c4-d4eb5b7ac3c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf613820-9fa2-4611-bc72-2e39aff36ed2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89c4b498-f875-446f-9b47-24afd6a2ed74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf613820-9fa2-4611-bc72-2e39aff36ed2",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "1d1ee6d1-9a9f-4165-80a7-412389f9ac90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "245516e6-d95e-4de3-a69e-cbefa2172018",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d1ee6d1-9a9f-4165-80a7-412389f9ac90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "341f585a-f491-4528-9ce6-6f13c9e2bfc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d1ee6d1-9a9f-4165-80a7-412389f9ac90",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "3ce23b1f-1026-44e2-b743-0ea0cf93500c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "070a56a4-296c-43d9-bd5c-37fc7a62f575",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ce23b1f-1026-44e2-b743-0ea0cf93500c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46503a94-7fcd-4e0e-9e28-8d24ccdd7db3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ce23b1f-1026-44e2-b743-0ea0cf93500c",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "83eaf722-c92d-43ae-bfb8-853a50f89009",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "04ebd392-5684-4f0f-a96d-b77eea21e957",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83eaf722-c92d-43ae-bfb8-853a50f89009",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad42d8c8-bf81-49c5-b79e-d53ebd8ff232",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83eaf722-c92d-43ae-bfb8-853a50f89009",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "9c06fd78-3e59-4baf-8c96-81d3a037a04d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "e108a126-25fc-4772-a85e-27463b6d57b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c06fd78-3e59-4baf-8c96-81d3a037a04d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8399ad6a-cd6f-4b8a-bcb1-da00bf452791",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c06fd78-3e59-4baf-8c96-81d3a037a04d",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "b1f4c031-dce5-4160-8c91-5b457eda53bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "ce0c960f-54b3-4a0f-9696-6e94b477c1ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1f4c031-dce5-4160-8c91-5b457eda53bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dce9f095-c2a4-40c2-9a0d-210cd8a5f94c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1f4c031-dce5-4160-8c91-5b457eda53bc",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "5f207148-7cea-4987-8c05-0ec35c752df7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "95f05132-94b7-495c-9294-4d37638928b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f207148-7cea-4987-8c05-0ec35c752df7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99e66837-2c2b-44d5-826e-9522e6004cb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f207148-7cea-4987-8c05-0ec35c752df7",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "e3e03f04-2d5e-4cc4-90af-acd4b269c96e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "9ec2d534-8bcb-4107-8acd-9e3fa36a85f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3e03f04-2d5e-4cc4-90af-acd4b269c96e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c3c595e-cb1a-49cb-b94c-15591d41ce8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3e03f04-2d5e-4cc4-90af-acd4b269c96e",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "a9ef86fe-c152-48d3-8de9-fbd26a37471b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "f1ed68f1-88da-4233-bd0d-f3fe5c2721f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9ef86fe-c152-48d3-8de9-fbd26a37471b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd45da79-91b6-4e32-ae82-9461e79cb699",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9ef86fe-c152-48d3-8de9-fbd26a37471b",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "9aa19d0d-9d9e-46dd-aab5-a32e7778200c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "ef101036-dbe3-4ae9-9b13-f0dcada3472d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aa19d0d-9d9e-46dd-aab5-a32e7778200c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f439095e-57cd-493e-a42f-68d6fc74bd61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aa19d0d-9d9e-46dd-aab5-a32e7778200c",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "be8213f2-d5b2-4fc2-9677-8771b4b035d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "c5f8c286-0341-4f58-a0ff-5f2f24c07761",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be8213f2-d5b2-4fc2-9677-8771b4b035d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d63f39d3-707a-4a96-83dd-ccef8dc6aa12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be8213f2-d5b2-4fc2-9677-8771b4b035d0",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "08c6fa0d-6b2f-4e35-bcdf-016dfd8f090b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "7c74be99-2b5c-4e1b-9087-4289e94c72b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08c6fa0d-6b2f-4e35-bcdf-016dfd8f090b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c6c72ff-2077-499e-85d8-6211a8647d13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08c6fa0d-6b2f-4e35-bcdf-016dfd8f090b",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "236f99c1-22d9-422a-af5e-a454cd2fc182",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "fca147c6-f72a-4179-becb-0b28f283f067",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "236f99c1-22d9-422a-af5e-a454cd2fc182",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ee5c909-574a-4832-8fdf-4c6e8a600a48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "236f99c1-22d9-422a-af5e-a454cd2fc182",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "afeac91c-44f9-42a3-a8c8-537833a15cae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "d6ac8b64-d75a-4fd3-ae64-c86b028c5bdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afeac91c-44f9-42a3-a8c8-537833a15cae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85378701-cf29-4a24-8fa7-d3557f8ba6ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afeac91c-44f9-42a3-a8c8-537833a15cae",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "eb3c26a9-d12e-49d4-a87b-2eb97e307c33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "9528eb24-8bed-421c-8b97-f4ffae3ff182",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb3c26a9-d12e-49d4-a87b-2eb97e307c33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05f4c81b-bf3f-4e80-bbcb-98ce9934b9fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb3c26a9-d12e-49d4-a87b-2eb97e307c33",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "0392ba3f-9fb1-4645-8fca-8be46c515b61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "74ee2312-bc87-4238-beb4-08bbde525c05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0392ba3f-9fb1-4645-8fca-8be46c515b61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d419efe0-f8bf-4a8d-b6bc-42bcf5c04ba6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0392ba3f-9fb1-4645-8fca-8be46c515b61",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "5bc30043-ec62-4851-b36f-a1483042e231",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "60ad2b5e-3e46-4a52-9d55-62f17d5071c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bc30043-ec62-4851-b36f-a1483042e231",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ad344fb-9359-4384-9149-cc067ee70920",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bc30043-ec62-4851-b36f-a1483042e231",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "4af3ece4-26c3-4191-9a2c-75e4cf882c08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "bf540794-b8e6-4504-870c-2e7fb2206f87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4af3ece4-26c3-4191-9a2c-75e4cf882c08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4352a47f-3552-4474-b868-e3234a424a1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4af3ece4-26c3-4191-9a2c-75e4cf882c08",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "a2bb9068-c65e-46d1-a432-5801d7417065",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "fba281f3-65ee-4ba6-98b7-da05e46b6a07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2bb9068-c65e-46d1-a432-5801d7417065",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bf956d6-18fa-4ab9-b0d0-f951d641a90b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2bb9068-c65e-46d1-a432-5801d7417065",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "e6cb94fc-843d-4649-9614-d873ecd47da6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "42fe464d-ff60-4cf1-b99c-b55c978ecf09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6cb94fc-843d-4649-9614-d873ecd47da6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da0a7691-5247-43d2-91bf-e5d3bb7ff099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6cb94fc-843d-4649-9614-d873ecd47da6",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "4ee02528-8a19-4f97-8250-b2e3a977fb4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "b60a004f-5f53-489e-b6f3-cddeab8ce627",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ee02528-8a19-4f97-8250-b2e3a977fb4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10fccf80-a8f9-40a2-b4d4-8f3229428f78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ee02528-8a19-4f97-8250-b2e3a977fb4a",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "09a0d012-b20d-4514-87da-7624ad19bfbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "96ec9d5c-6e6c-48cc-9e7d-93e8fdecc86b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09a0d012-b20d-4514-87da-7624ad19bfbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd57491a-8334-4eb6-93de-a802bc4d7d0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09a0d012-b20d-4514-87da-7624ad19bfbf",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "b7759726-ae5e-4373-9a28-c5ff57326463",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "8bddd136-d813-4668-87db-05a2076eaded",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7759726-ae5e-4373-9a28-c5ff57326463",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47d9c6ac-3a4e-4b45-a8dd-2eba0ca58ad9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7759726-ae5e-4373-9a28-c5ff57326463",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "81622c2e-7335-476a-90c8-2c6336c84605",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "803ff9bc-a4a7-4ffe-9548-8e4219354455",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81622c2e-7335-476a-90c8-2c6336c84605",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cff8de0-49a9-4ea5-b9a5-83ec7df1aa0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81622c2e-7335-476a-90c8-2c6336c84605",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "b596c5ff-69b8-4d5f-92ee-d97b80ce26b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "2e50fe3d-96bf-47ef-b079-bd8b3180494a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b596c5ff-69b8-4d5f-92ee-d97b80ce26b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff7543ed-07fe-4334-a577-cdc7c488d34a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b596c5ff-69b8-4d5f-92ee-d97b80ce26b8",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "07a46f46-3362-4df8-95db-fa597d94a9a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "1d1874a1-2968-48f7-a2b5-b6b10bee4e11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07a46f46-3362-4df8-95db-fa597d94a9a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02a78d1d-32d4-4f86-9053-21a7ad5e7132",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07a46f46-3362-4df8-95db-fa597d94a9a2",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "1d7ae950-5267-47c4-a0d4-6b911d078319",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "936fd7e8-a054-4d6d-9be3-1d73a4771259",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d7ae950-5267-47c4-a0d4-6b911d078319",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d6f909a-eaa0-4b49-ab25-27d7aeb1785b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d7ae950-5267-47c4-a0d4-6b911d078319",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "d10b2b6a-947f-498a-8d1d-b630cc59b8bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "359efad6-c55f-4edb-a50e-c3564cfe1448",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d10b2b6a-947f-498a-8d1d-b630cc59b8bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02528fb0-39ae-42ae-80a3-92ecfc9af54a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d10b2b6a-947f-498a-8d1d-b630cc59b8bb",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "8754dd5f-a970-4f74-8ebf-cbbb7377d478",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "9d9f32ab-60a9-406b-9771-c420fc7ea979",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8754dd5f-a970-4f74-8ebf-cbbb7377d478",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcc0067b-fd69-472d-bdc3-87d741c51b3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8754dd5f-a970-4f74-8ebf-cbbb7377d478",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "58d6de2a-7ec3-43ef-ab4a-6374147a59d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "98f9ef0e-99f8-46ce-b7ef-997d5ba27bc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58d6de2a-7ec3-43ef-ab4a-6374147a59d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c7bb9dc-a97e-4408-a748-45446e1424ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58d6de2a-7ec3-43ef-ab4a-6374147a59d4",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "955aaaab-1230-4c8c-ab66-f72ca8269abd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "1e4d4814-c548-476a-b517-ee915e12746c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "955aaaab-1230-4c8c-ab66-f72ca8269abd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6c6ff30-e027-4a24-8f81-75ce5f02a4e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "955aaaab-1230-4c8c-ab66-f72ca8269abd",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "d130c066-4bcd-47c0-bb11-4dd8b9291265",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "d46a4182-67c0-4e74-a9ff-ad8c43d410ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d130c066-4bcd-47c0-bb11-4dd8b9291265",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d2b2e12-3aba-4ae2-a96b-6a9a136726c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d130c066-4bcd-47c0-bb11-4dd8b9291265",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "784374fa-7995-45e9-869f-b7c2ebd83d4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "e65f60b3-761a-484f-a4ba-1a8f5c9e473d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "784374fa-7995-45e9-869f-b7c2ebd83d4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40ccba12-e603-4d2a-84ca-1da4f09ff5eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "784374fa-7995-45e9-869f-b7c2ebd83d4c",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "25054847-e6a2-471a-b23d-c23574763615",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "eab0424b-d660-4e4e-9ea8-a3b0a88f8047",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25054847-e6a2-471a-b23d-c23574763615",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d120ae4-55d0-4a19-a942-680b64423902",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25054847-e6a2-471a-b23d-c23574763615",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "0ee5031b-de15-4ae5-a971-7b09e2d444a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "8dac5d5c-2bac-457f-bcfd-77a0829fbf2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ee5031b-de15-4ae5-a971-7b09e2d444a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c11c7d5c-7aa9-4bcc-b541-60a05b68df2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ee5031b-de15-4ae5-a971-7b09e2d444a0",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "befa91c2-ace1-4a48-9208-112d61a01a2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "baae318f-32dc-4d92-81a9-8d115da1c689",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "befa91c2-ace1-4a48-9208-112d61a01a2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f0d8257-b104-43f6-9637-8998f912fd74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "befa91c2-ace1-4a48-9208-112d61a01a2a",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "8ede5822-7290-420f-aedc-e6e042dc163d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "1e840c3e-fc6e-4340-b474-2d516a50760b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ede5822-7290-420f-aedc-e6e042dc163d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55addb7c-1ba0-4c11-a38d-8b3b8342d42e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ede5822-7290-420f-aedc-e6e042dc163d",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "8f40b0ed-a977-43b2-afd8-c75f6b0d77ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "655220ac-4328-4bb9-9c77-41a900a78465",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f40b0ed-a977-43b2-afd8-c75f6b0d77ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa362c5c-fe9a-4ecc-a9dc-36bbca7a4e00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f40b0ed-a977-43b2-afd8-c75f6b0d77ce",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "c18a064e-535b-45a1-8373-37b617245f06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "3789a5a0-e712-490a-97bf-cd90dfde6015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c18a064e-535b-45a1-8373-37b617245f06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b380a18-e379-4590-a501-17ae1e62d969",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c18a064e-535b-45a1-8373-37b617245f06",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "c1139a56-b02b-4f02-8fe4-c12d758febae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "f18260e5-7c66-437a-807d-81c2454d5dd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1139a56-b02b-4f02-8fe4-c12d758febae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7003b31-e3e7-4646-a200-0bf2c9359fca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1139a56-b02b-4f02-8fe4-c12d758febae",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "fce5c26e-d448-43d9-a8c9-376aa7a606a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "0ae6010c-cc2c-4951-b014-c3fa93c15bb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fce5c26e-d448-43d9-a8c9-376aa7a606a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c4d436a-2980-43df-84f4-2a77aa560409",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fce5c26e-d448-43d9-a8c9-376aa7a606a8",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "f6d75fe6-c698-4d8d-aa7b-23703341dca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "84c0226b-baad-4530-b82e-22b36c4d2a70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6d75fe6-c698-4d8d-aa7b-23703341dca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6dfa44a-ac11-4f56-ba2f-3085b390e4d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6d75fe6-c698-4d8d-aa7b-23703341dca9",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "7884439e-aacb-484f-b297-441b52601003",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "232e539d-c21c-49e3-980d-4386d225bb01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7884439e-aacb-484f-b297-441b52601003",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba0939e4-8bc6-4a74-839a-e17770f3b0a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7884439e-aacb-484f-b297-441b52601003",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "759d07e0-b5f1-4f78-ad85-0adae0617e6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "1be82d38-fa34-4b0e-9aa8-b69c21a610d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "759d07e0-b5f1-4f78-ad85-0adae0617e6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf2a8cdc-b788-411f-a1ec-b540087fc94f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "759d07e0-b5f1-4f78-ad85-0adae0617e6a",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        },
        {
            "id": "64d75374-2802-49d8-a5ac-67f06cedd3e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "compositeImage": {
                "id": "137bc7b1-dd9c-4d7a-96e5-bc2148eef15c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64d75374-2802-49d8-a5ac-67f06cedd3e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41e22e9b-18e0-48a2-b01d-fc6cd735b9d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64d75374-2802-49d8-a5ac-67f06cedd3e1",
                    "LayerId": "2a366a21-14c1-4f59-a496-c6093eb48b22"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "2a366a21-14c1-4f59-a496-c6093eb48b22",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "decb777e-e907-450e-a61d-9f6972cc4a0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}