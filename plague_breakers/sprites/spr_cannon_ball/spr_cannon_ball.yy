{
    "id": "75cdcd5a-4765-4935-96b3-08cfad664e70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cannon_ball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f897099d-8a90-4d8c-b7ba-a1cc65bdb880",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75cdcd5a-4765-4935-96b3-08cfad664e70",
            "compositeImage": {
                "id": "645ea9b5-a57e-467a-ab95-d39a8bbc5b28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f897099d-8a90-4d8c-b7ba-a1cc65bdb880",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "539cbb43-1ab2-4187-938a-535c9ccd9b69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f897099d-8a90-4d8c-b7ba-a1cc65bdb880",
                    "LayerId": "f4f85059-e1cb-48da-af17-bb7fde8d29ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f4f85059-e1cb-48da-af17-bb7fde8d29ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75cdcd5a-4765-4935-96b3-08cfad664e70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}