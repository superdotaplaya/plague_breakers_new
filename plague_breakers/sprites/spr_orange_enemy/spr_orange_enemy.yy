{
    "id": "54a4c327-f5eb-4d20-ae00-f9f3043399d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_orange_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f0ef97f-3a8e-44e0-8a66-192d94500f1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a4c327-f5eb-4d20-ae00-f9f3043399d1",
            "compositeImage": {
                "id": "2431efcf-8238-403e-ab07-ffefe4623f9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f0ef97f-3a8e-44e0-8a66-192d94500f1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a69bc612-8cc2-4a7a-9f34-7891f639e9b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f0ef97f-3a8e-44e0-8a66-192d94500f1a",
                    "LayerId": "c6efd7ba-4212-4d77-a334-cc1b030ac23d"
                }
            ]
        },
        {
            "id": "4972ca85-49fa-4f36-a23d-4171ceec50de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a4c327-f5eb-4d20-ae00-f9f3043399d1",
            "compositeImage": {
                "id": "eeb3b3a4-32a3-44e8-8a7a-2c468a5c898e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4972ca85-49fa-4f36-a23d-4171ceec50de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dba7a70-c5a1-46ed-bfdd-72f7fed2ae30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4972ca85-49fa-4f36-a23d-4171ceec50de",
                    "LayerId": "c6efd7ba-4212-4d77-a334-cc1b030ac23d"
                }
            ]
        },
        {
            "id": "5f9c1678-07f2-42bc-ad7a-5ef6a0d99863",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a4c327-f5eb-4d20-ae00-f9f3043399d1",
            "compositeImage": {
                "id": "fd309319-6bf5-41bd-907f-836bcde13be1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f9c1678-07f2-42bc-ad7a-5ef6a0d99863",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad2d87eb-c907-431a-8b1c-fd084c00a532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f9c1678-07f2-42bc-ad7a-5ef6a0d99863",
                    "LayerId": "c6efd7ba-4212-4d77-a334-cc1b030ac23d"
                }
            ]
        },
        {
            "id": "58f6b5d3-965f-4847-bb2d-f23dd3796c86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a4c327-f5eb-4d20-ae00-f9f3043399d1",
            "compositeImage": {
                "id": "2efb7d70-cd40-4bfc-90cb-79059b58d00b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58f6b5d3-965f-4847-bb2d-f23dd3796c86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55462be3-2642-4cf1-90f4-5d1ff77f5c3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58f6b5d3-965f-4847-bb2d-f23dd3796c86",
                    "LayerId": "c6efd7ba-4212-4d77-a334-cc1b030ac23d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "c6efd7ba-4212-4d77-a334-cc1b030ac23d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54a4c327-f5eb-4d20-ae00-f9f3043399d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}