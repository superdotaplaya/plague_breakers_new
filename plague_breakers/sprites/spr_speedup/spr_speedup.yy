{
    "id": "98749d97-f760-4ccc-b56a-8e09e874f23b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_speedup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 347,
    "bbox_left": 45,
    "bbox_right": 603,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51a752c0-60bd-45d6-a95f-52b1aefb0230",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98749d97-f760-4ccc-b56a-8e09e874f23b",
            "compositeImage": {
                "id": "92eedaa1-bb6c-4242-b917-347cacf33eb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51a752c0-60bd-45d6-a95f-52b1aefb0230",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "718d2e58-157c-495c-9c28-1d0a4bc1c4ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51a752c0-60bd-45d6-a95f-52b1aefb0230",
                    "LayerId": "10f5521f-8072-4ca7-a479-3eab73c71fc2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "10f5521f-8072-4ca7-a479-3eab73c71fc2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98749d97-f760-4ccc-b56a-8e09e874f23b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 200
}