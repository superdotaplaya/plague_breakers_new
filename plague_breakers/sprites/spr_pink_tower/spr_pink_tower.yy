{
    "id": "92dc95d5-3f2b-416f-bc4b-84f7be27f74b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pink_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 6,
    "bbox_right": 57,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a7f5849-d527-46cf-9b72-fe26f7909b08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92dc95d5-3f2b-416f-bc4b-84f7be27f74b",
            "compositeImage": {
                "id": "2159f61b-f87c-4850-8663-11dd0dff235c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a7f5849-d527-46cf-9b72-fe26f7909b08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9588ddd9-eba4-40d9-b153-797b69767a68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a7f5849-d527-46cf-9b72-fe26f7909b08",
                    "LayerId": "79d59766-4df9-40f0-ac02-b64744f14f0c"
                }
            ]
        },
        {
            "id": "d2a10a72-375a-4a11-8384-59dbf5b7f030",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92dc95d5-3f2b-416f-bc4b-84f7be27f74b",
            "compositeImage": {
                "id": "66ffda26-5505-4028-97ec-92e96f99c821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2a10a72-375a-4a11-8384-59dbf5b7f030",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bb49685-88d7-440d-ac38-96f6118b61a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2a10a72-375a-4a11-8384-59dbf5b7f030",
                    "LayerId": "79d59766-4df9-40f0-ac02-b64744f14f0c"
                }
            ]
        },
        {
            "id": "886483e1-8967-4aff-826f-b120b13370b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92dc95d5-3f2b-416f-bc4b-84f7be27f74b",
            "compositeImage": {
                "id": "1c6284c6-d68a-4ddb-bc87-c0341ae715f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "886483e1-8967-4aff-826f-b120b13370b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d002d59-1c66-4d67-9589-7bf0823b6cc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "886483e1-8967-4aff-826f-b120b13370b0",
                    "LayerId": "79d59766-4df9-40f0-ac02-b64744f14f0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "79d59766-4df9-40f0-ac02-b64744f14f0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92dc95d5-3f2b-416f-bc4b-84f7be27f74b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}