{
    "id": "c9445489-393e-46b3-a8f8-f2e643423d54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shield",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 6,
    "bbox_right": 199,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d64bc92b-753c-4be8-8b3a-dacbe7e47bf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9445489-393e-46b3-a8f8-f2e643423d54",
            "compositeImage": {
                "id": "19f27303-b878-4ed4-9136-1ba44550a356",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d64bc92b-753c-4be8-8b3a-dacbe7e47bf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ab09da8-443b-47e6-a2c1-efb9962b80c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d64bc92b-753c-4be8-8b3a-dacbe7e47bf5",
                    "LayerId": "7d9d7d1d-4fb9-49b0-bb2c-6a78e2a9eecd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "7d9d7d1d-4fb9-49b0-bb2c-6a78e2a9eecd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9445489-393e-46b3-a8f8-f2e643423d54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 205,
    "xorig": 82,
    "yorig": 96
}