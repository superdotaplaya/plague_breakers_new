{
    "id": "13c90b07-9a30-4186-a5c4-eb994ac94cc9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_text_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 980,
    "bbox_left": 588,
    "bbox_right": 1425,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d421aecf-d890-4f93-b262-154e4edc2102",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13c90b07-9a30-4186-a5c4-eb994ac94cc9",
            "compositeImage": {
                "id": "7b3782de-6479-47ba-8feb-bcf15e359926",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d421aecf-d890-4f93-b262-154e4edc2102",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92a47fd1-4813-4eaa-8c1a-6de96be0e862",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d421aecf-d890-4f93-b262-154e4edc2102",
                    "LayerId": "6143106f-192b-4afe-9aff-affc31654ed1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1000,
    "layers": [
        {
            "id": "6143106f-192b-4afe-9aff-affc31654ed1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13c90b07-9a30-4186-a5c4-eb994ac94cc9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2000,
    "xorig": 1000,
    "yorig": 500
}