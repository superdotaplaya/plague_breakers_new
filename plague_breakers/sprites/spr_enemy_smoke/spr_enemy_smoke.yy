{
    "id": "e22497ba-920f-43fa-9f25-7ed63435e55d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_smoke",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 10,
    "bbox_right": 93,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0dc2a43f-21d1-4855-b836-8ddd4a3d0034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "7ee7ba95-cef5-471a-a0da-3070afe78385",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dc2a43f-21d1-4855-b836-8ddd4a3d0034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eb1e6e7-3509-4ad2-aa92-a88ed6bca835",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dc2a43f-21d1-4855-b836-8ddd4a3d0034",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "ec3eb8a6-57b4-4552-9bf5-a7c9be44627b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "b81860e9-fb83-4d4a-b4ec-a10a1f366c16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec3eb8a6-57b4-4552-9bf5-a7c9be44627b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d296dba8-c745-4dea-a9bb-0ac3cbc8ebb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec3eb8a6-57b4-4552-9bf5-a7c9be44627b",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "4891028c-ed20-42c9-8337-1e0f7d8c58e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "3779cab1-620f-4da8-9342-e0bacaeb8039",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4891028c-ed20-42c9-8337-1e0f7d8c58e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4874140c-8768-4352-bc0a-7591812f8388",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4891028c-ed20-42c9-8337-1e0f7d8c58e7",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "2d8d4d57-abec-4f88-831e-97c86866c281",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "96207898-b7c5-45fb-ad84-65cfc16034c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d8d4d57-abec-4f88-831e-97c86866c281",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "635261c4-38b6-4605-8bc0-12a5e1cf26bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d8d4d57-abec-4f88-831e-97c86866c281",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "6e9861e2-99e3-4c38-8f72-ea2758859713",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "c162b740-4ccc-4a37-84f4-2eed29259ac8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e9861e2-99e3-4c38-8f72-ea2758859713",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "487c0b06-26a6-45b8-9bdb-95e56e2016a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e9861e2-99e3-4c38-8f72-ea2758859713",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "5c27b7de-bd71-4afd-9f02-9cb5d0436202",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "ef65ed18-a0c3-4a08-8fa1-9d29a313e81d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c27b7de-bd71-4afd-9f02-9cb5d0436202",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fe5dedf-5344-4a5b-946f-8256ac6ed202",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c27b7de-bd71-4afd-9f02-9cb5d0436202",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "c3fa3b48-9e59-4dab-b2fe-bdd4d54d26bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "41e83a04-d799-41c2-a408-c8fdbb13c0ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3fa3b48-9e59-4dab-b2fe-bdd4d54d26bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "929c7095-5a88-4fdb-ba3c-35f534bffa1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3fa3b48-9e59-4dab-b2fe-bdd4d54d26bd",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "59d2b4d2-8794-4a8b-ba3c-f0b53b1f5fea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "cbba0b23-ee70-43e4-befb-7cfe9638ab0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59d2b4d2-8794-4a8b-ba3c-f0b53b1f5fea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6348469c-55b0-444f-8b17-723625c45570",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59d2b4d2-8794-4a8b-ba3c-f0b53b1f5fea",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "56ac57ef-31d9-48c7-9127-40d1375da857",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "af3ed64d-6bdc-4c98-8fa3-22a7af06c74c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56ac57ef-31d9-48c7-9127-40d1375da857",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f60544d-f300-4894-a80d-9287f588aaa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56ac57ef-31d9-48c7-9127-40d1375da857",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "cdf7f582-a77a-4d3b-9edd-2d2ed9e76ebd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "3404ebf5-5750-4919-bc98-ac778c1f6e55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdf7f582-a77a-4d3b-9edd-2d2ed9e76ebd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3396f5c8-fa74-48c9-a00d-ab3decad55aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdf7f582-a77a-4d3b-9edd-2d2ed9e76ebd",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "3b80c0f8-6cc1-4c94-88ec-425350ae008a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "9dc88a9b-8537-4897-ab73-bc31357198cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b80c0f8-6cc1-4c94-88ec-425350ae008a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ad4d31d-e7ec-4804-8930-bdf6c30dfcc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b80c0f8-6cc1-4c94-88ec-425350ae008a",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "4d0ab594-3380-44ec-9d6d-cb14d29dfcf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "e63dca36-0e4f-43c4-bb3b-0a47735f72f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d0ab594-3380-44ec-9d6d-cb14d29dfcf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "712ce4a3-f5fe-49c3-834f-a726a2ec5849",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d0ab594-3380-44ec-9d6d-cb14d29dfcf4",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "36121633-c9c1-4a58-b233-a45a9805ca75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "5fc362c0-67ea-4997-b3ed-343173e3438e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36121633-c9c1-4a58-b233-a45a9805ca75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eefa062f-350f-4587-ae16-006606c81af7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36121633-c9c1-4a58-b233-a45a9805ca75",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "6d6cb3b5-51e5-4f02-a783-d5eec1d9b715",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "c4eaa4c9-aff9-426c-8f5b-9d3b47f1d737",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d6cb3b5-51e5-4f02-a783-d5eec1d9b715",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5eaf35f-ad02-4772-8549-3c4e8cc22eab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d6cb3b5-51e5-4f02-a783-d5eec1d9b715",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "4b8d9ca8-9aac-4a5e-a733-65f643eeb7f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "61623fa8-42d9-4cb7-9538-12e4da64a90a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b8d9ca8-9aac-4a5e-a733-65f643eeb7f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb65567f-ec3f-48c3-94d3-aab5518dea81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b8d9ca8-9aac-4a5e-a733-65f643eeb7f4",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "861cf40a-7627-4427-b86c-d42ef6bf7a5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "23a8ad0a-5d1a-4937-8459-9343002dac0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "861cf40a-7627-4427-b86c-d42ef6bf7a5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "133eabe0-60bc-45e4-8e87-e045951007e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "861cf40a-7627-4427-b86c-d42ef6bf7a5a",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "cc07a316-53cb-4d23-bbfe-4be3d0fe614a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "0b8b6f56-c5e7-4a9c-8bda-3011aabd4899",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc07a316-53cb-4d23-bbfe-4be3d0fe614a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3b202ad-6b41-4b5b-8014-f687e334d9e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc07a316-53cb-4d23-bbfe-4be3d0fe614a",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "22a714e2-3336-4f63-b25f-ec4c3dab9747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "965be63e-61da-44b3-bb5f-2b7c5f537a8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22a714e2-3336-4f63-b25f-ec4c3dab9747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef2d22d5-25f4-4ffb-8de1-69ffaa6d19f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22a714e2-3336-4f63-b25f-ec4c3dab9747",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "2c65d6b4-295f-4e42-87dd-c0db31db0b21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "b11957fd-fbdd-48fe-af41-c1aff81612fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c65d6b4-295f-4e42-87dd-c0db31db0b21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60f27685-9a7f-4e07-a1f0-e10ae26f068b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c65d6b4-295f-4e42-87dd-c0db31db0b21",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "48c11b27-3e26-4465-bcde-bec7d1b81a33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "67a02b0b-b5bb-4bae-ab72-3f1c2ee5611b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48c11b27-3e26-4465-bcde-bec7d1b81a33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f1a54e0-2ead-4ced-994e-6aa59679f525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48c11b27-3e26-4465-bcde-bec7d1b81a33",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "68379c12-ca45-4e05-8c1a-580a3073fddf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "a8c71feb-9db2-4117-b7f1-a9ae7be1f206",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68379c12-ca45-4e05-8c1a-580a3073fddf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c37eb02-f798-4b03-bf1b-6b1171dad4b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68379c12-ca45-4e05-8c1a-580a3073fddf",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "4a13c9f7-9f68-413f-a56f-b30a4022e81c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "949913c7-2604-47ad-a453-f06f9621241b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a13c9f7-9f68-413f-a56f-b30a4022e81c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5658f5c1-7e1b-4f22-ac8d-f6db1f5d01d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a13c9f7-9f68-413f-a56f-b30a4022e81c",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "bbc80424-c56a-4cd8-b9e5-cf949b57905e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "297852c9-4ff6-4242-8c6b-13d18ac36338",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbc80424-c56a-4cd8-b9e5-cf949b57905e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2b005ca-f5e6-4477-a397-4f9bc8b6beb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbc80424-c56a-4cd8-b9e5-cf949b57905e",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "6f921435-52cd-40ba-b586-01a5afe23f48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "475293e7-1f05-4422-998a-8413cdc45bdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f921435-52cd-40ba-b586-01a5afe23f48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b186486d-313c-475e-97db-d53fcda513f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f921435-52cd-40ba-b586-01a5afe23f48",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "50c2aadc-225e-463c-ba15-59a60f359581",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "3a889651-c7d1-434d-8f69-a15e08a66333",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50c2aadc-225e-463c-ba15-59a60f359581",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "359d1291-c9ac-4464-afd2-0da9d1224817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50c2aadc-225e-463c-ba15-59a60f359581",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "9fc2f2ba-56b2-41a9-a48d-d89775cfd0bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "2a6ade95-8d80-4f42-9416-8e43d529d653",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fc2f2ba-56b2-41a9-a48d-d89775cfd0bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83467332-c6c7-4798-bdc0-0931e00c525d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fc2f2ba-56b2-41a9-a48d-d89775cfd0bf",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "32b5679e-2c1b-4158-8280-e2e8ec146c4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "76aad3ed-d4db-4dc3-9c6d-7ca209d941ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32b5679e-2c1b-4158-8280-e2e8ec146c4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c1e097c-d59f-4e6f-b09f-3bd851fd1894",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32b5679e-2c1b-4158-8280-e2e8ec146c4c",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "b0168bd7-5fde-4841-8d7a-3bea56c07c03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "32cab61d-32b8-4c6f-8c1b-f5e3afd9d86d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0168bd7-5fde-4841-8d7a-3bea56c07c03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2ca3769-4a99-4669-bbb9-09ba8904f265",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0168bd7-5fde-4841-8d7a-3bea56c07c03",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "d41dab56-7a91-49ec-bda0-909198eccb70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "86c37c1f-ae12-424a-be54-931bb233e765",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d41dab56-7a91-49ec-bda0-909198eccb70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14bb7168-927b-46c9-baa5-aef271a500ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d41dab56-7a91-49ec-bda0-909198eccb70",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "9aa19cc7-2a1c-4ce3-9eee-d3a2b736efd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "d2611ee4-3add-4f6e-8374-5ea2eabee36d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aa19cc7-2a1c-4ce3-9eee-d3a2b736efd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e16938a-add2-42e2-9b10-16bffdff585e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aa19cc7-2a1c-4ce3-9eee-d3a2b736efd3",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "456b7bda-9528-43eb-a274-901afd97d1fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "8283f59f-6261-441a-b29d-699018918b6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "456b7bda-9528-43eb-a274-901afd97d1fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "484d2fad-0608-49a4-b9b3-8a094fd03d17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "456b7bda-9528-43eb-a274-901afd97d1fe",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "91269207-0923-47d0-980a-c2e59055337c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "90410f8f-45f3-4506-8899-84e432d98bae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91269207-0923-47d0-980a-c2e59055337c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc754ee4-6bb4-4550-b5e9-569dcea49b14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91269207-0923-47d0-980a-c2e59055337c",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "ca31c160-d3b8-4f14-b71c-403e75d13748",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "b452f8ad-aed4-4142-aa9a-d7650df59ae7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca31c160-d3b8-4f14-b71c-403e75d13748",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e44946ee-5a80-496f-94b3-fb96eeb7bfb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca31c160-d3b8-4f14-b71c-403e75d13748",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "7780fb00-675e-4cf6-a228-e0dd3cbbc414",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "ede768ca-ca99-448a-8187-0e91e05baf79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7780fb00-675e-4cf6-a228-e0dd3cbbc414",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c8bbf15-3e36-4c3f-b09a-4ff2435bc717",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7780fb00-675e-4cf6-a228-e0dd3cbbc414",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "a7b92516-e515-44a3-adf3-4c3c31dd7aca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "b43570c7-54d2-4f5b-89b8-40ec56076c17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7b92516-e515-44a3-adf3-4c3c31dd7aca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c0fe2cf-b691-43ab-a98e-546527fb4d65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7b92516-e515-44a3-adf3-4c3c31dd7aca",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "203a26d4-8c7a-4b3a-9d6c-e9d26c00fff2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "013e04f3-7df3-4861-ab91-8dfa1fa4eb33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "203a26d4-8c7a-4b3a-9d6c-e9d26c00fff2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d4506ca-45df-43c0-b5ef-72688ebf777d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "203a26d4-8c7a-4b3a-9d6c-e9d26c00fff2",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "f476ecbb-6ab0-458b-b4a2-1adb4bd78f74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "153389c1-d0e9-4132-a867-84534e375fc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f476ecbb-6ab0-458b-b4a2-1adb4bd78f74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aa67d48-b3bb-49a1-bc12-b7e198fff42b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f476ecbb-6ab0-458b-b4a2-1adb4bd78f74",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "8a8ad9d7-0e9d-49df-a54d-adbac2b3b8de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "3406bcce-29cd-4679-924a-8924dca44289",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a8ad9d7-0e9d-49df-a54d-adbac2b3b8de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed253430-cc08-4139-aab1-3fffc30e4028",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a8ad9d7-0e9d-49df-a54d-adbac2b3b8de",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "25ea2794-4542-46f3-996c-92bfd03c5a13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "098c7cb5-0193-4c8c-b038-7be1b6b30f0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25ea2794-4542-46f3-996c-92bfd03c5a13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0053e0b-b087-4d7e-9769-102abd036687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25ea2794-4542-46f3-996c-92bfd03c5a13",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "74822c48-f9f3-4aba-89e4-b9ab31601808",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "bc589c70-8d2e-42b2-9f11-1c25ecff3b83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74822c48-f9f3-4aba-89e4-b9ab31601808",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7e8441d-b62e-4105-8097-24947dd60488",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74822c48-f9f3-4aba-89e4-b9ab31601808",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "35e79ea5-d0f9-444e-9c12-c31fc0bd0742",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "7f906370-2169-4147-b8a0-34eaafa36416",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35e79ea5-d0f9-444e-9c12-c31fc0bd0742",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7108bc4b-37cd-467e-9255-5d4e2b1681d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35e79ea5-d0f9-444e-9c12-c31fc0bd0742",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "81fd786f-499e-44ab-bb31-83326c017ec2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "e86e1c6f-9954-4348-bc2b-7e7ab04b3c11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81fd786f-499e-44ab-bb31-83326c017ec2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbb8262d-198a-42b0-a45e-59b2d796ffbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81fd786f-499e-44ab-bb31-83326c017ec2",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "0f053ae0-9c33-457f-800b-76c04e060ad0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "644515a2-fe4f-4f02-8968-2da782794c9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f053ae0-9c33-457f-800b-76c04e060ad0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c043aa7e-4f3d-46e5-b194-69d3bc14c896",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f053ae0-9c33-457f-800b-76c04e060ad0",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "2aa44b4b-087d-4e5b-8dfd-9be323216ba8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "f3dc50b0-9542-4ea0-94c9-99909524ac28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aa44b4b-087d-4e5b-8dfd-9be323216ba8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9ee4433-926c-42cc-ae45-cea387d99698",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aa44b4b-087d-4e5b-8dfd-9be323216ba8",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "d62517aa-72ad-41ed-bd10-dc4f587d51f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "30712074-785b-4929-bb10-4e0d26994138",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d62517aa-72ad-41ed-bd10-dc4f587d51f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b2a0155-6548-4506-a10b-a4196ada0f27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d62517aa-72ad-41ed-bd10-dc4f587d51f8",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "468d0e57-d177-45a5-9389-a0d775bda1e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "d9eb0758-5417-4063-ab56-ea0949df95fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "468d0e57-d177-45a5-9389-a0d775bda1e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02175138-d1bd-49f1-b77b-fa3914f26e6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "468d0e57-d177-45a5-9389-a0d775bda1e5",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "7a355612-3653-47d0-82a1-ca8812235cd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "e62a4668-214e-4205-987a-b6b4b95c60aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a355612-3653-47d0-82a1-ca8812235cd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d621116e-26a8-4401-9665-0e6eccafc3f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a355612-3653-47d0-82a1-ca8812235cd4",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "7db49070-e235-4e9a-90f6-d58eaaaadbaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "5e11c98b-2fe6-47b4-b71d-f4ecad572656",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7db49070-e235-4e9a-90f6-d58eaaaadbaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0a92bd3-0f4e-4fac-8f19-0725a7a3e3bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7db49070-e235-4e9a-90f6-d58eaaaadbaf",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "a1c8cc75-f5e6-4cdc-a027-70d6201b85a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "74fcce02-b4dd-436b-829f-7b58db4440a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1c8cc75-f5e6-4cdc-a027-70d6201b85a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "097816cf-8c94-4b87-adf4-29393c989840",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1c8cc75-f5e6-4cdc-a027-70d6201b85a0",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "f048f492-a14c-462f-90d8-208147a2bac7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "a6fb181c-4783-429b-af07-7b65e05b476c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f048f492-a14c-462f-90d8-208147a2bac7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0efe9b1a-41bd-43a4-9c2b-6ad1702fb9a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f048f492-a14c-462f-90d8-208147a2bac7",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "3b839691-dd8b-4c59-b690-a7e3da1e0383",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "6df9e5c2-af04-4fbc-b128-d9f892568e6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b839691-dd8b-4c59-b690-a7e3da1e0383",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fb7cf5c-a13d-4880-86a7-c1537c840447",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b839691-dd8b-4c59-b690-a7e3da1e0383",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "d4b5f629-f47d-42dc-91e2-81b3e80aaefd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "fabf53e6-c23a-42dc-87b6-42d073a95016",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4b5f629-f47d-42dc-91e2-81b3e80aaefd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d1c8c19-9e4a-4b04-b3b3-1b5112e0f314",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4b5f629-f47d-42dc-91e2-81b3e80aaefd",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "d60ebc4c-e5b6-43fe-8733-fb80bbfa2bfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "9f52196f-b5d3-4287-9e65-d75bfd088a7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d60ebc4c-e5b6-43fe-8733-fb80bbfa2bfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb0fcf74-b996-4de6-8622-35b7d2c46284",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d60ebc4c-e5b6-43fe-8733-fb80bbfa2bfe",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "a1168113-116d-4550-9235-32513e05514b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "ba4333b5-ecef-4d32-8e03-54b4bd63b2fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1168113-116d-4550-9235-32513e05514b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6306f8ce-b7c5-4064-8c25-062acaa2259e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1168113-116d-4550-9235-32513e05514b",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "f9cf83d1-22d0-4d0c-a0dd-eb6ae57f08c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "bcc3ac77-00af-49a6-b641-eed0a5b298aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9cf83d1-22d0-4d0c-a0dd-eb6ae57f08c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbba74fd-efdd-46d5-b9c3-8942fa0992c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9cf83d1-22d0-4d0c-a0dd-eb6ae57f08c3",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "d4e7e180-9911-4b22-859e-b65746fbaf03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "00997753-2f03-4e69-9f10-c8edce128d7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4e7e180-9911-4b22-859e-b65746fbaf03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6545122d-240d-408c-a59d-6c3fdf708ac4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4e7e180-9911-4b22-859e-b65746fbaf03",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "a92d6e0e-c378-4bb6-8272-1939cb20e9c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "26fc1017-4510-4e7e-81ee-1e1c0c1396ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a92d6e0e-c378-4bb6-8272-1939cb20e9c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a8b9eeb-80da-4461-a791-489dbb91df81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a92d6e0e-c378-4bb6-8272-1939cb20e9c3",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "af591a07-dff1-4356-b3ff-094a97e1a5e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "dc926874-ab41-42b5-ac53-1c51e256f051",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af591a07-dff1-4356-b3ff-094a97e1a5e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0b14964-5c5a-476a-8978-7ffdf958e4ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af591a07-dff1-4356-b3ff-094a97e1a5e0",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "344d851b-9745-4725-990c-f07abb671d47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "6f5154f0-026d-4e69-81a2-1e06b9479f7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "344d851b-9745-4725-990c-f07abb671d47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b82d8674-098e-4ba2-a929-4e2d20152c02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "344d851b-9745-4725-990c-f07abb671d47",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "6563ccab-4c88-423e-ab93-27a380a1b2e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "1618f3e7-38d0-4d93-ba5b-fb05eac5f01f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6563ccab-4c88-423e-ab93-27a380a1b2e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d96073d5-9d20-48af-a02b-0ddb7e4d88e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6563ccab-4c88-423e-ab93-27a380a1b2e0",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "d6e4f712-1b8e-4697-a788-43dc4cec2df3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "b4b63dce-b847-4a53-a3e6-4aec5b533ac5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6e4f712-1b8e-4697-a788-43dc4cec2df3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97cbbc09-5cfe-4002-97df-a69d771aedb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6e4f712-1b8e-4697-a788-43dc4cec2df3",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "7bd291da-16fd-4ed6-9549-a8f00bf27d23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "4271c305-15ec-40f1-8759-6b9b3b1f93f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bd291da-16fd-4ed6-9549-a8f00bf27d23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95e75422-199f-4349-9f9a-a907a75bde3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bd291da-16fd-4ed6-9549-a8f00bf27d23",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "b994ded7-ec50-4c8b-9893-7ff3c080b7a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "1fd32fa0-8d51-4686-9905-732ff5f846b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b994ded7-ec50-4c8b-9893-7ff3c080b7a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee4720a1-7070-4085-87b7-51a0a349153d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b994ded7-ec50-4c8b-9893-7ff3c080b7a2",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "47bdb3bb-4c6b-40b2-b285-6fba944a48e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "7b8b3ed1-345e-4bab-9c9d-f542e7e646a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47bdb3bb-4c6b-40b2-b285-6fba944a48e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cbf0059-80a5-4688-b0a3-11ac8f9a0563",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47bdb3bb-4c6b-40b2-b285-6fba944a48e3",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        },
        {
            "id": "2fdbd1ba-70b8-471c-b8ed-a5df2057692d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "compositeImage": {
                "id": "48f8d35d-c5cf-4207-ba7d-2e777f4ef2e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fdbd1ba-70b8-471c-b8ed-a5df2057692d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1a6e795-1dcc-484c-9ef6-dcdbf54e8270",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fdbd1ba-70b8-471c-b8ed-a5df2057692d",
                    "LayerId": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "85ed65b7-f4ff-4b42-b1f5-ce86ea6279b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}