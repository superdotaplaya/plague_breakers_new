{
    "id": "2bb70071-0834-45b3-9d76-856304ab5d8f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_slider_handle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 14,
    "bbox_right": 49,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62833c99-5462-4ccc-a1d5-300744a892e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bb70071-0834-45b3-9d76-856304ab5d8f",
            "compositeImage": {
                "id": "da84cd2d-bcac-47d5-88d6-052d52310fef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62833c99-5462-4ccc-a1d5-300744a892e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "244a9f8f-5174-4e0c-8fcb-738a1441f55d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62833c99-5462-4ccc-a1d5-300744a892e4",
                    "LayerId": "894c3c31-7273-4331-8fba-b61fc3ad6670"
                },
                {
                    "id": "b9582a73-cb6a-447a-abf6-527fb99bbbc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62833c99-5462-4ccc-a1d5-300744a892e4",
                    "LayerId": "be80c9b5-7d86-4c04-96fd-932b679062c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "894c3c31-7273-4331-8fba-b61fc3ad6670",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2bb70071-0834-45b3-9d76-856304ab5d8f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "be80c9b5-7d86-4c04-96fd-932b679062c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2bb70071-0834-45b3-9d76-856304ab5d8f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}