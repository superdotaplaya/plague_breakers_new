{
    "id": "e564aebc-39ea-457e-9d53-6cba7b2083f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mud",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 61,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01ea9e16-6dc5-47fd-80fe-98565aa9b31f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e564aebc-39ea-457e-9d53-6cba7b2083f1",
            "compositeImage": {
                "id": "35cf9ee6-ef18-489e-acd7-0f87246dd71a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01ea9e16-6dc5-47fd-80fe-98565aa9b31f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63b03a2b-4e84-4f86-9e0f-ed3572681630",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01ea9e16-6dc5-47fd-80fe-98565aa9b31f",
                    "LayerId": "5637b377-e54b-4227-aeba-8006d3a05cbc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5637b377-e54b-4227-aeba-8006d3a05cbc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e564aebc-39ea-457e-9d53-6cba7b2083f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}