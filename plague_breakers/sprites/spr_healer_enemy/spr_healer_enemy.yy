{
    "id": "000fd899-3f4f-4531-a8e2-b5052ac5c416",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_healer_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05604c51-88d8-48e6-999a-785f8b28fd2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "000fd899-3f4f-4531-a8e2-b5052ac5c416",
            "compositeImage": {
                "id": "c6ffe3a5-75d3-4471-974c-016ce67077ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05604c51-88d8-48e6-999a-785f8b28fd2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3efbafc8-dc6e-403a-98e5-7b9cc89decbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05604c51-88d8-48e6-999a-785f8b28fd2b",
                    "LayerId": "62a557e8-9962-4795-aa80-5309deafca47"
                }
            ]
        },
        {
            "id": "ab05fa5d-577b-4e30-b638-3f4756df0a8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "000fd899-3f4f-4531-a8e2-b5052ac5c416",
            "compositeImage": {
                "id": "1718dd03-6a50-4570-a530-73675eef4a3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab05fa5d-577b-4e30-b638-3f4756df0a8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "472f8e40-7249-4607-b2bb-4b17fe7d9a6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab05fa5d-577b-4e30-b638-3f4756df0a8c",
                    "LayerId": "62a557e8-9962-4795-aa80-5309deafca47"
                }
            ]
        },
        {
            "id": "a8600fe1-6048-465e-969d-899f0c20e2a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "000fd899-3f4f-4531-a8e2-b5052ac5c416",
            "compositeImage": {
                "id": "e0402c91-68cb-4a0c-8372-44b08917a265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8600fe1-6048-465e-969d-899f0c20e2a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e953f12-eb50-4346-913c-8511e94c7e3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8600fe1-6048-465e-969d-899f0c20e2a5",
                    "LayerId": "62a557e8-9962-4795-aa80-5309deafca47"
                }
            ]
        },
        {
            "id": "e8f919cb-f4bc-41d7-a6a3-8703d1ce446b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "000fd899-3f4f-4531-a8e2-b5052ac5c416",
            "compositeImage": {
                "id": "30a4cc44-52d0-49c1-acd9-1320d1a7d30c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8f919cb-f4bc-41d7-a6a3-8703d1ce446b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b3ffa88-6d80-4783-ad12-3c527b90ba80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8f919cb-f4bc-41d7-a6a3-8703d1ce446b",
                    "LayerId": "62a557e8-9962-4795-aa80-5309deafca47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "62a557e8-9962-4795-aa80-5309deafca47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "000fd899-3f4f-4531-a8e2-b5052ac5c416",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 10
}