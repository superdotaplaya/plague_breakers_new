{
    "id": "0e9def90-1c4b-4906-aca7-fe7e996062de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_slowdown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 347,
    "bbox_left": 45,
    "bbox_right": 603,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1fef375d-cf7b-456c-9702-357cc3dfb12f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e9def90-1c4b-4906-aca7-fe7e996062de",
            "compositeImage": {
                "id": "027cf495-b2ce-4231-9290-7cce15c56e4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fef375d-cf7b-456c-9702-357cc3dfb12f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c59857b-8baa-41e5-8fc8-3ca060130f51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fef375d-cf7b-456c-9702-357cc3dfb12f",
                    "LayerId": "6000cad7-0918-41fd-beb2-515821581d36"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "6000cad7-0918-41fd-beb2-515821581d36",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e9def90-1c4b-4906-aca7-fe7e996062de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 200
}