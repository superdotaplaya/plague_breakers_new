{
    "id": "72079f4f-9558-4696-8b39-21e6a15d0714",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blue_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 15,
    "bbox_right": 47,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5bfea43-4a90-47b6-8ac9-585f6a7be34a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72079f4f-9558-4696-8b39-21e6a15d0714",
            "compositeImage": {
                "id": "162dadac-6966-4a10-93b8-79cba82f8277",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5bfea43-4a90-47b6-8ac9-585f6a7be34a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bae7375-9e83-498a-909f-bcdb68c02e8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5bfea43-4a90-47b6-8ac9-585f6a7be34a",
                    "LayerId": "3a8f9259-b800-4a0d-bec9-7f862115afa9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3a8f9259-b800-4a0d-bec9-7f862115afa9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72079f4f-9558-4696-8b39-21e6a15d0714",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 41
}