{
    "id": "253138d9-82ea-4fef-9b55-02494aead4bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_green_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 29,
    "bbox_right": 41,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84745074-1c24-4d2a-843e-10a11a603162",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "253138d9-82ea-4fef-9b55-02494aead4bd",
            "compositeImage": {
                "id": "cb81389f-4c89-41d2-9269-bc3460ea1376",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84745074-1c24-4d2a-843e-10a11a603162",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c281a5df-a892-46fd-aae0-1adda7cd23c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84745074-1c24-4d2a-843e-10a11a603162",
                    "LayerId": "d5593b85-c148-4dec-9d0e-3c3dec8f5b64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d5593b85-c148-4dec-9d0e-3c3dec8f5b64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "253138d9-82ea-4fef-9b55-02494aead4bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 34,
    "yorig": 34
}