{
    "id": "90850e67-fd74-4467-8e48-35a4afdcfdeb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_start_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 347,
    "bbox_left": 45,
    "bbox_right": 603,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e23d66d9-e1d7-49fd-b150-d44782ef9e17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90850e67-fd74-4467-8e48-35a4afdcfdeb",
            "compositeImage": {
                "id": "a024b80b-148f-43c9-8c81-65ddda3f0091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e23d66d9-e1d7-49fd-b150-d44782ef9e17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b45ab27d-3fa6-4738-8d0c-929abc015dc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e23d66d9-e1d7-49fd-b150-d44782ef9e17",
                    "LayerId": "8970d6d3-ed75-4ad0-97b3-4548fb565762"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "8970d6d3-ed75-4ad0-97b3-4548fb565762",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90850e67-fd74-4467-8e48-35a4afdcfdeb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 200
}