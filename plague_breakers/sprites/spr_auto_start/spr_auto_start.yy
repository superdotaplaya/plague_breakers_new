{
    "id": "e7ce6e11-c388-4c41-b619-9ba8ec6982d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_auto_start",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 333,
    "bbox_left": 0,
    "bbox_right": 333,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d0e32faa-9af4-49e0-8ff4-0de90ca8a50f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7ce6e11-c388-4c41-b619-9ba8ec6982d7",
            "compositeImage": {
                "id": "b1c48145-26a9-4046-9544-45262a6721c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0e32faa-9af4-49e0-8ff4-0de90ca8a50f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b38b8896-a9c5-4197-b039-3cef3e34c0b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0e32faa-9af4-49e0-8ff4-0de90ca8a50f",
                    "LayerId": "f51e19e3-4e9f-4c5b-903c-968ceb8ecf6b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 334,
    "layers": [
        {
            "id": "f51e19e3-4e9f-4c5b-903c-968ceb8ecf6b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7ce6e11-c388-4c41-b619-9ba8ec6982d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 334,
    "xorig": 0,
    "yorig": 0
}