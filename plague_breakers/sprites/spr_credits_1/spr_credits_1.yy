{
    "id": "66a9167b-427c-49da-8f19-52d9b580c997",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_credits_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 347,
    "bbox_left": 45,
    "bbox_right": 603,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee8343ed-10dd-415a-bf52-e3221b23837f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66a9167b-427c-49da-8f19-52d9b580c997",
            "compositeImage": {
                "id": "4456e87e-3203-4528-b73b-6b86eddd34a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee8343ed-10dd-415a-bf52-e3221b23837f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a8d225b-0632-4f53-b54f-c94090c42ed9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee8343ed-10dd-415a-bf52-e3221b23837f",
                    "LayerId": "146c0254-51d3-498b-a555-483cbe45a37a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "146c0254-51d3-498b-a555-483cbe45a37a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66a9167b-427c-49da-8f19-52d9b580c997",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 200
}