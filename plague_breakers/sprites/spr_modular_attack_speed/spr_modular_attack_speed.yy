{
    "id": "cd319e63-fd52-4717-92e8-dfc9f73a289b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_modular_attack_speed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 17,
    "bbox_right": 47,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ef466a7-bfde-4f5d-b89f-68cf21824e2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd319e63-fd52-4717-92e8-dfc9f73a289b",
            "compositeImage": {
                "id": "110f2044-1b0f-4eda-93bd-0c0c1edea797",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ef466a7-bfde-4f5d-b89f-68cf21824e2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e65c154-1710-4405-b626-27a36acd565f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ef466a7-bfde-4f5d-b89f-68cf21824e2a",
                    "LayerId": "04924080-2b70-413d-8631-84f4d188098e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "04924080-2b70-413d-8631-84f4d188098e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd319e63-fd52-4717-92e8-dfc9f73a289b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}