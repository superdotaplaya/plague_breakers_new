{
    "id": "6cbe9fed-19c8-4f0e-a3f6-a50f68feb433",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 105,
    "bbox_left": 6,
    "bbox_right": 385,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f57abd4-fb38-472c-945b-d4b8bd1ecae9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cbe9fed-19c8-4f0e-a3f6-a50f68feb433",
            "compositeImage": {
                "id": "e86b5670-b929-4f99-bc51-5a9aff3c6982",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f57abd4-fb38-472c-945b-d4b8bd1ecae9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e326cc0-aed7-414f-9c1d-a4e1e25a9c7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f57abd4-fb38-472c-945b-d4b8bd1ecae9",
                    "LayerId": "6aa14c7a-ba46-4ef5-b57c-8dab7c976302"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 113,
    "layers": [
        {
            "id": "6aa14c7a-ba46-4ef5-b57c-8dab7c976302",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6cbe9fed-19c8-4f0e-a3f6-a50f68feb433",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 393,
    "xorig": 0,
    "yorig": 0
}