{
    "id": "45f085ce-2bc3-400d-a76d-c59ff7a6103c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_coin_vacuum",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 17,
    "bbox_right": 47,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78cf0c9b-603c-4518-a6be-44473d934982",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45f085ce-2bc3-400d-a76d-c59ff7a6103c",
            "compositeImage": {
                "id": "c538baab-c674-4a4a-baa2-236df7355ef0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78cf0c9b-603c-4518-a6be-44473d934982",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "725f8d2f-3169-4d8d-aa9e-e7438e61055f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78cf0c9b-603c-4518-a6be-44473d934982",
                    "LayerId": "ecca82ef-c45b-45d7-b8de-9654e8eee299"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ecca82ef-c45b-45d7-b8de-9654e8eee299",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45f085ce-2bc3-400d-a76d-c59ff7a6103c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}