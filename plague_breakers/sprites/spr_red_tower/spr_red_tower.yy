{
    "id": "afd048dc-5f11-4a66-a632-1d454265b957",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_red_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 9,
    "bbox_right": 56,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7482df47-dd85-4fdd-9485-ff943c867a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afd048dc-5f11-4a66-a632-1d454265b957",
            "compositeImage": {
                "id": "f9559f9b-38b4-4891-92ab-bf93719adc31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7482df47-dd85-4fdd-9485-ff943c867a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c070f5d3-4bab-466d-a070-ce3f4f632ed8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7482df47-dd85-4fdd-9485-ff943c867a05",
                    "LayerId": "99045e4e-ffde-4d8b-bf64-8b656c25815d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "99045e4e-ffde-4d8b-bf64-8b656c25815d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afd048dc-5f11-4a66-a632-1d454265b957",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 38
}