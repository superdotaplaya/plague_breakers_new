{
    "id": "3b3bff45-fa73-4183-95aa-099898b87b1c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mute_music",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 27,
    "bbox_right": 190,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e5eb35d-9932-4c5d-b27c-2e92b65b4933",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b3bff45-fa73-4183-95aa-099898b87b1c",
            "compositeImage": {
                "id": "6e9815e1-01c4-4378-b0da-a266dc1786ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e5eb35d-9932-4c5d-b27c-2e92b65b4933",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a73bcc2d-6326-4b13-be7e-cf4f7275038c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e5eb35d-9932-4c5d-b27c-2e92b65b4933",
                    "LayerId": "b6ad619f-b4c8-45a4-b4a5-0beea8fdb4a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "b6ad619f-b4c8-45a4-b4a5-0beea8fdb4a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b3bff45-fa73-4183-95aa-099898b87b1c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}