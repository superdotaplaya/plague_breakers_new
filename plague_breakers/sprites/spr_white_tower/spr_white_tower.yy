{
    "id": "92de3f75-7cf1-48b2-ae14-43cffb5901c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_white_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 9,
    "bbox_right": 53,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13c28816-d62b-41b5-ab96-43c637a609a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92de3f75-7cf1-48b2-ae14-43cffb5901c3",
            "compositeImage": {
                "id": "332a8419-da35-4da7-b683-647ae309cda6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13c28816-d62b-41b5-ab96-43c637a609a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fb17ae8-cd30-4002-8c43-ed4ac4458805",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13c28816-d62b-41b5-ab96-43c637a609a0",
                    "LayerId": "998b234f-896e-42b6-bd44-a48ee50c4b2e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "998b234f-896e-42b6-bd44-a48ee50c4b2e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92de3f75-7cf1-48b2-ae14-43cffb5901c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 39
}