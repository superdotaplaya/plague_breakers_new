{
    "id": "fbd12d3e-0329-453f-9aa5-5ada9508f44f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yellow_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ceb76d0-7137-4e83-8960-230fd2da2d5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbd12d3e-0329-453f-9aa5-5ada9508f44f",
            "compositeImage": {
                "id": "e37d9cfa-432e-428e-8802-ac7219543bcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ceb76d0-7137-4e83-8960-230fd2da2d5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f5bd2d5-25c6-4285-af28-586e451a7e8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ceb76d0-7137-4e83-8960-230fd2da2d5b",
                    "LayerId": "33c1fecf-a14f-4dac-9371-f6acb6e9e2e5"
                }
            ]
        },
        {
            "id": "71f300d0-c904-445d-a60a-dcb1f837ba00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbd12d3e-0329-453f-9aa5-5ada9508f44f",
            "compositeImage": {
                "id": "cc67fe0d-8355-481f-aeb4-84356d10bd7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71f300d0-c904-445d-a60a-dcb1f837ba00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07dfcb3a-ce35-4b6f-811e-a9759c3f50bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71f300d0-c904-445d-a60a-dcb1f837ba00",
                    "LayerId": "33c1fecf-a14f-4dac-9371-f6acb6e9e2e5"
                }
            ]
        },
        {
            "id": "9fbb2f41-255e-4f0c-a3bc-5f3452f91bea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbd12d3e-0329-453f-9aa5-5ada9508f44f",
            "compositeImage": {
                "id": "5d4bc4a3-a5ae-4109-aee1-f0a49e994f86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fbb2f41-255e-4f0c-a3bc-5f3452f91bea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd7754f6-28d7-4dab-a87e-78d77f5798b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fbb2f41-255e-4f0c-a3bc-5f3452f91bea",
                    "LayerId": "33c1fecf-a14f-4dac-9371-f6acb6e9e2e5"
                }
            ]
        },
        {
            "id": "505be45d-2ce3-4f3f-808e-37ebdb26226b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbd12d3e-0329-453f-9aa5-5ada9508f44f",
            "compositeImage": {
                "id": "9c31967b-e8c1-4ec4-ba44-1e4b6be7dd25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "505be45d-2ce3-4f3f-808e-37ebdb26226b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ce3bb18-e0ef-4fc8-88c6-933dc0fd6da9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "505be45d-2ce3-4f3f-808e-37ebdb26226b",
                    "LayerId": "33c1fecf-a14f-4dac-9371-f6acb6e9e2e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "33c1fecf-a14f-4dac-9371-f6acb6e9e2e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbd12d3e-0329-453f-9aa5-5ada9508f44f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}