{
    "id": "692933ab-3c13-4a12-b13e-3265730f1324",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_modular_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 21,
    "bbox_right": 44,
    "bbox_top": 19,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c5a24b7-8dd4-492b-8bfa-10629339851c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "692933ab-3c13-4a12-b13e-3265730f1324",
            "compositeImage": {
                "id": "ec89411d-59b5-4a4f-b3c9-bc1857e3cced",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c5a24b7-8dd4-492b-8bfa-10629339851c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73404a25-9e57-4e61-b8ec-5e01eb78d41a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c5a24b7-8dd4-492b-8bfa-10629339851c",
                    "LayerId": "87874729-f13b-461e-9790-95ee8cfa08d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "87874729-f13b-461e-9790-95ee8cfa08d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "692933ab-3c13-4a12-b13e-3265730f1324",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}