{
    "id": "f73f4dae-ee41-4718-a8ae-a1ef0fb2ec36",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite40",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 104,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ebedb2a6-be7f-450b-984b-4c952b85c6ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f73f4dae-ee41-4718-a8ae-a1ef0fb2ec36",
            "compositeImage": {
                "id": "f2072e39-b6c3-4cc0-8bba-835f4cd6706a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebedb2a6-be7f-450b-984b-4c952b85c6ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d964cafb-f17f-4152-a9aa-98e693717bb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebedb2a6-be7f-450b-984b-4c952b85c6ec",
                    "LayerId": "6e0334fa-90cd-4c44-8017-abb11118a09a"
                }
            ]
        },
        {
            "id": "d713c0be-3b79-43a6-8fb3-a2927e55a6a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f73f4dae-ee41-4718-a8ae-a1ef0fb2ec36",
            "compositeImage": {
                "id": "0741527d-1816-45b6-8230-c69b53198cbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d713c0be-3b79-43a6-8fb3-a2927e55a6a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66259f7d-0ce9-46fc-ba75-a7361a5354af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d713c0be-3b79-43a6-8fb3-a2927e55a6a6",
                    "LayerId": "6e0334fa-90cd-4c44-8017-abb11118a09a"
                }
            ]
        },
        {
            "id": "6c9f5f09-9bd3-49ff-88d7-7e5a3c16d9ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f73f4dae-ee41-4718-a8ae-a1ef0fb2ec36",
            "compositeImage": {
                "id": "4bd246f4-1270-4d62-8ea0-90fcf735519a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c9f5f09-9bd3-49ff-88d7-7e5a3c16d9ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef33b425-be3b-45cc-b66c-5bba9d735295",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c9f5f09-9bd3-49ff-88d7-7e5a3c16d9ed",
                    "LayerId": "6e0334fa-90cd-4c44-8017-abb11118a09a"
                }
            ]
        },
        {
            "id": "de4a057d-019e-406a-be6c-d61e48c3cf41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f73f4dae-ee41-4718-a8ae-a1ef0fb2ec36",
            "compositeImage": {
                "id": "0ffd0111-8082-4d7c-98c5-80228097293d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de4a057d-019e-406a-be6c-d61e48c3cf41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d489861-69cd-4123-af12-80e5f9843088",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de4a057d-019e-406a-be6c-d61e48c3cf41",
                    "LayerId": "6e0334fa-90cd-4c44-8017-abb11118a09a"
                }
            ]
        },
        {
            "id": "d6d45c93-f148-4859-940f-37a1ebfa9816",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f73f4dae-ee41-4718-a8ae-a1ef0fb2ec36",
            "compositeImage": {
                "id": "6f6da269-a153-43ed-abd9-22edcd09ba86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6d45c93-f148-4859-940f-37a1ebfa9816",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fba1fd42-017c-4a6c-89ca-75c227fe34b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6d45c93-f148-4859-940f-37a1ebfa9816",
                    "LayerId": "6e0334fa-90cd-4c44-8017-abb11118a09a"
                }
            ]
        },
        {
            "id": "fecc3c11-6683-4077-b1b4-b73ffd8c46d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f73f4dae-ee41-4718-a8ae-a1ef0fb2ec36",
            "compositeImage": {
                "id": "d39cb2ea-f303-4ed1-b8ba-465e7b12a04f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fecc3c11-6683-4077-b1b4-b73ffd8c46d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f06dcc29-dad4-4246-9631-6270467e7c46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fecc3c11-6683-4077-b1b4-b73ffd8c46d1",
                    "LayerId": "6e0334fa-90cd-4c44-8017-abb11118a09a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "6e0334fa-90cd-4c44-8017-abb11118a09a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f73f4dae-ee41-4718-a8ae-a1ef0fb2ec36",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 105,
    "xorig": 0,
    "yorig": 12
}