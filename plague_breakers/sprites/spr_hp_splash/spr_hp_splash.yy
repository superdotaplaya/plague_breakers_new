{
    "id": "8ede8fa8-0b2e-4578-bcca-df67edf75c6b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hp_splash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 1,
    "bbox_right": 59,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "490f668e-6e1a-45aa-8f07-0b74695b6c8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ede8fa8-0b2e-4578-bcca-df67edf75c6b",
            "compositeImage": {
                "id": "d042f4c8-b618-4f0a-9873-49348d6fce71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "490f668e-6e1a-45aa-8f07-0b74695b6c8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "836418d1-dcbb-4d08-b59d-723866ce3c2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "490f668e-6e1a-45aa-8f07-0b74695b6c8e",
                    "LayerId": "27d8c200-7462-448b-b469-28c09592c832"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "27d8c200-7462-448b-b469-28c09592c832",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ede8fa8-0b2e-4578-bcca-df67edf75c6b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}