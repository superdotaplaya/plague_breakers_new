{
    "id": "bd7ec843-ded7-4fcc-99c6-e7a58292f42f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blue_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30158021-1e60-48db-9e86-3da8c9b45e7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd7ec843-ded7-4fcc-99c6-e7a58292f42f",
            "compositeImage": {
                "id": "d3e52649-f81c-49ed-a19b-5821884e824f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30158021-1e60-48db-9e86-3da8c9b45e7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a2e4eb4-d706-470d-baf6-04076d5d5ffb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30158021-1e60-48db-9e86-3da8c9b45e7e",
                    "LayerId": "b41786ea-c8f5-46a9-86fc-5ee3d8511ddd"
                }
            ]
        },
        {
            "id": "81daf19d-b9d6-40ce-8b40-e3a117789151",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd7ec843-ded7-4fcc-99c6-e7a58292f42f",
            "compositeImage": {
                "id": "f80eb6af-cfac-47cb-9ff1-d6081d092406",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81daf19d-b9d6-40ce-8b40-e3a117789151",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87054c34-6ee6-4ac6-83e1-f89242d41728",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81daf19d-b9d6-40ce-8b40-e3a117789151",
                    "LayerId": "b41786ea-c8f5-46a9-86fc-5ee3d8511ddd"
                }
            ]
        },
        {
            "id": "be48d5d5-6a7c-4a32-b40a-aa7ea0dd656f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd7ec843-ded7-4fcc-99c6-e7a58292f42f",
            "compositeImage": {
                "id": "3ceddd4e-6b06-488a-a60a-3d6affcca47a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be48d5d5-6a7c-4a32-b40a-aa7ea0dd656f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a95fccae-a004-4eb0-a7da-a062e0cc27d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be48d5d5-6a7c-4a32-b40a-aa7ea0dd656f",
                    "LayerId": "b41786ea-c8f5-46a9-86fc-5ee3d8511ddd"
                }
            ]
        },
        {
            "id": "5bd4dba3-a259-4527-91a6-02553c05f9b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd7ec843-ded7-4fcc-99c6-e7a58292f42f",
            "compositeImage": {
                "id": "ee65fee0-d90b-4ed7-9422-e7fab46258e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bd4dba3-a259-4527-91a6-02553c05f9b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4d29735-884d-4add-a9a6-736100bd8126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bd4dba3-a259-4527-91a6-02553c05f9b8",
                    "LayerId": "b41786ea-c8f5-46a9-86fc-5ee3d8511ddd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "b41786ea-c8f5-46a9-86fc-5ee3d8511ddd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd7ec843-ded7-4fcc-99c6-e7a58292f42f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}