{
    "id": "c4e7204c-bd0b-46b6-b2a1-b15035274049",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_orange_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 2,
    "bbox_right": 59,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00f4b448-3042-4aa9-9c1e-7b16443db077",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4e7204c-bd0b-46b6-b2a1-b15035274049",
            "compositeImage": {
                "id": "946232b4-b6d9-495f-844b-f6f5b8c62fe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00f4b448-3042-4aa9-9c1e-7b16443db077",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "317e6bd2-eb82-43a3-b408-8905d4eca5a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00f4b448-3042-4aa9-9c1e-7b16443db077",
                    "LayerId": "6d0e6881-d468-4be8-9421-3382f058648d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6d0e6881-d468-4be8-9421-3382f058648d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4e7204c-bd0b-46b6-b2a1-b15035274049",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}