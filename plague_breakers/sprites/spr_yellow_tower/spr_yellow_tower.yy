{
    "id": "c82849b0-f7ad-4a5d-aadd-977a6138ee59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yellow_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 10,
    "bbox_right": 52,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5edb6bd8-18d7-4fa6-91a7-147a89029e39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c82849b0-f7ad-4a5d-aadd-977a6138ee59",
            "compositeImage": {
                "id": "42ddf8e8-a4a1-4136-bb5a-36e2a06387ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5edb6bd8-18d7-4fa6-91a7-147a89029e39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fef5ae8-0832-40a6-85fa-92e628fa60ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5edb6bd8-18d7-4fa6-91a7-147a89029e39",
                    "LayerId": "d227a764-82b2-40f4-bf42-4b45c0fc06b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d227a764-82b2-40f4-bf42-4b45c0fc06b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c82849b0-f7ad-4a5d-aadd-977a6138ee59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 40
}