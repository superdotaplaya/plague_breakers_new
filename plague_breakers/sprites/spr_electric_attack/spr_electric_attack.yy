{
    "id": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_electric_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 218,
    "bbox_left": 94,
    "bbox_right": 214,
    "bbox_top": 107,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b2825eb-00f2-425f-84c5-647ec50f90ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "140d86b7-87e0-47f1-81cf-a7d0bb16ec24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b2825eb-00f2-425f-84c5-647ec50f90ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbf63515-0aa0-42a9-b188-0d470380a23d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b2825eb-00f2-425f-84c5-647ec50f90ec",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "cb0d5ce3-fac4-4c9e-b70c-2a170309a81e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "94607251-0185-40ed-a083-11bd55a8ce94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb0d5ce3-fac4-4c9e-b70c-2a170309a81e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bba1f82f-dda0-400c-b45c-59ef7c8e7530",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb0d5ce3-fac4-4c9e-b70c-2a170309a81e",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "1d2dcecd-8fb7-4d8f-9735-187c24dbe32b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "a1ab32f1-7004-46c6-92a8-6f695ee594ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d2dcecd-8fb7-4d8f-9735-187c24dbe32b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84bd6781-5224-4382-a8c2-ec800f0770c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d2dcecd-8fb7-4d8f-9735-187c24dbe32b",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "cfbc72ad-1f74-4939-9c40-792d0ebd41d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "2ee8d8bd-2d3f-4c49-a3c0-b84c5fb98534",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfbc72ad-1f74-4939-9c40-792d0ebd41d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d4cddd3-edf7-4972-91a7-19ad1344eab9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfbc72ad-1f74-4939-9c40-792d0ebd41d8",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "be92f715-2f79-48d0-8cd6-5aa549bc10e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "6f05d869-e298-4a62-b503-cf8e463fbbb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be92f715-2f79-48d0-8cd6-5aa549bc10e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb7af8bc-b509-4220-ab23-56959349e5a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be92f715-2f79-48d0-8cd6-5aa549bc10e1",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "83ced9dd-ed23-4cd6-8159-da646213df95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "b44cccfb-c1b4-4d6f-8e18-792236d28060",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83ced9dd-ed23-4cd6-8159-da646213df95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "387fc19b-c043-46d3-bfe6-1be8329c84e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83ced9dd-ed23-4cd6-8159-da646213df95",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "9511493b-45f8-4854-830a-05d5daf93409",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "da0c0298-25d9-4257-b2d0-0cec99808ae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9511493b-45f8-4854-830a-05d5daf93409",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e17ae34-ecb1-44c7-bd74-217368efde8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9511493b-45f8-4854-830a-05d5daf93409",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "82798d2e-f5df-4683-ac69-6a94e6439322",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "42855a85-01dd-4b53-981f-4ff9d9c9ef37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82798d2e-f5df-4683-ac69-6a94e6439322",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed270766-d530-43fa-a17b-d0549befa032",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82798d2e-f5df-4683-ac69-6a94e6439322",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "f50371ba-8294-4750-85cb-cd198d395c83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "550548c8-ff31-497d-8702-a2b6de1b3058",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f50371ba-8294-4750-85cb-cd198d395c83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3f65ce0-55ee-45f6-95c8-a54f67b14179",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f50371ba-8294-4750-85cb-cd198d395c83",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "0b0c1a66-89b4-4e4e-b379-120bf253e22e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "a5a71851-755d-47a1-bb6f-50a202a6d2ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b0c1a66-89b4-4e4e-b379-120bf253e22e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aa61a89-efee-4c74-855f-aae6de5e50fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b0c1a66-89b4-4e4e-b379-120bf253e22e",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "10bc4add-f523-4faa-af54-e25fcd438c32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "c2cf5f16-dc34-41d0-8cc5-d6142886f15d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10bc4add-f523-4faa-af54-e25fcd438c32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab94b0e5-24b7-42a3-a59c-e6902994d22a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10bc4add-f523-4faa-af54-e25fcd438c32",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "08e244f4-e55d-483f-91bf-8b9e6c388723",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "4b4aed47-ee09-48d4-8f8c-7664385c90e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08e244f4-e55d-483f-91bf-8b9e6c388723",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "495f6967-aec0-41fa-a335-9e977e36ba2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08e244f4-e55d-483f-91bf-8b9e6c388723",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "c715658c-c794-4960-a940-7838b595196a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "784855e4-8bbb-48d2-9513-287425627418",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c715658c-c794-4960-a940-7838b595196a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71a35ca8-4df8-4817-a4d9-3d0e3ba2a2dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c715658c-c794-4960-a940-7838b595196a",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "d72f90c9-303a-410c-a8f0-d0e445225cc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "db54b399-2ed0-4262-848c-78bd339e8931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d72f90c9-303a-410c-a8f0-d0e445225cc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e848ded2-0cd6-4835-9da4-783f1c585610",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d72f90c9-303a-410c-a8f0-d0e445225cc9",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "961840e9-8244-43db-a8de-3697d26fa3da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "d71663af-54c8-4a2a-9316-773f548b9c18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "961840e9-8244-43db-a8de-3697d26fa3da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32cfb99b-211a-4b52-9d6c-02677a2fb879",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "961840e9-8244-43db-a8de-3697d26fa3da",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "9988f341-74f9-42ca-b836-92a4b3e2df61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "7c2bf530-83e1-4d5d-a35c-c4bb913051b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9988f341-74f9-42ca-b836-92a4b3e2df61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "645a4959-9bab-4236-9b9c-f46263865777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9988f341-74f9-42ca-b836-92a4b3e2df61",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "b21acda1-7bd9-4b0c-9e88-786adb1fef19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "c35f45f3-9767-446d-8998-9c74eea6e8c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b21acda1-7bd9-4b0c-9e88-786adb1fef19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfc00f1b-1160-4637-879a-07e3cbd72eb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b21acda1-7bd9-4b0c-9e88-786adb1fef19",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "59a54705-a225-4d0b-99cb-1d9b55b01553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "9c4a0ced-52da-4fd1-9705-451426b36581",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59a54705-a225-4d0b-99cb-1d9b55b01553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2044220-0616-43da-ad5e-b415b13faa0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59a54705-a225-4d0b-99cb-1d9b55b01553",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "40e88fa8-144f-4115-a847-ea53db9be32c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "95891b2c-5afd-478f-a847-8ae688a221f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40e88fa8-144f-4115-a847-ea53db9be32c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9acceec1-5959-435c-a56b-c4ceb11d6185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40e88fa8-144f-4115-a847-ea53db9be32c",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "7dd03b6c-f17a-4e63-9bdd-0fcc58dac883",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "ff46b2bf-161e-4535-b9ac-4155420d7fe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dd03b6c-f17a-4e63-9bdd-0fcc58dac883",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00d0580f-94ba-482f-ba67-ba1372d6ff75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dd03b6c-f17a-4e63-9bdd-0fcc58dac883",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "82ae457e-e99b-4321-a15a-c684b395e8ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "d6f78ae7-c1ba-426a-bc00-ccb79031e786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82ae457e-e99b-4321-a15a-c684b395e8ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8204c127-3b92-49c9-b9b0-beb23537d226",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82ae457e-e99b-4321-a15a-c684b395e8ef",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "8cfeef0d-80e2-450c-a3c2-585d3d01cefa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "c9c7a4e5-72d1-4614-a4a9-eb2f78a76680",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cfeef0d-80e2-450c-a3c2-585d3d01cefa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25d7e256-156a-4a66-817f-d37131861d8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cfeef0d-80e2-450c-a3c2-585d3d01cefa",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "9d171f60-817d-478f-9f9e-e4c476d6ae19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "b3c7f0eb-1354-40cd-95a6-b4ef68cd6f10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d171f60-817d-478f-9f9e-e4c476d6ae19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e31f6d4-c8c1-4849-a027-941aaa753f44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d171f60-817d-478f-9f9e-e4c476d6ae19",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "2caee322-e720-487e-b50b-1bf1ac4815c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "1b07ae24-d39c-4794-9abf-639c0656069c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2caee322-e720-487e-b50b-1bf1ac4815c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47018921-438a-44de-8d73-ac30fe2b827c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2caee322-e720-487e-b50b-1bf1ac4815c0",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "23f98d0c-c3ec-4e98-9017-ac36a08bccc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "3b4a219d-3eb9-4628-8199-61b704861159",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23f98d0c-c3ec-4e98-9017-ac36a08bccc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec1244c5-f18e-451f-a105-f512aac08eef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23f98d0c-c3ec-4e98-9017-ac36a08bccc1",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "ffb40cb6-2e9a-43e8-842a-96d897ac6700",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "6099a528-9a38-4660-9591-9ded64d10fb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffb40cb6-2e9a-43e8-842a-96d897ac6700",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9529b5a5-9a2a-4cb3-8ced-7364bd696a4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffb40cb6-2e9a-43e8-842a-96d897ac6700",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "c7986b1f-52fb-4909-9be1-317031360ca3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "cbe31bf9-3f0f-433a-85ee-164ceb72f715",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7986b1f-52fb-4909-9be1-317031360ca3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0289bf9-ceae-42f0-83da-96fa14400bed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7986b1f-52fb-4909-9be1-317031360ca3",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "27e26d46-4917-4d58-8f49-95f403203071",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "4e9e15bb-48a3-4a89-9110-eda47698d8be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27e26d46-4917-4d58-8f49-95f403203071",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f08bd4ab-a417-4eac-89e3-0fccd0ad7456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27e26d46-4917-4d58-8f49-95f403203071",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "df274444-70fb-4fce-ad86-17d6c02f2ba2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "7ae36c05-5aad-4a70-b14a-958d801a2b7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df274444-70fb-4fce-ad86-17d6c02f2ba2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35f030bf-aa92-40ab-baf5-3f468b6da1da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df274444-70fb-4fce-ad86-17d6c02f2ba2",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "6914d704-c7ed-4385-801d-bfd7f47e356f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "aa39c612-e6c9-48da-97d4-86ed7d1a9c0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6914d704-c7ed-4385-801d-bfd7f47e356f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c545d7d-b8d5-48b0-8259-a038b1f46989",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6914d704-c7ed-4385-801d-bfd7f47e356f",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "ce158a58-0229-4966-bf05-a9642897727c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "f8985eb5-2051-471a-b7aa-82573cb3b0e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce158a58-0229-4966-bf05-a9642897727c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d23db7db-4089-4c5d-ac9c-ef0274ee8387",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce158a58-0229-4966-bf05-a9642897727c",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "0d7fd650-cfc8-41da-be11-929f7dd4c235",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "9f471062-c0e2-41a0-89e0-c980d93d8c75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d7fd650-cfc8-41da-be11-929f7dd4c235",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "386e3b68-8d6a-4403-a8a4-73b542102f90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d7fd650-cfc8-41da-be11-929f7dd4c235",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "ed4ef230-ed90-4c6f-a258-6da26c389955",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "89f6408b-ea37-477a-ac23-1d4e1611182f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed4ef230-ed90-4c6f-a258-6da26c389955",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4320c76-668b-43ae-b756-214bc334d357",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed4ef230-ed90-4c6f-a258-6da26c389955",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "825fa287-9541-4f13-9e05-b3e81071843c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "6ee3e0d6-a757-422b-ba1f-ba5baf64d1b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "825fa287-9541-4f13-9e05-b3e81071843c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78c1194c-3d7a-43fb-89d1-da4a9058ddad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "825fa287-9541-4f13-9e05-b3e81071843c",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "adfdb104-2255-4ab8-b737-2baa471fdc6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "5e36bd05-11ea-44f7-b129-6ee0dfc89dba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adfdb104-2255-4ab8-b737-2baa471fdc6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e1b204c-f484-454e-a9b1-231a1c7ce630",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adfdb104-2255-4ab8-b737-2baa471fdc6c",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "311f4bc0-0ae4-4292-9ca6-0f4ddfb32da5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "dee8c363-6db9-431f-b3c4-0ec456fb4351",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "311f4bc0-0ae4-4292-9ca6-0f4ddfb32da5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e432669b-485a-4bd1-9012-2976d165c015",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "311f4bc0-0ae4-4292-9ca6-0f4ddfb32da5",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "30e99628-8aaf-405b-9e30-308025514438",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "bd8ac0fb-e236-45e1-9e97-26c251a5698b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30e99628-8aaf-405b-9e30-308025514438",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8a14580-2f2b-41bd-a146-3587dcf64b07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30e99628-8aaf-405b-9e30-308025514438",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "a8e79ff1-5e03-4a20-abd5-d2b378d0e318",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "760a9c78-f14a-45a7-b55f-fa81ed87e029",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8e79ff1-5e03-4a20-abd5-d2b378d0e318",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaf7971c-0668-4ef1-bb41-e5700f6940f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8e79ff1-5e03-4a20-abd5-d2b378d0e318",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "58ace3c6-5108-400d-9913-f7ad821e0964",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "ceecc9e4-7c27-446f-8969-08ac3a8b7f3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58ace3c6-5108-400d-9913-f7ad821e0964",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b06fe6c-1fba-4625-bfae-b556e13545ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58ace3c6-5108-400d-9913-f7ad821e0964",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "81d7fbae-de22-4768-9ca3-1bd05d0501c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "ca774547-22e8-4427-b7c3-17fd2c1c1c62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81d7fbae-de22-4768-9ca3-1bd05d0501c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a92ffc03-688e-4d99-8cb2-41f72b35073d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81d7fbae-de22-4768-9ca3-1bd05d0501c6",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "d8c6f730-b173-43ca-8291-548801fd81f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "62632eea-10b5-4c77-ab3a-b108ae7546b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8c6f730-b173-43ca-8291-548801fd81f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15ab33db-65e1-4d64-99c6-00110726712c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8c6f730-b173-43ca-8291-548801fd81f9",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "5159d240-d4f3-48b9-ad80-d53739342792",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "689a3381-9a4f-4713-bd30-ac8c4a738ce9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5159d240-d4f3-48b9-ad80-d53739342792",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "815a37a1-26b7-4b5d-8030-937e9a1cea74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5159d240-d4f3-48b9-ad80-d53739342792",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "62ae3ca6-3181-4d95-9853-40660ebf27f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "42e8a464-a121-4acc-8486-c93d95e9bb4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62ae3ca6-3181-4d95-9853-40660ebf27f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abd732d2-125c-433a-9cd3-8ab4a57a1f31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62ae3ca6-3181-4d95-9853-40660ebf27f5",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "ae80fd12-2e87-4d6c-bb9d-e7910b692722",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "61deb35c-0758-4251-9aae-503f3ced549a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae80fd12-2e87-4d6c-bb9d-e7910b692722",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e21a2be8-241d-447d-a12d-604612813234",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae80fd12-2e87-4d6c-bb9d-e7910b692722",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "cf5bdeda-ad6b-44c1-8eaf-10036440b8bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "cceed7ff-99f5-4a27-8fc2-7271ee456fd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf5bdeda-ad6b-44c1-8eaf-10036440b8bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82d5e5e1-b113-4872-b6e5-144c2fbc150f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf5bdeda-ad6b-44c1-8eaf-10036440b8bb",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "4dafac71-11ba-41e2-a94e-98dc9f83622c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "a8cb6079-5ed9-415d-b3ab-b4297ff7f3de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dafac71-11ba-41e2-a94e-98dc9f83622c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b020f1dc-ea32-481b-b72b-52a830095518",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dafac71-11ba-41e2-a94e-98dc9f83622c",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "50c15b06-8e17-4cdf-ab87-26cb2e9a4516",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "aa6aa827-2121-4e1d-809e-a186ecd97237",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50c15b06-8e17-4cdf-ab87-26cb2e9a4516",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5f01d0a-6fa2-413f-ac57-5cb7fee4fd85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50c15b06-8e17-4cdf-ab87-26cb2e9a4516",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "f1fd3bb8-4cce-450e-aefe-f9a085106ab3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "0f921e51-8ec7-44b2-ae5d-1cbfab562a6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1fd3bb8-4cce-450e-aefe-f9a085106ab3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d23966f2-09d8-40e8-95bb-1dbfd7646423",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1fd3bb8-4cce-450e-aefe-f9a085106ab3",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "7f80bf69-2909-4782-b59f-79110239abd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "e90fc054-a96f-409c-89a8-a8519c9779b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f80bf69-2909-4782-b59f-79110239abd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db79d281-a1a7-4fa8-b288-a1d539d1e6c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f80bf69-2909-4782-b59f-79110239abd0",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "18ad6cf8-8c32-4a2e-b291-5c0737c7dba0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "30c589e0-9aca-4908-8af5-9f2157624bd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18ad6cf8-8c32-4a2e-b291-5c0737c7dba0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cebae3b-9b07-439f-b41b-7c88c5b79e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18ad6cf8-8c32-4a2e-b291-5c0737c7dba0",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "423109b4-6f05-4c0e-99c0-4f6e4a2212be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "8d1e65d6-c186-4b9b-a9db-f034b7fa653b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "423109b4-6f05-4c0e-99c0-4f6e4a2212be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "822ab84c-a612-4234-91a1-f354a4f1e296",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "423109b4-6f05-4c0e-99c0-4f6e4a2212be",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "15be0c13-09db-49f4-b40e-f38f924feb02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "42038ac3-5edf-454f-9f64-0a2744bf8f81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15be0c13-09db-49f4-b40e-f38f924feb02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec618b9a-da40-4c7a-ad09-5846607d4eb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15be0c13-09db-49f4-b40e-f38f924feb02",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "462991a6-a850-4b45-94b4-31da22f5d256",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "787fc2f7-7a8e-4dad-a3b6-9e00df9ac4cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "462991a6-a850-4b45-94b4-31da22f5d256",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a0f0917-1622-4d77-befb-99afbd8dd6dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "462991a6-a850-4b45-94b4-31da22f5d256",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "8fba2108-1e5b-4c70-888a-bed4c75ecf98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "6f9bfb50-1b70-4bc3-a5dd-96adbf1f5be7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fba2108-1e5b-4c70-888a-bed4c75ecf98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "890f18ff-4ef9-4de5-b1b9-17768c1a54b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fba2108-1e5b-4c70-888a-bed4c75ecf98",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "179d6a10-0e5b-432e-a81b-80ceb6fa7f1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "142b620d-e74c-495c-b218-19cf683d6410",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "179d6a10-0e5b-432e-a81b-80ceb6fa7f1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa8b26cf-0409-4618-bb8d-c6e1f6678b30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "179d6a10-0e5b-432e-a81b-80ceb6fa7f1d",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "98cbbbb8-2e7e-40a1-ad3a-08247a983f63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "38e16ccd-6faa-4af1-8ceb-823c02bd4cb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98cbbbb8-2e7e-40a1-ad3a-08247a983f63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "527b8e78-5d22-46e3-a01c-08a46c6bbf09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98cbbbb8-2e7e-40a1-ad3a-08247a983f63",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "4ac8ea9f-2571-4ed9-b4aa-b6194e12c157",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "7564d35b-6281-4604-8eda-3899fc9d3b61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ac8ea9f-2571-4ed9-b4aa-b6194e12c157",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e90c3126-52be-42d2-bba5-c405e0111834",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ac8ea9f-2571-4ed9-b4aa-b6194e12c157",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "88f71ea8-809d-4721-a8f3-45864e6a8447",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "fbdbbaa5-9c45-4943-84bb-411b3e6680e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88f71ea8-809d-4721-a8f3-45864e6a8447",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c03dd2a-2828-4aac-affa-105506c82fc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88f71ea8-809d-4721-a8f3-45864e6a8447",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "c70ba265-bbe5-40aa-a8e9-9696bba42421",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "fa979991-2609-4f93-9a81-cb2600bd2bde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c70ba265-bbe5-40aa-a8e9-9696bba42421",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b311559-ebd8-4fac-9c99-c25bc8d7a566",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c70ba265-bbe5-40aa-a8e9-9696bba42421",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "f40dc9ee-1c1d-40e0-b718-0db5d738ac01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "28ad8318-a6f1-4a8f-a321-f3bfeac6b3a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f40dc9ee-1c1d-40e0-b718-0db5d738ac01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57cb92ef-dd48-4097-8660-9255ddfa4a2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f40dc9ee-1c1d-40e0-b718-0db5d738ac01",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "96f947c3-9c6e-4c36-85b6-185edf4750d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "2151d977-e231-4405-8650-4129b01fee8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96f947c3-9c6e-4c36-85b6-185edf4750d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec2468ec-fb69-4d54-8e68-3db8eb5ebac1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96f947c3-9c6e-4c36-85b6-185edf4750d1",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "8cd49030-ac83-4b3f-9f60-486e6c301dfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "23cc604d-04fc-4b47-9f42-71cee0c57a16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cd49030-ac83-4b3f-9f60-486e6c301dfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0c9c85e-dab7-4e4b-a366-af747763bc6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cd49030-ac83-4b3f-9f60-486e6c301dfb",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "b1edc808-6757-46a8-aacc-b15276952053",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "bc7f8008-b06b-449e-b985-05764598f7e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1edc808-6757-46a8-aacc-b15276952053",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ea61f59-ff9f-4742-9e3d-bae98d260933",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1edc808-6757-46a8-aacc-b15276952053",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "4a8a0ff9-e438-42bb-b86e-c5f5e3d84159",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "44eab792-08ee-42a8-83f5-610e991e4a97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a8a0ff9-e438-42bb-b86e-c5f5e3d84159",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a17df75-91c0-493f-9abb-2c9ccaabe138",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a8a0ff9-e438-42bb-b86e-c5f5e3d84159",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "6a24d92e-da21-46c2-ac5d-40040c37605f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "e0ace58e-2119-4f99-89a9-985a4eb27bd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a24d92e-da21-46c2-ac5d-40040c37605f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03064f9b-ae38-4671-a0ff-ba5e17c974c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a24d92e-da21-46c2-ac5d-40040c37605f",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "0da1ff40-1d7d-435e-bc48-5d66912cd648",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "a710b7d8-d3c7-40f4-8025-3779381f07f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0da1ff40-1d7d-435e-bc48-5d66912cd648",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2e5c1a3-b965-46bc-9377-1919dea8f66b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0da1ff40-1d7d-435e-bc48-5d66912cd648",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "47160c81-7a1c-4859-bd3d-4bc42bddd8e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "d63c27a0-72f0-45ad-b7d5-cdc914b2b98e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47160c81-7a1c-4859-bd3d-4bc42bddd8e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f8ff4be-141f-4484-aa5a-f27c70da8b96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47160c81-7a1c-4859-bd3d-4bc42bddd8e4",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "d6d57757-32ca-458c-a0ce-96d1fc3a85bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "a67d62f0-9bd2-4672-ac88-35d70d5c7d7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6d57757-32ca-458c-a0ce-96d1fc3a85bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78bda932-c3fd-415e-8f3c-e0e1578109cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6d57757-32ca-458c-a0ce-96d1fc3a85bc",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "cb93ab77-d92b-496d-a6c2-cda874a6317a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "e2812945-0d91-45e0-a334-faf03a97607e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb93ab77-d92b-496d-a6c2-cda874a6317a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "700c5638-a374-40e5-a25f-dc3397ae9c6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb93ab77-d92b-496d-a6c2-cda874a6317a",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "47baa7c0-6082-4aeb-8d64-09c50cff4dc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "fd72f3fd-f1f8-4a03-9938-5bce16611f1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47baa7c0-6082-4aeb-8d64-09c50cff4dc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "162802a0-5f6c-4925-8622-fe64ed7e0b23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47baa7c0-6082-4aeb-8d64-09c50cff4dc7",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "92b1c67f-6e3d-410a-a1ae-41ac2e2a9c35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "b0ec38c6-d8ce-428a-a8ad-5c4188828f1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92b1c67f-6e3d-410a-a1ae-41ac2e2a9c35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40b0f8a5-231c-4f2b-b493-92b89519b610",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92b1c67f-6e3d-410a-a1ae-41ac2e2a9c35",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "cf0b7605-14ba-41ff-88df-dce82921d68f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "71267553-771b-4264-94e1-7add4543973a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf0b7605-14ba-41ff-88df-dce82921d68f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b46d0d1-08f7-444c-806e-657908af777f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf0b7605-14ba-41ff-88df-dce82921d68f",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "a40dcb9b-3c17-4760-be2c-7c6921af4e18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "6470d441-2956-474b-ab59-1f74fb20f05b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a40dcb9b-3c17-4760-be2c-7c6921af4e18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff2121c4-6a7e-4b83-8a1e-8b3e154ab6b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a40dcb9b-3c17-4760-be2c-7c6921af4e18",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "774f88e9-5a6f-4766-9660-a8312eed932f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "fca9cc59-fb28-4919-8e58-69a111cdf0aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "774f88e9-5a6f-4766-9660-a8312eed932f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0289cde9-dabc-4bb7-a5b3-97289a83377d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "774f88e9-5a6f-4766-9660-a8312eed932f",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "8fd5c32e-de5e-4994-ae7c-229c494f6d95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "f5213c09-8a79-4ce8-9103-a755765685e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fd5c32e-de5e-4994-ae7c-229c494f6d95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07c1cd0d-86c6-404a-a573-151c777819c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fd5c32e-de5e-4994-ae7c-229c494f6d95",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "ebc6e312-e1f1-4ca9-88ff-2dfb9673a429",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "4d29487c-7d2c-4fe0-8088-69a12f1ad689",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebc6e312-e1f1-4ca9-88ff-2dfb9673a429",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd104825-99f5-4fde-a115-d3e2c5e9f4a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebc6e312-e1f1-4ca9-88ff-2dfb9673a429",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        },
        {
            "id": "98be980e-07f8-4ced-9f8f-b5fb3d653266",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "compositeImage": {
                "id": "b6fcfc16-b4fb-4921-9b21-850e2f93c15d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98be980e-07f8-4ced-9f8f-b5fb3d653266",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfb72014-f2ba-4461-ab3b-250e8628bb57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98be980e-07f8-4ced-9f8f-b5fb3d653266",
                    "LayerId": "72690772-a05b-4c00-8eeb-010bc4b0f558"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "72690772-a05b-4c00-8eeb-010bc4b0f558",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 150,
    "yorig": 150
}