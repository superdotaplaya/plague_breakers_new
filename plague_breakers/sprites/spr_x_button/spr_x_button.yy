{
    "id": "e1e0b95f-ad8a-4e3b-adfd-d2dc213ad78b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_x_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a4374a3-7d93-4cac-b7ce-54c3980456e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1e0b95f-ad8a-4e3b-adfd-d2dc213ad78b",
            "compositeImage": {
                "id": "7a1cd365-1873-44e0-8102-1b674d9ed957",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a4374a3-7d93-4cac-b7ce-54c3980456e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7733cd9c-f3be-4cb6-9882-6e923fbfcea4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a4374a3-7d93-4cac-b7ce-54c3980456e2",
                    "LayerId": "dcd3b7ee-3aae-4ea8-826f-0f7334792f5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dcd3b7ee-3aae-4ea8-826f-0f7334792f5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1e0b95f-ad8a-4e3b-adfd-d2dc213ad78b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}