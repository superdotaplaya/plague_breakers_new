{
    "id": "86e1ef81-744a-4a49-b005-72f7f36bddf5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_modular_attack_range",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 17,
    "bbox_right": 47,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e41503dd-3fb3-4562-80fa-4ef5bdac3eda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86e1ef81-744a-4a49-b005-72f7f36bddf5",
            "compositeImage": {
                "id": "1e21e076-fc06-434f-8ee4-b84cc0d4424c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e41503dd-3fb3-4562-80fa-4ef5bdac3eda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c910701f-cb3e-4fd6-b059-9e1d77393947",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e41503dd-3fb3-4562-80fa-4ef5bdac3eda",
                    "LayerId": "11bc8793-650d-486c-a04d-10f744cfcebd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "11bc8793-650d-486c-a04d-10f744cfcebd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86e1ef81-744a-4a49-b005-72f7f36bddf5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}