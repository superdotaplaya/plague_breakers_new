{
    "id": "fb2ab5e1-6228-4de6-8807-874807409083",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_red_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48449811-61cd-4f83-a6fe-bdad3fe9054b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb2ab5e1-6228-4de6-8807-874807409083",
            "compositeImage": {
                "id": "191e930f-bd9d-425a-9557-221d7de4355a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48449811-61cd-4f83-a6fe-bdad3fe9054b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6e89de3-b949-446d-bfc0-da4773d6cd65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48449811-61cd-4f83-a6fe-bdad3fe9054b",
                    "LayerId": "e1f0b225-0c4a-4f59-b304-b78ab0e07372"
                }
            ]
        },
        {
            "id": "010da26a-b632-4c87-9836-508a1f1962af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb2ab5e1-6228-4de6-8807-874807409083",
            "compositeImage": {
                "id": "e30d4eea-4e32-4c3f-b214-10665f073980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "010da26a-b632-4c87-9836-508a1f1962af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b96f1f4f-90c7-438b-b3c8-e702cc3f8a25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "010da26a-b632-4c87-9836-508a1f1962af",
                    "LayerId": "e1f0b225-0c4a-4f59-b304-b78ab0e07372"
                }
            ]
        },
        {
            "id": "b0879c12-fb2c-4529-a873-a57d785783d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb2ab5e1-6228-4de6-8807-874807409083",
            "compositeImage": {
                "id": "323a1c57-92bd-470a-ac6a-3f57552a529e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0879c12-fb2c-4529-a873-a57d785783d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b0939c2-142c-4645-8c15-c0f81c19d212",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0879c12-fb2c-4529-a873-a57d785783d5",
                    "LayerId": "e1f0b225-0c4a-4f59-b304-b78ab0e07372"
                }
            ]
        },
        {
            "id": "ac929121-60dc-4dfa-abe1-26a65b6cce4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb2ab5e1-6228-4de6-8807-874807409083",
            "compositeImage": {
                "id": "e07a722a-6645-43ab-b162-c7f695eb064f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac929121-60dc-4dfa-abe1-26a65b6cce4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28a83364-2379-425f-95bd-dc7d31b3824c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac929121-60dc-4dfa-abe1-26a65b6cce4e",
                    "LayerId": "e1f0b225-0c4a-4f59-b304-b78ab0e07372"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "e1f0b225-0c4a-4f59-b304-b78ab0e07372",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb2ab5e1-6228-4de6-8807-874807409083",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}