{
    "id": "37c696c9-9191-4e4e-81ab-e7ac309cb552",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "desert",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "bd3363ec-dcdf-42d0-b04a-95b013b2c138",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": -32,
            "y": 288,
            "speed": 100
        },
        {
            "id": "662a1ec1-fcfb-4bae-bf2f-d9b1e9abe7c1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1007.699,
            "y": 288.267334,
            "speed": 100
        },
        {
            "id": "21a72913-a841-4dc6-a60f-641ae8aee8a9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1008.20557,
            "y": 985.864746,
            "speed": 100
        },
        {
            "id": "279dba49-df58-43a7-9ef6-7a08bca027cb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1619.94287,
            "y": 985.658569,
            "speed": 100
        },
        {
            "id": "5ae1eeda-36ea-4e74-9dd3-848494d77564",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1618.94263,
            "y": 289.354126,
            "speed": 100
        },
        {
            "id": "f7745bc2-4450-45fd-8016-8f13416090a8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2346.46436,
            "y": 289.875946,
            "speed": 100
        },
        {
            "id": "95728a2a-d705-4756-83b1-1b0a24bf8531",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2346.15942,
            "y": 1496.70215,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}