{
    "id": "6ce8db01-5fd6-4915-a783-e6afae5fee65",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "forest",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "56efb526-9590-48b7-8c95-319c65b60a2b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": -32,
            "y": 288,
            "speed": 100
        },
        {
            "id": "a35b7627-c50a-4d00-ab5c-9de655645b33",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 288,
            "speed": 100
        },
        {
            "id": "a30e83b7-58b7-4f61-8228-d0c493dc5a6f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 159,
            "y": 271,
            "speed": 100
        },
        {
            "id": "aa83a0c9-3abd-4725-959d-5b00babb8a26",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 301.773926,
            "y": 243,
            "speed": 100
        },
        {
            "id": "114f16db-b149-456f-8b13-20b2a1c940eb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 437.773926,
            "y": 216,
            "speed": 100
        },
        {
            "id": "4bf30fcd-c6c8-42c4-b622-2be4901cf061",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 606.7739,
            "y": 187,
            "speed": 100
        },
        {
            "id": "45aab0a7-3433-4b1f-8f36-bffe60d9b88d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 766.7739,
            "y": 162,
            "speed": 100
        },
        {
            "id": "f610b5a1-8575-44bb-96a8-89e699442f18",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1070.77393,
            "y": 95,
            "speed": 100
        },
        {
            "id": "0659d6aa-681d-4051-ab78-59af55edf5c0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1097.77393,
            "y": 100,
            "speed": 100
        },
        {
            "id": "2af4ff19-aa44-4b18-a1f6-1a034afd233e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1114.77393,
            "y": 115,
            "speed": 100
        },
        {
            "id": "56fbebf2-4e85-417f-83b7-24518fc705c5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1095.77393,
            "y": 151,
            "speed": 100
        },
        {
            "id": "6d4eeecc-8edc-44c2-83c8-eeec122e5c99",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1037.77393,
            "y": 199,
            "speed": 100
        },
        {
            "id": "276138ac-d0a2-49d3-963b-5902e13743d5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 932.7739,
            "y": 292,
            "speed": 100
        },
        {
            "id": "b15eda47-8618-4118-96b5-b3e7b251076b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 783.7739,
            "y": 410,
            "speed": 100
        },
        {
            "id": "4a164660-852c-4096-99e8-6e452fa03611",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 755.7739,
            "y": 416,
            "speed": 100
        },
        {
            "id": "a7f9759b-1c51-4fef-ba10-8f94d8d1a460",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 715.7739,
            "y": 411,
            "speed": 100
        },
        {
            "id": "bceca55e-30cf-439e-85f4-2b78bb2cf3a7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 499.773926,
            "y": 384,
            "speed": 100
        },
        {
            "id": "3d7dd6a2-8baf-4879-ad6f-d1bdd49e145c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 468.773926,
            "y": 383,
            "speed": 100
        },
        {
            "id": "79da18d7-9941-4b6b-b8d8-1908e7affaf7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 436.773926,
            "y": 394,
            "speed": 100
        },
        {
            "id": "67539ffd-7feb-46b0-bd87-2da13a06e032",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 413.773926,
            "y": 416,
            "speed": 100
        },
        {
            "id": "38425001-498b-43af-83c9-134a1004c1a0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 258.773926,
            "y": 541,
            "speed": 100
        },
        {
            "id": "83216a97-2ef1-43fc-a2da-618da0e7357a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 271.773926,
            "y": 563,
            "speed": 100
        },
        {
            "id": "a5fb6727-0fe6-4dc8-b797-34d7e8cc454c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 288.773926,
            "y": 577,
            "speed": 100
        },
        {
            "id": "d892ac23-5a98-4d25-a0f2-65e602cef878",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 315.773926,
            "y": 586,
            "speed": 100
        },
        {
            "id": "6971980c-b63d-41fd-b44c-53b59939662e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 477.773926,
            "y": 632,
            "speed": 100
        },
        {
            "id": "3a6ac58c-aacf-4396-8bcc-9567332d2efc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 504.773926,
            "y": 645,
            "speed": 100
        },
        {
            "id": "5afe424b-e5c0-4745-8f56-a9245742af20",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 521.7739,
            "y": 663,
            "speed": 100
        },
        {
            "id": "ad03e329-49d6-4bd0-b92d-75a2fda5c862",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 521.7739,
            "y": 684,
            "speed": 100
        },
        {
            "id": "9ae0e430-f2c2-4822-9856-1ab3052129fe",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 521.7739,
            "y": 708,
            "speed": 100
        },
        {
            "id": "7d3d9516-64af-4f74-9252-4919ecee0b27",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 507.773926,
            "y": 726,
            "speed": 100
        },
        {
            "id": "1c7760a6-080d-469f-a013-a58e42119897",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 489.773926,
            "y": 749,
            "speed": 100
        },
        {
            "id": "2037dbec-d6cb-4d9a-873e-7dab91efedb9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 549.7739,
            "y": 751,
            "speed": 100
        },
        {
            "id": "5f1c6bc8-154b-499b-b482-f2f3b41c9351",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 622.7739,
            "y": 744,
            "speed": 100
        },
        {
            "id": "e3f485f6-5fe3-4c0d-815e-3642fc8a46cb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 677.7739,
            "y": 736,
            "speed": 100
        },
        {
            "id": "9d49b4c5-9b78-440e-bd28-6d7fbb9dec71",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 704.7739,
            "y": 726,
            "speed": 100
        },
        {
            "id": "223c7421-4c6f-4c62-8a75-af69d0a40451",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 721.7739,
            "y": 702,
            "speed": 100
        },
        {
            "id": "17af6d46-84c7-4e98-aeae-a86f1ebc6cbd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 723.7739,
            "y": 676,
            "speed": 100
        },
        {
            "id": "cd9598a7-5507-46dd-856a-ef8c55a220a3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 716.7739,
            "y": 654,
            "speed": 100
        },
        {
            "id": "085ab351-aced-44a7-8b8e-69627520d46c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 694.7739,
            "y": 596,
            "speed": 100
        },
        {
            "id": "4055dd31-d40d-435d-aae6-7f0b86759e95",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 711.7739,
            "y": 573,
            "speed": 100
        },
        {
            "id": "74d7ad6a-0a4c-48da-a201-144527fbeafa",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 734.7739,
            "y": 556,
            "speed": 100
        },
        {
            "id": "8151e652-8435-49a0-b7b1-8a592bd6b8b6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 762.7739,
            "y": 550,
            "speed": 100
        },
        {
            "id": "61013802-1584-433e-9ab9-e4d4fdeaf7bd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 896.7739,
            "y": 516,
            "speed": 100
        },
        {
            "id": "83f47543-278a-4e87-bc3f-21a0a0a93bb3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1025.77393,
            "y": 504,
            "speed": 100
        },
        {
            "id": "deb68f8d-e33f-40ca-8887-4804fc23b7af",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1137.77393,
            "y": 509,
            "speed": 100
        },
        {
            "id": "0a806d7f-91ff-4335-be2b-ad4354ee8939",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1347.77393,
            "y": 542,
            "speed": 100
        },
        {
            "id": "40fe3631-a988-44d8-8048-17f5f2a236ff",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1391.77393,
            "y": 562,
            "speed": 100
        },
        {
            "id": "82f01986-a0f6-44db-a6f9-04360eea1885",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1406.77393,
            "y": 581,
            "speed": 100
        },
        {
            "id": "c2a01e67-7831-4d40-8c1e-700c7d36217b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1394.77393,
            "y": 596,
            "speed": 100
        },
        {
            "id": "d7e4f850-ad3b-4940-89f2-2a3c70a44224",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1371.77393,
            "y": 607,
            "speed": 100
        },
        {
            "id": "9b70d4fb-e3fe-4bc9-a693-f39ca051d618",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1320.77393,
            "y": 620,
            "speed": 100
        },
        {
            "id": "fa71a821-d1e1-4285-a1e1-8cbbb6bd4dc4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1007.77393,
            "y": 668,
            "speed": 100
        },
        {
            "id": "6b48be81-dc9f-4019-9a43-754f20ec095b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 975.7739,
            "y": 691,
            "speed": 100
        },
        {
            "id": "ceb7665a-2790-46c9-ae1d-5391852c7639",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 990.7739,
            "y": 712,
            "speed": 100
        },
        {
            "id": "c1423df7-871e-466d-98a8-84021f485397",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1019.77393,
            "y": 729,
            "speed": 100
        },
        {
            "id": "1abd124e-5410-412c-bf5e-de2b4642443d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1059.77393,
            "y": 740,
            "speed": 100
        },
        {
            "id": "0fe82555-0936-42f8-984e-458a1385ae92",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1245.77393,
            "y": 780,
            "speed": 100
        },
        {
            "id": "1ff6f9c5-9896-4ebc-b69c-2bfd03baa8d4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1621.77393,
            "y": 862,
            "speed": 100
        },
        {
            "id": "58b8b6df-156f-4cc2-8751-77faabfbd588",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1653.77393,
            "y": 865,
            "speed": 100
        },
        {
            "id": "35443c7e-29cd-4da4-b7b8-9565183d3b37",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1687.77393,
            "y": 852,
            "speed": 100
        },
        {
            "id": "ddac8dbc-78f1-4f89-ad55-4884dc7d5d07",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1717.77393,
            "y": 825,
            "speed": 100
        },
        {
            "id": "03e74da9-6013-4f6e-aad4-33ddaf3fc5fa",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1832.77393,
            "y": 682,
            "speed": 100
        },
        {
            "id": "b7642114-8fea-4e3e-a976-637e0250085b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1919.77393,
            "y": 541,
            "speed": 100
        },
        {
            "id": "5856efd1-8361-41ad-b8b6-01b39303cac3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2020.77393,
            "y": 342,
            "speed": 100
        },
        {
            "id": "8404878a-c6bb-4dcc-ad88-857251e0eb9d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2042.77393,
            "y": 332,
            "speed": 100
        },
        {
            "id": "ebe3070a-5f7c-46e7-a7ac-513b6b681356",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2069.774,
            "y": 329,
            "speed": 100
        },
        {
            "id": "dcf244d1-cb1d-47df-819b-c48bbbf3e177",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2224.774,
            "y": 333,
            "speed": 100
        },
        {
            "id": "b3bac8f4-5b20-4e13-b420-9563a34e9802",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2289.774,
            "y": 336,
            "speed": 100
        },
        {
            "id": "f66ec662-b31b-4bfc-82bb-67665956fcdc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2584.774,
            "y": 361,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}