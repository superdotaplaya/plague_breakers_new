{
    "id": "6058b07f-930e-4758-9f4d-1b3a2c3a1cf7",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_main",
    "AntiAlias": 1,
    "TTFName": "${project_dir}\\fonts\\fnt_main\\Montserrat-Bold.otf",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Montserrat",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "78225fd7-aade-4fa3-9440-a799896a874f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 49,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7745efc2-c693-4786-a08f-88f655868bc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 49,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 110,
                "y": 155
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4218ad91-a78f-40d9-bc67-0c19c9f49cec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 49,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 93,
                "y": 155
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e475ef9a-bdfe-4695-8b8c-573fbbdc6ba0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 49,
                "offset": 0,
                "shift": 29,
                "w": 28,
                "x": 63,
                "y": 155
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "067edff5-2271-482a-b92a-687ce6929128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 49,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 37,
                "y": 155
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "1b7ee21c-5905-4395-ad0c-54951ecf4a71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 49,
                "offset": 1,
                "shift": 35,
                "w": 33,
                "x": 2,
                "y": 155
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "dd871a41-7aa1-4153-90df-ea5e7b5bde4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 49,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 472,
                "y": 104
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8946c2ff-c37d-4651-bde8-29fafa33fa31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 49,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 463,
                "y": 104
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6674022a-141e-4384-9a4e-07b1f7ced7c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 49,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 449,
                "y": 104
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "917e144e-9cfc-4802-9957-d66d3a450057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 49,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 435,
                "y": 104
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "57b7ddea-0300-4d71-a034-a33378c590e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 49,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 121,
                "y": 155
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e0ae5e20-7e2e-45a4-9b21-05c87618eb22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 49,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 413,
                "y": 104
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b44e42ab-3e82-4bbf-a3b7-ce0b9fa2821e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 49,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 377,
                "y": 104
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "18e3be56-d93e-40fb-a53a-7f9c1a2e06e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 49,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 363,
                "y": 104
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "df43406e-4b31-4e9f-ad4e-1cf51a8894cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 49,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 352,
                "y": 104
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "07589f45-c8cb-458f-9cb8-9123701c91fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 49,
                "offset": -2,
                "shift": 16,
                "w": 20,
                "x": 330,
                "y": 104
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3ee480ea-5060-4ca2-93da-7d336923a03a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 49,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 303,
                "y": 104
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "1b6d8c8c-1dc2-49f2-8a54-b6b43c765481",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 49,
                "offset": 0,
                "shift": 16,
                "w": 13,
                "x": 288,
                "y": 104
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "324a4a4d-dac1-4904-900b-e84c3929ebef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 49,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 263,
                "y": 104
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "6ba33f02-a7be-4102-a00a-d04f0f6ba746",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 49,
                "offset": -1,
                "shift": 24,
                "w": 24,
                "x": 237,
                "y": 104
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6b930d8d-ed73-480d-9d16-034f4f8bf655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 49,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 208,
                "y": 104
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "1839d4df-b27c-474d-9ee7-4f1665afaad1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 49,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 388,
                "y": 104
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "07382b1b-d1b9-4cb2-9b61-b6c708ce63ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 49,
                "offset": 1,
                "shift": 25,
                "w": 24,
                "x": 140,
                "y": 155
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "338e375c-e45c-4e4a-810e-ba5cc2a70544",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 49,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 166,
                "y": 155
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7d964557-6b31-4ad8-9284-27914574153b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 49,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 191,
                "y": 155
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "fa235d7a-1d00-4356-ade7-8dddf40fc269",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 49,
                "offset": 0,
                "shift": 25,
                "w": 24,
                "x": 219,
                "y": 206
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1eb953bf-6355-4c6c-b0bf-9647ed9a21d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 49,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 208,
                "y": 206
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c105344c-875d-4d84-8623-daac940e6342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 49,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 197,
                "y": 206
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a1dac687-fa1c-4d3d-a4f1-67ec92442f8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 49,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 175,
                "y": 206
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2758b363-ffd4-4a83-bf8b-2f23d6d95d94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 49,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 153,
                "y": 206
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "4d0f0ce3-5d8c-48ed-a40c-d10516fc5af6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 49,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 131,
                "y": 206
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2ec1e056-3c22-42b9-8299-f86f6cfeb476",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 49,
                "offset": -1,
                "shift": 24,
                "w": 23,
                "x": 106,
                "y": 206
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "5245a8be-b27b-47c8-ad04-761b5d77c985",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 49,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 65,
                "y": 206
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "05285bae-d66d-4302-b7bf-f2be167c54d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 49,
                "offset": -1,
                "shift": 31,
                "w": 33,
                "x": 30,
                "y": 206
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "4aba76d7-2c07-42b5-99fe-bee31f801c5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 49,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 2,
                "y": 206
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "7f581384-9f49-43d0-b371-8f096bf34b52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 49,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 472,
                "y": 155
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "5c33053c-e320-44d4-b69b-a05c60d19de4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 49,
                "offset": 3,
                "shift": 33,
                "w": 29,
                "x": 441,
                "y": 155
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "ce7c59dc-99da-4faa-b4b4-cb7b0b130dc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 49,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 417,
                "y": 155
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4c3ad9d9-bfe1-4525-9466-895bd946c8b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 49,
                "offset": 3,
                "shift": 26,
                "w": 22,
                "x": 393,
                "y": 155
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3b50a2be-772f-477d-8480-672d6e235ed4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 49,
                "offset": 1,
                "shift": 31,
                "w": 28,
                "x": 363,
                "y": 155
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "2ddc2cd7-6e8c-4661-ba26-dabb86f6c440",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 49,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 335,
                "y": 155
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f81abefa-da1b-4e9c-a0ec-0ef9f87e175c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 49,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 326,
                "y": 155
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "680e9464-efca-4548-8253-6e01f76365c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 49,
                "offset": -1,
                "shift": 22,
                "w": 20,
                "x": 304,
                "y": 155
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "dd4d1ad0-e114-477c-9f37-a2aeac4d5ded",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 49,
                "offset": 3,
                "shift": 30,
                "w": 28,
                "x": 274,
                "y": 155
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "8a06afdf-626a-4495-ab59-b0fd73a6f062",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 49,
                "offset": 3,
                "shift": 24,
                "w": 21,
                "x": 251,
                "y": 155
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5b9f8ee4-e206-4ab7-a44e-bd126af7a6fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 49,
                "offset": 3,
                "shift": 38,
                "w": 32,
                "x": 217,
                "y": 155
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b876b1a7-32e8-450c-ae58-203fe4b7ae1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 49,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 180,
                "y": 104
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b4f2fc45-5c43-46ed-92b1-f24a14fbf051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 49,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 146,
                "y": 104
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "872b97ca-327e-43d3-be15-b84f833498d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 49,
                "offset": 3,
                "shift": 29,
                "w": 25,
                "x": 119,
                "y": 104
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a4dd9f14-64f1-4272-9c0e-6593e442cce2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 49,
                "offset": 1,
                "shift": 34,
                "w": 33,
                "x": 44,
                "y": 53
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b77791f9-1e67-49b0-8673-e835606627f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 49,
                "offset": 3,
                "shift": 29,
                "w": 26,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d5c7a993-75d6-4af4-82e5-27e39e12d2df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 49,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 480,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "0e0d36fa-d4b7-4784-abab-03f36038f9cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 49,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 453,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b376fa6d-064c-4765-948f-d1770b8911da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 49,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 425,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7448032b-8466-48cd-87c9-c61475fea59c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 49,
                "offset": -1,
                "shift": 30,
                "w": 32,
                "x": 391,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6206ac69-a3af-45ea-ac6e-8fa29294d08b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 49,
                "offset": 0,
                "shift": 47,
                "w": 46,
                "x": 343,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "158def76-b908-487d-873e-5b17157490a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 49,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 312,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "a7277373-6654-45ab-b2f1-00c6f81ba5df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 49,
                "offset": -1,
                "shift": 27,
                "w": 29,
                "x": 281,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9168fb3b-389d-4d8a-b1bc-057c0a056cf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 49,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 254,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4e12a9f8-547d-46c5-90ec-443dd5cd6b2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 49,
                "offset": 3,
                "shift": 15,
                "w": 12,
                "x": 30,
                "y": 53
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a910575b-4788-4707-940a-68b7b75c3649",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 49,
                "offset": -2,
                "shift": 16,
                "w": 20,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "211cc023-1bac-4ca3-aabe-e341486217e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 49,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ae65f73a-1df5-459f-8cf7-de3d2fb5a8a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 49,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c76f6d57-6c84-4e9a-a045-7554545debdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 49,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ca9bd937-8dbb-40c2-8a0a-fbe4872bb5ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 49,
                "offset": 3,
                "shift": 24,
                "w": 13,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "768180f5-370f-49e9-a024-5247a20f080a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 49,
                "offset": 1,
                "shift": 25,
                "w": 21,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4b3119e1-5fce-4ace-b17e-2bfe741155d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 49,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8a2363a4-e430-443f-9b4b-dce936e2b6f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "df82ce02-d685-4b54-9549-dd21708405cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 49,
                "offset": 1,
                "shift": 28,
                "w": 24,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "71903e23-3c3a-4b7f-a4bb-9c30e623162b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 49,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "74c405d1-d0d3-4873-9256-5d154b6b7a6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 49,
                "offset": 0,
                "shift": 15,
                "w": 17,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f8c3de0e-5805-4e60-83c0-2d0b1e16829c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 49,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 79,
                "y": 53
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9349f377-7b7f-415d-8b6e-199c89329e97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 49,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 326,
                "y": 53
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "bb8f9245-e866-48e2-af38-e11589cb5933",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 49,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 106,
                "y": 53
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "33ba32c5-d209-48db-a11c-506a08dde708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 49,
                "offset": -4,
                "shift": 12,
                "w": 15,
                "x": 79,
                "y": 104
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "6541c0a2-514f-46c5-8e39-c65b5028e9dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 49,
                "offset": 2,
                "shift": 26,
                "w": 25,
                "x": 52,
                "y": 104
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "0034568d-8aa4-4d40-bde4-829f6ce7e176",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 49,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 42,
                "y": 104
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "bde14e46-2b18-46bd-be48-b00a07422f38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 49,
                "offset": 2,
                "shift": 42,
                "w": 38,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "fa1876f2-ee77-44a2-b928-fbdf419e8c0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 49,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 466,
                "y": 53
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5a526060-b596-45b9-bd47-66a7efabe58f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 49,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 440,
                "y": 53
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b8c0ee41-94ac-40be-bb8e-96d067b6e82a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 49,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 413,
                "y": 53
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "1229b9d1-c62a-4a93-802b-5a1f7f6b102c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 49,
                "offset": 1,
                "shift": 28,
                "w": 24,
                "x": 387,
                "y": 53
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c85008f0-1ed8-4cba-a21f-563c52f2dfdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 49,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 370,
                "y": 53
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "6eadee32-8bb5-448c-b2cb-6e8c72614d36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 49,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 96,
                "y": 104
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d6bfed13-e933-4231-ab2d-469759468526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 49,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 351,
                "y": 53
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "2dadbb87-5c38-46eb-a176-277f5c5629b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 49,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 301,
                "y": 53
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a973f40f-f7ea-4e1a-b505-45538f8eeebf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 49,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 273,
                "y": 53
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3059e938-4bad-4d56-97cc-f2fba65ad91e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 49,
                "offset": 0,
                "shift": 37,
                "w": 38,
                "x": 233,
                "y": 53
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d3e043fd-1333-4dab-a8c2-656b7bfa23fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 49,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 207,
                "y": 53
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8260be9c-d3d6-46fb-8457-877e3dbeae07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 49,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 179,
                "y": 53
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9a6ee2e2-402a-47b7-9798-7bfe2f7a65e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 49,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 157,
                "y": 53
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "bda4f82b-6b62-40b8-9f35-09f4b191f3ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 49,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 141,
                "y": 53
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f2c74d43-22df-470b-8c0d-8d082b4e0fbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 49,
                "offset": 3,
                "shift": 12,
                "w": 7,
                "x": 132,
                "y": 53
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "4adfde27-45fa-4ca4-8434-39fcfb0c3feb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 49,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 116,
                "y": 53
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "349f18e9-5fd1-45a3-9b80-bb47f16a90d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 245,
                "y": 206
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "eeb34b66-9b07-48ee-ae95-3850c28afbf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 49,
                "offset": 8,
                "shift": 39,
                "w": 23,
                "x": 268,
                "y": 206
            }
        }
    ],
    "image": null,
    "includeTTF": true,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 30,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}