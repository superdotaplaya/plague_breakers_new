/// @description twit_retweet(tweet_id)
/// @param tweet_id
/*
** Script by TheXtraTechnologies
** 
** This script allows you to retweet something
** The tweet id is the number on the end of the tweet link
*/
var tweet_id = argument0;
url_open_ext("http://twitter.com/intent/retweet?tweet_id="+string(tweet_id),"_blank");
