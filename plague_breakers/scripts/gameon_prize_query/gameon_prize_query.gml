/// @description Queries prize details
/// @param {script} prizeId
/// @param {script} [callbackSuccess]
/// @param {script} [callbackFailure]

if(!gameon_util_check_auth())
	return undefined;

var prizeId =			argument[0];
var callbackSuccess =	argument_count > 1 ? argument[1] : undefined;
var callbackFailure =	argument_count > 2 ? argument[2] : undefined;

var promise = gameon_util_object_create(oGameOnPromiseData);
promise.request_result_data_type = oGameOnPrize;
promise.request_result_list_key = "prizes";

var requestURL = gameon_util_get_api_url(GAMEON_API_URL_PRIZES_GET, prizeId);
gameon_util_api_request(requestURL, "", oGameOnClient.http_headers_default, "GET", 
						callbackSuccess, callbackFailure, promise);
return promise;