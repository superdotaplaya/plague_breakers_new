/// @description Initializes the GameOn client. Authenticates the player if requested.
/// @param {string}		 apiKey
/// @param {eGameOnAuth} authType
/// @param {string}		 [gameKey]
/// @param {string}		 [apiUrl]
/// @param {script}		 [callbackSuccess]
/// @param {script}		 [callbackFailure]

var apiKey = argument[0];
var authType = argument[1];
var gameKey = (argument_count > 2 ? argument[2] : undefined);
var apiUrl = (argument_count > 3 ? (is_undefined(argument[3]) ? GAMEON_API_URL_ROOT : argument[3]) : GAMEON_API_URL_ROOT);
var callbackSuccess = (argument_count > 4 ? argument[4] : undefined);
var callbackFail = (argument_count > 5 ? argument[5] : undefined);

// Check if our client has already been initialised
if(gameon_is_initialized())
{
	gameon_util_log("Client already initialized. Call gameon_destroy before initializing a new one.", true);
	return undefined;
}

// Welcome message
gameon_util_log("Initializing GameOn Client. This library is compatible with GameOn API v1 release 20190103.");

// Intiailise our GameOn client singleton
var authPromise = undefined;
var clientInstance = gameon_util_object_create(oGameOnClient);
with(clientInstance)
{
	api_url = apiUrl;
	
	game_api_key = apiKey;
	game_public_key = gameKey;
	security_type = (is_undefined(gameKey) == false ? eGameOnSecurity.ADVANCED : eGameOnSecurity.STANDARD);
	state = eGameOnState.INITIALIZED;
	
	// Set up default HTTP headers
	http_headers_default = ds_map_create();
	ds_map_add(http_headers_default, "Content-Type", "application/json");
	ds_map_add(http_headers_default, "X-Api-Key", game_api_key);
	
	gameon_util_log("GameOn Client initialized successfully!");
	
	// Authenticate player if required
	switch(authType)
	{
		case eGameOnAuth.REGISTER_NEW:
			authPromise = gameon_player_register(true, callbackSuccess, callbackFail);
			break;
		
		case eGameOnAuth.LOAD_EXISTING:
			if(gameon_util_session_data_load())
				authPromise = gameon_player_auth(oGameOnClient.player_token, callbackSuccess, callbackFail);
			else
				gameon_util_log("Could not load previous player session data. Please authenticate manually or register a new player.");
			break;
		
		case eGameOnAuth.NONE:
			gameon_util_log("Please authenticate via gameon_player_auth or gameon_player_register before making any API calls.");
			break;
	}
}

return authPromise;