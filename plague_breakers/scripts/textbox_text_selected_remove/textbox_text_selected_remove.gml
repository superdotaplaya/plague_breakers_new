/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 6C9B9D0D
/// @DnDArgument : "code" "/// @description textbox_text_selected_remove(textbox)$(13_10)/// @param textbox$(13_10)if(cursor < selectedStart)$(13_10)    text = string_delete(text, cursor + 1, selectedStart - cursor);$(13_10)else$(13_10){$(13_10)    text = string_delete(text, selectedStart + 1, cursor - selectedStart);$(13_10)    cursor = selectedStart;$(13_10)}$(13_10)if(cursor > string_length(text))$(13_10)    cursor = string_length(text);$(13_10)selectedStart = -1;$(13_10)"
/// @description textbox_text_selected_remove(textbox)
/// @param textbox
if(cursor < selectedStart)
    text = string_delete(text, cursor + 1, selectedStart - cursor);
else
{
    text = string_delete(text, selectedStart + 1, cursor - selectedStart);
    cursor = selectedStart;
}
if(cursor > string_length(text))
    cursor = string_length(text);
selectedStart = -1;