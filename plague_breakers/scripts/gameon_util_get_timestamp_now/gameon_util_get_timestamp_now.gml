/// @description Get current timestamp in the GameOn API format
var prevTimezone = date_get_timezone();
date_set_timezone(timezone_utc);
	
var epochStart = date_create_datetime(1970, 1, 1, 0, 0, 0);
var timestamp = int64(round(date_second_span(epochStart, date_current_datetime()) * 1000));

date_set_timezone(prevTimezone);
return timestamp;