/// @description Creates a GameOn object of the given type (promise, data, etc.)
/// @param objectType
var objectType = argument0;

// TODO: This will be replaced with some form of lightweight object/instance type in the future
var instance = instance_create_depth(0, 0, 0, objectType);
instance.visible = false;
instance.solid = false;
instance.persistent = true;
return instance;