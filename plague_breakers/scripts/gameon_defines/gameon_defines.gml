// Amazon GameOn GML Library v0.3
// Compatible with GameOn API v1 release 20190103
// (c) 2019 YoYo Games Ltd

// LOCAL SAVES
#macro GAMEON_LOCAL_SAVE							"\\_amazon_gameon_session.dat"

#macro GAMEON_LOCAL_SAVE_TOKEN						"p_t"
#macro GAMEON_LOCAL_SAVE_PLAYER_ID					"p_id"
#macro GAMEON_LOCAL_SAVE_PLAYER_EXT_ID				"p_eid"
#macro GAMEON_LOCAL_SAVE_PLAYER_NAME				"p_n"
#macro GAMEON_LOCAL_SAVE_SESSION_ID					"s_id"
#macro GAMEON_LOCAL_SAVE_SESSION_API_KEY			"s_api"
#macro GAMEON_LOCAL_SAVE_SESSION_EXP				"s_exp"
#macro GAMEON_LOCAL_SAVE_CLIENT_KEY_PUB				"ck_pub"
#macro GAMEON_LOCAL_SAVE_CLIENT_KEY_PRIV			"ck_priv"

// API ENDPOINT URLS
#macro GAMEON_API_URL_ROOT							"https://api.amazongameon.com/v1"

#macro GAMEON_API_URL_PLAYER_REGISTER				"/players/register"
#macro GAMEON_API_URL_PLAYER_AUTH					"/players/auth"
#macro GAMEON_API_URL_PLAYER_UPDATE					"/players"

#macro GAMEON_API_URL_TOURNAMENT_GET				"/tournaments"
#macro GAMEON_API_URL_TOURNAMENT_GET_DETAILS		"/tournaments/{1}"
#macro GAMEON_API_URL_TOURNAMENT_ENTER				"/tournaments/{1}/enter"

#macro GAMEON_API_URL_TOURNAMENT_PLAYER_ADD			"/player-tournaments"
#macro GAMEON_API_URL_TOURNAMENT_PLAYER_GET			"/player-tournaments"
#macro GAMEON_API_URL_TOURNAMENT_PLAYER_DETAILS		"/player-tournaments/{1}"
#macro GAMEON_API_URL_TOURNAMENT_PLAYER_UPDATE		"/player-tournaments/{1}"
#macro GAMEON_API_URL_TOURNAMENT_PLAYER_ENTER		"/player-tournaments/{1}/enter"

#macro GAMEON_API_URL_MATCH_GET						"/matches"
#macro GAMEON_API_URL_MATCH_GET_DETAILS				"/matches/{1}"
#macro GAMEON_API_URL_MATCH_ENTER					"/matches/{1}/enter"
#macro GAMEON_API_URL_MATCH_SUBMIT_SCORE			"/matches/{1}/score"
#macro GAMEON_API_URL_MATCH_ADD_ATTEMPTS			"/matches/{1}/attempts"
#macro GAMEON_API_URL_MATCH_GET_LEADERBOARD			"/matches/{1}/leaderboard"

#macro GAMEON_API_URL_PRIZES_GET					"/prizes/{1}"
#macro GAMEON_API_URL_PRIZES_CLAIM					"/prizes/claim"
#macro GAMEON_API_URL_PRIZES_FULFILL				"/prizes/fulfill"

#macro GAMEON_API_URL_TEAM_GET_DETAILS				"/teams/{1}"