/// @description Checks if a valid GameOn client exists
return instance_exists(oGameOnClient);