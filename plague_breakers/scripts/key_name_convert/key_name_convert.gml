/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 106D8846
/// @DnDArgument : "code" "if( argument0 > 48 && argument0 < 91 )$(13_10){ return chr(argument0); }$(13_10)switch( argument0 )$(13_10){$(13_10)    case 8: return "Backspace"; break;$(13_10)    case 9: return "Tab"; break;$(13_10)    case 13: return "Enter"; break;$(13_10)    case 16: return "Shift"; break;$(13_10)    case 17: return "Ctrl"; break;$(13_10)    case 18: return "Alt"; break;$(13_10)    case 19: return "Pause/Break"; break;$(13_10)    case 20: return "CAPS"; break;$(13_10)    case 27: return "Esc"; break;$(13_10)    case 33: return "!"; break;$(13_10)    case 34: return "''"; break;$(13_10)    case 35: return "3"; break;$(13_10)    case 36: return "4"; break;$(13_10)    case 37: return "5"; break;$(13_10)    case 38: return "7"; break;$(13_10)    case 39: return "]"; break;$(13_10)    case 40: return "("; break;$(13_10)    case 45: return "-"; break;$(13_10)    case 46: return "Period"; break;$(13_10)    case 96: return "`"; break;$(13_10)    case 97: return "A"; break;$(13_10)    case 98: return "B"; break;$(13_10)    case 99: return "C"; break;$(13_10)    case 100: return "D"; break;$(13_10)    case 101: return "E"; break;$(13_10)    case 102: return "F"; break;$(13_10)    case 103: return "G"; break;$(13_10)    case 104: return "H"; break;$(13_10)    case 105: return "I"; break;$(13_10)    case 106: return "J"; break;$(13_10)    case 107: return "K"; break;$(13_10)	case 108: return "L"; break;$(13_10)    case 109: return "M"; break;$(13_10)    case 110: return "N"; break;$(13_10)    case 111: return "O"; break;$(13_10)    case 112: return "P"; break;$(13_10)    case 113: return "Q"; break;$(13_10)    case 114: return "R"; break;$(13_10)    case 115: return "S"; break;$(13_10)    case 116: return "T"; break;$(13_10)    case 117: return "U"; break;$(13_10)    case 118: return "V"; break;$(13_10)    case 119: return "W"; break;$(13_10)    case 120: return "X"; break;$(13_10)    case 121: return "Y"; break;$(13_10)    case 122: return "Z"; break;$(13_10)    case 123: return "["; break;$(13_10)	case 124: return "\\"; break;$(13_10)$(13_10)}"
if( argument0 > 48 && argument0 < 91 )
{ return chr(argument0); }
switch( argument0 )
{
    case 8: return "Backspace"; break;
    case 9: return "Tab"; break;
    case 13: return "Enter"; break;
    case 16: return "Shift"; break;
    case 17: return "Ctrl"; break;
    case 18: return "Alt"; break;
    case 19: return "Pause/Break"; break;
    case 20: return "CAPS"; break;
    case 27: return "Esc"; break;
    case 33: return "!"; break;
    case 34: return "''"; break;
    case 35: return "3"; break;
    case 36: return "4"; break;
    case 37: return "5"; break;
    case 38: return "7"; break;
    case 39: return "]"; break;
    case 40: return "("; break;
    case 45: return "-"; break;
    case 46: return "Period"; break;
    case 96: return "`"; break;
    case 97: return "A"; break;
    case 98: return "B"; break;
    case 99: return "C"; break;
    case 100: return "D"; break;
    case 101: return "E"; break;
    case 102: return "F"; break;
    case 103: return "G"; break;
    case 104: return "H"; break;
    case 105: return "I"; break;
    case 106: return "J"; break;
    case 107: return "K"; break;
	case 108: return "L"; break;
    case 109: return "M"; break;
    case 110: return "N"; break;
    case 111: return "O"; break;
    case 112: return "P"; break;
    case 113: return "Q"; break;
    case 114: return "R"; break;
    case 115: return "S"; break;
    case 116: return "T"; break;
    case 117: return "U"; break;
    case 118: return "V"; break;
    case 119: return "W"; break;
    case 120: return "X"; break;
    case 121: return "Y"; break;
    case 122: return "Z"; break;
    case 123: return "["; break;
	case 124: return "\\"; break;

}