/// @description Enters a tournament
/// @param {string} tournamentId
/// @param {string} [accessKey]
/// @param {ds_map} [playerAttributes]
/// @param {string} [teamId]
/// @param {script} [callbackSuccess]
/// @param {script} [callbackFailure]

if(!gameon_util_check_auth())
	return undefined;

var tournamentId =		argument[0];
var accessKey =			argument_count > 1 ? argument[1] : undefined;
var playerAttributes =	argument_count > 2 ? argument[2] : undefined;
var teamId =			argument_count > 3 ? argument[3] : undefined;
var callbackSuccess =	argument_count > 4 ? argument[4] : undefined;
var callbackFailure =	argument_count > 5 ? argument[5] : undefined;

var requestURL = gameon_util_get_api_url(GAMEON_API_URL_TOURNAMENT_ENTER, tournamentId);

// Set up optional request body
var requestBody = "";
if(!is_undefined(playerAttributes) || !is_undefined(accessKey))
{
	var requestBodyMap = ds_map_create();
	if(!is_undefined(playerAttributes) && ds_exists(playerAttributes, ds_type_map))
	{
		var playerAttributesCopy = ds_map_create();
		ds_map_copy(playerAttributesCopy, playerAttributes);
		ds_map_add_map(requestBodyMap, "playerAttributes", playerAttributesCopy);
	}
	
	if(!is_undefined(accessKey))
		ds_map_add(requestBodyMap, "accessKey", accessKey);
		
	if(!is_undefined(teamId))
		ds_map_add(requestBodyMap, "teamId", teamId);
		
	requestBody = json_encode(requestBodyMap);
	ds_map_destroy(requestBodyMap);
}

// Perform request
var promise = gameon_util_object_create(oGameOnPromiseData);
promise.request_result_data_type = oGameOnMatchEntry;

gameon_util_api_request(requestURL, requestBody, oGameOnClient.http_headers_default, "POST", 
						callbackSuccess, callbackFailure, promise);
return promise;