/// @description Log a debug message
/// @param {string} message
/// @param {bool}	isError
/// @param {string}	category
/// @param {bool}	stopExecution

var message = argument[0];
var errStr = (argument_count > 1 && !is_undefined(argument[1]) && argument[1] == true) ? "ERROR: " : "";

var category = "GameOn"; 
if(argument_count > 2 && !is_undefined(argument[2])) category = argument[2];

var stopExecution = false;
if(argument_count > 3 && !is_undefined(argument[3])) stopExecution = argument[3];

var logString = "["+string(category)+"]\t" + string(errStr) + string(message);
if(stopExecution)
	show_error(logString, true);
else
	show_debug_message(logString);
	log(logString);