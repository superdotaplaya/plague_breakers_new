/// @description Creates a player tournament
/// @param {oGameOnTournament} tournamentData
/// @param {script}			   [callbackSuccess]
/// @param {script}			   [callbackFailure]

if(!gameon_util_check_auth())
	return undefined;

var tournamentData =  argument[0];
var callbackSuccess = argument_count > 1 ? argument[1] : undefined;
var callbackFailure = argument_count > 2 ? argument[2] : undefined;

var requestURL = gameon_util_get_api_url(GAMEON_API_URL_TOURNAMENT_PLAYER_ADD);

// TEMP FIX: Manually add our int64 data to the encoded string (ugly)
// This is due to json_encode not being able to handle int64 values correctly,
// which will be addressed in a future runner update and the mess below can be replaced 
// with a nice simple json_encode call.

with(tournamentData) { event_user(eGameOnDataEvent.TO_JSON); }
var requestBodyJSON = ds_map_create();
ds_map_copy(requestBodyJSON, tournamentData.json_raw_data);

// Remove int64 entries
ds_map_delete(requestBodyJSON, "dateStart");
ds_map_delete(requestBodyJSON, "dateEnd");

var requestBody = json_encode(requestBodyJSON);
ds_map_destroy(requestBodyJSON);

// HACK: Manually append the int64 entries to our request body string (our JSON encoder fails to encode them automatically)
var requestBody = string_copy(requestBody, 1, string_length(requestBody) - 1);
requestBody += ", \"dateStart\":"+string(int64(tournamentData.dateStart));
requestBody += ", \"dateEnd\":"+string(int64(tournamentData.dateEnd));
requestBody += "}";

// Set up promise
var promise = gameon_util_object_create(oGameOnPromiseData);
promise.request_result_data_type = oGameOnTournamentUpdate;

// Execute query
gameon_util_api_request(requestURL, requestBody, oGameOnClient.http_headers_default, "POST", 
						callbackSuccess, callbackFailure, promise);
return promise;