global.log_count = 0;
global.show_debug = true;
var os = "";
switch (os_type) {
	case os_windows:
		os = "Windows OS";
	break;
	case os_uwp:
		os = "Windows 10 Universal Windows Platform";
	break;
	case os_linux:
		os = "Linux OS";
	break;
	case os_macosx:
		os = "Mac OS X";
	break;
	case os_ios:
		os = "iOS";
	break;
	case os_android:
		os = "Android OS";
	break;
	case os_ps4:
		os = "PS4";
	break;
	case os_xboxone:
		os = "X-Box One";
	break;
	case os_unknown:
		os = "Unknown OS";
	break;
}
var date_string = string(current_day)+"-"+string(current_month)+"-"+string(current_year);
global.current_log_path = "Data/log "+date_string+"--"+string(get_timer())+".debug";

ini_open(global.current_log_path);
ini_write_string("Base","Working dir",working_directory);
ini_write_string("Base","Executable",program_directory);
ini_write_string("Base","Build Date",date_time_string(GM_build_date));
ini_write_string("Base","Game Version",GM_version);
ini_write_string("Base","IDE Version",GM_runtime_version);
ini_write_string("Base","Current Time",date_time_string(date_current_datetime()));
ini_write_string("Base","Current Date",date_string);
ini_close();

file = file_text_open_append(global.current_log_path)
file_text_write_string(file, "[Errors]\n")
file_text_close(file)

