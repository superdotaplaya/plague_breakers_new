/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 37F5703F
/// @DnDArgument : "code" "randomize()$(13_10)quest1 = irandom_range(0,4)$(13_10)quest2 = irandom_range(0,4)$(13_10){$(13_10)ini_open("quests.ini")$(13_10)$(13_10)if quest1 = 0 then$(13_10){$(13_10)ini_write_string("Quest 1","quest","Survive 30 rounds on any map without losing a life")$(13_10)} else$(13_10)if quest1 = 1 then$(13_10){$(13_10)ini_write_string("Quest 1","quest","Kill a thief that is carrying more than 10 gold")$(13_10)} else$(13_10)if quest1 = 2 then$(13_10){$(13_10)ini_write_string("Quest 1","quest","Kill 50 enemies")$(13_10)} else$(13_10)if quest1 = 3 then$(13_10){$(13_10)ini_write_string("Quest 1","quest","Repair a tower for 50 hp")$(13_10)} else$(13_10)if quest1 = 4 then$(13_10){$(13_10)ini_write_string("Quest 1","quest","Kill 20 enemies with the poison tower")$(13_10)}$(13_10)if quest2 = 0 then$(13_10){$(13_10)ini_write_string("Quest 2","quest","Survive until round 30 on any map without losing a life")$(13_10)} else$(13_10)if quest2 = 1 then$(13_10){$(13_10)ini_write_string("Quest 2","quest","Kill a thief that is carrying more than 20 gold")$(13_10)} else$(13_10)if quest2 = 2 then$(13_10){$(13_10)ini_write_string("Quest 2","quest","Kill 80 enemies")$(13_10)} else$(13_10)if quest2 = 3 then$(13_10){$(13_10)ini_write_string("Quest 2","quest","Repair a tower for 70 hp")$(13_10)} else$(13_10)if quest2 = 4 then$(13_10){$(13_10)ini_write_string("Quest 2","quest","Upgrade a tower to level 8 in either its range or damage")$(13_10)}$(13_10)$(13_10)text = ini_read_string("Quest1","quest","none")$(13_10)log("[DAILY QUESTS]   Daily quests have been reset! New quess 1 is: " + string(text))$(13_10)ini_close()$(13_10)}$(13_10)"
randomize()
quest1 = irandom_range(0,4)
quest2 = irandom_range(0,4)
{
ini_open("quests.ini")

if quest1 = 0 then
{
ini_write_string("Quest 1","quest","Survive 30 rounds on any map without losing a life")
} else
if quest1 = 1 then
{
ini_write_string("Quest 1","quest","Kill a thief that is carrying more than 10 gold")
} else
if quest1 = 2 then
{
ini_write_string("Quest 1","quest","Kill 50 enemies")
} else
if quest1 = 3 then
{
ini_write_string("Quest 1","quest","Repair a tower for 50 hp")
} else
if quest1 = 4 then
{
ini_write_string("Quest 1","quest","Kill 20 enemies with the poison tower")
}
if quest2 = 0 then
{
ini_write_string("Quest 2","quest","Survive until round 30 on any map without losing a life")
} else
if quest2 = 1 then
{
ini_write_string("Quest 2","quest","Kill a thief that is carrying more than 20 gold")
} else
if quest2 = 2 then
{
ini_write_string("Quest 2","quest","Kill 80 enemies")
} else
if quest2 = 3 then
{
ini_write_string("Quest 2","quest","Repair a tower for 70 hp")
} else
if quest2 = 4 then
{
ini_write_string("Quest 2","quest","Upgrade a tower to level 8 in either its range or damage")
}

text = ini_read_string("Quest1","quest","none")
log("[DAILY QUESTS]   Daily quests have been reset! New quess 1 is: " + string(text))
ini_close()
}