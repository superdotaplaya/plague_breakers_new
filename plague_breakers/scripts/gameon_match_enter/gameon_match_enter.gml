/// @description Enters an existing match
/// @param {string} matchId
/// @param {ds_map} [playerAttributes]
/// @param {script} [callbackSuccess]
/// @param {script} [callbackFailure]

if(!gameon_util_check_auth())
	return undefined;

var matchId = argument[0];
var playerAttributes =	argument_count > 1 ? argument[1] : undefined;
var callbackSuccess =	argument_count > 2 ? argument[2] : undefined;
var callbackFailure =	argument_count > 3 ? argument[3] : undefined;

var requestURL = gameon_util_get_api_url(GAMEON_API_URL_MATCH_ENTER, matchId);

// Set up optional request body
var requestBody = "";
if(!is_undefined(playerAttributes))
{
	var requestBodyMap = ds_map_create();
	if(!is_undefined(playerAttributes) && ds_exists(playerAttributes, ds_type_map))
	{
		var playerAttributesCopy = ds_map_create();
		ds_map_copy(playerAttributesCopy, playerAttributes);
		ds_map_add_map(requestBodyMap, "playerAttributes", playerAttributesCopy);
	}
		
	requestBody = json_encode(requestBodyMap);
	ds_map_destroy(requestBodyMap);
}

// Set up promise
var promise = gameon_util_object_create(oGameOnPromiseData);
promise.request_result_data_type = oGameOnMatchEntry;

// Perform request
gameon_util_api_request(requestURL, requestBody, oGameOnClient.http_headers_default, "POST", 
						callbackSuccess, callbackFailure, promise);
return promise;