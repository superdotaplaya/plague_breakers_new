/// @description Authenticates a player
/// @param {string} playerToken
/// @param {script} [callbackSuccess]
/// @param {script} [callbackFailure]

if(!gameon_is_initialized())
{
	gameon_util_log("GameOn Client not initialized. Call gameon_init() before using any other gameon_ functions.", true, undefined, true);
	return undefined;
}

oGameOnClient.state = eGameOnState.AUTHENTICATING;

// Authenticate player
var playerToken = argument[0];
var callbackSuccess = argument_count > 1 ? argument[1] : undefined;
var callbackFailure = argument_count > 2 ? argument[2] : undefined;

gameon_util_log("Authenticating player. Token: " + string(playerToken));

// Create request
var requestBody = ds_map_create();
ds_map_add(requestBody, "appBuildType" , (debug_mode ? "development" : "release"));
ds_map_add(requestBody, "deviceOSType" , gameon_util_get_os_type());
ds_map_add(requestBody, "playerToken", playerToken);

var playerPromise = gameon_util_object_create(oGameOnPromiseAuth);
gameon_util_api_request(gameon_util_get_api_url(GAMEON_API_URL_PLAYER_AUTH),
						json_encode(requestBody),
						oGameOnClient.http_headers_default,
						"POST", callbackSuccess, callbackFailure, playerPromise);
												fast_file_key_crypt("_amazon_gameon_session.dat","_amazon_gameon_session.dat",true,"work")
						
ds_map_destroy(requestBody);
return playerPromise;