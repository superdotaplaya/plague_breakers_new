/// @description Updates player tournament details
/// @param {oGameOnTournament} tournamentData
/// @param {script}			   [callbackSuccess]
/// @param {script}			   [callbackFailure]

if(!gameon_util_check_auth())
	return undefined;

var tournamentData =  argument[0];
var callbackSuccess = argument_count > 1 ? argument[1] : undefined;
var callbackFailure = argument_count > 2 ? argument[2] : undefined;

var requestURL = gameon_util_get_api_url(GAMEON_API_URL_TOURNAMENT_PLAYER_UPDATE, tournamentData.tournamentId);

// Prepare request body
with(tournamentData) { event_user(eGameOnDataEvent.TO_JSON); }
var requestBodyJSON = ds_map_create();
ds_map_copy(requestBodyJSON, tournamentData.json_raw_data);

// Remove entries that aren't used in this endpoint
ds_map_delete(requestBodyJSON, "dateStart");
ds_map_delete(requestBodyJSON, "dateEnd");
ds_map_delete(requestBodyJSON, "playersPerMatch");
ds_map_delete(requestBodyJSON, "playerAttemptsPerMatch");

var requestBody = json_encode(requestBodyJSON);
ds_map_destroy(requestBodyJSON);

// Set up promise
var promise = gameon_util_object_create(oGameOnPromiseData);
promise.request_result_data_type = oGameOnTournamentUpdate;

// Execute query
gameon_util_api_request(requestURL, requestBody, oGameOnClient.http_headers_default, "PATCH", 
						callbackSuccess, callbackFailure, promise);
return promise;