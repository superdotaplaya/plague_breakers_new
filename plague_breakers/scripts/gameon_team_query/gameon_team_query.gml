/// @description Queries details for a team under teamId
/// @param {string} [teamId]
/// @param {script} [callbackSuccess]
/// @param {script} [callbackFailure]

if(!gameon_util_check_auth())
	return undefined;

var teamId =			argument[0];
var callbackSuccess =	argument_count > 1 ? argument[1] : undefined;
var callbackFailure =	argument_count > 2 ? argument[2] : undefined;

var requestURL = gameon_util_get_api_url(GAMEON_API_URL_TEAM_GET_DETAILS, teamId);

// Set up promise
var promise = gameon_util_object_create(oGameOnPromiseData);
promise.request_result_data_type = oGameOnTeam;

// Execute query
gameon_util_api_request(requestURL, "", oGameOnClient.http_headers_default, "GET", 
						callbackSuccess, callbackFailure, promise);
						
return promise;