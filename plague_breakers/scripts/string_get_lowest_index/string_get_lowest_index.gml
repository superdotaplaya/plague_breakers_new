/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 5ADDD4B2
/// @DnDArgument : "code" "/// @description string_get_lowest_index(string, find, lowest)$(13_10)/// @param string$(13_10)/// @param  find$(13_10)/// @param  lowest$(13_10)$(13_10)var m = string_length(argument0);$(13_10)for(var i = argument2; i <= m; i ++)$(13_10){$(13_10)    if(string_char_at(argument0, i) == argument1)$(13_10)    {$(13_10)        if(i == m)$(13_10)            return i;$(13_10)        else$(13_10)        {$(13_10)            if(string_char_at(argument0, i + 1) != argument1)$(13_10)                return i;$(13_10)        }$(13_10)    }$(13_10)}$(13_10)return m;$(13_10)"
/// @description string_get_lowest_index(string, find, lowest)
/// @param string
/// @param  find
/// @param  lowest

var m = string_length(argument0);
for(var i = argument2; i <= m; i ++)
{
    if(string_char_at(argument0, i) == argument1)
    {
        if(i == m)
            return i;
        else
        {
            if(string_char_at(argument0, i + 1) != argument1)
                return i;
        }
    }
}
return m;