/// @description Posts the current player's score to an existing match
/// @param {string} matchId
/// @param {int}	matchScore
/// @param {script} [callbackSuccess]
/// @param {script} [callbackFailure]

if(!gameon_util_check_auth())
	return undefined;

var matchId = argument[0];
var matchScore = argument[1];
var callbackSuccess =	argument_count > 2 ? argument[2] : undefined;
var callbackFailure =	argument_count > 3 ? argument[3] : undefined;

var requestURL = gameon_util_get_api_url(GAMEON_API_URL_MATCH_SUBMIT_SCORE, matchId);
var requestBody = "{\"score\": " + string(round(matchScore)) + "}";

var promise = gameon_util_object_create(oGameOnPromiseData);
promise.request_result_data_type = oGameOnMatchScoreEntry;

// Perform request
gameon_util_api_request(requestURL, requestBody, oGameOnClient.http_headers_default, "PUT", 
						callbackSuccess, callbackFailure, promise);
return promise;