/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 01C39248
/// @DnDArgument : "code" "/// @description twit_post(message)$(13_10)/// @param message$(13_10)/*$(13_10)** Script by TheXtraTechnologies$(13_10)**  $(13_10)**  Using this script you can easily add the possibilty to tweet things like: Hi check out this game$(13_10)*/$(13_10)$(13_10)var tweet = argument0;$(13_10)$(13_10)tweet = string_replace_all(tweet,"Check out this awesome new game!","%20");$(13_10)$(13_10)url_open_ext("https://twitter.com/intent/tweet?text="+tweet,"_blank");$(13_10)"
/// @description twit_post(message)
/// @param message
/*
** Script by TheXtraTechnologies
**  
**  Using this script you can easily add the possibilty to tweet things like: Hi check out this game
*/

var tweet = argument0;

tweet = string_replace_all(tweet,"Check out this awesome new game!","%20");

url_open_ext("https://twitter.com/intent/tweet?text="+tweet,"_blank");
/**/