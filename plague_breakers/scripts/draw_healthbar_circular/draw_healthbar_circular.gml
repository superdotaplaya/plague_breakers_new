/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 510A873D
/// @DnDArgument : "code" "/// @description draw_healthbar_circular(center x, center y, radius, start angle, percent health, sprite)$(13_10)/// @param center x$(13_10)/// @param  center y$(13_10)/// @param  radius$(13_10)/// @param  start angle$(13_10)/// @param  percent health$(13_10)/// @param  sprite$(13_10)var cx,cy,rad,sang,hp,tex,steps,thick,oc;$(13_10)cx=argument0$(13_10)cy=argument1$(13_10)rad=argument2$(13_10)sang=argument3$(13_10)hp=argument4$(13_10)tex=sprite_get_texture(argument5,0)$(13_10)steps=200$(13_10)thick=sprite_get_height(argument5)$(13_10)$(13_10)if ceil(steps*(hp/100)) >= 1 {$(13_10)    $(13_10)    oc=draw_get_color()$(13_10)draw_set_color(c_white)$(13_10)$(13_10)    var step,ang,side,hps,hpd;$(13_10)    step=0$(13_10)    ang=sang$(13_10)    side=0$(13_10)    draw_primitive_begin_texture(pr_trianglestrip,tex)$(13_10)    draw_vertex_texture(cx+lengthdir_x(rad-thick/2+thick*side,ang),cy+lengthdir_y(rad-thick/2+thick*side,ang),side,side)$(13_10)    side=!side$(13_10)    draw_vertex_texture(cx+lengthdir_x(rad-thick/2+thick*side,ang),cy+lengthdir_y(rad-thick/2+thick*side,ang),side,side)$(13_10)    side=!side$(13_10)    draw_vertex_texture(cx+lengthdir_x(rad-thick/2+thick*side,ang+360/steps),cy+lengthdir_y(rad-thick/2+thick*side,ang+360/steps),side,side)$(13_10)    side=!side$(13_10)    hps=hp/(ceil(steps*(hp/100))+1)$(13_10)    hpd=0$(13_10)    repeat ceil(steps*(hp/100)+1) {$(13_10)        step+=1$(13_10)        $(13_10)        if step=ceil(steps*(hp/100)+1) { //final step$(13_10)            ang+=(360/steps)*(hp - hpd)/2$(13_10)            if ang>sang+360 ang=sang+360$(13_10)            draw_vertex_texture(cx+lengthdir_x(rad-thick/2+thick*side,ang),cy+lengthdir_y(rad-thick/2+thick*side,ang),side,side)$(13_10)            side=!side$(13_10)            draw_vertex_texture(cx+lengthdir_x(rad-thick/2+thick*side,ang),cy+lengthdir_y(rad-thick/2+thick*side,ang),side,side)$(13_10)        }$(13_10)        else {$(13_10)            ang+=360/steps$(13_10)            draw_vertex_texture(cx+lengthdir_x(rad-thick/2+thick*side,ang),cy+lengthdir_y(rad-thick/2+thick*side,ang),side,side)$(13_10)            side=!side$(13_10)        }$(13_10)        hpd+=hps$(13_10)    }$(13_10)    draw_primitive_end()$(13_10)    $(13_10)    draw_set_color(oc)$(13_10)}"
/// @description draw_healthbar_circular(center x, center y, radius, start angle, percent health, sprite)
/// @param center x
/// @param  center y
/// @param  radius
/// @param  start angle
/// @param  percent health
/// @param  sprite
var cx,cy,rad,sang,hp,tex,steps,thick,oc;
cx=argument0
cy=argument1
rad=argument2
sang=argument3
hp=argument4
tex=sprite_get_texture(argument5,0)
steps=200
thick=sprite_get_height(argument5)

if ceil(steps*(hp/100)) >= 1 {
    
    oc=draw_get_color()
draw_set_color(c_white)

    var step,ang,side,hps,hpd;
    step=0
    ang=sang
    side=0
    draw_primitive_begin_texture(pr_trianglestrip,tex)
    draw_vertex_texture(cx+lengthdir_x(rad-thick/2+thick*side,ang),cy+lengthdir_y(rad-thick/2+thick*side,ang),side,side)
    side=!side
    draw_vertex_texture(cx+lengthdir_x(rad-thick/2+thick*side,ang),cy+lengthdir_y(rad-thick/2+thick*side,ang),side,side)
    side=!side
    draw_vertex_texture(cx+lengthdir_x(rad-thick/2+thick*side,ang+360/steps),cy+lengthdir_y(rad-thick/2+thick*side,ang+360/steps),side,side)
    side=!side
    hps=hp/(ceil(steps*(hp/100))+1)
    hpd=0
    repeat ceil(steps*(hp/100)+1) {
        step+=1
        
        if step=ceil(steps*(hp/100)+1) { //final step
            ang+=(360/steps)*(hp - hpd)/2
            if ang>sang+360 ang=sang+360
            draw_vertex_texture(cx+lengthdir_x(rad-thick/2+thick*side,ang),cy+lengthdir_y(rad-thick/2+thick*side,ang),side,side)
            side=!side
            draw_vertex_texture(cx+lengthdir_x(rad-thick/2+thick*side,ang),cy+lengthdir_y(rad-thick/2+thick*side,ang),side,side)
        }
        else {
            ang+=360/steps
            draw_vertex_texture(cx+lengthdir_x(rad-thick/2+thick*side,ang),cy+lengthdir_y(rad-thick/2+thick*side,ang),side,side)
            side=!side
        }
        hpd+=hps
    }
    draw_primitive_end()
    
    draw_set_color(oc)
}