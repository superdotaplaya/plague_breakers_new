/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 0E99FDD0
/// @DnDArgument : "code" "/// @description @function rousr_dissonance_set_details(_details)$(13_10)/// @param _details$(13_10)///@desc set the details string$(13_10)///@param {String} _details   what the player is currently doing$(13_10)var _details = argument0;$(13_10)$(13_10)with (global.__rousr_dissonance) {$(13_10)  Is_dirty = true;$(13_10)  discord_set_details(_details);$(13_10)}$(13_10)"
/// @description l0E99FDD0_0 rousr_dissonance_set_details(_details)
/// @param _details
///@desc set the details string
///@param {String} _details   what the player is currently doing
var _details = argument0;

with (global.__rousr_dissonance) {
  Is_dirty = true;
  discord_set_details(_details);
}