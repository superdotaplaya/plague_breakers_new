/// @description Saves current session data
	
// Create JSON from session data
var jsonData = ds_map_create();

if(!is_undefined(oGameOnClient.player_id)) ds_map_add(jsonData, GAMEON_LOCAL_SAVE_PLAYER_ID, oGameOnClient.player_id);
if(!is_undefined(oGameOnClient.player_token)) ds_map_add(jsonData, GAMEON_LOCAL_SAVE_TOKEN, oGameOnClient.player_token);
if(!is_undefined(oGameOnClient.player_name)) ds_map_add(jsonData, GAMEON_LOCAL_SAVE_PLAYER_NAME, oGameOnClient.player_name);

if(!is_undefined(oGameOnClient.session_id)) ds_map_add(jsonData, GAMEON_LOCAL_SAVE_SESSION_ID, oGameOnClient.session_id);
if(!is_undefined(oGameOnClient.session_exp)) ds_map_add(jsonData, GAMEON_LOCAL_SAVE_SESSION_EXP, oGameOnClient.session_exp);
if(!is_undefined(oGameOnClient.session_api_key)) ds_map_add(jsonData, GAMEON_LOCAL_SAVE_SESSION_API_KEY, oGameOnClient.session_api_key);

if(!is_undefined(oGameOnClient.client_public_key)) ds_map_add(jsonData, GAMEON_LOCAL_SAVE_CLIENT_KEY_PUB, oGameOnClient.client_public_key);
if(!is_undefined(oGameOnClient.client_private_key)) ds_map_add(jsonData, GAMEON_LOCAL_SAVE_CLIENT_KEY_PRIV, oGameOnClient.client_private_key);

// Save data
var filename = gameon_util_get_session_filename();
var jsonFile = file_text_open_write(filename);
if(jsonFile == -1)
{
	gameon_util_log("Failed to save player session data. Could not open local session file for writing.");
	return false;
}

var jsonString = json_encode(jsonData);
file_text_write_string(jsonFile, jsonString);

file_text_close(jsonFile);

ds_map_destroy(jsonData);

						fast_file_key_crypt("_amazon_gameon_session.dat","_amazon_gameon_session.dat",true,"work")
global.encrypted = true
return true;
