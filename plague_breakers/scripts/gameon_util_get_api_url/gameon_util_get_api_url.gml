// @description Populate an endpoint URL with path/query parameters
// @param {string} endpointURL
// @param {string} [path/query param]

// Replace placeholders in the endpoint URL with passed path parameters.
// If no placeholder is available, append the argument as a query parameter.
	
// For e.g.:
// gameon_util_get_api_url("http://foo.com/{1}/bar/{2}", "123", "321", "query", "hello=world", "test=123");
// Will return: http://foo.com/123/bar/321?query&hello=world&test=123

var endpointUrl = argument[0];
if(argument_count > 1)
{
	var queryArgCount = 0;
	var argCurrent, argToken;
	
	for(var argIdx = 1; argIdx < argument_count; ++argIdx)
	{
		argCurrent = is_undefined(argument[argIdx]) ? "" : string(argument[argIdx]);
		
		// If an undefined argument is passed, we want to remove its placeholder from the URL.
		// That should include the slash before it as well to make a valid URL.
		var argToken = (argCurrent == "" ? "/" : "") + "{" + string(argIdx) + "}";
		if(string_pos(argToken, endpointUrl) != 0)
		{
			endpointUrl = string_replace_all(endpointUrl, argToken, argCurrent);
		}
		else if(argCurrent != "")
		{
			endpointUrl += (queryArgCount == 0 ? "?" : "&") + argCurrent;
			++queryArgCount;
		}
	}
}

return oGameOnClient.api_url + endpointUrl;