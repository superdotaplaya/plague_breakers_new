/// @description Loads current session data

// Check if file exists
var localDataPath = gameon_util_get_session_filename();
if(!file_exists(localDataPath))
	return false;
	
// Attempt to load file
var localDataFile = file_text_open_read(localDataPath);
if(localDataFile == -1)
{
	gameon_util_log("Failed to load local session data.", true);
	return false;
}

var localDataContents = "";

// Parse game library json file
while (!file_text_eof(localDataFile))
{
	localDataContents += file_text_read_string(localDataFile);
	file_text_readln(localDataFile);
}

file_text_close(localDataFile);

// Convert to JSON
var localData = json_decode(localDataContents);

// If we have no valid session data, exit out
if(!ds_map_exists(localData, GAMEON_LOCAL_SAVE_TOKEN) || !ds_map_exists(localData, GAMEON_LOCAL_SAVE_SESSION_ID))
{
	ds_map_destroy(localData);
	return false;
}

// Read data
oGameOnClient.player_id = localData[? GAMEON_LOCAL_SAVE_PLAYER_ID];
oGameOnClient.player_token = localData[? GAMEON_LOCAL_SAVE_TOKEN];
oGameOnClient.player_name = localData[? GAMEON_LOCAL_SAVE_PLAYER_NAME];	
	
oGameOnClient.session_id = localData[? GAMEON_LOCAL_SAVE_SESSION_ID];
oGameOnClient.session_exp = localData[? GAMEON_LOCAL_SAVE_SESSION_EXP];
oGameOnClient.session_api_key = localData[? GAMEON_LOCAL_SAVE_SESSION_API_KEY];
	
oGameOnClient.client_public_key = localData[? GAMEON_LOCAL_SAVE_CLIENT_KEY_PUB];
oGameOnClient.client_private_key = localData[? GAMEON_LOCAL_SAVE_CLIENT_KEY_PRIV];
	
ds_map_destroy(localData);
return true;

