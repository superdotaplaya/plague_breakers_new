/// @description Queries a single player tournament (if tournamentId is given) or all player tournaments
/// @param {string} [tournamentId]
/// @param {string} [filterBy]
/// @param {int}	[limit]
/// @param {string} [period]
/// @param {script} [callbackSuccess]
/// @param {script} [callbackFailure]

if(!gameon_util_check_auth())
	return undefined;

var tournamentId =		argument_count > 0 ? argument[0] : undefined;
var filterBy =			argument_count > 1 ? gameon_util_get_query_param(argument[1], "filterBy") : undefined;
var limit =				argument_count > 2 ? gameon_util_get_query_param(argument[2], "limit") : undefined;
var period =			argument_count > 3 ? gameon_util_get_query_param(argument[3], "period") : undefined;
var callbackSuccess =	argument_count > 4 ? argument[4] : undefined;
var callbackFailure =	argument_count > 5 ? argument[5] : undefined;

var requestURL;
if(!is_undefined(tournamentId))
{
	// Query single player tournament
	requestURL = gameon_util_get_api_url(GAMEON_API_URL_TOURNAMENT_PLAYER_DETAILS, tournamentId);
}
else
{
	// Query all tournaments
	requestURL = gameon_util_get_api_url(GAMEON_API_URL_TOURNAMENT_PLAYER_GET, filterBy, limit, period);
}

// Set up promise
var promise = gameon_util_object_create(oGameOnPromiseData);
promise.request_result_data_type = oGameOnTournament;
promise.request_result_list_key = "tournaments";

// Execute query
gameon_util_api_request(requestURL, "", oGameOnClient.http_headers_default, "GET", 
						callbackSuccess, callbackFailure, promise);
						
return promise;