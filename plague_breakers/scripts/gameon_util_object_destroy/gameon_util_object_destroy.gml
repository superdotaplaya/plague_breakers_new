/// @description Destroys the given GameOn object (promise, data, etc.)
/// @param object
var object = argument0;
if(!gameon_util_object_exists(object))
{
	gameon_util_log("Invalid object passed to gameon_util_object_destroy().", true);
	return;
}

with(object) { instance_destroy(); }