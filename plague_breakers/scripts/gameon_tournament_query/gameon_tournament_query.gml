/// @description Queries a single tournament (if tournamentId is given) or all tournaments
/// @param {string} [tournamentId]
/// @param {ds_map} [playerAttributes]
/// @param {string} [filterBy]
/// @param {int}	[limit]
/// @param {string} [period]
/// @param {string} [participantType]
/// @param {script} [callbackSuccess]
/// @param {script} [callbackFailure]

if(!gameon_util_check_auth())
	return undefined;

var tournamentId =		argument_count > 0 ? argument[0] : undefined;
var playerAttributes =	argument_count > 1 ? gameon_util_get_query_param(argument[1], "playerAttributes", true) : undefined;
var filterBy =			argument_count > 2 ? gameon_util_get_query_param(argument[2], "filterBy") : undefined;
var limit =				argument_count > 3 ? gameon_util_get_query_param(argument[3], "limit") : undefined;
var period =			argument_count > 4 ? gameon_util_get_query_param(argument[4], "period") : undefined;
var participantType =	argument_count > 5 ? gameon_util_get_query_param(argument[5], "participantType") : undefined;
var callbackSuccess =	argument_count > 6 ? argument[6] : undefined;
var callbackFailure =	argument_count > 7 ? argument[7] : undefined;

var requestURL;
if(!is_undefined(tournamentId))
{
	// Query single tournament
	requestURL = gameon_util_get_api_url(GAMEON_API_URL_TOURNAMENT_GET_DETAILS, tournamentId, playerAttributes);
}
else
{
	// Query all tournaments
	requestURL = gameon_util_get_api_url(GAMEON_API_URL_TOURNAMENT_GET, playerAttributes, filterBy, limit, period, participantType);
}

// Set up promise
var promise = gameon_util_object_create(oGameOnPromiseData);
promise.request_result_data_type = oGameOnTournament;
promise.request_result_list_key = "tournaments";

// Execute query
gameon_util_api_request(requestURL, "", oGameOnClient.http_headers_default, "GET", 
						callbackSuccess, callbackFailure, promise);
						
return promise;