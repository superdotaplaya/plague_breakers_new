/// @description Destroys the GameOn client
/// @param {bool} removeSession
var removeSession = argument0;

// Check if we've initialized a valid GameOn client
if(gameon_util_object_exists(oGameOnClient) == false)
{
	gameon_util_log("Attempting to destroy GameOn client without having initialized one. gameon_init must be called first.", true, undefined, true);
	return false;
}

// Remove local session data
if(removeSession && gameon_util_session_data_exists())
	file_delete(gameon_util_get_session_filename());

// Destroy the client
with(oGameOnClient)
{
	instance_destroy();
}

return true;