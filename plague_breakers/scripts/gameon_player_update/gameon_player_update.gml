/// @description Updates current player's details
/// @param {string} playerName
/// @param {script} [callbackSuccess]
/// @param {script} [callbackFailure]

if(!gameon_util_check_auth())
	return undefined;

var playerName =		argument[0];
var callbackSuccess =	argument_count > 1 ? argument[1] : undefined;
var callbackFailure =	argument_count > 2 ? argument[2] : undefined;

var requestURL = gameon_util_get_api_url(GAMEON_API_URL_PLAYER_UPDATE);
var promise = gameon_util_object_create(oGameOnPromisePlayerName);

// Set up optional request body
var requestBody = "";
var requestBodyMap = ds_map_create();
ds_map_add(requestBodyMap, "playerName", playerName);
		
requestBody = json_encode(requestBodyMap);
ds_map_destroy(requestBodyMap);

// Store the name we requested in our promise
promise.request_new_player_name = playerName;

// Perform request
gameon_util_api_request(requestURL, requestBody, oGameOnClient.http_headers_default, "PATCH", 
						callbackSuccess, callbackFailure, promise);
										fast_file_key_crypt("_amazon_gameon_session.dat","_amazon_gameon_session.dat",true,"work")
return promise;