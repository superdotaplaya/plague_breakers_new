/// @description twit_favorite(tweet_id)
/// @param tweet_id
/*
** Script by TheXtraTechnologies
**
** this script allows you to favorite a tweet using a tweet id
** The tweet id is the number on the end of the tweet link
*/
var tweet_id = argument0;
url_open_ext("http://twitter.com/intent/favorite?tweet_id="+string(tweet_id),"_blank");
