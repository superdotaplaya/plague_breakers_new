/// @description Add attempts remaining to an existing match
/// @param {string} matchId
/// @param {int}	attemptsToAdd
/// @param {script} [callbackSuccess]
/// @param {script} [callbackFailure]

if(!gameon_util_check_auth())
	return undefined;

var matchId =			argument[0];
var attemptsToAdd =		argument[1];
var callbackSuccess =	argument_count > 2 ? argument[2] : undefined;
var callbackFailure =	argument_count > 3 ? argument[3] : undefined;

var promise = gameon_util_object_create(oGameOnPromiseNumber);
promise.request_result_data_key = "attemptsRemaining";

var requestURL = gameon_util_get_api_url(GAMEON_API_URL_MATCH_ADD_ATTEMPTS, matchId);
var requestBody = "{\"addAttempts\": " + string(round(attemptsToAdd)) + "}";

// Perform request
gameon_util_api_request(requestURL, requestBody, oGameOnClient.http_headers_default, "POST", 
						callbackSuccess, callbackFailure, promise);
return promise;