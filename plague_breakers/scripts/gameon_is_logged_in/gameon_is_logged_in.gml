/// @description Checks if the GameOn client has been authenticated
if(!gameon_is_initialized())
	return false;

// Check if we're authenticated
if(oGameOnClient.state != eGameOnState.AUTHENTICATED)
	return false;

return true;