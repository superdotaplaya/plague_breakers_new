///@description log();
///@param desc

var desc = argument0;

file = file_text_open_append(global.current_log_path)
file_text_write_string(file,string(global.log_count) +": " + string(desc));
file_text_writeln(file)
file_text_close(file)

++global.log_count;
