/// @description Checks if the given GameOn object instance exists
/// @param objectType
var object = argument0;
return (!is_undefined(object) && instance_exists(object));