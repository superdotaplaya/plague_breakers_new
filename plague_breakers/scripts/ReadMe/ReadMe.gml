/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 551C37FD
/// @DnDArgument : "code" "/*$(13_10)$(13_10)# Dissonance$(13_10)$(13_10)**Dissonance is no way created by, endorsed by, or affiliated with Discord.**$(13_10)Dissonance is built on [discord-rpc](https://github.com/discordapp/discord-rpc)$(13_10)$(13_10)## Welcome to Dissonance$(13_10)$(13_10)Initial Steps:$(13_10)$(13_10)* Setup your [app with discord](https://discordapp.com/developers/applications/me)$(13_10)* Familiarized yourself with the Rich Presence FAQ / How-To: https://discordapp.com/developers/docs/rich-presence/how-to$(13_10)$(13_10)Once you've done this, you're ready to use Dissonance!$(13_10)$(13_10)### Creating Dissonance$(13_10)$(13_10)Before you can call **Any** `rousr_dissonance_` functions: $(13_10)$(13_10)* Call `rousr_dissonance_create(<your-app-id>)` to create the Dissonance object and initialize the Discord RPC.$(13_10)$(13_10)Next, setup your handlers.. none of them are actually required, but if you're going to have join / spectate functionality, or $(13_10)want to catch when the RPC is ready / log errors, they are available:$(13_10)$(13_10)* rousr_dissonance_handler_on_ready(_script_index, [_user_data])$(13_10)* rousr_dissonance_handler_on_disconnected(_script_index, [_user_data])$(13_10)* rousr_dissonance_handler_on_error(_script_index, [_user_data])$(13_10)* rousr_dissonance_handler_on_join(_script_index, [_user_data])$(13_10)* rousr_dissonance_handler_on_spectate(_script_index, [_user_data])$(13_10)* rousr_dissonance_handler_on_join_request(_script_index, [_user_data])$(13_10)$(13_10)**Note:** When receiving a join request, prompt the player with options to accept or reject the invite, sending the reply with the following:  $(13_10)* rousr_dissonance_respond_to_join(_user_id, _reply) (_reply is a `EDissonanceJoinReply` value)$(13_10)$(13_10)### Set Rich Presence Fields$(13_10)$(13_10)The following functions set the various [fields](https://discordapp.com/developers/docs/rich-presence/how-to#updating-presence-update-presence-payload-fields):$(13_10)$(13_10)* rousr_dissonance_set_details(_details)$(13_10)* rousr_dissonance_set_large_image(_image_key, [_image_text])$(13_10)* rousr_dissonance_set_small_image(_image_key, [_image_text])$(13_10)* rousr_dissonance_set_timestamps(_secondsFromNowStart, _secondsFromNowEnd)$(13_10)* rousr_dissonance_set_state(_state)$(13_10)* rousr_dissonance_set_party(_party_size, _party_max, [_party_id])$(13_10)* rousr_dissonance_set_join_secret(_secret)$(13_10)* rousr_dissonance_set_spectate_secret(_secret)$(13_10)* rousr_dissonance_set_match_secrete(_match_secret, _is_instance)$(13_10)$(13_10)#### Note on Example Assets$(13_10)$(13_10)If you want to follow the Example exactly, I've included two 1024x1024 png files in the Included Files section of the resource tree. $(13_10)Upload these both to the "Rich Presence Assets" section of your dashboard:$(13_10)$(13_10)* **dissonance_small.png**$(13_10)  * Name: `dissonance` $(13_10)  * Type: `Small`$(13_10)* **dissonance_large.png**$(13_10)  * Name: `dissonance_large` $(13_10)  * Type: `Large`$(13_10)$(13_10)*/$(13_10)"
/*

# Dissonance

**Dissonance is no way created by, endorsed by, or affiliated with Discord.**
Dissonance is built on [discord-rpc](https://github.com/discordapp/discord-rpc)

## Welcome to Dissonance

Initial Steps:

* Setup your [app with discord](https://discordapp.com/developers/applications/me)
* Familiarized yourself with the Rich Presence FAQ / How-To: https://discordapp.com/developers/docs/rich-presence/how-to

Once you've done this, you're ready to use Dissonance!

### Creating Dissonance

Before you can call **Any** `rousr_dissonance_` functions: 

* Call `rousr_dissonance_create(<your-app-id>)` to create the Dissonance object and initialize the Discord RPC.

Next, setup your handlers.. none of them are actually required, but if you're going to have join / spectate functionality, or 
want to catch when the RPC is ready / log errors, they are available:

* rousr_dissonance_handler_on_ready(_script_index, [_user_data])
* rousr_dissonance_handler_on_disconnected(_script_index, [_user_data])
* rousr_dissonance_handler_on_error(_script_index, [_user_data])
* rousr_dissonance_handler_on_join(_script_index, [_user_data])
* rousr_dissonance_handler_on_spectate(_script_index, [_user_data])
* rousr_dissonance_handler_on_join_request(_script_index, [_user_data])

**Note:** When receiving a join request, prompt the player with options to accept or reject the invite, sending the reply with the following:  
* rousr_dissonance_respond_to_join(_user_id, _reply) (_reply is a `EDissonanceJoinReply` value)

### Set Rich Presence Fields

The following functions set the various [fields](https://discordapp.com/developers/docs/rich-presence/how-to#updating-presence-update-presence-payload-fields):

* rousr_dissonance_set_details(_details)
* rousr_dissonance_set_large_image(_image_key, [_image_text])
* rousr_dissonance_set_small_image(_image_key, [_image_text])
* rousr_dissonance_set_timestamps(_secondsFromNowStart, _secondsFromNowEnd)
* rousr_dissonance_set_state(_state)
* rousr_dissonance_set_party(_party_size, _party_max, [_party_id])
* rousr_dissonance_set_join_secret(_secret)
* rousr_dissonance_set_spectate_secret(_secret)
* rousr_dissonance_set_match_secrete(_match_secret, _is_instance)

#### Note on Example Assets

If you want to follow the Example exactly, I've included two 1024x1024 png files in the Included Files section of the resource tree. 
Upload these both to the "Rich Presence Assets" section of your dashboard:

* **dissonance_small.png**
  * Name: `dissonance` 
  * Type: `Small`
* **dissonance_large.png**
  * Name: `dissonance_large` 
  * Type: `Large`

*/
/**/