/// @description Registers and authenticates a player (if authAutomatically is set to true)
/// @param {bool}	authAutomatically
/// @param {script} [callbackSuccess]
/// @param {script} [callbackFailure]

if(!gameon_is_initialized())
{
	gameon_util_log("Client not initialized. Call gameon_init() before using any other gameon_ functions.", true, undefined, true);
	return undefined;
}

oGameOnClient.state = eGameOnState.REGISTRATION;

// Register the player
var authAutomatically = argument[0];
var callbackSuccess = argument_count > 1 ? argument[1] : undefined;
var callbackFailure = argument_count > 2 ? argument[2] : undefined;

gameon_util_log("Registering new player..");

var playerPromise = gameon_util_object_create(oGameOnPromiseRegister);
playerPromise.auth_on_success = authAutomatically;
	
gameon_util_api_request(gameon_util_get_api_url(GAMEON_API_URL_PLAYER_REGISTER),
						"", oGameOnClient.http_headers_default, "POST", 
						callbackSuccess, callbackFailure, playerPromise);


return playerPromise;