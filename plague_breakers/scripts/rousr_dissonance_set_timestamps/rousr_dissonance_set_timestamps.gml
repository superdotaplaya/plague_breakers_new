/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 1707E6A2
/// @DnDArgument : "code" "/// @description @function rousr_dissonance_set_timestamps(_secondsFromNowStart, _secondsFromNowEnd)$(13_10)/// @param _secondsFromNowStart$(13_10)/// @param  _secondsFromNowEnd$(13_10)///@desc set the timestamp variables$(13_10)///@param {Real|undefined} _secondsFromNowStart   the amount of seconds since the game began, can be undefined$(13_10)///@param {Real|undefined} _secondsFromNowEnd     the amount of seconds until the game ends, can be undefined$(13_10)var _secondsFromNowStart = argument[0];$(13_10)var _secondsFromNowEnd   = undefined;$(13_10)if (argument_count > 1)$(13_10)    _secondsFromNowEnd = argument[1];$(13_10)    $(13_10)with (global.__rousr_dissonance) {$(13_10)  var startTimeLo = 0;$(13_10)  var startTimeHi = 0;$(13_10)  var endTimeLo = 0;$(13_10)  var endTimeHi = 0;$(13_10)$(13_10)  if (_secondsFromNowStart != undefined) {$(13_10)    var startTimeStamp = gmlscripts_unix_timestamp();$(13_10)    startTimeStamp += _secondsFromNowStart;$(13_10)  $(13_10)    startTimeLo = startTimeStamp & $FFFFFFFF;$(13_10)    startTimeHi = startTimeStamp >> 32;$(13_10)  }$(13_10)$(13_10)  if (_secondsFromNowEnd != undefined) {$(13_10)    var endTimeStamp = gmlscripts_unix_timestamp();$(13_10)    endTimeStamp += _secondsFromNowEnd;$(13_10)  $(13_10)    endTimeLo = endTimeStamp & $FFFFFFFF;$(13_10)    endTimeHi = endTimeStamp >> 32;  $(13_10)  $(13_10)    var test = endTimeLo + (endTimeHi << 32);$(13_10)  }$(13_10)  $(13_10)  Is_dirty = true; $(13_10)  discord_set_timestamps(startTimeLo, startTimeHi, endTimeLo, endTimeHi);$(13_10)}$(13_10)"
/// @description l1707E6A2_0 rousr_dissonance_set_timestamps(_secondsFromNowStart, _secondsFromNowEnd)
/// @param _secondsFromNowStart
/// @param  _secondsFromNowEnd
///@desc set the timestamp variables
///@param {Real|undefined} _secondsFromNowStart   the amount of seconds since the game began, can be undefined
///@param {Real|undefined} _secondsFromNowEnd     the amount of seconds until the game ends, can be undefined
var _secondsFromNowStart = argument[0];
var _secondsFromNowEnd   = undefined;
if (argument_count > 1)
    _secondsFromNowEnd = argument[1];
    
with (global.__rousr_dissonance) {
  var startTimeLo = 0;
  var startTimeHi = 0;
  var endTimeLo = 0;
  var endTimeHi = 0;

  if (_secondsFromNowStart != undefined) {
    var startTimeStamp = gmlscripts_unix_timestamp();
    startTimeStamp += _secondsFromNowStart;
  
    startTimeLo = startTimeStamp & $FFFFFFFF;
    startTimeHi = startTimeStamp >> 32;
  }

  if (_secondsFromNowEnd != undefined) {
    var endTimeStamp = gmlscripts_unix_timestamp();
    endTimeStamp += _secondsFromNowEnd;
  
    endTimeLo = endTimeStamp & $FFFFFFFF;
    endTimeHi = endTimeStamp >> 32;  
  
    var test = endTimeLo + (endTimeHi << 32);
  }
  
  Is_dirty = true; 
  discord_set_timestamps(startTimeLo, startTimeHi, endTimeLo, endTimeHi);
}