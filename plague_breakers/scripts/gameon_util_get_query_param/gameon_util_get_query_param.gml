var paramValue = argument[0];
var paramName = argument[1];
var paramIsMap = argument_count > 2 ? argument[2] : false;

if(is_undefined(paramValue))
	return undefined;
	
// If our query parameter is a map, encode it
var paramAsString;
if(paramIsMap && is_real(paramValue) && ds_exists(paramValue, ds_type_map))
	paramAsString = json_encode(paramValue);
else
	paramAsString = string(paramValue);
	
// Construct the query argument
return string(paramName) + "=" + gameon_util_url_encode(paramAsString);
