/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 40D5E847
/// @DnDArgument : "code" "/// @description @function rousr_dissonance_set_small_image(_image_key, [_image_text])$(13_10)/// @param _image_key$(13_10)/// @param  [_image_text]$(13_10)///@desc set the small image and the tooltip text for it$(13_10)///@param {String} _image_key    name of the uploaded image for the small profile artwork$(13_10)///@param {String} [_image_text]   tooltip for the `_image_key`$(13_10)var _image_key = argument[0],$(13_10)    _image_text = undefined;$(13_10)$(13_10)if (argument_count > 1)$(13_10)  _image_text = argument[1];$(13_10)    $(13_10)with (global.__rousr_dissonance) {$(13_10)  Is_dirty = true;$(13_10)  $(13_10)  if (_image_key == undefined) _image_key = "";$(13_10)  if (_image_text == undefined) _image_text = ""; $(13_10)  $(13_10)  discord_set_small_image(_image_key, _image_text);$(13_10)}$(13_10)"
/// @description l40D5E847_0 rousr_dissonance_set_small_image(_image_key, [_image_text])
/// @param _image_key
/// @param  [_image_text]
///@desc set the small image and the tooltip text for it
///@param {String} _image_key    name of the uploaded image for the small profile artwork
///@param {String} [_image_text]   tooltip for the `_image_key`
var _image_key = argument[0],
    _image_text = undefined;

if (argument_count > 1)
  _image_text = argument[1];
    
with (global.__rousr_dissonance) {
  Is_dirty = true;
  
  if (_image_key == undefined) _image_key = "";
  if (_image_text == undefined) _image_text = ""; 
  
  discord_set_small_image(_image_key, _image_text);
}