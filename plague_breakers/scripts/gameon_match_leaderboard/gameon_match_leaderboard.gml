/// @description Queries the leaderboard of an existing match
/// @param matchId
/// @param {int}	[currentPlayerNeighbors]
/// @param {int}	[limit]
/// @param {int}	[nextUrl]
/// @param {script} [callbackSuccess]
/// @param {script} [callbackFailure]

if(!gameon_util_check_auth())
	return undefined;

var matchId =				 argument[0];
var currentPlayerNeighbors = argument_count > 1 ? gameon_util_get_query_param(argument[1], "currentPlayerNeighbors") : undefined;
var limit =					 argument_count > 2 ? gameon_util_get_query_param(argument[2], "limit") : undefined;
var nextUrl =				 argument_count > 3 ? argument[3] : undefined;
var callbackSuccess =		 argument_count > 4 ? argument[4] : undefined;
var callbackFailure =		 argument_count > 5 ? argument[5] : undefined;

// If nextURL is supplied - use that as our request URL immediately
// nextURL can be returned in oGameOnLeaderboard data in the case of very large leaderboards that require pagination
var requestURL;
if(is_undefined(nextUrl))
{
	requestURL = gameon_util_get_api_url(GAMEON_API_URL_MATCH_GET_LEADERBOARD, matchId, 
										 currentPlayerNeighbors, limit);
}
else
{
	requestURL = nextUrl	
}

var requestBody = "";

// Create promise
var promise = gameon_util_object_create(oGameOnPromiseData);
promise.request_result_data_type = oGameOnLeaderboard;

// Perform request
gameon_util_api_request(requestURL, requestBody, oGameOnClient.http_headers_default, "GET", 
						callbackSuccess, callbackFailure, promise);
return promise;