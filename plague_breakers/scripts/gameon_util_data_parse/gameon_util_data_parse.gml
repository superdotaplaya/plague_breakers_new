/// @description Parse JSON data
/// @param {oGameOnData} dataObject
/// @param {ds_map}		 jsonData

var dataObject = argument0;
var jsonData = argument1;

with(dataObject)
{
	// Clean up old raw data
	if(!is_undefined(json_raw_data) && ds_exists(json_raw_data, ds_type_map))
		ds_map_destroy(json_raw_data);
		
	// Create a copy of our raw data
	json_raw_data = ds_map_create();
	ds_map_copy(json_raw_data, jsonData);
	
	// Parse
	event_user(eGameOnDataEvent.FROM_JSON);
}

return dataObject;