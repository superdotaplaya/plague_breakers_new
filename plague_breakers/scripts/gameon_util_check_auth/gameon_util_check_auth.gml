if(!gameon_is_logged_in())
{
	gameon_util_log("Client not authenticated. Call gameon_init() before using any other gameon_ functions.", true, undefined, true);
	return false;
}

return true;