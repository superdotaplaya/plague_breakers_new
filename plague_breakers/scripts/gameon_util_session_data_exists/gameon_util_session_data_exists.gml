/// @description Returns true if a cached user session exists	
var localDataPath = gameon_util_get_session_filename();
return file_exists(localDataPath);