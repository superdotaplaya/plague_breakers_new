/// @description Queries a single match (if matchId is provided) or all matches
/// @param {string} [matchId]
/// @param {ds_map} [playerAttributes]
/// @param {string} [filterBy]
/// @param {int}	[limit]
/// @param {string} [period]
/// @param {string} [matchType]
/// @param {script} [callbackSuccess]
/// @param {script} [callbackFailure]

if(!gameon_util_check_auth())
	return undefined;

var matchId =			argument_count > 0 ? argument[0] : undefined;
var playerAttributes =	argument_count > 1 ? gameon_util_get_query_param(argument[1], "playerAttributes", true) : undefined;
var filterBy =			argument_count > 2 ? gameon_util_get_query_param(argument[2], "filterBy") : undefined;
var limit =				argument_count > 3 ? gameon_util_get_query_param(argument[3], "limit") : undefined;
var period =			argument_count > 4 ? gameon_util_get_query_param(argument[4], "period") : undefined;
var matchType =			argument_count > 4 ? gameon_util_get_query_param(argument[5], "matchType") : undefined;
var callbackSuccess =	argument_count > 6 ? argument[6] : undefined;
var callbackFailure =	argument_count > 7 ? argument[7] : undefined;


var promise = gameon_util_object_create(oGameOnPromiseData);
var requestURL;

if(!is_undefined(matchId))
{
	// Query single match
	requestURL = gameon_util_get_api_url(GAMEON_API_URL_MATCH_GET_DETAILS, matchId, playerAttributes);
	promise.request_result_data_type = oGameOnMatch;
}
else
{
	// Query all matches
	requestURL = gameon_util_get_api_url(GAMEON_API_URL_MATCH_GET, playerAttributes, filterBy, limit, period, matchType);
	promise.request_result_data_type = oGameOnMatchList;
}

// Execute query
gameon_util_api_request(requestURL, "", oGameOnClient.http_headers_default, "GET", 
						callbackSuccess, callbackFailure, promise);
						
return promise;