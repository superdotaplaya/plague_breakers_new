/// @description Claims or fulfills the given prizes for the current player
/// @param {ds_list} prizeIds
/// @param {bool}   fulfillPrize
/// @param {script} [callbackSuccess]
/// @param {script} [callbackFailure]

if(!gameon_util_check_auth())
	return undefined;

var prizeIds =			argument[0];
var fulfillPrize =		argument[1];
var callbackSuccess =	argument_count > 2 ? argument[2] : undefined;
var callbackFailure =	argument_count > 3 ? argument[3] : undefined;

// Get the URL base for our request depending on the method used
var requestURLBase = (fulfillPrize ? GAMEON_API_URL_PRIZES_FULFILL : GAMEON_API_URL_PRIZES_CLAIM);
var requestURL = gameon_util_get_api_url(requestURLBase);

// Build request body
var requestBodyJson = ds_map_create();
var prizeIdsJson = ds_list_create();
ds_list_copy(prizeIdsJson, prizeIds);

ds_map_add_list(requestBodyJson, "awardedPrizeIds", prizeIdsJson);
var requestBody = json_encode(requestBodyJson);
ds_map_destroy(requestBodyJson);

// Set up promise
var promise = gameon_util_object_create(oGameOnPromiseData);
promise.request_result_data_type = oGameOnPrizeClaim;

// Perform request
gameon_util_api_request(requestURL, requestBody, oGameOnClient.http_headers_default, "POST", 
						callbackSuccess, callbackFailure, promise);
return promise;