/// @description Returns an OS type supported by GameOn API requests
var osTypeString = "unknown";
if(os_browser != browser_not_a_browser)
{
	osTypeString = "html";
}
else
{
	switch(os_type)
	{
		case os_android: osTypeString = "android"; break;
		case os_ios: osTypeString = "ios"; break;
		case os_macosx: osTypeString = "mac"; break;
		case os_linux: osTypeString = "linux"; break;
		case os_xboxone: osTypeString = "xbox"; break;
		//case os_switch: osTypeString = "nintendo"; break;
	
		case os_ps3:
		case os_ps4:
		case os_psvita:
			osTypeString = "playstation"; 
			break;
	
		case os_win8native: 
		case os_windows: 
		case os_winphone:
		case os_uwp:
			osTypeString = "pc"; 
			break;
	}
}

return osTypeString;