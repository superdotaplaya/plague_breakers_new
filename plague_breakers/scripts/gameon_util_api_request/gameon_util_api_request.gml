/// @description Executes an API request
/// @param {string}			requestURL
/// @param {string}			requestBody
/// @param {ds_map}			requestHeader
/// @param {string}			requestMethod
/// @param {script}			callbackSuccess
/// @param {script}			callbackFailed
/// @param {oGameOnPromise} promiseObject

var requestURL = argument0;
var requestBody = argument1;
var requestHeader = argument2;
var requestMethod = argument3;
var callbackSuccess = argument4;
var callbackFailed = argument5;
var promiseObject = argument6;

with(promiseObject)
{
	request_url = requestURL;
	request_body = requestBody;
	request_method = requestMethod;
	request_header = requestHeader;
	
	callback_failed = callbackFailed;
	callback_success = callbackSuccess;
	
	gameon_util_log("Performing " + string(requestMethod) + " request to URL: " + string(requestURL));
	
	gameon_util_log("Request headers:");
	
	var current_key = ds_map_find_first(requestHeader);
	var size = ds_map_size(requestHeader);
	for (var i = 0; i < size; i++)
	{
		gameon_util_log(string(current_key) + " : " + string(requestHeader[? current_key]));
	    current_key = ds_map_find_next(requestHeader, current_key);
	}
	
	event_user(eGameOnPromiseEvent.REQUEST_START);
}

return promiseObject;