/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 291A07DB
/// @DnDArgument : "code" "/// @description @function rousr_dissonance_create(_application_id, [_steam_id])$(13_10)/// @param _application_id$(13_10)/// @param  [_steam_id]$(13_10)///@desc initialize the Discord RPC library, and create the GML Discord management object$(13_10)///@param {String} _application_id   the application id string from your developer dashboard$(13_10)///@param {String} [_steamd_id]      your steam ID if you'd like Discord to be able to launch your game through Steam.$(13_10)gml_pragma("global", "global.__rousr_dissonance = noone;");$(13_10)enum EDiscordHandlers {$(13_10)  Ready = 0,$(13_10)  Disconnected,$(13_10)  Error,$(13_10)  Join,$(13_10)  Spectate,$(13_10)  JoinRequest,$(13_10)  $(13_10)  Num,$(13_10)};$(13_10)$(13_10)var _application_id = argument[0];$(13_10)var _steam_id = "";$(13_10)if (argument_count > 1) _steam_id = argument[1];$(13_10)$(13_10)if (!instance_exists(global.__rousr_dissonance)) {$(13_10)  with(instance_create(0, 0, rousrDissonance)) { $(13_10)    global.__rousr_dissonance = id; $(13_10)  }$(13_10)}$(13_10)$(13_10)with(global.__rousr_dissonance) {$(13_10)  Is_dirty = false;  $(13_10)  Is_ready = false;$(13_10)  Last_error_code = undefined;$(13_10)  Last_error_msg  = undefined;$(13_10)$(13_10)  Handlers = array_create(EDiscordHandlers.Num);$(13_10)  for (var i = 0; i < EDiscordHandlers.Num; ++i) {$(13_10)    var handler_array = array_create(2);$(13_10)    handler_array[@ 0] = rousr_dissonance_dummy_function;$(13_10)    handler_array[@ 1] = undefined;$(13_10)    Handlers[@ i] = handler_array;$(13_10) }$(13_10)     $(13_10)  Avatars     = array_create(0);$(13_10)  Num_avatars = 0;$(13_10)  $(13_10)  if (!Discord_initialized) {$(13_10)    Discord_initialized = discord_init(_application_id, _steam_id) == 1.0;$(13_10)  }$(13_10) $(13_10)  if (!Discord_initialized) {$(13_10)    show_debug_message("Discord RPC not initalized!");$(13_10)    return false;$(13_10)  }$(13_10)}$(13_10)$(13_10)return true;$(13_10)"
/// @description l291A07DB_0 rousr_dissonance_create(_application_id, [_steam_id])
/// @param _application_id
/// @param  [_steam_id]
///@desc initialize the Discord RPC library, and create the GML Discord management object
///@param {String} _application_id   the application id string from your developer dashboard
///@param {String} [_steamd_id]      your steam ID if you'd like Discord to be able to launch your game through Steam.
gml_pragma("global", "global.__rousr_dissonance = noone;");
enum EDiscordHandlers {
  Ready = 0,
  Disconnected,
  Error,
  Join,
  Spectate,
  JoinRequest,
  
  Num,
};

var _application_id = argument[0];
var _steam_id = "";
if (argument_count > 1) _steam_id = argument[1];

if (!instance_exists(global.__rousr_dissonance)) {
  with(instance_create(0, 0, rousrDissonance)) { 
    global.__rousr_dissonance = id; 
  }
}

with(global.__rousr_dissonance) {
  Is_dirty = false;  
  Is_ready = false;
  Last_error_code = undefined;
  Last_error_msg  = undefined;

  Handlers = array_create(EDiscordHandlers.Num);
  for (var i = 0; i < EDiscordHandlers.Num; ++i) {
    var handler_array = array_create(2);
    handler_array[@ 0] = rousr_dissonance_dummy_function;
    handler_array[@ 1] = undefined;
    Handlers[@ i] = handler_array;
 }
     
  Avatars     = array_create(0);
  Num_avatars = 0;
  
  if (!Discord_initialized) {
    Discord_initialized = discord_init(_application_id, _steam_id) == 1.0;
  }
 
  if (!Discord_initialized) {
    show_debug_message("Discord RPC not initalized!");
    return false;
  }
}

return true;