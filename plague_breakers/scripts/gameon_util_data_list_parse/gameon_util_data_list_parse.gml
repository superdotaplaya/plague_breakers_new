/// @description Parses a list of JSON items into the given data type
/// @param {ds_map}  jsonData
/// @param {ds_list} outList
/// @param {object}  parsedDataType
/// @param {string}	 listKey

var jsonData = argument0;
var outList = argument1;
var parsedDataType = argument2;
var listJSONKey = argument3;

if(is_undefined(jsonData) || !ds_exists(jsonData, ds_type_map))
{
	gameon_util_log("Invalid JSON data passed to gameon_util_data_list_parse.", true, undefined, true);
	return undefined;
}

var listJSON = is_undefined(listJSONKey) ? undefined : jsonData[? listJSONKey];
var parsedData;

// Check if we've been given a list of items, or a single item
// If we're dealing with a single item, we'll still return it in a list for consistency
if(!is_undefined(listJSON) && ds_exists(listJSON, ds_type_list))
{
	// Parse a list of items
	var itemCount = ds_list_size(listJSON);
	for(var itemIdx = 0; itemIdx < itemCount; ++itemIdx)
	{
		parsedData = gameon_util_object_create(parsedDataType);
		gameon_util_data_parse(parsedData, listJSON[| itemIdx]);
		ds_list_add(outList, parsedData);
	}
}
else
{
	// Parse a single item and add it to the resulting list
	parsedData = gameon_util_object_create(parsedDataType);
	gameon_util_data_parse(parsedData, jsonData);
	ds_list_add(outList, parsedData);
}

return outList;