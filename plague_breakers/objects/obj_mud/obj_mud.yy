{
    "id": "0d4bf462-0cbf-4eae-bddf-eb9d2b4a2d5c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mud",
    "eventList": [
        {
            "id": "db051504-783c-4168-b2bf-065ea5bf4366",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0d4bf462-0cbf-4eae-bddf-eb9d2b4a2d5c"
        },
        {
            "id": "27d64cd2-9d41-4102-9a9b-13b593af0e82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0d4bf462-0cbf-4eae-bddf-eb9d2b4a2d5c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e564aebc-39ea-457e-9d53-6cba7b2083f1",
    "visible": true
}