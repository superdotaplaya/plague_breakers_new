{
    "id": "0429cd6d-8436-411d-80d9-dfc9bdd55063",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_rebind_shop5",
    "eventList": [
        {
            "id": "d3f798e2-17e8-45d0-8437-2d38d474ec48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0429cd6d-8436-411d-80d9-dfc9bdd55063"
        },
        {
            "id": "da389537-be2d-45d8-b307-6f462e4a3451",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0429cd6d-8436-411d-80d9-dfc9bdd55063"
        },
        {
            "id": "eda46254-7ff4-47da-bf43-eda7663c357b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "0429cd6d-8436-411d-80d9-dfc9bdd55063"
        },
        {
            "id": "a1e2588a-4095-43ef-9357-6519cf0a944a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "0429cd6d-8436-411d-80d9-dfc9bdd55063"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a832f2b-2273-4cc3-b553-8c06b7dc617f",
    "visible": true
}