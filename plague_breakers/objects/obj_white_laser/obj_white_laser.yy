{
    "id": "0c6eaf9f-ca87-4055-855b-4224f662fc43",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_white_laser",
    "eventList": [
        {
            "id": "8cc581d4-172e-458e-9dfb-6eee05da6147",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0c6eaf9f-ca87-4055-855b-4224f662fc43"
        },
        {
            "id": "51acd7cc-6945-4da7-a651-bfbd59c94694",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0c6eaf9f-ca87-4055-855b-4224f662fc43"
        },
        {
            "id": "77bf51df-5cef-4c7d-8951-98b70d2a510d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0c6eaf9f-ca87-4055-855b-4224f662fc43"
        },
        {
            "id": "fa415485-d791-4781-9f6b-f1bbd2917892",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "0c6eaf9f-ca87-4055-855b-4224f662fc43"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f73f4dae-ee41-4718-a8ae-a1ef0fb2ec36",
    "visible": true
}