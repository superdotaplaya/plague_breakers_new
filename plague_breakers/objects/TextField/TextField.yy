{
    "id": "b60d9c39-d513-4a74-beb1-6b4005d08fbc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "TextField",
    "eventList": [
        {
            "id": "ea471e25-a6ef-4647-b140-9e2be0596c75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b60d9c39-d513-4a74-beb1-6b4005d08fbc"
        },
        {
            "id": "19e4525b-b607-4794-a6da-5b4085c15ea4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b60d9c39-d513-4a74-beb1-6b4005d08fbc"
        },
        {
            "id": "548bcf42-efa7-4ae4-ac3f-a87bb8cc84d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b60d9c39-d513-4a74-beb1-6b4005d08fbc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}