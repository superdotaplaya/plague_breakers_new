{
    "id": "c4bf4476-4a9d-49a1-943b-10216c4adfd6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_back_to_menu",
    "eventList": [
        {
            "id": "ccf04c64-5dec-48c7-9efb-99edcf1a78de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "c4bf4476-4a9d-49a1-943b-10216c4adfd6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a70d20f5-51b5-47ae-b5aa-c0d83a06280c",
    "visible": true
}