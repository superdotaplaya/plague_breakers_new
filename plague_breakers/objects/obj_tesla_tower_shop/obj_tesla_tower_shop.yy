{
    "id": "90a0bd5f-c90d-4cee-8bb7-72c2fd9704c9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tesla_tower_shop",
    "eventList": [
        {
            "id": "f6f6bddf-261b-4089-b592-36df5569c3cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "90a0bd5f-c90d-4cee-8bb7-72c2fd9704c9"
        },
        {
            "id": "d35f9b58-e8aa-4560-b3f6-e36b352d6323",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "90a0bd5f-c90d-4cee-8bb7-72c2fd9704c9"
        },
        {
            "id": "73f4fb07-3ec3-4e76-bcf2-696fe583da8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "90a0bd5f-c90d-4cee-8bb7-72c2fd9704c9"
        },
        {
            "id": "116a078c-d926-4dd8-9fed-0adaf82c990c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "90a0bd5f-c90d-4cee-8bb7-72c2fd9704c9"
        },
        {
            "id": "7a8b88ca-8aa9-43d5-b64b-81d2ce643c03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "90a0bd5f-c90d-4cee-8bb7-72c2fd9704c9"
        },
        {
            "id": "62002f2a-929d-4cfa-b7dd-3e6c02587716",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "90a0bd5f-c90d-4cee-8bb7-72c2fd9704c9"
        },
        {
            "id": "38b462e8-0f68-42c1-9358-0ad3528e248d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "90a0bd5f-c90d-4cee-8bb7-72c2fd9704c9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "45cfa927-ae76-40bc-80ed-369d8ccc3501",
    "visible": true
}