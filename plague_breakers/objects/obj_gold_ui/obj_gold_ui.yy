{
    "id": "c670709f-0786-4f76-b666-b70debe1bf31",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gold_ui",
    "eventList": [
        {
            "id": "3e675117-e616-4aea-adc7-a1099a498d16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c670709f-0786-4f76-b666-b70debe1bf31"
        },
        {
            "id": "115fa5e6-76ca-4362-afb1-2abc0ecab3b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "721454be-8a92-444d-8eaf-873df8cbf18d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c670709f-0786-4f76-b666-b70debe1bf31"
        },
        {
            "id": "80100a56-de7b-4fe5-8bfd-6c6c6d786b33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c670709f-0786-4f76-b666-b70debe1bf31"
        },
        {
            "id": "fb433e82-b7fd-4922-b381-90b1c5666eb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c670709f-0786-4f76-b666-b70debe1bf31"
        },
        {
            "id": "0b067434-8377-444d-96d8-81f5ed8ca493",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c670709f-0786-4f76-b666-b70debe1bf31"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b1ac8a00-419f-4e13-b41d-288459b9a5ce",
    "visible": true
}