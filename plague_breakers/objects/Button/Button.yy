{
    "id": "eceacffe-f991-4676-ae26-43bd75253214",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Button",
    "eventList": [
        {
            "id": "36fb5243-a9fe-4e0c-a50e-9f925f642c20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eceacffe-f991-4676-ae26-43bd75253214"
        },
        {
            "id": "436da54b-9dd0-405d-adb7-0c321ef357e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "eceacffe-f991-4676-ae26-43bd75253214"
        },
        {
            "id": "3c3d9d89-653c-4535-a27c-f39a803b120b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "eceacffe-f991-4676-ae26-43bd75253214"
        },
        {
            "id": "fddf45da-5782-4d8d-8355-bae3f8c3f13a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "eceacffe-f991-4676-ae26-43bd75253214"
        },
        {
            "id": "98a3a69c-3b02-44cc-a100-d3defcb23284",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "eceacffe-f991-4676-ae26-43bd75253214"
        },
        {
            "id": "fdcae918-cb5d-4232-84ea-39945c344f13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "eceacffe-f991-4676-ae26-43bd75253214"
        },
        {
            "id": "50ccc77c-908a-4c4d-bff9-b5e7d54c0366",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "eceacffe-f991-4676-ae26-43bd75253214"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}