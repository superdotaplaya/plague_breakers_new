{
    "id": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_red_tower",
    "eventList": [
        {
            "id": "37b88873-2275-42dc-88d7-9def8f91f7b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2"
        },
        {
            "id": "c301d457-d996-49e3-8653-2a61913485d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2"
        },
        {
            "id": "6cc70e14-0218-4153-a72a-dfa9e3034966",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2"
        },
        {
            "id": "68ec2827-def6-4491-a8ac-322fec75dc2c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2"
        },
        {
            "id": "5e6bdd0b-29e9-4299-b49b-e9920f787b23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2"
        },
        {
            "id": "eb3a974c-68ab-4e1e-8c17-d1c2415cc7a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2"
        },
        {
            "id": "974ccfdd-5914-4bb5-9fb3-a892f033de00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2"
        },
        {
            "id": "2a3315b0-2761-42b6-99f6-4d0f7ae3509f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2"
        },
        {
            "id": "7b6c8802-4fdc-4c7a-aee5-87e3923bf1a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2"
        },
        {
            "id": "a67fbe1e-2735-4031-b46f-d4bbd69fe810",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2"
        },
        {
            "id": "036b954a-8e8f-48d2-b07c-04a1c4f54910",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2"
        },
        {
            "id": "725a3763-9173-4632-9a21-f062066c199c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2"
        },
        {
            "id": "74d7c3bc-2e39-4cba-a1b8-9cb14ce15fb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2"
        },
        {
            "id": "267c2669-e53c-4990-8596-860ce0b6e6bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "f0875ffd-fd8e-428c-aabc-956eb91215a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "afd048dc-5f11-4a66-a632-1d454265b957",
    "visible": true
}