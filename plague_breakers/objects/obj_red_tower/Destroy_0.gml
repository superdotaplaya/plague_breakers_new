/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 1AA3B465
/// @DnDArgument : "code" "if highground = true then$(13_10){$(13_10)instance_create_layer(x,y,"Instances",obj_high_grid)$(13_10)} else$(13_10){$(13_10)	instance_deactivate_layer("TOP_GRID")$(13_10)	instance_deactivate_layer("Instances")$(13_10)	instance_activate_layer("Instances")$(13_10)	instance_activate_layer("TOP_GRID")$(13_10)instance_create_layer(x,y,"instances_towers",obj_return_enemies)$(13_10)	instance_create_layer(x,y,"TOP_GRID",obj_grid)$(13_10)}"
if highground = true then
{
instance_create_layer(x,y,"Instances",obj_high_grid)
} else
{
	instance_deactivate_layer("TOP_GRID")
	instance_deactivate_layer("Instances")
	instance_activate_layer("Instances")
	instance_activate_layer("TOP_GRID")
instance_create_layer(x,y,"instances_towers",obj_return_enemies)
	instance_create_layer(x,y,"TOP_GRID",obj_grid)
}

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 26132375
/// @DnDApplyTo : be61b32a-8c4e-4b4d-9925-6965634dbb17
with(obj_green_damage_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 3E1987F3
/// @DnDApplyTo : d2a5ac99-b5d0-40cd-95c5-11df01d2059d
with(obj_green_attack_range_up) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 523349A0
/// @DnDApplyTo : 8ae6c06b-2c3b-4787-b574-78c7112cddf2
with(obj_blue_damage_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 26F00BD2
/// @DnDApplyTo : ad3c7a93-2d91-49e2-8273-cf85eb89e048
with(obj_blue_range_up) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 6DC5E375
/// @DnDApplyTo : 2631dbca-5d33-4648-bc49-3198bc4211d8
with(obj_red_damage_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 72E2979C
/// @DnDApplyTo : b0acdc6d-c837-4818-b5c6-27ddb536fadc
with(obj_red_range_up) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 491A7321
/// @DnDApplyTo : ff372800-7810-49bb-898f-a7defb5cc613
with(obj_tower_3_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 7000CFC8
/// @DnDApplyTo : 95206be0-6e6c-4734-8b6c-0cf228a2d3bc
with(obj_yellow_damage_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 0FAE0EA5
/// @DnDApplyTo : b1324da9-7d47-4d53-9da2-8d713b110ca5
with(obj_yellow_range_up) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 526F6E92
/// @DnDApplyTo : 2631dbca-5d33-4648-bc49-3198bc4211d8
with(obj_red_damage_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 71988842
/// @DnDApplyTo : b0acdc6d-c837-4818-b5c6-27ddb536fadc
with(obj_red_range_up) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 3EF0226A
/// @DnDApplyTo : 1839b936-ae8c-45b5-b836-3f28b8a336e8
with(obj_white_damage_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 0FB7CF50
/// @DnDApplyTo : 2260b290-3999-45c1-8c9e-8777dba2f23a
with(obj_white_range_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 03971740
/// @DnDApplyTo : a550b399-af22-4b2c-8d4f-698f20706196
with(obj_orange_damage_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 262A993F
/// @DnDApplyTo : 5cab794b-de08-4f2e-84e5-abed593dd509
with(obj_poison_damage_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 063A2014
/// @DnDApplyTo : 8aea4293-de50-4063-9f50-d75be1378031
with(obj_tower_1_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 31BD6861
/// @DnDApplyTo : 073fec17-ba80-4dec-a3c8-288b74078b2d
with(obj_tower_2_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 091CDE07
/// @DnDApplyTo : ff372800-7810-49bb-898f-a7defb5cc613
with(obj_tower_3_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 5C1E2D18
/// @DnDApplyTo : 43c1b6e1-4e5c-402d-9b7f-54483b5eb297
with(obj_tower_4_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 7783F54A
/// @DnDApplyTo : 16d11c02-1a7d-4f1e-8cf3-b39df52c2b8b
with(obj_tower_5_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 6A74191D
/// @DnDApplyTo : b44ce0f6-e314-48d5-9a11-f7e037ea1145
with(obj_tower_6_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 751B789A
/// @DnDApplyTo : 32183b7a-d784-471e-939b-c846cb1d5088
with(obj_tower_poison_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 060A32C2
/// @DnDApplyTo : 71b2a270-606b-4041-bf6a-7f7b00d49850
with(obj_mud_buy) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 5D210E8B
/// @DnDApplyTo : 769e7b14-4bf0-442f-a5a3-f5dc8b61975d
with(obj_wall_buy) instance_destroy();