/// @DnDAction : YoYo Games.Common.Function_Call
/// @DnDVersion : 1
/// @DnDHash : 14439B9E
/// @DnDArgument : "function" "instance_destroy_shop"
instance_destroy_shop();

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 6689F9B3
/// @DnDArgument : "var" "global.shop_open"
/// @DnDArgument : "value" "false"
if(global.shop_open == false)
{
	/// @DnDAction : YoYo Games.Common.Set_Global
	/// @DnDVersion : 1
	/// @DnDHash : 4F6782AA
	/// @DnDParent : 6689F9B3
	/// @DnDArgument : "value" "true"
	/// @DnDArgument : "var" "shop_open"
	global.shop_open = true;

	/// @DnDAction : YoYo Games.Paths.Start_Path
	/// @DnDVersion : 1.1
	/// @DnDHash : 4EEA6B52
	/// @DnDApplyTo : 37f57748-c862-493a-bbbd-c8ffdf8f8d60
	/// @DnDParent : 6689F9B3
	/// @DnDArgument : "path" "shop"
	/// @DnDArgument : "speed" "40"
	/// @DnDArgument : "relative" "true"
	/// @DnDSaveInfo : "path" "51c200a6-a5a1-4aee-9b09-55296bbe01a1"
	with(obj_shop_menu) path_start(shop, 40, path_action_stop, true);

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 27A1A235
	/// @DnDParent : 6689F9B3
	/// @DnDArgument : "var" "global.ui"
	/// @DnDArgument : "value" "false"
	if(global.ui == false)
	{
		/// @DnDAction : YoYo Games.Paths.Start_Path
		/// @DnDVersion : 1.1
		/// @DnDHash : 2B017C57
		/// @DnDApplyTo : dce01893-52fc-4398-ad95-8c88eef129f0
		/// @DnDParent : 27A1A235
		/// @DnDArgument : "path" "ui_info_open"
		/// @DnDArgument : "speed" "40"
		/// @DnDArgument : "relative" "true"
		/// @DnDSaveInfo : "path" "9dcfed44-8f1f-4d49-ada5-ce48f3e2d95d"
		with(obj_ui_info) path_start(ui_info_open, 40, path_action_stop, true);
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 33CBCEA4
		/// @DnDParent : 27A1A235
		/// @DnDArgument : "expr" "true"
		/// @DnDArgument : "var" "global.ui"
		global.ui = true;
	}
}

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 5620798F
/// @DnDArgument : "code" "global.upgrade_current = "green"$(13_10)global.current_tower = layer_instance_get_instance(self)$(13_10)instance_create_layer(95,830,"shop_icons",obj_red_damage_up1)$(13_10)instance_create_layer(95,830,"shop_icons",obj_red_range_up)$(13_10)global.x = x$(13_10)global.y = y$(13_10)global.current_tower = instance_nearest(global.x,global.y,obj_red_tower)$(13_10)$(13_10)"
global.upgrade_current = "green"
global.current_tower = layer_instance_get_instance(self)
instance_create_layer(95,830,"shop_icons",obj_red_damage_up1)
instance_create_layer(95,830,"shop_icons",obj_red_range_up)
global.x = x
global.y = y
global.current_tower = instance_nearest(global.x,global.y,obj_red_tower)

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 05442303
/// @DnDApplyTo : 8aea4293-de50-4063-9f50-d75be1378031
with(obj_tower_1_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 388E8BDC
/// @DnDApplyTo : 073fec17-ba80-4dec-a3c8-288b74078b2d
with(obj_tower_2_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 738FA453
/// @DnDApplyTo : ff372800-7810-49bb-898f-a7defb5cc613
with(obj_tower_3_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 2CA7F39F
/// @DnDApplyTo : 43c1b6e1-4e5c-402d-9b7f-54483b5eb297
with(obj_tower_4_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 4AF4A007
/// @DnDApplyTo : 16d11c02-1a7d-4f1e-8cf3-b39df52c2b8b
with(obj_tower_5_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 4B3B59AB
/// @DnDApplyTo : b44ce0f6-e314-48d5-9a11-f7e037ea1145
with(obj_tower_6_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 0AC1B6B4
/// @DnDApplyTo : 32183b7a-d784-471e-939b-c846cb1d5088
with(obj_tower_poison_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 7C6CE928
/// @DnDApplyTo : 71b2a270-606b-4041-bf6a-7f7b00d49850
with(obj_mud_buy) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 3696C7DA
/// @DnDApplyTo : 769e7b14-4bf0-442f-a5a3-f5dc8b61975d
with(obj_wall_buy) instance_destroy();