{
    "id": "3bcec0c4-d2ca-4e2d-a262-3516f60bb218",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_continue_game",
    "eventList": [
        {
            "id": "b659018c-7dde-46fb-8da7-a1f7024a239b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3bcec0c4-d2ca-4e2d-a262-3516f60bb218"
        },
        {
            "id": "4c79752a-e893-460b-ac9e-ab95e16b34b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "3bcec0c4-d2ca-4e2d-a262-3516f60bb218"
        },
        {
            "id": "45e21547-0a26-4a62-9f30-29ff1cd676ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "3bcec0c4-d2ca-4e2d-a262-3516f60bb218"
        },
        {
            "id": "9c16afc6-18b0-40b2-b62d-997f806770e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "3bcec0c4-d2ca-4e2d-a262-3516f60bb218"
        },
        {
            "id": "b15dd851-95f7-4bad-8655-938e014b98e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3bcec0c4-d2ca-4e2d-a262-3516f60bb218"
        },
        {
            "id": "0bf9843e-72d8-48e2-9709-8adbe84170dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3bcec0c4-d2ca-4e2d-a262-3516f60bb218"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e6f5584b-c13d-48a9-9d69-ec6c75a74a54",
    "visible": true
}