{
    "id": "6a6c0074-c14a-4d23-b1b6-e2f85a65361d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_options",
    "eventList": [
        {
            "id": "b6ff67e4-4566-4af3-8895-d053f82fbf77",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "6a6c0074-c14a-4d23-b1b6-e2f85a65361d"
        },
        {
            "id": "33540a50-5e1c-4379-9908-0a477bad6578",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "6a6c0074-c14a-4d23-b1b6-e2f85a65361d"
        },
        {
            "id": "49bfffaf-f54c-42c4-a164-0c9392613803",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "6a6c0074-c14a-4d23-b1b6-e2f85a65361d"
        },
        {
            "id": "f6f27239-c002-4379-98b2-3aed7932f25f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6a6c0074-c14a-4d23-b1b6-e2f85a65361d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "33a3fd3d-dd23-4893-871f-9e7ffacc4088",
    "visible": true
}