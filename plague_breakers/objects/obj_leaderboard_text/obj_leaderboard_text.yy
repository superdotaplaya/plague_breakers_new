{
    "id": "952c2c21-27c7-45e2-b0b4-d38506820d1d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_leaderboard_text",
    "eventList": [
        {
            "id": "dab2d329-16c9-458f-b67c-2de2ff8e477e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "952c2c21-27c7-45e2-b0b4-d38506820d1d"
        },
        {
            "id": "cc3b14a2-b1b2-4acf-bc60-1ece828a52f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "952c2c21-27c7-45e2-b0b4-d38506820d1d"
        },
        {
            "id": "13f425b2-4932-4f77-b869-0cbc79f2d7ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "952c2c21-27c7-45e2-b0b4-d38506820d1d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}