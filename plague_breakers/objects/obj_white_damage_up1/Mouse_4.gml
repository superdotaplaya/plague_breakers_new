/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 14753057
/// @DnDArgument : "var" "global.gold"
/// @DnDArgument : "op" "4"
/// @DnDArgument : "value" "(cost + cost_bonus)"
if(global.gold >= (cost + cost_bonus))
{
	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 62FF32FD
	/// @DnDParent : 14753057
	/// @DnDArgument : "code" "current_damage = variable_instance_get(instance_nearest(global.x,global.y,obj_white_tower),"damage")$(13_10)variable_instance_set(instance_nearest(global.x,global.y,obj_white_tower),"damage",current_damage + .01)$(13_10)global.gold -= (cost + cost_bonus)$(13_10)audio_play_sound(snd_buy,1,0)$(13_10)current_level += 1$(13_10)variable_instance_set(instance_nearest(global.x,global.y,obj_white_tower),"dmg_lvl",current_level)$(13_10)variable_instance_set(instance_nearest(global.x,global.y,obj_white_tower),"upgrade_damage_cost", (cost_bonus * 2) + cost)"
	current_damage = variable_instance_get(instance_nearest(global.x,global.y,obj_white_tower),"damage")
	variable_instance_set(instance_nearest(global.x,global.y,obj_white_tower),"damage",current_damage + .01)
	global.gold -= (cost + cost_bonus)
	audio_play_sound(snd_buy,1,0)
	current_level += 1
	variable_instance_set(instance_nearest(global.x,global.y,obj_white_tower),"dmg_lvl",current_level)
	variable_instance_set(instance_nearest(global.x,global.y,obj_white_tower),"upgrade_damage_cost", (cost_bonus * 2) + cost)
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 083BF42C
else
{
	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 55121AAF
	/// @DnDParent : 083BF42C
	/// @DnDArgument : "code" "audio_play_sound(snd_not_enough_gold,1,0)$(13_10)global.not_enough_gold = true$(13_10)"
	audio_play_sound(snd_not_enough_gold,1,0)
	global.not_enough_gold = true

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 00C6835C
	/// @DnDApplyTo : bd506929-690c-4d0b-bcc7-76f64dfd6fdb
	/// @DnDParent : 083BF42C
	/// @DnDArgument : "steps" "600"
	with(obj_score_money) {
	alarm_set(0, 600);
	
	}
}