{
    "id": "1839b936-ae8c-45b5-b836-3f28b8a336e8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_white_damage_up1",
    "eventList": [
        {
            "id": "022f2793-2dcc-4f80-a494-92fbc5ba889d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1839b936-ae8c-45b5-b836-3f28b8a336e8"
        },
        {
            "id": "9e3c2be8-528a-40c2-a127-b6ff42ca7508",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1839b936-ae8c-45b5-b836-3f28b8a336e8"
        },
        {
            "id": "d4cca41c-f6fd-459b-a5f0-e2836bcc62a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "1839b936-ae8c-45b5-b836-3f28b8a336e8"
        },
        {
            "id": "23e84837-0915-4d26-925b-2518c1854952",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "1839b936-ae8c-45b5-b836-3f28b8a336e8"
        },
        {
            "id": "4b75a6e9-b95a-408c-b6d8-2eb262ffbcf8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "1839b936-ae8c-45b5-b836-3f28b8a336e8"
        },
        {
            "id": "e3dd9a97-7417-424d-86e7-890791c8e1d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1839b936-ae8c-45b5-b836-3f28b8a336e8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "92de3f75-7cf1-48b2-ae14-43cffb5901c3",
    "visible": true
}