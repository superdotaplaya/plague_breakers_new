{
    "id": "a550b399-af22-4b2c-8d4f-698f20706196",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_orange_damage_up1",
    "eventList": [
        {
            "id": "474b0ff2-f732-4984-80f0-a454cd3ec66e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a550b399-af22-4b2c-8d4f-698f20706196"
        },
        {
            "id": "ecf8a4b9-6c4a-4c35-a71d-db56ec2dec05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a550b399-af22-4b2c-8d4f-698f20706196"
        },
        {
            "id": "e913f466-fc30-45b5-9f5d-bbf8f87dba76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "a550b399-af22-4b2c-8d4f-698f20706196"
        },
        {
            "id": "6ac6dea0-d00e-4d69-b9a6-e6bca184ef35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "a550b399-af22-4b2c-8d4f-698f20706196"
        },
        {
            "id": "ea7a420b-2c62-48a1-abbf-50c4cea58a7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "a550b399-af22-4b2c-8d4f-698f20706196"
        },
        {
            "id": "91318855-6b3e-492d-ab09-99aff5df6fd3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a550b399-af22-4b2c-8d4f-698f20706196"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4e7204c-bd0b-46b6-b2a1-b15035274049",
    "visible": true
}