/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 2216A3B7
/// @DnDArgument : "code" "global.gold += cost/2$(13_10)instance_deactivate_layer("shop_icons")$(13_10)audio_play_sound(snd_buy,1,0)$(13_10)bonus_attack_speed = variable_instance_get(instance_nearest(x,y,obj_modular_area),"attack_speed_bonus")$(13_10)variable_instance_set(instance_nearest(x,y,obj_modular_area),"attack_speed_bonus", bonus_attack_speed - 5)$(13_10)instance_destroy()$(13_10)$(13_10)$(13_10)"
global.gold += cost/2
instance_deactivate_layer("shop_icons")
audio_play_sound(snd_buy,1,0)
bonus_attack_speed = variable_instance_get(instance_nearest(x,y,obj_modular_area),"attack_speed_bonus")
variable_instance_set(instance_nearest(x,y,obj_modular_area),"attack_speed_bonus", bonus_attack_speed - 5)
instance_destroy()