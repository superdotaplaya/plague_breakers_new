{
    "id": "e79f9a45-e3f7-4d5b-be0d-7022e4227488",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_modular_attack_speed",
    "eventList": [
        {
            "id": "f8f93b79-68e3-411b-bad2-b1d9a3c26f64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e79f9a45-e3f7-4d5b-be0d-7022e4227488"
        },
        {
            "id": "786caee0-edb3-4935-a261-2da594cae7eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e79f9a45-e3f7-4d5b-be0d-7022e4227488"
        },
        {
            "id": "cc1b8133-8a15-4ef8-8009-4ae128c1bb4f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "e79f9a45-e3f7-4d5b-be0d-7022e4227488"
        },
        {
            "id": "bd878999-8569-46e7-98eb-c4c7dbac703b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "e79f9a45-e3f7-4d5b-be0d-7022e4227488"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cd319e63-fd52-4717-92e8-dfc9f73a289b",
    "visible": true
}