{
    "id": "0c2a7328-1b75-408d-8bab-28d88afd0f55",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Example",
    "eventList": [
        {
            "id": "079765cc-adf3-4679-8f48-41bff2e17ebc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0c2a7328-1b75-408d-8bab-28d88afd0f55"
        },
        {
            "id": "130df544-9434-472e-8f28-f9ea6bdab687",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0c2a7328-1b75-408d-8bab-28d88afd0f55"
        },
        {
            "id": "c18d0218-0a66-4747-ab3a-b88b8d6d9d66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0c2a7328-1b75-408d-8bab-28d88afd0f55"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}