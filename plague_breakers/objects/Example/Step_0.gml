/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 296DE656
/// @DnDArgument : "code" "/// @description Example - Step$(13_10)if (Discord_ready) {$(13_10)  Time_left -= (delta_time / 1000000);$(13_10)  if (Time_left <= 0) {$(13_10)    $(13_10)    if (Pupper_check.Toggled) {$(13_10)      Puppers++;$(13_10)      Time_left = floor(10 + sqrt(Puppers + Puppers));$(13_10)      Details_string = string(Puppers) + " Puppers sprouted.";$(13_10)      rousr_dissonance_set_details(Details_string);$(13_10)    } else {$(13_10)     Time_left = Timestamp;$(13_10)    }$(13_10)    if (Timer_check.Toggled)$(13_10)      rousr_dissonance_set_timestamps(0, Time_left);$(13_10)  }$(13_10)}$(13_10)$(13_10)if (keyboard_check_pressed(vk_escape) && global.__field_focus == noone)$(13_10)  game_end();$(13_10)$(13_10)$(13_10)"
/// @description Example - Step
if (Discord_ready) {
  Time_left -= (delta_time / 1000000);
  if (Time_left <= 0) {
    
    if (Pupper_check.Toggled) {
      Puppers++;
      Time_left = floor(10 + sqrt(Puppers + Puppers));
      Details_string = string(Puppers) + " Puppers sprouted.";
      rousr_dissonance_set_details(Details_string);
    } else {
     Time_left = Timestamp;
    }
    if (Timer_check.Toggled)
      rousr_dissonance_set_timestamps(0, Time_left);
  }
}

if (keyboard_check_pressed(vk_escape) && global.__field_focus == noone)
  game_end();