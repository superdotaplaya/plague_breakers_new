/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 2A696EFB
/// @DnDArgument : "code" "/// @description Example - Create$(13_10)randomize();$(13_10)draw_set_font(fnt_game);$(13_10)draw_set_color(c_black);$(13_10)$(13_10)Discord_ready = false$(13_10)Last_time     = -1;$(13_10)Time_left     = 10;$(13_10)Puppers       = 0;$(13_10)$(13_10)Join_requested     = undefined;$(13_10)Spectate_requested = undefined;$(13_10)Join               = undefined;$(13_10)Error_msg          = undefined;$(13_10)$(13_10)// put your app-id string here$(13_10)var App_id = "<put your app id here>";$(13_10)$(13_10)if (!rousr_dissonance_create(App_id)) { $(13_10)  Error_msg = "Discord RPC unable to initialize";   $(13_10)}$(13_10)$(13_10)State_string   = "until a puppy is birthed.";$(13_10)Details_string = "0 Puppers sprouted.";$(13_10)Large_text = "A GameMaker Extension for discord rich presence.";$(13_10)Small_text = "by @babyjeans#7177 (@babyj3ans)";$(13_10)Timestamp  = 10;$(13_10)$(13_10)rousr_dissonance_handler_on_ready(example_on_ready, id);$(13_10)rousr_dissonance_handler_on_disconnected(example_on_disconnected, id);$(13_10)rousr_dissonance_handler_on_error(example_on_error, id);$(13_10)rousr_dissonance_handler_on_join(example_on_join, id);$(13_10)rousr_dissonance_handler_on_spectate(example_on_spectate, id);$(13_10)rousr_dissonance_handler_on_join_request(example_on_join_request, id);$(13_10)$(13_10)rousr_dissonance_set_details(Details_string);$(13_10)rousr_dissonance_set_state(State_string);$(13_10)rousr_dissonance_set_large_image("dissonance_large", Large_text);  // set images from your app dashboard here$(13_10)rousr_dissonance_set_small_image("dissonance",             Small_text); // set images from your app dashboard here$(13_10)rousr_dissonance_set_timestamps(0, Timestamp);$(13_10)  $(13_10)// generate unique keys$(13_10)Party_id     = random_key();$(13_10)Join_key     = random_key();$(13_10)Spectate_key = random_key();$(13_10)Match_key    = random_key();$(13_10)  $(13_10)rousr_dissonance_set_party(1, 5, Party_id);$(13_10)rousr_dissonance_set_join_secret(Join_key);$(13_10)rousr_dissonance_set_spectate_secret(Spectate_key);$(13_10)rousr_dissonance_set_match_secret(Match_key, 1);$(13_10)$(13_10)//////////////////////////////$(13_10)// Create the UI$(13_10)//////////////////////////////$(13_10)$(13_10)AcceptButton = instance_create(x, 0, Button);$(13_10)RejectButton = instance_create(x, 0, Button);$(13_10)IgnoreButton = instance_create(x, 0, Button);$(13_10)$(13_10)var sw = string_width(string_hash_to_newline("Accept")) + 10;$(13_10)var ix = sw / sprite_get_width(AcceptButton.sprite_index);$(13_10)var fw = 40;$(13_10)$(13_10)AcceptButton.Label = "Accept";$(13_10)AcceptButton.image_xscale = ix;$(13_10)AcceptButton.clickAction = example_click_accept;$(13_10)AcceptButton.visible = false$(13_10)$(13_10)RejectButton.Label = "Reject"; $(13_10)RejectButton.image_xscale = ix;$(13_10)RejectButton.clickAction = example_click_reject;$(13_10)RejectButton.visible = false$(13_10)$(13_10)IgnoreButton.Label = "Ignore";$(13_10)IgnoreButton.image_xscale = ix;$(13_10)IgnoreButton.clickAction = example_click_ignore;$(13_10)IgnoreButton.visible = false;$(13_10)$(13_10)Details_field = instance_create(x, 0, TextField);$(13_10)Details_field.Default    = "Harvesting some good boys";$(13_10)Details_field.Text       = "Harvesting some good boys";$(13_10)Details_field.Text_width = fw;$(13_10)Details_field.On_lost_focus = example_text_field;$(13_10)Details_field.On_submit     = example_text_field;$(13_10)Details_field.visible = false;$(13_10)$(13_10)State_field = instance_create(x, 0, TextField);$(13_10)State_field.Default    = "Famished from all the zooms";$(13_10)State_field.Text       = "Famished from all the zooms";$(13_10)State_field.Text_width = fw;$(13_10)State_field.On_lost_focus = example_text_field;$(13_10)State_field.On_submit     = example_text_field;$(13_10)State_field.visible = false;$(13_10)$(13_10)Timer_field = instance_create(x, 0, TextField);$(13_10)Timer_field.Text = string(10);$(13_10)Timer_field.Text_width    = 3;$(13_10)Timer_field.On_lost_focus = example_text_field;$(13_10)Timer_field.On_submit     = example_text_field;$(13_10)$(13_10)Timer_field.visible = false;$(13_10)$(13_10)Pupper_check = instance_create(x, 0, CheckBox);$(13_10)Pupper_check.Label = "Sprout Puppers";$(13_10)Pupper_check.visible = false;$(13_10)Pupper_check.Toggled = true;$(13_10)Pupper_check.On_toggle = example_toggle;$(13_10)$(13_10)Timer_check = instance_create(x, 0, CheckBox);$(13_10)Timer_check.Label = "Use Timer";$(13_10)Timer_check.visible = false;$(13_10)Timer_check.Toggled = true;$(13_10)Timer_check.On_toggle = example_toggle;$(13_10)$(13_10)Party_check = instance_create(x, 0, CheckBox);$(13_10)Party_check.Label = "Show Party";$(13_10)Party_check.visible = false;$(13_10)Party_check.Toggled = true;$(13_10)Party_check.On_toggle = example_toggle;$(13_10)$(13_10)Party_size_field = instance_create(x, 0, TextField);$(13_10)Party_size_field.Text = string(1);$(13_10)Party_size_field.Text_width    = 3;$(13_10)Party_size_field.On_lost_focus = example_text_field;$(13_10)Party_size_field.On_submit     = example_text_field;$(13_10)Party_size_field.visible = false;$(13_10)$(13_10)Party_max_field = instance_create(x, 0, TextField);$(13_10)Party_max_field.Text = string(5);$(13_10)Party_max_field.Text_width    = 3;$(13_10)Party_max_field.On_lost_focus = example_text_field;$(13_10)Party_max_field.On_submit     = example_text_field;$(13_10)Party_max_field.visible = false;$(13_10)$(13_10)$(13_10)"
/// @description Example - Create
randomize();
draw_set_font(fnt_game);
draw_set_color(c_black);

Discord_ready = false
Last_time     = -1;
Time_left     = 10;
Puppers       = 0;

Join_requested     = undefined;
Spectate_requested = undefined;
Join               = undefined;
Error_msg          = undefined;

// put your app-id string here
var App_id = "<put your app id here>";

if (!rousr_dissonance_create(App_id)) { 
  Error_msg = "Discord RPC unable to initialize";   
}

State_string   = "until a puppy is birthed.";
Details_string = "0 Puppers sprouted.";
Large_text = "A GameMaker Extension for discord rich presence.";
Small_text = "by l2A696EFB_0#7177 (l2A696EFB_1)";
Timestamp  = 10;

rousr_dissonance_handler_on_ready(example_on_ready, id);
rousr_dissonance_handler_on_disconnected(example_on_disconnected, id);
rousr_dissonance_handler_on_error(example_on_error, id);
rousr_dissonance_handler_on_join(example_on_join, id);
rousr_dissonance_handler_on_spectate(example_on_spectate, id);
rousr_dissonance_handler_on_join_request(example_on_join_request, id);

rousr_dissonance_set_details(Details_string);
rousr_dissonance_set_state(State_string);
rousr_dissonance_set_large_image("dissonance_large", Large_text);  // set images from your app dashboard here
rousr_dissonance_set_small_image("dissonance",             Small_text); // set images from your app dashboard here
rousr_dissonance_set_timestamps(0, Timestamp);
  
// generate unique keys
Party_id     = random_key();
Join_key     = random_key();
Spectate_key = random_key();
Match_key    = random_key();
  
rousr_dissonance_set_party(1, 5, Party_id);
rousr_dissonance_set_join_secret(Join_key);
rousr_dissonance_set_spectate_secret(Spectate_key);
rousr_dissonance_set_match_secret(Match_key, 1);

//////////////////////////////
// Create the UI
//////////////////////////////

AcceptButton = instance_create(x, 0, Button);
RejectButton = instance_create(x, 0, Button);
IgnoreButton = instance_create(x, 0, Button);

var sw = string_width(string_hash_to_newline("Accept")) + 10;
var ix = sw / sprite_get_width(AcceptButton.sprite_index);
var fw = 40;

AcceptButton.Label = "Accept";
AcceptButton.image_xscale = ix;
AcceptButton.clickAction = example_click_accept;
AcceptButton.visible = false

RejectButton.Label = "Reject"; 
RejectButton.image_xscale = ix;
RejectButton.clickAction = example_click_reject;
RejectButton.visible = false

IgnoreButton.Label = "Ignore";
IgnoreButton.image_xscale = ix;
IgnoreButton.clickAction = example_click_ignore;
IgnoreButton.visible = false;

Details_field = instance_create(x, 0, TextField);
Details_field.Default    = "Harvesting some good boys";
Details_field.Text       = "Harvesting some good boys";
Details_field.Text_width = fw;
Details_field.On_lost_focus = example_text_field;
Details_field.On_submit     = example_text_field;
Details_field.visible = false;

State_field = instance_create(x, 0, TextField);
State_field.Default    = "Famished from all the zooms";
State_field.Text       = "Famished from all the zooms";
State_field.Text_width = fw;
State_field.On_lost_focus = example_text_field;
State_field.On_submit     = example_text_field;
State_field.visible = false;

Timer_field = instance_create(x, 0, TextField);
Timer_field.Text = string(10);
Timer_field.Text_width    = 3;
Timer_field.On_lost_focus = example_text_field;
Timer_field.On_submit     = example_text_field;

Timer_field.visible = false;

Pupper_check = instance_create(x, 0, CheckBox);
Pupper_check.Label = "Sprout Puppers";
Pupper_check.visible = false;
Pupper_check.Toggled = true;
Pupper_check.On_toggle = example_toggle;

Timer_check = instance_create(x, 0, CheckBox);
Timer_check.Label = "Use Timer";
Timer_check.visible = false;
Timer_check.Toggled = true;
Timer_check.On_toggle = example_toggle;

Party_check = instance_create(x, 0, CheckBox);
Party_check.Label = "Show Party";
Party_check.visible = false;
Party_check.Toggled = true;
Party_check.On_toggle = example_toggle;

Party_size_field = instance_create(x, 0, TextField);
Party_size_field.Text = string(1);
Party_size_field.Text_width    = 3;
Party_size_field.On_lost_focus = example_text_field;
Party_size_field.On_submit     = example_text_field;
Party_size_field.visible = false;

Party_max_field = instance_create(x, 0, TextField);
Party_max_field.Text = string(5);
Party_max_field.Text_width    = 3;
Party_max_field.On_lost_focus = example_text_field;
Party_max_field.On_submit     = example_text_field;
Party_max_field.visible = false;