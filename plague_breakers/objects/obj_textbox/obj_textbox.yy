{
    "id": "816156cc-564d-462f-874e-31376e165234",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_textbox",
    "eventList": [
        {
            "id": "e112cb5d-16f6-49cd-816c-896f2a320372",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "816156cc-564d-462f-874e-31376e165234"
        },
        {
            "id": "ad2dc0ba-c059-4dc5-970f-5df0f0758fd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "816156cc-564d-462f-874e-31376e165234"
        },
        {
            "id": "c7b82a9c-9683-4aea-b322-80c3d5810abb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "816156cc-564d-462f-874e-31376e165234"
        },
        {
            "id": "0d55a508-ee4c-4fe6-b185-dee387f13fcc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "816156cc-564d-462f-874e-31376e165234"
        },
        {
            "id": "ca49d0e6-5c65-4bc1-86f9-bc26ff5dcae6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 5,
            "m_owner": "816156cc-564d-462f-874e-31376e165234"
        },
        {
            "id": "638dfd0f-1aa3-4b20-aae9-87688c4065b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "816156cc-564d-462f-874e-31376e165234"
        },
        {
            "id": "a550d7c3-b099-4f83-b5b8-8bb41bad5726",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "816156cc-564d-462f-874e-31376e165234"
        },
        {
            "id": "ff63834b-d06f-4c6b-99ce-438e7b2104f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 6,
            "m_owner": "816156cc-564d-462f-874e-31376e165234"
        },
        {
            "id": "ea16c03c-12dc-4a90-a61b-386c7317d4a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "816156cc-564d-462f-874e-31376e165234"
        },
        {
            "id": "37e8886c-95ff-4a38-b0ed-da234256c9ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "816156cc-564d-462f-874e-31376e165234"
        },
        {
            "id": "59793de4-9067-415e-845d-e6c8591212d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "816156cc-564d-462f-874e-31376e165234"
        },
        {
            "id": "1f2da898-cc0b-4301-a36e-fa252256baa2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "816156cc-564d-462f-874e-31376e165234"
        },
        {
            "id": "a6abbefb-051d-49bd-9387-8ee17dbc8dd0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 10,
            "m_owner": "816156cc-564d-462f-874e-31376e165234"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "60027976-6d41-437e-9ab6-de7be070a220",
    "visible": true
}