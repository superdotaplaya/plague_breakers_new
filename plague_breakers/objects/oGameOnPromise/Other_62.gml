// Ignore HTTP events if we aren't pending a response
if(status != eGameOnPromiseStatus.PENDING)
	return;

// Bail if this HTTP response doesn't match our request ID
var reqId = async_load[? "id"];
if(reqId != request_id)
	return;
	
// Process our HTTP response
request_result_status = async_load[? "status"];
request_result_http_status = async_load[? "http_status"];
request_result_data = async_load[? "result"];

if(is_undefined(request_result_http_status))
	request_result_http_status = -1;

// Update download progress
var reqSize = async_load[? "contentLength"];
var reqDownloaded = async_load[? "sizeDownloaded"];
request_result_progress = 0;

if(is_undefined(reqSize) == false && is_undefined(reqDownloaded) == false)
	request_result_progress = clamp(reqDownloaded / reqSize, 0, 1);

// Handle the request result
if(request_result_status > 0)
{
	event_user(eGameOnPromiseEvent.PROGRESS);
}
else if(request_result_status == 0 || request_result_http_status == request_result_http_success_code)
{
	event_user(eGameOnPromiseEvent.RESPONSE);
}
else if(request_result_status < 0)
{
	event_user(eGameOnPromiseEvent.FAILURE);
}