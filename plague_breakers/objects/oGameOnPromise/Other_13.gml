/// @description Start HTTP request
event_user(eGameOnPromiseEvent.CLEAN_UP_RESULT);

request_result_status = undefined;
request_result_http_status = undefined;
request_result_data = undefined;
request_result_data_parsed = undefined;
request_result_progress = 0;
request_result_error = undefined;

status = eGameOnPromiseStatus.PENDING;
	
request_id = http_request(request_url, request_method, request_header, request_body);