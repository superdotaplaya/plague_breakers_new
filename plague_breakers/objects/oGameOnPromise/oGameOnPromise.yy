{
    "id": "6d6eb90d-22ce-42f0-b45c-d4b59286039c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameOnPromise",
    "eventList": [
        {
            "id": "5379e162-5080-462b-ad43-409c7cdbc767",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6d6eb90d-22ce-42f0-b45c-d4b59286039c"
        },
        {
            "id": "6c2da4ed-ad88-4db8-a4c2-69cc6cebe807",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 62,
            "eventtype": 7,
            "m_owner": "6d6eb90d-22ce-42f0-b45c-d4b59286039c"
        },
        {
            "id": "5b40fa31-039c-4564-b947-4e6591480b5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "6d6eb90d-22ce-42f0-b45c-d4b59286039c"
        },
        {
            "id": "beecb54a-8532-4f79-a084-8157bf4399b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "6d6eb90d-22ce-42f0-b45c-d4b59286039c"
        },
        {
            "id": "1723d252-54be-40cd-b4c1-542dcf704996",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "6d6eb90d-22ce-42f0-b45c-d4b59286039c"
        },
        {
            "id": "c00ccfcf-bf68-44fb-b0e1-fd80a790bb27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "6d6eb90d-22ce-42f0-b45c-d4b59286039c"
        },
        {
            "id": "053292a7-bcad-4e23-b093-cd71f843358e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "6d6eb90d-22ce-42f0-b45c-d4b59286039c"
        },
        {
            "id": "4cd0d90d-e5e8-4f03-9a9c-6aaea8eb6fcf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 15,
            "eventtype": 7,
            "m_owner": "6d6eb90d-22ce-42f0-b45c-d4b59286039c"
        },
        {
            "id": "d7ca17e7-1880-4229-a3f9-d7bb861537b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "6d6eb90d-22ce-42f0-b45c-d4b59286039c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": false,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}