/// @description HTTP Request Failed

// Try parsing error message and display it
request_result_error = "";
var errorDataParsed = json_decode(request_result_data);
if(errorDataParsed != -1)
{
	request_result_error = errorDataParsed[? "message"];
	
	// Check if error has constraints and add them to the message
	var errorDataConstraints = errorDataParsed[? "constraints"];
	if(!is_undefined(errorDataConstraints) && ds_exists(errorDataConstraints, ds_type_list))
	{
		request_result_error += ". Constraints: ";
		var constraintCount = ds_list_size(errorDataConstraints);
		for(var constraintIdx = 0; constraintIdx < constraintCount; ++constraintIdx)
		{
			request_result_error += (constraintIdx != 0 ? ", " : "") + string(errorDataConstraints[| constraintIdx]);
		}
	}
	
	ds_map_destroy(errorDataParsed);
}
else
{
	// Generic error codes if we didn't get a unique message back
	switch(request_result_http_status)
	{
		case 400: request_result_error = "Reason: Invalid request parameters"; break;
		case 401: request_result_error = "Reason: Unauthorized. Invalid Session ID provided."; break;
		case 403: request_result_error = "Reason: Forbidden. Invalid API key or player is banned."; break;
	}
}

gameon_util_log("Request " + string(request_url) + " failed with HTTP status code " + string(request_result_http_status), true);
gameon_util_log("Error Message: " + string(request_result_error), true);
gameon_util_log("Response data: " + string(request_result_data), true);

// Update status
status = eGameOnPromiseStatus.FAILED;

// Call failure user event on delegate
var eventIdx = delegate_event_failure;
var currentPromise = self;
if(eventIdx >= 0 && !is_undefined(delegate_instance) && instance_exists(delegate_instance))
{
	with(delegate_instance) 
	{ 
		delegate_current_promise = currentPromise;
		event_user(eventIdx);
		delegate_current_promise = undefined;
	}
}

// Call failure callback
if(is_undefined(callback_failed) == false)
	script_execute(callback_failed, self);
