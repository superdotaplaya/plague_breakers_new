/// @description HTTP Request Response Received
switch(request_result_http_status)
{
	case request_result_http_success_code: event_user(eGameOnPromiseEvent.SUCCESS); break;
	default: event_user(eGameOnPromiseEvent.FAILURE); break;
}