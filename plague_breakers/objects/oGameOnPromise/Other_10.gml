/// @description HTTP Request Successful
status = eGameOnPromiseStatus.SUCCESS;
gameon_util_log("Request successful. URL: " + string(request_url));

// Call success user event on delegate
var eventIdx = delegate_event_success;
var currentPromise = self;
if(eventIdx >= 0 && !is_undefined(delegate_instance) && instance_exists(delegate_instance))
{
	with(delegate_instance) 
	{ 
		delegate_current_promise = currentPromise;
		event_user(eventIdx); 
		delegate_current_promise = undefined;
	}
}

// Call success callback
if(!is_undefined(callback_success))
	script_execute(callback_success, self);