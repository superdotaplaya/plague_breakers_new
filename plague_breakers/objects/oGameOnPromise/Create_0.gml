// Promise user events
enum eGameOnPromiseEvent
{
	SUCCESS = 0,
	FAILURE = 1,
	PROGRESS = 2,
	REQUEST_START = 3,
	RESPONSE = 4,
	CLEAN_UP_RESULT = 5
};

enum eGameOnPromiseStatus
{
	INITIALIZED = 0,
	PENDING,
	SUCCESS,
	FAILED
};

// Members
request_url = undefined;
request_body = undefined;
request_method = undefined;
request_header = undefined;

callback_failed = undefined;
callback_success = undefined;

delegate_instance = undefined;
delegate_event_success = -1;
delegate_event_failure = -1;

request_id = undefined;

request_result_http_success_code = 200;

request_result_status = undefined;
request_result_http_status = undefined;
request_result_data = undefined;
request_result_data_parsed = undefined;
request_result_progress = 0;
request_result_error = undefined;

status = eGameOnPromiseStatus.INITIALIZED;