{
    "id": "dea794b8-2ebf-47ad-b7a7-43f9f650a437",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_info_button",
    "eventList": [
        {
            "id": "9ecce700-4919-4d85-89ab-d89bfb6c89d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "dea794b8-2ebf-47ad-b7a7-43f9f650a437"
        },
        {
            "id": "232112c5-9162-441e-a707-c53d2ca6e72c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dea794b8-2ebf-47ad-b7a7-43f9f650a437"
        },
        {
            "id": "aef418b0-5ddf-4955-90ef-f56e25bcadf5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dea794b8-2ebf-47ad-b7a7-43f9f650a437"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6826cbeb-67f8-4f65-b8bd-2950128ae748",
    "visible": true
}