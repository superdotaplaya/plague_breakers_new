{
    "id": "30082ecf-a284-473a-8176-7fe44ce08047",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_return_enemies",
    "eventList": [
        {
            "id": "a6df90d3-9952-4fa3-b9ed-dfe48211fe82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "30082ecf-a284-473a-8176-7fe44ce08047"
        },
        {
            "id": "34b704d1-e072-43f5-9450-4236d4889425",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "30082ecf-a284-473a-8176-7fe44ce08047"
        }
    ],
    "maskSpriteId": "8024de4a-5f56-4836-9a65-fd17fd8bed4b",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}