{
    "id": "c78d6a00-2346-4475-a3aa-c8904e073ace",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_rebind_shop6",
    "eventList": [
        {
            "id": "bd53fe0d-97f4-4336-ad2b-6eb34e782db6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c78d6a00-2346-4475-a3aa-c8904e073ace"
        },
        {
            "id": "ee4fc92b-1490-4ec8-8bc6-949465238b2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c78d6a00-2346-4475-a3aa-c8904e073ace"
        },
        {
            "id": "202f56ee-d77a-4680-bf23-66dc6c844914",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "c78d6a00-2346-4475-a3aa-c8904e073ace"
        },
        {
            "id": "bfae98b9-2a19-4c7c-a27c-652c12bea1f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "c78d6a00-2346-4475-a3aa-c8904e073ace"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a832f2b-2273-4cc3-b553-8c06b7dc617f",
    "visible": true
}