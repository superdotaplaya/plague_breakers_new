{
    "id": "a9ead9fc-0797-4bb8-b9ba-c233ba9d072e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tesla_attack_range_up",
    "eventList": [
        {
            "id": "727ae690-4f47-4aa9-b89c-7e4eb1528ffb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a9ead9fc-0797-4bb8-b9ba-c233ba9d072e"
        },
        {
            "id": "76f523de-3e05-4b35-9777-50951e466de8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a9ead9fc-0797-4bb8-b9ba-c233ba9d072e"
        },
        {
            "id": "db89e261-9443-4923-abda-1247b1919c38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "a9ead9fc-0797-4bb8-b9ba-c233ba9d072e"
        },
        {
            "id": "a5f545de-4050-4270-adf5-cd2b6de63849",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "a9ead9fc-0797-4bb8-b9ba-c233ba9d072e"
        },
        {
            "id": "36d0ba6b-5a7c-46f9-83b5-8c5329945d76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "a9ead9fc-0797-4bb8-b9ba-c233ba9d072e"
        },
        {
            "id": "14a5ad7c-2147-49ab-abc8-0b663d46eb5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a9ead9fc-0797-4bb8-b9ba-c233ba9d072e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "45cfa927-ae76-40bc-80ed-369d8ccc3501",
    "visible": true
}