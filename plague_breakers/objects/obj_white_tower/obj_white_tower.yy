{
    "id": "5c37c902-777c-43b5-a437-9dca52f80486",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_white_tower",
    "eventList": [
        {
            "id": "0b9fb177-d3a5-4173-804f-b5115adbae4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5c37c902-777c-43b5-a437-9dca52f80486"
        },
        {
            "id": "1b82f451-8ab3-4b01-9da0-c419f0542612",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5c37c902-777c-43b5-a437-9dca52f80486"
        },
        {
            "id": "63c8b213-a175-4fd8-a173-e38049d2f805",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "5c37c902-777c-43b5-a437-9dca52f80486"
        },
        {
            "id": "fb652ed5-afcb-4e32-88f2-83666df08b83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "5c37c902-777c-43b5-a437-9dca52f80486"
        },
        {
            "id": "df62b0bd-daca-451b-aab7-82d41688a543",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5c37c902-777c-43b5-a437-9dca52f80486"
        },
        {
            "id": "eed19a3d-828c-4211-97a6-4c9f811653a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5c37c902-777c-43b5-a437-9dca52f80486"
        },
        {
            "id": "2c5822eb-2410-424e-8b58-723d31b9a885",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "5c37c902-777c-43b5-a437-9dca52f80486"
        },
        {
            "id": "6b38c82d-41b8-4e00-b6a7-d0691d29b836",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "5c37c902-777c-43b5-a437-9dca52f80486"
        },
        {
            "id": "5c7bde72-15fb-459d-998a-085cf0953c15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "5c37c902-777c-43b5-a437-9dca52f80486"
        },
        {
            "id": "6dfc2de8-f342-4428-9c7d-9b59196466fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "5c37c902-777c-43b5-a437-9dca52f80486"
        },
        {
            "id": "729ad964-c2f6-4c77-a0c1-8f3c61278fe1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "5c37c902-777c-43b5-a437-9dca52f80486"
        },
        {
            "id": "48171bff-95b1-4580-ba3d-1b1b1877b255",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "5c37c902-777c-43b5-a437-9dca52f80486"
        },
        {
            "id": "61f7de55-f09a-4255-8de1-759427f85ebd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5c37c902-777c-43b5-a437-9dca52f80486"
        },
        {
            "id": "4e710f46-23be-4708-a0d9-e4232965fb06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "f0875ffd-fd8e-428c-aabc-956eb91215a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5c37c902-777c-43b5-a437-9dca52f80486"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "92de3f75-7cf1-48b2-ae14-43cffb5901c3",
    "visible": true
}