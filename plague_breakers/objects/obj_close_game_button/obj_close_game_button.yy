{
    "id": "a60b157a-11e0-4985-8303-7d04372d4230",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_close_game_button",
    "eventList": [
        {
            "id": "9f719073-a145-48e0-856e-f49861e6fc3b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "a60b157a-11e0-4985-8303-7d04372d4230"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e1e0b95f-ad8a-4e3b-adfd-d2dc213ad78b",
    "visible": true
}