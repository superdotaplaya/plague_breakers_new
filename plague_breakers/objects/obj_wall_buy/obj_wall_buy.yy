{
    "id": "769e7b14-4bf0-442f-a5a3-f5dc8b61975d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wall_buy",
    "eventList": [
        {
            "id": "4a0238ec-e7a7-42c1-acac-b7cc5727939c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "769e7b14-4bf0-442f-a5a3-f5dc8b61975d"
        },
        {
            "id": "ac783b2f-4d94-4c52-9964-84ce62754d8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "769e7b14-4bf0-442f-a5a3-f5dc8b61975d"
        },
        {
            "id": "e9367517-e0a8-4e4a-9b93-afeca61bd080",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "769e7b14-4bf0-442f-a5a3-f5dc8b61975d"
        },
        {
            "id": "454f7eab-feec-4283-a13a-366245ba8875",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "769e7b14-4bf0-442f-a5a3-f5dc8b61975d"
        },
        {
            "id": "1b1e20f4-2e93-4088-b89d-0927b4f6c214",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "769e7b14-4bf0-442f-a5a3-f5dc8b61975d"
        },
        {
            "id": "c3dbccb3-2bd8-4005-b876-6f572ce2a516",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "769e7b14-4bf0-442f-a5a3-f5dc8b61975d"
        },
        {
            "id": "d4f92b22-3a46-4e4e-96a8-3a4bdb792f7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "769e7b14-4bf0-442f-a5a3-f5dc8b61975d"
        },
        {
            "id": "e43d3e71-2407-4b25-84fe-5bc94468841d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "769e7b14-4bf0-442f-a5a3-f5dc8b61975d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8790704c-191e-4222-95ed-89cd5354d33d",
    "visible": true
}