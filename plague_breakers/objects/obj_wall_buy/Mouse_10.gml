/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 3C21E6D7
/// @DnDArgument : "expr" ""A wall that blocks enemies until it is destroyed""
/// @DnDArgument : "var" "global.tooltip"
global.tooltip = "A wall that blocks enemies until it is destroyed";

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 59ECDBA0
/// @DnDArgument : "code" "image_xscale = 1.2$(13_10)image_yscale = 1.7$(13_10)instance_create_layer(x-160,y-450,"shop_icons",obj_tooltips)"
image_xscale = 1.2
image_yscale = 1.7
instance_create_layer(x-160,y-450,"shop_icons",obj_tooltips)