/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 18FCA3D3
/// @DnDArgument : "code" "draw_self()$(13_10)if collected >= 1 then$(13_10){$(13_10)draw_sprite_stretched(spr_tooltip,0,x-60,y-70,120,60)$(13_10)draw_text(x-47,y-70,collected)$(13_10)}$(13_10)if hp < 100 then$(13_10){$(13_10)draw_healthbar(x-20,y-20,x+20,y,(hp/100)*100,c_black,c_red,c_green,0,true,false)	$(13_10)}"
draw_self()
if collected >= 1 then
{
draw_sprite_stretched(spr_tooltip,0,x-60,y-70,120,60)
draw_text(x-47,y-70,collected)
}
if hp < 100 then
{
draw_healthbar(x-20,y-20,x+20,y,(hp/100)*100,c_black,c_red,c_green,0,true,false)	
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 79D65F68
/// @DnDArgument : "var" "hover"
/// @DnDArgument : "value" "true"
if(hover == true)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 1DF4AC87
	/// @DnDParent : 79D65F68
	/// @DnDArgument : "alpha" ".5"
	draw_set_alpha(.5);

	/// @DnDAction : YoYo Games.Drawing.Draw_Gradient_Ellipse
	/// @DnDVersion : 1
	/// @DnDHash : 06FE5E1F
	/// @DnDParent : 79D65F68
	/// @DnDArgument : "x1" "x -range"
	/// @DnDArgument : "y1" "y -range"
	/// @DnDArgument : "x2" "x + range"
	/// @DnDArgument : "y2" "y + range"
	/// @DnDArgument : "col1" "$FFFFFFFF"
	/// @DnDArgument : "col2" "$FFB3B3B3"
	/// @DnDArgument : "fill" "1"
	draw_ellipse_colour(x -range, y -range, x + range, y + range, $FFFFFFFF & $FFFFFF, $FFB3B3B3 & $FFFFFF, 0);

	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 37D85086
	/// @DnDParent : 79D65F68
	draw_set_alpha(1);
}