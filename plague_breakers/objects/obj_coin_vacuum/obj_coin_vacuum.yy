{
    "id": "b3cb2dcd-08ed-4097-9aa8-dcbc23463fe2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_coin_vacuum",
    "eventList": [
        {
            "id": "19c6b06c-0799-47c7-9e97-52a7475c526f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b3cb2dcd-08ed-4097-9aa8-dcbc23463fe2"
        },
        {
            "id": "8e8b773e-fe54-4415-8aa9-e7a0cb4a4ed4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b3cb2dcd-08ed-4097-9aa8-dcbc23463fe2"
        },
        {
            "id": "b79c9838-aa43-44f5-b2dc-5d6ec937e9d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b3cb2dcd-08ed-4097-9aa8-dcbc23463fe2"
        },
        {
            "id": "c2cee715-0347-4654-b61a-db22d772c27c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b3cb2dcd-08ed-4097-9aa8-dcbc23463fe2"
        },
        {
            "id": "abcf65a8-9bf9-4bf1-91b2-1b2a138a3717",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "f0875ffd-fd8e-428c-aabc-956eb91215a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b3cb2dcd-08ed-4097-9aa8-dcbc23463fe2"
        },
        {
            "id": "d00cb6ff-ca7f-4974-abf3-bbed37acd8cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "b3cb2dcd-08ed-4097-9aa8-dcbc23463fe2"
        },
        {
            "id": "31787692-54e0-47d6-9ef2-5dbe04fc3c9e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "b3cb2dcd-08ed-4097-9aa8-dcbc23463fe2"
        },
        {
            "id": "3bc1c93e-d9d6-4a12-b818-fce4b81369c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b3cb2dcd-08ed-4097-9aa8-dcbc23463fe2"
        },
        {
            "id": "3d474ea2-bfc1-45c2-8c30-a0a878a9c12f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "b3cb2dcd-08ed-4097-9aa8-dcbc23463fe2"
        },
        {
            "id": "9d310044-bb92-41a8-828d-48b3aab102c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "b3cb2dcd-08ed-4097-9aa8-dcbc23463fe2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "45f085ce-2bc3-400d-a76d-c59ff7a6103c",
    "visible": true
}