/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 55D69F06
/// @DnDArgument : "code" "if !instance_exists(obj_gold) then$(13_10){$(13_10)target = instance_nearest(x,y,all)	$(13_10)} else$(13_10){$(13_10)target = instance_nearest(x,y,obj_gold)$(13_10)}$(13_10)move_towards_point(target.x,target.y,0)$(13_10)$(13_10)if hp <= 0 then$(13_10){$(13_10)instance_destroy()	$(13_10)instance_create_layer(x,y,"Instances_towers",obj_return_enemies)$(13_10)}"
if !instance_exists(obj_gold) then
{
target = instance_nearest(x,y,all)	
} else
{
target = instance_nearest(x,y,obj_gold)
}
move_towards_point(target.x,target.y,0)

if hp <= 0 then
{
instance_destroy()	
instance_create_layer(x,y,"Instances_towers",obj_return_enemies)
}