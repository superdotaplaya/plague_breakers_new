{
    "id": "b0acdc6d-c837-4818-b5c6-27ddb536fadc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_red_range_up",
    "eventList": [
        {
            "id": "10219d5c-a24e-47da-90a5-a253b38f92ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b0acdc6d-c837-4818-b5c6-27ddb536fadc"
        },
        {
            "id": "35e7ffa5-98be-4a6c-88a7-e12fce5da3a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b0acdc6d-c837-4818-b5c6-27ddb536fadc"
        },
        {
            "id": "d5d3b08c-23ae-487c-8ec2-cc6f15c8f425",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "b0acdc6d-c837-4818-b5c6-27ddb536fadc"
        },
        {
            "id": "eb1688d8-4ad1-4e68-9ac7-669cfe26ba26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "b0acdc6d-c837-4818-b5c6-27ddb536fadc"
        },
        {
            "id": "7439ed9d-422f-434e-b30b-8b6e5b9996ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b0acdc6d-c837-4818-b5c6-27ddb536fadc"
        },
        {
            "id": "db8b301a-5270-4e16-b5e4-0057c64f8ef9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b0acdc6d-c837-4818-b5c6-27ddb536fadc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "afd048dc-5f11-4a66-a632-1d454265b957",
    "visible": true
}