/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 0A8AFA18
/// @DnDArgument : "code" "if global.autostart = false then$(13_10){$(13_10)global.autostart = true$(13_10)log("[AUTO START FUNCTION]   Autostart waves enabled")$(13_10)} else$(13_10)if global.autostart = true then$(13_10){$(13_10)global.autostart = false	$(13_10)log("[AUTO START FUNCTION]   Autostart waves disabled")$(13_10)}"
if global.autostart = false then
{
global.autostart = true
log("[AUTO START FUNCTION]   Autostart waves enabled")
} else
if global.autostart = true then
{
global.autostart = false	
log("[AUTO START FUNCTION]   Autostart waves disabled")
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 62684D54
/// @DnDArgument : "var" "global.autostart"
/// @DnDArgument : "value" "true"
if(global.autostart == true)
{
	/// @DnDAction : YoYo Games.Instances.Set_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 494EE763
	/// @DnDParent : 62684D54
	/// @DnDArgument : "spriteind" "spr_auto_start_on"
	/// @DnDSaveInfo : "spriteind" "53451810-dd54-4dca-8431-ebba1e786091"
	sprite_index = spr_auto_start_on;
	image_index = 0;
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 2B2C5E80
else
{
	/// @DnDAction : YoYo Games.Instances.Set_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 4F455003
	/// @DnDParent : 2B2C5E80
	/// @DnDArgument : "spriteind" "spr_auto_start"
	/// @DnDSaveInfo : "spriteind" "e7ce6e11-c388-4c41-b619-9ba8ec6982d7"
	sprite_index = spr_auto_start;
	image_index = 0;
}