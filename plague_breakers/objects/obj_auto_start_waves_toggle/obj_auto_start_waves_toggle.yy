{
    "id": "ccd4ea8c-493d-4f92-a2e1-2e90933641a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_auto_start_waves_toggle",
    "eventList": [
        {
            "id": "cc92071e-cfe6-4879-bca0-c154fae1633d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ccd4ea8c-493d-4f92-a2e1-2e90933641a5"
        },
        {
            "id": "ed8d64e1-8562-4cb3-a7c6-86e4f3ad89be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ccd4ea8c-493d-4f92-a2e1-2e90933641a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e7ce6e11-c388-4c41-b619-9ba8ec6982d7",
    "visible": true
}