{
    "id": "f514efd6-2758-4d78-bce7-3376a3055a12",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "rousrDissonance",
    "eventList": [
        {
            "id": "b7cc0237-d67f-4919-a8f1-47e5b9c036c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f514efd6-2758-4d78-bce7-3376a3055a12"
        },
        {
            "id": "c69adb3e-76e7-4f55-8acb-290a01ef52ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "f514efd6-2758-4d78-bce7-3376a3055a12"
        },
        {
            "id": "22b4a617-134f-4788-8816-08f413221124",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f514efd6-2758-4d78-bce7-3376a3055a12"
        },
        {
            "id": "294fe1e3-d5e6-40fa-abe5-7fdba7782c4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 70,
            "eventtype": 7,
            "m_owner": "f514efd6-2758-4d78-bce7-3376a3055a12"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}