{
    "id": "e2b49a6d-6db2-4ec5-a186-b25158aff264",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_red_attack",
    "eventList": [
        {
            "id": "65faafce-d6af-4876-9781-59e218f7fbf5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e2b49a6d-6db2-4ec5-a186-b25158aff264"
        },
        {
            "id": "60983bff-03a6-4d59-8593-2394f2f05458",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e2b49a6d-6db2-4ec5-a186-b25158aff264"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f8636494-beb1-4902-9739-ccc4fd8e31f0",
    "visible": true
}