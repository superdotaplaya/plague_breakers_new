{
    "id": "d2a5ac99-b5d0-40cd-95c5-11df01d2059d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_green_attack_range_up",
    "eventList": [
        {
            "id": "c1814e01-0eae-4729-907e-78789f254413",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d2a5ac99-b5d0-40cd-95c5-11df01d2059d"
        },
        {
            "id": "2754e71e-d75b-4c05-a7ca-4c8ecd6fcf8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d2a5ac99-b5d0-40cd-95c5-11df01d2059d"
        },
        {
            "id": "badead6c-33af-460f-b750-a0ba880ec633",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "d2a5ac99-b5d0-40cd-95c5-11df01d2059d"
        },
        {
            "id": "fe8ed60e-c6ac-47d7-8936-9a4f955475d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "d2a5ac99-b5d0-40cd-95c5-11df01d2059d"
        },
        {
            "id": "f52bad97-9328-4ff7-9f56-c281380c2fb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d2a5ac99-b5d0-40cd-95c5-11df01d2059d"
        },
        {
            "id": "3113302c-5459-4a74-a2c4-a2d01fc580de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d2a5ac99-b5d0-40cd-95c5-11df01d2059d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ff17cdf8-f43a-45c6-8068-c71366134d73",
    "visible": true
}