/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 15492D90
/// @DnDArgument : "code" "// spawns enemies at the rate defined earlier in the create event and handles some of the discord rich prescence information$(13_10)alarm_set(0,rate)$(13_10)if global.units_spawned < global.units_to_spawn then$(13_10){$(13_10)$(13_10)instance_create_layer(0,0,"Instances_1",obj_enemy)$(13_10)global.units_spawned += 1$(13_10)global.units_alive += 1$(13_10)alarm_set(0,rate)$(13_10)}"
// spawns enemies at the rate defined earlier in the create event and handles some of the discord rich prescence information
alarm_set(0,rate)
if global.units_spawned < global.units_to_spawn then
{

instance_create_layer(0,0,"Instances_1",obj_enemy)
global.units_spawned += 1
global.units_alive += 1
alarm_set(0,rate)
}