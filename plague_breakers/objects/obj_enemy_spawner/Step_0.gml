/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 30D29B87
/// @DnDArgument : "code" "// @description handles saving, advancing to the next wave, and uploading scores to gameon$(13_10)if global.units_spawned = global.units_to_spawn and global.units_alive = 0 then$(13_10){$(13_10)// Handles moving ot the next wave when a wave is completed$(13_10)global.currentlevel += 1	$(13_10)global.units_to_spawn = global.currentlevel + 10$(13_10)global.units_spawned = 0$(13_10)alarm_set(0,-1)$(13_10)// Creates the next wave button and grants the player bonus gold for the completion of the wave$(13_10)instance_create_layer(2400,1000,"Instances_1",obj_next_level)$(13_10)global.gold += global.currentlevel + 2$(13_10)audio_play_sound(snd_round_bonus,1,0)$(13_10)// Gives the payer shields for completing the wave$(13_10)global.premium_currency += 1$(13_10)instance_create(room_width/2,room_height/2,obj_premium_currency_add)$(13_10)audio_play_sound(snd_premium_collected,2,0)$(13_10)ds_map_add(global.map, 5, 1);$(13_10)ds_map_replace(global.map, "currency", global.premium_currency);$(13_10)// encrypts the file which contains the players shields$(13_10)ds_map_secure_save(global.map, "info.sav");$(13_10)global.daily_level += 1$(13_10)// Submiits the players completed wave to the leaderboard$(13_10)if global.currentlevel >= 20 then$(13_10){$(13_10)gameon_match_enter("773f5620-182a-4dee-811f-2019887b5925")	$(13_10)gameon_match_score("773f5620-182a-4dee-811f-2019887b5925",global.currentlevel)$(13_10)}$(13_10)// saves the game for reloading later$(13_10)game_save("autosave.dat")$(13_10)// Updates the version of the game to the new current version to allow reloading the game with the correct version$(13_10)ds_map_replace(global.map,"Version",global.versionCurrent)$(13_10)ds_map_secure_save(global.map,"info.sav")$(13_10)log("[SAVE GAME]   Wave completed and game has been saved")$(13_10)}$(13_10)"
// l30D29B87_0 handles saving, advancing to the next wave, and uploading scores to gameon
if global.units_spawned = global.units_to_spawn and global.units_alive = 0 then
{
// Handles moving ot the next wave when a wave is completed
global.currentlevel += 1	
global.units_to_spawn = global.currentlevel + 10
global.units_spawned = 0
alarm_set(0,-1)
// Creates the next wave button and grants the player bonus gold for the completion of the wave
instance_create_layer(2400,1000,"Instances_1",obj_next_level)
global.gold += global.currentlevel + 2
audio_play_sound(snd_round_bonus,1,0)
// Gives the payer shields for completing the wave
global.premium_currency += 1
instance_create(room_width/2,room_height/2,obj_premium_currency_add)
audio_play_sound(snd_premium_collected,2,0)
ds_map_add(global.map, 5, 1);
ds_map_replace(global.map, "currency", global.premium_currency);
// encrypts the file which contains the players shields
ds_map_secure_save(global.map, "info.sav");
global.daily_level += 1
// Submiits the players completed wave to the leaderboard
if global.currentlevel >= 20 then
{
gameon_match_enter("773f5620-182a-4dee-811f-2019887b5925")	
gameon_match_score("773f5620-182a-4dee-811f-2019887b5925",global.currentlevel)
}
// saves the game for reloading later
game_save("autosave.dat")
// Updates the version of the game to the new current version to allow reloading the game with the correct version
ds_map_replace(global.map,"Version",global.versionCurrent)
ds_map_secure_save(global.map,"info.sav")
log("[SAVE GAME]   Wave completed and game has been saved")
}