/// @description Parse JSON
event_inherited();

// Parse normal match data
var neighborJSON = json_raw_data[? "matches"];
if(!is_undefined(neighborJSON))
{
	matches = ds_list_create();
	gameon_util_data_list_parse(json_raw_data, matches, oGameOnMatch, "matches");
}

// Parse player match data
var leaderboardJSON = json_raw_data[? "playerMatches"];
if(!is_undefined(leaderboardJSON))
{
	playerMatches = ds_list_create();
	gameon_util_data_list_parse(json_raw_data, playerMatches, oGameOnMatch, "playerMatches");
}