if(!is_undefined(matches) && ds_exists(matches, ds_type_list))
	ds_list_destroy(matches);
	
if(!is_undefined(playerMatches) && ds_exists(playerMatches, ds_type_list))
	ds_list_destroy(playerMatches);
	
matches = undefined;
playerMatches = undefined;

event_inherited();