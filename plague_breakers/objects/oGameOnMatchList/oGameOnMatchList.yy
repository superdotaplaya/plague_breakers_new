{
    "id": "a5a48e16-8439-44b7-a369-718ab28bcb08",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameOnMatchList",
    "eventList": [
        {
            "id": "55e6608e-fde1-4a69-b996-322d6e5f5991",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "a5a48e16-8439-44b7-a369-718ab28bcb08"
        },
        {
            "id": "7e2843c1-c66b-45f1-9a09-78e86234794a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a5a48e16-8439-44b7-a369-718ab28bcb08"
        },
        {
            "id": "a7d28058-ca1d-49a6-9cf9-06cc98e7fe61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "a5a48e16-8439-44b7-a369-718ab28bcb08"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0330c072-566e-4a20-bbb9-c6edf5a161cf",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}