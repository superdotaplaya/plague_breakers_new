{
    "id": "9d51a110-9d73-4b27-b6c5-ad58fbcf27da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "CheckBox",
    "eventList": [
        {
            "id": "daf62d24-a654-4e0e-b092-ad3bb7914ba6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9d51a110-9d73-4b27-b6c5-ad58fbcf27da"
        },
        {
            "id": "6a52402f-66b6-4df5-8c8a-6a87beff49ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "9d51a110-9d73-4b27-b6c5-ad58fbcf27da"
        },
        {
            "id": "dafc3689-aedc-417a-a660-b552521634bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9d51a110-9d73-4b27-b6c5-ad58fbcf27da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}