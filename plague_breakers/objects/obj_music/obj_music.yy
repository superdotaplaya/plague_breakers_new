{
    "id": "44996411-de9c-4d33-81f5-b14ec2484c3c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_music",
    "eventList": [
        {
            "id": "7be34f29-6adf-438e-8f76-2b3d10bacbd3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "44996411-de9c-4d33-81f5-b14ec2484c3c"
        },
        {
            "id": "54bfcdde-bfc0-4dfe-b817-65f0074bc93a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "44996411-de9c-4d33-81f5-b14ec2484c3c"
        },
        {
            "id": "66c6ed32-b91e-4129-b630-b00aadf7ad26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 122,
            "eventtype": 9,
            "m_owner": "44996411-de9c-4d33-81f5-b14ec2484c3c"
        },
        {
            "id": "72f66b53-c528-45ce-8f38-046c81942a31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "44996411-de9c-4d33-81f5-b14ec2484c3c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}