{
    "id": "0330c072-566e-4a20-bbb9-c6edf5a161cf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameOnData",
    "eventList": [
        {
            "id": "9df7ffa3-8611-4ef7-84be-af68ddee2820",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0330c072-566e-4a20-bbb9-c6edf5a161cf"
        },
        {
            "id": "538703d2-1304-4e68-87a0-5df4b72e091a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "0330c072-566e-4a20-bbb9-c6edf5a161cf"
        },
        {
            "id": "d30deb99-f1c8-43db-bd17-7ff35abb7622",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "0330c072-566e-4a20-bbb9-c6edf5a161cf"
        },
        {
            "id": "8999aca2-a233-4819-91c4-529afe993c7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "0330c072-566e-4a20-bbb9-c6edf5a161cf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}