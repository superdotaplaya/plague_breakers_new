/// @description Load from JSON

if(is_undefined(json_raw_data))
{
	gameon_util_log("Could not parse data - raw JSON data is undefined", true, undefined, true);
	return;	
}

if(!ds_exists(json_raw_data, ds_type_map) && !ds_exists(json_raw_data, ds_type_list))
{
	gameon_util_log("Could not parse data - raw JSON data is invalid", true, undefined, true);
	return;	
}