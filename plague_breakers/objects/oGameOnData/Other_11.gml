/// @description Save to JSON
if(!is_undefined(json_raw_data) && ds_exists(json_raw_data, ds_type_map))
	ds_map_clear(json_raw_data);
else
	json_raw_data = ds_map_create();