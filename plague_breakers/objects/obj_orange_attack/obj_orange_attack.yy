{
    "id": "bac4d5f9-44f3-4434-aa73-829bda86ce59",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_orange_attack",
    "eventList": [
        {
            "id": "02230c4c-a672-47b0-ae9d-6d663778987c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bac4d5f9-44f3-4434-aa73-829bda86ce59"
        },
        {
            "id": "d66477f4-9c57-4b2f-987f-26d5ef9b440f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "bac4d5f9-44f3-4434-aa73-829bda86ce59"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f3f49e62-29bf-4389-aae4-b22d528fccb0",
    "visible": true
}