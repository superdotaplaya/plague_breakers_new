{
    "id": "b69a5d5d-926a-4cc5-b306-6b0be9c55b19",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cannon_tower_shop",
    "eventList": [
        {
            "id": "a93e0b99-d927-493c-917b-7f80430f68f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b69a5d5d-926a-4cc5-b306-6b0be9c55b19"
        },
        {
            "id": "949632c1-3951-4b1b-b6ee-1c7b2e97610e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b69a5d5d-926a-4cc5-b306-6b0be9c55b19"
        },
        {
            "id": "75a2cb83-957d-4b89-940f-0351ffbbb01a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b69a5d5d-926a-4cc5-b306-6b0be9c55b19"
        },
        {
            "id": "0e1f82d8-9197-4b12-8540-641d78addb16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "b69a5d5d-926a-4cc5-b306-6b0be9c55b19"
        },
        {
            "id": "945194bd-afb7-4821-b59a-299532fc9d0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "b69a5d5d-926a-4cc5-b306-6b0be9c55b19"
        },
        {
            "id": "17a1089e-6d6f-4aa6-a83e-ed3bed442e41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b69a5d5d-926a-4cc5-b306-6b0be9c55b19"
        },
        {
            "id": "0c85c322-a39b-4cf6-b54e-fa007f184f6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "b69a5d5d-926a-4cc5-b306-6b0be9c55b19"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "22b2a54a-e74f-4bcc-b8e1-19fa267438ec",
    "visible": true
}