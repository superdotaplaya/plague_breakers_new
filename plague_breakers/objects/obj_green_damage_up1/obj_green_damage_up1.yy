{
    "id": "be61b32a-8c4e-4b4d-9925-6965634dbb17",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_green_damage_up1",
    "eventList": [
        {
            "id": "141e2a50-ca8a-494f-9839-01cb81a05149",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "be61b32a-8c4e-4b4d-9925-6965634dbb17"
        },
        {
            "id": "ef70bdba-6fc6-4bcf-a3d1-87eb6e219896",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "be61b32a-8c4e-4b4d-9925-6965634dbb17"
        },
        {
            "id": "2d4368a6-faf3-4ba6-b7d5-a502fa4f2230",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "be61b32a-8c4e-4b4d-9925-6965634dbb17"
        },
        {
            "id": "c92a30ba-fcde-4515-b209-1213880af82c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "be61b32a-8c4e-4b4d-9925-6965634dbb17"
        },
        {
            "id": "efba4a20-c2d3-44aa-9038-a356f9c66523",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "be61b32a-8c4e-4b4d-9925-6965634dbb17"
        },
        {
            "id": "ad7627a8-0923-48c5-aca2-651d15d52527",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "be61b32a-8c4e-4b4d-9925-6965634dbb17"
        },
        {
            "id": "9f40870c-b54e-4c54-9374-c14576f80dbd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "be61b32a-8c4e-4b4d-9925-6965634dbb17"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ff17cdf8-f43a-45c6-8068-c71366134d73",
    "visible": true
}