{
    "id": "b1324da9-7d47-4d53-9da2-8d713b110ca5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_yellow_range_up",
    "eventList": [
        {
            "id": "0eca0bd1-8a09-4e8c-a599-a97fdd6ef03d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b1324da9-7d47-4d53-9da2-8d713b110ca5"
        },
        {
            "id": "c6cb5293-ca8e-4f59-99ae-99ce6927fc9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b1324da9-7d47-4d53-9da2-8d713b110ca5"
        },
        {
            "id": "e3f8871a-6961-4c29-b9fd-f74d8c258a99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "b1324da9-7d47-4d53-9da2-8d713b110ca5"
        },
        {
            "id": "96ed8020-23e6-469e-990b-9ee079946b03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "b1324da9-7d47-4d53-9da2-8d713b110ca5"
        },
        {
            "id": "3e2e8f50-6869-4b02-9d90-d5dcc39f22a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b1324da9-7d47-4d53-9da2-8d713b110ca5"
        },
        {
            "id": "86e63981-3c1c-4ef2-8137-df0525146b7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b1324da9-7d47-4d53-9da2-8d713b110ca5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c82849b0-f7ad-4a5d-aadd-977a6138ee59",
    "visible": true
}