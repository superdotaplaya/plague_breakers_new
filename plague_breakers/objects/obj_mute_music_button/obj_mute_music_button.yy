{
    "id": "1bd6e42c-b49a-4c55-a329-d8e520ace452",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mute_music_button",
    "eventList": [
        {
            "id": "a03ac08b-bdf4-46f1-abf3-bac2dd9e07fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "1bd6e42c-b49a-4c55-a329-d8e520ace452"
        },
        {
            "id": "aeb46ef3-b2f9-447a-acf7-3f1b4687d482",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1bd6e42c-b49a-4c55-a329-d8e520ace452"
        },
        {
            "id": "7f83e308-342e-4103-bc63-d371fdc22fe8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1bd6e42c-b49a-4c55-a329-d8e520ace452"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3b3bff45-fa73-4183-95aa-099898b87b1c",
    "visible": true
}