/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 590AA24F
/// @DnDArgument : "sound" "snd_song_001"
/// @DnDArgument : "volume" "0"
/// @DnDSaveInfo : "sound" "74394b18-9603-4db2-99d6-82c499677b8a"
audio_sound_gain(snd_song_001, 0, 0);

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 4E012931
/// @DnDArgument : "sound" "snd_song_001_LOOP"
/// @DnDArgument : "volume" "0"
/// @DnDSaveInfo : "sound" "d6477212-dc94-449c-998f-fe175b2dfce2"
audio_sound_gain(snd_song_001_LOOP, 0, 0);

/// @DnDAction : YoYo Games.Instances.Create_Instance
/// @DnDVersion : 1
/// @DnDHash : 11E8BDB3
/// @DnDArgument : "xpos" "x"
/// @DnDArgument : "ypos" "y"
/// @DnDArgument : "objectid" "obj_unmute_music"
/// @DnDArgument : "layer" ""Instances_1""
/// @DnDSaveInfo : "objectid" "7a1bfa0a-00cf-42f7-acd5-7958a95e4fdd"
instance_create_layer(x, y, "Instances_1", obj_unmute_music);

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 51729970
/// @DnDArgument : "expr" "true"
/// @DnDArgument : "var" "global.muted"
global.muted = true;

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 7495CDF1
instance_destroy();