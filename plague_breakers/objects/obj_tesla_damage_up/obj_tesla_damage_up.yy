{
    "id": "373bd00c-8c46-4839-8256-28c72a6fc26a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tesla_damage_up",
    "eventList": [
        {
            "id": "0637bdf1-ea40-44d8-b9a1-701561df0b0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "373bd00c-8c46-4839-8256-28c72a6fc26a"
        },
        {
            "id": "e2cbff74-559e-4e6c-a2b9-df026f73b7b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "373bd00c-8c46-4839-8256-28c72a6fc26a"
        },
        {
            "id": "31e6ca54-81f9-4966-a6a4-6ac7f7fd7b02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "373bd00c-8c46-4839-8256-28c72a6fc26a"
        },
        {
            "id": "73800fa6-922b-48ad-998b-99da3fe17366",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "373bd00c-8c46-4839-8256-28c72a6fc26a"
        },
        {
            "id": "eed424c0-fa4a-4af9-8599-854995d3811d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "373bd00c-8c46-4839-8256-28c72a6fc26a"
        },
        {
            "id": "c1c98402-f9fa-4b8a-a754-8da3888fca8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "373bd00c-8c46-4839-8256-28c72a6fc26a"
        },
        {
            "id": "1b5475a1-6e79-4b6e-afff-54522a2c3e86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "373bd00c-8c46-4839-8256-28c72a6fc26a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "45cfa927-ae76-40bc-80ed-369d8ccc3501",
    "visible": true
}