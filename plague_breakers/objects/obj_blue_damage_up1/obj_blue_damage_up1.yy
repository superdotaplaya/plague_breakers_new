{
    "id": "8ae6c06b-2c3b-4787-b574-78c7112cddf2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blue_damage_up1",
    "eventList": [
        {
            "id": "d4f2d690-0d6e-4247-ac90-308e762b798f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8ae6c06b-2c3b-4787-b574-78c7112cddf2"
        },
        {
            "id": "510d6eb8-73b7-4878-b0fb-483b0769b69e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8ae6c06b-2c3b-4787-b574-78c7112cddf2"
        },
        {
            "id": "06725cb4-507c-48ed-9e30-792990a00a30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "8ae6c06b-2c3b-4787-b574-78c7112cddf2"
        },
        {
            "id": "e1b57320-c06a-4650-9501-84af4c6e793b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "8ae6c06b-2c3b-4787-b574-78c7112cddf2"
        },
        {
            "id": "02850392-adfa-4c3f-8410-7c80ca953470",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8ae6c06b-2c3b-4787-b574-78c7112cddf2"
        },
        {
            "id": "18eafcbe-47aa-4b54-a830-ddba3023081c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8ae6c06b-2c3b-4787-b574-78c7112cddf2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "72079f4f-9558-4696-8b39-21e6a15d0714",
    "visible": true
}