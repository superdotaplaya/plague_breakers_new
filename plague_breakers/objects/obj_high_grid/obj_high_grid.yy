{
    "id": "e9d3eac2-8eae-4820-a275-9052ddad84ac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_high_grid",
    "eventList": [
        {
            "id": "49d06a95-daa7-4d44-a067-f1a06a51b3d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "460e261a-ab50-4cae-b39e-bfd1fbcb5caa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "5f889525-e8b0-4ea1-b1d2-ab749920d9c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "93933ffd-d5c7-447c-a7da-569f26206cb3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "f6df5999-9b26-400c-913e-73e762dac899",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "cef2509a-97dc-41ba-ad4a-c9ca25c8db64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "12a615a1-fa0f-4167-95d1-7acdee951786",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "cebf2f6b-770c-49c3-8d33-4e4f6b159779",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "fcc4a588-8fbd-458e-babb-3b22b3aba70f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "d2a3d477-8ece-4da4-8828-f089104794df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "aebbf5ff-b177-45c8-a235-8b4b05766816",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "6f874b9d-0faa-4fc4-8ecf-c2611ff983e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "d8d0cdc9-2e0c-4632-853c-e4098b464edd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "5c37c902-777c-43b5-a437-9dca52f80486",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "56eb93ac-89f6-4ece-8b53-7197eaf8a8cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "aaed9215-c428-4c82-8888-f4755a5e4f72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "8f474088-e4dc-410b-947c-2b9e909cbb62",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "8b674faf-09fd-4167-8da1-16b5ffd57c84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "917d34ca-80aa-417f-83d3-99b44da049e3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "596892ea-46b0-47f4-b168-0bc9dc31d465",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "8c6657eb-ba1a-4bb3-a87b-942defa24304",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "3be80e28-b7ba-4574-a10a-1e00db6401c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "bdf1ed79-e8a5-4a31-86f4-90e162d41bee",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "3f513757-52db-4b0f-af67-ada762985880",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d61b52f9-1fcc-4338-8c5f-3fe842626af3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "e806b671-c264-4d9b-aa12-6b023e0d6b74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e79f9a45-e3f7-4d5b-be0d-7022e4227488",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "eff0d888-32d6-463d-aa15-60be8663542e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "95f15ea7-e8f3-487a-b143-3a45457f6eb6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "8a3dc15f-893d-413a-a25d-bd5112ec110c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "345fca6a-7fab-4f58-ac30-804473824c5a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "d70837ce-aa68-4641-b895-42886461bf0e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "6805e61e-ad9e-4afe-a331-68c08d156c9c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        },
        {
            "id": "e45cd94c-bdba-4b1e-9343-eeeb099d3c4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cae81964-d4ac-454f-a5a2-c281915d634f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e9d3eac2-8eae-4820-a275-9052ddad84ac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a24c7859-41b2-49be-94ac-822c889831ce",
    "visible": true
}