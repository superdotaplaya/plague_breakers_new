/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 590AA24F
/// @DnDArgument : "sound" "snd_song_001"
/// @DnDArgument : "volume" ".08"
/// @DnDSaveInfo : "sound" "74394b18-9603-4db2-99d6-82c499677b8a"
audio_sound_gain(snd_song_001, .08, 0);

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 4E012931
/// @DnDArgument : "sound" "snd_song_001_LOOP"
/// @DnDArgument : "volume" ".08"
/// @DnDSaveInfo : "sound" "d6477212-dc94-449c-998f-fe175b2dfce2"
audio_sound_gain(snd_song_001_LOOP, .08, 0);

/// @DnDAction : YoYo Games.Instances.Create_Instance
/// @DnDVersion : 1
/// @DnDHash : 11E8BDB3
/// @DnDArgument : "xpos" "x"
/// @DnDArgument : "ypos" "y"
/// @DnDArgument : "objectid" "obj_mute_music_button"
/// @DnDSaveInfo : "objectid" "1bd6e42c-b49a-4c55-a329-d8e520ace452"
instance_create_layer(x, y, "Instances", obj_mute_music_button);

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 6E2957E5
/// @DnDArgument : "expr" "false"
/// @DnDArgument : "var" "global.muted"
global.muted = false;

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 7495CDF1
instance_destroy();