{
    "id": "7a1bfa0a-00cf-42f7-acd5-7958a95e4fdd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_unmute_music",
    "eventList": [
        {
            "id": "cf6036a9-0e37-4986-a2ca-70e9deadc64c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "7a1bfa0a-00cf-42f7-acd5-7958a95e4fdd"
        },
        {
            "id": "bbf63e6b-391b-44ca-a653-bd7cb81e4670",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7a1bfa0a-00cf-42f7-acd5-7958a95e4fdd"
        },
        {
            "id": "64a069fb-8d35-4523-91e9-c36382521508",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7a1bfa0a-00cf-42f7-acd5-7958a95e4fdd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e33b44f4-0c2d-40e1-a300-2d89b0ef4fa2",
    "visible": true
}