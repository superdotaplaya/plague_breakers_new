{
    "id": "8f474088-e4dc-410b-947c-2b9e909cbb62",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_poison_tower",
    "eventList": [
        {
            "id": "adeedb78-852b-4234-b9b8-3193cff75dc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8f474088-e4dc-410b-947c-2b9e909cbb62"
        },
        {
            "id": "4953f543-9a80-461b-b21a-3a285549e05b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8f474088-e4dc-410b-947c-2b9e909cbb62"
        },
        {
            "id": "168f4327-b568-4bd8-b891-86b94e9633fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "8f474088-e4dc-410b-947c-2b9e909cbb62"
        },
        {
            "id": "012f9d63-7b33-4e7b-bd6a-5c3efcf57c98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8f474088-e4dc-410b-947c-2b9e909cbb62"
        },
        {
            "id": "ba9589b1-4e5c-467b-b7ef-b958674d5889",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "8f474088-e4dc-410b-947c-2b9e909cbb62"
        },
        {
            "id": "18cc7972-661b-45d8-9b79-712574710bbd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8f474088-e4dc-410b-947c-2b9e909cbb62"
        },
        {
            "id": "a250ddeb-2fc8-44b9-9362-d44f78377493",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "8f474088-e4dc-410b-947c-2b9e909cbb62"
        },
        {
            "id": "f29d5671-b45d-4d01-8d59-e39758551a67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "8f474088-e4dc-410b-947c-2b9e909cbb62"
        },
        {
            "id": "feafde31-36fd-4401-afc5-9583fc18e93a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8f474088-e4dc-410b-947c-2b9e909cbb62"
        },
        {
            "id": "d99d5b64-d2f8-4667-885e-b19ad8bd5155",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "8f474088-e4dc-410b-947c-2b9e909cbb62"
        },
        {
            "id": "21169c07-5eb0-4165-a6e7-435bcb11a834",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "8f474088-e4dc-410b-947c-2b9e909cbb62"
        },
        {
            "id": "0240c79c-7eca-47dd-be46-a35df0a0997c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "8f474088-e4dc-410b-947c-2b9e909cbb62"
        },
        {
            "id": "3b9f9e0a-9ed8-45f1-b246-b641916b0839",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "f0875ffd-fd8e-428c-aabc-956eb91215a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8f474088-e4dc-410b-947c-2b9e909cbb62"
        },
        {
            "id": "ef72d2f2-1ec0-48c8-920e-1465c29ce73a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8f474088-e4dc-410b-947c-2b9e909cbb62"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "92dc95d5-3f2b-416f-bc4b-84f7be27f74b",
    "visible": true
}