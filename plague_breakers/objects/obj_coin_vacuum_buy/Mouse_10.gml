/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 3C21E6D7
/// @DnDArgument : "expr" ""Can be placed aywhere to auto collect coins for the player""
/// @DnDArgument : "var" "global.tooltip"
global.tooltip = "Can be placed aywhere to auto collect coins for the player";

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 59ECDBA0
/// @DnDArgument : "code" "image_xscale = 1.2$(13_10)image_yscale = 1.7$(13_10)instance_create_layer(x-160,y-450,"shop_icons",obj_tooltips)$(13_10)hover = true"
image_xscale = 1.2
image_yscale = 1.7
instance_create_layer(x-160,y-450,"shop_icons",obj_tooltips)
hover = true