{
    "id": "67f921a0-8608-40c0-800f-047aae8a9c19",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_coin_vacuum_buy",
    "eventList": [
        {
            "id": "45abdd58-c662-4584-b70e-b34edfbef105",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "67f921a0-8608-40c0-800f-047aae8a9c19"
        },
        {
            "id": "be4ae677-50a5-4cf3-bdb9-768dca513b1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "67f921a0-8608-40c0-800f-047aae8a9c19"
        },
        {
            "id": "f49dfc23-fc2b-445f-84ba-9d066ca8fe76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "67f921a0-8608-40c0-800f-047aae8a9c19"
        },
        {
            "id": "ea957a73-9e7c-4025-abab-e0b859b98560",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "67f921a0-8608-40c0-800f-047aae8a9c19"
        },
        {
            "id": "43d16a49-57f2-4878-8f1c-e365497de245",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "67f921a0-8608-40c0-800f-047aae8a9c19"
        },
        {
            "id": "9c58c93e-c20e-43b3-8cae-d3dc14d5fe38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "67f921a0-8608-40c0-800f-047aae8a9c19"
        },
        {
            "id": "931230bf-5a90-4b5e-a521-55a64b5a0b9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "67f921a0-8608-40c0-800f-047aae8a9c19"
        },
        {
            "id": "de7f0884-cf46-4671-80ed-5a12884cef86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "67f921a0-8608-40c0-800f-047aae8a9c19"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "45f085ce-2bc3-400d-a76d-c59ff7a6103c",
    "visible": true
}