/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 0336DFF6
/// @DnDArgument : "code" "if global.gold >= cost then$(13_10){$(13_10)instance_create_layer(x,y,"Instances_towers",obj_coin_vacuum)$(13_10)global.gold -= cost$(13_10)held = false$(13_10)}$(13_10)instance_create_layer(1560,1120,"shop_icons",obj_coin_vacuum_buy)$(13_10)instance_destroy(obj_tooltips)$(13_10)audio_play_sound(snd_buy,1,0)$(13_10)instance_destroy()"
if global.gold >= cost then
{
instance_create_layer(x,y,"Instances_towers",obj_coin_vacuum)
global.gold -= cost
held = false
}
instance_create_layer(1560,1120,"shop_icons",obj_coin_vacuum_buy)
instance_destroy(obj_tooltips)
audio_play_sound(snd_buy,1,0)
instance_destroy()