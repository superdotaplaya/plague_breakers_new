/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 5F475DFA
/// @DnDArgument : "code" "draw_self()$(13_10)draw_set_color(c_white)$(13_10)$(13_10)if hover = true then$(13_10){$(13_10)	draw_set_alpha(.4)$(13_10)draw_circle_color(x,y,range,c_white,c_white,c_white)	$(13_10)draw_set_alpha(1)$(13_10)}"
draw_self()
draw_set_color(c_white)

if hover = true then
{
	draw_set_alpha(.4)
draw_circle_color(x,y,range,c_white,c_white,c_white)	
draw_set_alpha(1)
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4F10662E
/// @DnDArgument : "var" "hover"
/// @DnDArgument : "value" "true"
if(hover == true)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 5023D9A5
	/// @DnDParent : 4F10662E
	/// @DnDArgument : "alpha" ".5"
	draw_set_alpha(.5);

	/// @DnDAction : YoYo Games.Drawing.Draw_Gradient_Ellipse
	/// @DnDVersion : 1
	/// @DnDHash : 23318BE9
	/// @DnDParent : 4F10662E
	/// @DnDArgument : "x1" "x -range"
	/// @DnDArgument : "y1" "y -range"
	/// @DnDArgument : "x2" "x + range"
	/// @DnDArgument : "y2" "y + range"
	/// @DnDArgument : "col1" "$FFFFFFFF"
	/// @DnDArgument : "col2" "$FFB3B3B3"
	/// @DnDArgument : "fill" "1"
	draw_ellipse_colour(x -range, y -range, x + range, y + range, $FFFFFFFF & $FFFFFF, $FFB3B3B3 & $FFFFFF, 0);

	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 00BDF36D
	/// @DnDParent : 4F10662E
	draw_set_alpha(1);
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 621ADD91
/// @DnDArgument : "var" "held"
/// @DnDArgument : "value" "false"
if(held == false)
{
	/// @DnDAction : YoYo Games.Drawing.Draw_Value
	/// @DnDVersion : 1
	/// @DnDHash : 4CE2D955
	/// @DnDParent : 621ADD91
	/// @DnDArgument : "x" "-75"
	/// @DnDArgument : "x_relative" "1"
	/// @DnDArgument : "y" "115"
	/// @DnDArgument : "y_relative" "1"
	/// @DnDArgument : "caption" ""Cost: ""
	/// @DnDArgument : "var" "cost"
	draw_text(x + -75, y + 115, string("Cost: ") + string(cost));
}