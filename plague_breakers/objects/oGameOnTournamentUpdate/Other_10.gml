/// @description Load JSON
event_inherited();

accessKey =		json_raw_data[? "accessKey"];
tournamentId =	json_raw_data[? "tournamentId"];