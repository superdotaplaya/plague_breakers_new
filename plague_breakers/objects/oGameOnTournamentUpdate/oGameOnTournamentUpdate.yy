{
    "id": "df2ceb1b-8eb7-4566-a4f5-c0cb383672e5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameOnTournamentUpdate",
    "eventList": [
        {
            "id": "7569750f-4ed7-4e16-8f56-2a0aa41f8f81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "df2ceb1b-8eb7-4566-a4f5-c0cb383672e5"
        },
        {
            "id": "7a7c3fd5-7230-4836-b17c-303c2afd12da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df2ceb1b-8eb7-4566-a4f5-c0cb383672e5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0330c072-566e-4a20-bbb9-c6edf5a161cf",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}