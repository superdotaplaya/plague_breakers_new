event_inherited();

attemptsRemaining = json_raw_data[? "attemptsRemaining"];
matchId =			json_raw_data[? "matchId"];
metadata =			json_raw_data[? "metadata"];
tournamentId =		json_raw_data[? "tournamentId"];
teamId =			json_raw_data[? "teamId"];