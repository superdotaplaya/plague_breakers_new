{
    "id": "6c851cca-5d2c-4053-8c43-5f859136d070",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cannon_attack_range_up",
    "eventList": [
        {
            "id": "0f177cc3-5c4a-4d45-8a37-c8c174871eea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6c851cca-5d2c-4053-8c43-5f859136d070"
        },
        {
            "id": "eed9a6a6-e104-4387-a155-38e776d97b04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6c851cca-5d2c-4053-8c43-5f859136d070"
        },
        {
            "id": "e62a245b-0211-46ef-9dae-6da3009cf9ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "6c851cca-5d2c-4053-8c43-5f859136d070"
        },
        {
            "id": "66774485-2d47-4dcc-b4d6-598d4e89bcb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "6c851cca-5d2c-4053-8c43-5f859136d070"
        },
        {
            "id": "89428480-7543-425a-ac63-13121c10121c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "6c851cca-5d2c-4053-8c43-5f859136d070"
        },
        {
            "id": "8f6bffc6-0a0c-44ea-88cc-43310016919e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6c851cca-5d2c-4053-8c43-5f859136d070"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "22b2a54a-e74f-4bcc-b8e1-19fa267438ec",
    "visible": true
}