/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 14753057
/// @DnDArgument : "var" "global.gold"
/// @DnDArgument : "op" "4"
/// @DnDArgument : "value" "(cost + cost_bonus)"
if(global.gold >= (cost + cost_bonus))
{
	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 62FF32FD
	/// @DnDParent : 14753057
	/// @DnDArgument : "code" "current_range = variable_instance_get(instance_nearest(global.x,global.y,obj_cannon_tower),"range")$(13_10)variable_instance_set(instance_nearest(global.x,global.y,obj_cannon_tower),"range",current_range + 70)$(13_10)global.gold -= (cost + cost_bonus)$(13_10)audio_play_sound(snd_buy,1,0)$(13_10)current_level += 1$(13_10)variable_instance_set(instance_nearest(global.x,global.y,obj_cannon_tower),"range_lvl",current_level)$(13_10)variable_instance_set(instance_nearest(global.x,global.y,obj_cannon_tower),"upgrade_range_cost", (cost_bonus * 2) + cost)"
	current_range = variable_instance_get(instance_nearest(global.x,global.y,obj_cannon_tower),"range")
	variable_instance_set(instance_nearest(global.x,global.y,obj_cannon_tower),"range",current_range + 70)
	global.gold -= (cost + cost_bonus)
	audio_play_sound(snd_buy,1,0)
	current_level += 1
	variable_instance_set(instance_nearest(global.x,global.y,obj_cannon_tower),"range_lvl",current_level)
	variable_instance_set(instance_nearest(global.x,global.y,obj_cannon_tower),"upgrade_range_cost", (cost_bonus * 2) + cost)
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 19D3E9F2
else
{
	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 47897498
	/// @DnDParent : 19D3E9F2
	/// @DnDArgument : "code" "audio_play_sound(snd_not_enough_gold,1,0)$(13_10)global.not_enough_gold = true$(13_10)"
	audio_play_sound(snd_not_enough_gold,1,0)
	global.not_enough_gold = true

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 668B384C
	/// @DnDApplyTo : bd506929-690c-4d0b-bcc7-76f64dfd6fdb
	/// @DnDParent : 19D3E9F2
	/// @DnDArgument : "steps" "600"
	with(obj_score_money) {
	alarm_set(0, 600);
	
	}
}