event_inherited();

awardedPrizeId =   json_raw_data[? "awardedPrizeId"];
prizeTitle =	   json_raw_data[? "prizeTitle"];
status =		   json_raw_data[? "status"];
prizeInfoType =    json_raw_data[? "prizeInfoType"];
description =	   json_raw_data[? "description"];
imageUrl =		   json_raw_data[? "imageUrl"];
dateOfExpiration = json_raw_data[? "dateOfExpiration"];