/// @description Request Success
event_inherited();

teamId =			json_raw_data[? "teamId"];
teamName =			json_raw_data[? "teamName"];
imageUrl =			json_raw_data[? "imageUrl"];
teamSizeMax =		json_raw_data[? "teamSizeMax"];
isPlayerInTeam =	json_raw_data[? "isPlayerInTeam"];

var externalPlayerIdsJson = json_raw_data[? "externalPlayerIds"];
if(is_real(externalPlayerIdsJson) && ds_exists(externalPlayerIdsJson, ds_type_list))
{
	ds_list_copy(externalPlayerIds, externalPlayerIdsJson);
}