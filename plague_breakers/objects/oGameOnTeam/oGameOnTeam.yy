{
    "id": "d840b90f-49a5-4ede-ba19-d3eb8970aec2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameOnTeam",
    "eventList": [
        {
            "id": "ef2f01b0-804a-4fdb-a2dd-cb9c9f843ded",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d840b90f-49a5-4ede-ba19-d3eb8970aec2"
        },
        {
            "id": "b93f4852-5fc0-4860-b67e-00b47ac0785c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "d840b90f-49a5-4ede-ba19-d3eb8970aec2"
        },
        {
            "id": "0f6b5157-a25a-40cb-8c94-89f771f58b04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "d840b90f-49a5-4ede-ba19-d3eb8970aec2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0330c072-566e-4a20-bbb9-c6edf5a161cf",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}