{
    "id": "95206be0-6e6c-4734-8b6c-0cf228a2d3bc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_yellow_damage_up1",
    "eventList": [
        {
            "id": "a718a51f-2277-4c8d-9d0f-36ae7d9500f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "95206be0-6e6c-4734-8b6c-0cf228a2d3bc"
        },
        {
            "id": "97378de0-99da-4bfc-96da-68a6f701cd45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "95206be0-6e6c-4734-8b6c-0cf228a2d3bc"
        },
        {
            "id": "c439671e-8948-4aea-b078-89787c1c9443",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "95206be0-6e6c-4734-8b6c-0cf228a2d3bc"
        },
        {
            "id": "989a647f-4a14-4ab0-a2da-a1a6b0b0aee1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "95206be0-6e6c-4734-8b6c-0cf228a2d3bc"
        },
        {
            "id": "d25e445c-63e3-4f50-8e44-634584ccc681",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "95206be0-6e6c-4734-8b6c-0cf228a2d3bc"
        },
        {
            "id": "7a7e53be-45bf-4943-9816-e18700b2bd61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "95206be0-6e6c-4734-8b6c-0cf228a2d3bc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c82849b0-f7ad-4a5d-aadd-977a6138ee59",
    "visible": true
}