{
    "id": "77d96826-3241-4b20-bd2c-b5663a1c2549",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_rebind_shop9",
    "eventList": [
        {
            "id": "36ee1e41-77bc-42cb-99e4-5da00d85d678",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "77d96826-3241-4b20-bd2c-b5663a1c2549"
        },
        {
            "id": "a52d1a06-cf3a-4467-8f1d-4a2646d9ced4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "77d96826-3241-4b20-bd2c-b5663a1c2549"
        },
        {
            "id": "8f7bfa3a-e856-4647-b31a-a2153c204637",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "77d96826-3241-4b20-bd2c-b5663a1c2549"
        },
        {
            "id": "49739bf2-27b1-484a-9a21-035af672e073",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "77d96826-3241-4b20-bd2c-b5663a1c2549"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a832f2b-2273-4cc3-b553-8c06b7dc617f",
    "visible": true
}