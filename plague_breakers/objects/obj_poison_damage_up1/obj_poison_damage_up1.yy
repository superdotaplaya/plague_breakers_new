{
    "id": "5cab794b-de08-4f2e-84e5-abed593dd509",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_poison_damage_up1",
    "eventList": [
        {
            "id": "713e019f-9b3d-41d6-9eb5-61e8e459de4a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5cab794b-de08-4f2e-84e5-abed593dd509"
        },
        {
            "id": "3e7d43f9-7f24-402b-a641-c69c22a616c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5cab794b-de08-4f2e-84e5-abed593dd509"
        },
        {
            "id": "7affe7fd-f867-41ff-97d2-39e128d27199",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "5cab794b-de08-4f2e-84e5-abed593dd509"
        },
        {
            "id": "acf4e066-98da-4e46-bea6-5496cd6cc10d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "5cab794b-de08-4f2e-84e5-abed593dd509"
        },
        {
            "id": "61560a3f-6aec-488c-af63-239df908c2cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "5cab794b-de08-4f2e-84e5-abed593dd509"
        },
        {
            "id": "15dccce2-6bca-46d1-ba9a-132eb8604d16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5cab794b-de08-4f2e-84e5-abed593dd509"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7e33d25b-48a8-4127-8cc6-b0829cb4b5ac",
    "visible": true
}