{
    "id": "5a39ad46-1804-4ad1-951d-bd9239e0b8cd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_green_attack1",
    "eventList": [
        {
            "id": "eb0d122f-eebc-4234-9c26-66e4f865bf76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5a39ad46-1804-4ad1-951d-bd9239e0b8cd"
        },
        {
            "id": "035407c6-e0d5-4918-826a-9d55221d8931",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5a39ad46-1804-4ad1-951d-bd9239e0b8cd"
        },
        {
            "id": "c0c74378-009e-47b7-9cd2-c75c1aa0f7d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5a39ad46-1804-4ad1-951d-bd9239e0b8cd"
        },
        {
            "id": "ab66e950-db59-4401-9960-4437881eec17",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5a39ad46-1804-4ad1-951d-bd9239e0b8cd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "253138d9-82ea-4fef-9b55-02494aead4bd",
    "visible": true
}