{
    "id": "e85eb012-3cf8-427a-b58a-edc87618bdf6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_start_game1",
    "eventList": [
        {
            "id": "ab158857-7a40-401f-8eca-5f361d90adba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "e85eb012-3cf8-427a-b58a-edc87618bdf6"
        },
        {
            "id": "efc00014-d2d9-4d3f-b707-4e182e74af21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "e85eb012-3cf8-427a-b58a-edc87618bdf6"
        },
        {
            "id": "89bb60fa-9f75-4f46-bf1d-3660f683bf68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e85eb012-3cf8-427a-b58a-edc87618bdf6"
        },
        {
            "id": "7abff9b1-6ade-48a5-bbfd-983623e003c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "e85eb012-3cf8-427a-b58a-edc87618bdf6"
        },
        {
            "id": "d817c8cd-bd2a-4a58-a3e6-d0a841ef7660",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e85eb012-3cf8-427a-b58a-edc87618bdf6"
        },
        {
            "id": "87cf230e-580c-44d5-bcfe-41bf3c8f5001",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e85eb012-3cf8-427a-b58a-edc87618bdf6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "90850e67-fd74-4467-8e48-35a4afdcfdeb",
    "visible": true
}