/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 7255E6BD
/// @DnDArgument : "code" "log("[GAME STATE]   Player has started a new game")$(13_10)variable_instance_set(instance_nearest(x,y,obj_daily_quests),"enemies_killed",0)$(13_10)rousr_dissonance_set_details("Playing on Desert Map Wave: " + string(global.currentlevel))$(13_10)global.daily_level = 0$(13_10)room_goto(map_002)$(13_10)instance_destroy(obj_continue_game)"
log("[GAME STATE]   Player has started a new game")
variable_instance_set(instance_nearest(x,y,obj_daily_quests),"enemies_killed",0)
rousr_dissonance_set_details("Playing on Desert Map Wave: " + string(global.currentlevel))
global.daily_level = 0
room_goto(map_002)
instance_destroy(obj_continue_game)