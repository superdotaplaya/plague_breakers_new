{
    "id": "af11b85e-afa8-47b0-8a71-57f575717584",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_rebind_shop3",
    "eventList": [
        {
            "id": "6fd0aa7a-659d-4431-b319-c85a56a516c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "af11b85e-afa8-47b0-8a71-57f575717584"
        },
        {
            "id": "158c3267-2eb4-4dd5-a7fc-b5bde4614d63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "af11b85e-afa8-47b0-8a71-57f575717584"
        },
        {
            "id": "e87315d0-4acd-4ec0-a07c-0c874bb5d552",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "af11b85e-afa8-47b0-8a71-57f575717584"
        },
        {
            "id": "ff0a6f1c-b341-43ef-9eb5-220fe7e1910d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "af11b85e-afa8-47b0-8a71-57f575717584"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a832f2b-2273-4cc3-b553-8c06b7dc617f",
    "visible": true
}