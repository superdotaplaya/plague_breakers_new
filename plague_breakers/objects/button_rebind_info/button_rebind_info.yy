{
    "id": "60c32a39-8434-4801-a23b-4ea06957970e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_rebind_info",
    "eventList": [
        {
            "id": "d689b229-0973-425b-a8c8-7e1df0f6673f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "60c32a39-8434-4801-a23b-4ea06957970e"
        },
        {
            "id": "03deb1d6-ef91-4085-a5b4-7f36faf85e31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "60c32a39-8434-4801-a23b-4ea06957970e"
        },
        {
            "id": "2a2a7791-3aa4-468c-9620-bb7b96649b38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "60c32a39-8434-4801-a23b-4ea06957970e"
        },
        {
            "id": "11ca4434-bbd3-44d8-bbca-bec12ea11c27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "60c32a39-8434-4801-a23b-4ea06957970e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a832f2b-2273-4cc3-b553-8c06b7dc617f",
    "visible": true
}