{
    "id": "199c3581-8afa-4d78-a75d-25ddafd22b9c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_speeddown",
    "eventList": [
        {
            "id": "362d8a08-f983-4c1c-93ac-4778a800f4af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "199c3581-8afa-4d78-a75d-25ddafd22b9c"
        },
        {
            "id": "9ed90d09-c405-492e-aa98-83f9b3b287c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "199c3581-8afa-4d78-a75d-25ddafd22b9c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0e9def90-1c4b-4906-aca7-fe7e996062de",
    "visible": true
}