{
    "id": "e21bdc6b-c4fe-4cd3-a62d-d4872cf14c20",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bug_report_button",
    "eventList": [
        {
            "id": "425bab3e-6b25-4b5d-82be-c69d02088c86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "e21bdc6b-c4fe-4cd3-a62d-d4872cf14c20"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7e2d22d2-2ee3-47d6-b267-f07eb76b5fe7",
    "visible": true
}