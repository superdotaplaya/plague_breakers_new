{
    "id": "cae81964-d4ac-454f-a5a2-c281915d634f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_green_tower",
    "eventList": [
        {
            "id": "7e51d06e-f727-490c-87ae-ed66810c3fac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "265d07f8-4c3b-444f-9632-58f8a3d8c549",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "357189bb-ca41-412a-aeb4-7197d776b57c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "3d87b79e-3f8d-4728-a91d-180d6ad8e995",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "63c87ac8-906d-4051-ade1-45fb5e3ef8fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "09ede52a-594a-4c58-9e81-5e8495c3d10d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "06edc251-b73b-4c68-b75e-0a236aa0a607",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "98bac85b-fbdc-47cf-bfa3-a0edfd776e51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "d231a922-944c-423f-8732-d3eb682260c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "8ee055e4-bca9-44a3-af05-10e355a022dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "ae563bb3-363a-4fc0-b9bd-8df4423c267a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "f0875ffd-fd8e-428c-aabc-956eb91215a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "45194cc2-4cd7-49df-9230-7d5dc36be72e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "b4e32b2b-5f9c-4226-9927-69d6fe966a62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "cf9c8bd9-115d-4f29-bb00-9db389aba918",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ff17cdf8-f43a-45c6-8068-c71366134d73",
    "visible": true
}