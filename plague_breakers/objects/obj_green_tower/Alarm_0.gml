/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 4EEC039D
/// @DnDArgument : "code" "// Handles attacks$(13_10)if distance_to_object(obj_enemy) <= range then$(13_10){$(13_10)instance_create_depth(x,y,0,obj_green_attack)$(13_10)audio_play_sound(snd_tower_attack_basic,1,0)$(13_10)}$(13_10)alarm_set(0,50)"
// Handles attacks
if distance_to_object(obj_enemy) <= range then
{
instance_create_depth(x,y,0,obj_green_attack)
audio_play_sound(snd_tower_attack_basic,1,0)
}
alarm_set(0,50)