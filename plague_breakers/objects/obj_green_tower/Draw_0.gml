/// @DnDAction : YoYo Games.Drawing.Draw_Self
/// @DnDVersion : 1
/// @DnDHash : 73BF8011
draw_self();

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 370F652F
/// @DnDArgument : "code" "image_speed = 60$(13_10)if global.fortify = true then$(13_10){$(13_10)draw_sprite(spr_fortify_tower,-1,x,y)	$(13_10)	$(13_10)}"
image_speed = 60
if global.fortify = true then
{
draw_sprite(spr_fortify_tower,-1,x,y)	
	
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4B458BDE
/// @DnDArgument : "var" "hover"
/// @DnDArgument : "value" "true"
if(hover == true)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 1DE5C2AE
	/// @DnDParent : 4B458BDE
	/// @DnDArgument : "alpha" ".5"
	draw_set_alpha(.5);

	/// @DnDAction : YoYo Games.Drawing.Draw_Gradient_Ellipse
	/// @DnDVersion : 1
	/// @DnDHash : 277347EB
	/// @DnDParent : 4B458BDE
	/// @DnDArgument : "x1" "-range"
	/// @DnDArgument : "x1_relative" "1"
	/// @DnDArgument : "y1" "-range"
	/// @DnDArgument : "y1_relative" "1"
	/// @DnDArgument : "x2" "range"
	/// @DnDArgument : "x2_relative" "1"
	/// @DnDArgument : "y2" "range"
	/// @DnDArgument : "y2_relative" "1"
	/// @DnDArgument : "col1" "$FFCCCCCC"
	/// @DnDArgument : "col2" "$FFB3B3B3"
	/// @DnDArgument : "fill" "1"
	draw_ellipse_colour(x + -range, y + -range, x + range, y + range, $FFCCCCCC & $FFFFFF, $FFB3B3B3 & $FFFFFF, 0);
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 72A02951
/// @DnDArgument : "var" "hp"
/// @DnDArgument : "op" "1"
/// @DnDArgument : "value" "100"
if(hp < 100)
{
	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 16ACF2F8
	/// @DnDParent : 72A02951
	/// @DnDArgument : "code" "draw_healthbar(x-20,y-20,x+20,y,(hp/100)*100,c_black,c_red,c_green,0,true,false)"
	draw_healthbar(x-20,y-20,x+20,y,(hp/100)*100,c_black,c_red,c_green,0,true,false)
}