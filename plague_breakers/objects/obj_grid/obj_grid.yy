{
    "id": "7b4a9b34-6545-4240-869f-e66525bd0fcc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_grid",
    "eventList": [
        {
            "id": "101e5554-e1ee-47e4-abba-964a2981bbeb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "5ac97f4c-a451-4d2f-b60f-9b893902ca06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "0b8e1916-8e4b-4306-bea2-2d6f28004fdb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "f0728237-40f8-4866-b875-99fe4c5fb6ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "c2f2644a-9f30-45db-aa2a-63f9f6b3c0d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "c8bb56c3-c43b-40d2-9a66-661a7f05101c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cae81964-d4ac-454f-a5a2-c281915d634f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "6b112e54-b319-40f5-8348-3768e800f52e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "12a615a1-fa0f-4167-95d1-7acdee951786",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "11271126-1e50-4467-9f48-659457d76cbc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "f5e64be6-8d16-4cae-926f-95b2370459e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "dd25781d-7e1a-4a9f-8055-dba8c6865c21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "aebbf5ff-b177-45c8-a235-8b4b05766816",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "35888f89-8ed9-432e-baab-3b087681129a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "4327a1d1-eca8-4c78-b6c4-57f4ac9e5686",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "5c37c902-777c-43b5-a437-9dca52f80486",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "387e9902-929c-4283-88d0-50ca11eec7e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "e2a77f85-1625-49be-9ee2-b0bc88018551",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "8f474088-e4dc-410b-947c-2b9e909cbb62",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "d069ef1f-6716-47d3-857f-dc7419d25f5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "917d34ca-80aa-417f-83d3-99b44da049e3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "13d40818-1a60-47c6-aca6-d1d8c1678a1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "8c6657eb-ba1a-4bb3-a87b-942defa24304",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "b98ef5e5-7545-4cce-bb16-1321aa8ab707",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "6805e61e-ad9e-4afe-a331-68c08d156c9c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "c1b7b154-17e2-4b14-a2aa-26d3e63f20ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e79f9a45-e3f7-4d5b-be0d-7022e4227488",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "122e0e69-4211-4cc8-9550-abe85dfc65ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d61b52f9-1fcc-4338-8c5f-3fe842626af3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "0b342962-dea7-49ac-acce-e645e4fd6bf1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "95f15ea7-e8f3-487a-b143-3a45457f6eb6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "721e24b4-4fb1-4550-8a09-e64fa80ca347",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "345fca6a-7fab-4f58-ac30-804473824c5a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "8bc31b21-6e10-47f5-892d-8562cb4d23a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "bdf1ed79-e8a5-4a31-86f4-90e162d41bee",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8024de4a-5f56-4836-9a65-fd17fd8bed4b",
    "visible": true
}