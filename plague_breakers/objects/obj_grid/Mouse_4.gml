/// @DnDAction : YoYo Games.Instances.Create_Instance
/// @DnDVersion : 1
/// @DnDHash : 68EB2C21
/// @DnDArgument : "objectid" "obj_shop_menu"
/// @DnDSaveInfo : "objectid" "37f57748-c862-493a-bbbd-c8ffdf8f8d60"
instance_create_layer(0, 0, "Instances", obj_shop_menu);

/// @DnDAction : YoYo Games.Common.Function_Call
/// @DnDVersion : 1
/// @DnDHash : 255069C5
/// @DnDArgument : "function" "instance_destroy_shop"
instance_destroy_shop();

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 00F70E6D
/// @DnDArgument : "code" "global.highround = false$(13_10)global.x = x$(13_10)global.y = y$(13_10)global.activecell = instance_id$(13_10)if modular = false$(13_10){$(13_10)instance_create_layer(0,0,"shop_icons",obj_tower_1_shop)$(13_10)instance_create_layer(0,0,"shop_icons",obj_tower_2_shop)$(13_10)instance_create_layer(0,0,"shop_icons",obj_tower_3_shop)$(13_10)instance_create_layer(0,0,"shop_icons",obj_tower_4_shop)$(13_10)instance_create_layer(0,0,"shop_icons",obj_tower_5_shop)$(13_10)instance_create_layer(0,0,"shop_icons",obj_tower_6_shop)$(13_10)instance_create_layer(0,0,"shop_icons",obj_tower_poison_shop)$(13_10)instance_create_layer(0,0,"shop_icons",obj_tesla_tower_shop)$(13_10)instance_create_layer(0,0,"shop_icons",obj_cannon_tower_shop)$(13_10)instance_deactivate_layer("Instances")$(13_10)$(13_10)if instance_nearest(x,y,obj_shop_menu) = noone then$(13_10){$(13_10)instance_create_layer(x,y,"Instances_1",obj_shop_menu)	$(13_10)}$(13_10)}$(13_10)else$(13_10){$(13_10)instance_create_layer(95,830,"shop_icons",obj_bonus_attack_speed_shop)	$(13_10)instance_create_layer(95,830,"shop_icons",obj_bonus_attack_damage_shop)	$(13_10)instance_create_layer(95,830,"shop_icons",obj_bonus_attack_range_shop)	$(13_10)instance_create_layer(95,830,"shop_icons",obj_bonus_projectiles_shop)$(13_10)}$(13_10)$(13_10)"
global.highround = false
global.x = x
global.y = y
global.activecell = instance_id
if modular = false
{
instance_create_layer(0,0,"shop_icons",obj_tower_1_shop)
instance_create_layer(0,0,"shop_icons",obj_tower_2_shop)
instance_create_layer(0,0,"shop_icons",obj_tower_3_shop)
instance_create_layer(0,0,"shop_icons",obj_tower_4_shop)
instance_create_layer(0,0,"shop_icons",obj_tower_5_shop)
instance_create_layer(0,0,"shop_icons",obj_tower_6_shop)
instance_create_layer(0,0,"shop_icons",obj_tower_poison_shop)
instance_create_layer(0,0,"shop_icons",obj_tesla_tower_shop)
instance_create_layer(0,0,"shop_icons",obj_cannon_tower_shop)
instance_deactivate_layer("Instances")

if instance_nearest(x,y,obj_shop_menu) = noone then
{
instance_create_layer(x,y,"Instances_1",obj_shop_menu)	
}
}
else
{
instance_create_layer(95,830,"shop_icons",obj_bonus_attack_speed_shop)	
instance_create_layer(95,830,"shop_icons",obj_bonus_attack_damage_shop)	
instance_create_layer(95,830,"shop_icons",obj_bonus_attack_range_shop)	
instance_create_layer(95,830,"shop_icons",obj_bonus_projectiles_shop)
}

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 1F3D3F6F
/// @DnDArgument : "expr" "false"
/// @DnDArgument : "var" "global.highground"
global.highground = false;

/// @DnDAction : YoYo Games.Instances.Color_Sprite
/// @DnDVersion : 1
/// @DnDHash : 34358DA2
/// @DnDApplyTo : 7b4a9b34-6545-4240-869f-e66525bd0fcc
with(obj_grid) {
image_blend = $FFFFFFFF & $ffffff;
image_alpha = ($FFFFFFFF >> 24) / $ff;
}

/// @DnDAction : YoYo Games.Instances.Color_Sprite
/// @DnDVersion : 1
/// @DnDHash : 3A0C9A1E
/// @DnDArgument : "colour" "$FF000200"
image_blend = $FF000200 & $ffffff;
image_alpha = ($FF000200 >> 24) / $ff;

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 56F18384
/// @DnDArgument : "var" "global.shop_open"
/// @DnDArgument : "value" "false"
if(global.shop_open == false)
{
	/// @DnDAction : YoYo Games.Common.Set_Global
	/// @DnDVersion : 1
	/// @DnDHash : 594826F9
	/// @DnDParent : 56F18384
	/// @DnDArgument : "value" "true"
	/// @DnDArgument : "var" "shop_open"
	global.shop_open = true;

	/// @DnDAction : YoYo Games.Paths.Start_Path
	/// @DnDVersion : 1.1
	/// @DnDHash : 77EF29C8
	/// @DnDApplyTo : 37f57748-c862-493a-bbbd-c8ffdf8f8d60
	/// @DnDParent : 56F18384
	/// @DnDArgument : "path" "shop"
	/// @DnDArgument : "speed" "40"
	/// @DnDArgument : "relative" "true"
	/// @DnDSaveInfo : "path" "51c200a6-a5a1-4aee-9b09-55296bbe01a1"
	with(obj_shop_menu) path_start(shop, 40, path_action_stop, true);

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 1FDB932D
	/// @DnDParent : 56F18384
	/// @DnDArgument : "var" "global.ui"
	/// @DnDArgument : "value" "false"
	if(global.ui == false)
	{
		/// @DnDAction : YoYo Games.Paths.Start_Path
		/// @DnDVersion : 1.1
		/// @DnDHash : 6FBF1299
		/// @DnDApplyTo : dce01893-52fc-4398-ad95-8c88eef129f0
		/// @DnDParent : 1FDB932D
		/// @DnDArgument : "path" "ui_info_open"
		/// @DnDArgument : "speed" "40"
		/// @DnDArgument : "relative" "true"
		/// @DnDSaveInfo : "path" "9dcfed44-8f1f-4d49-ada5-ce48f3e2d95d"
		with(obj_ui_info) path_start(ui_info_open, 40, path_action_stop, true);
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 77025513
		/// @DnDParent : 1FDB932D
		/// @DnDArgument : "expr" "true"
		/// @DnDArgument : "var" "global.ui"
		global.ui = true;
	}
}