{
    "id": "f4d08bcb-28f4-4f01-8e65-dc0ad26c8a4a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rich_prescence",
    "eventList": [
        {
            "id": "db149cd7-1317-454a-ad23-a46da0b206a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f4d08bcb-28f4-4f01-8e65-dc0ad26c8a4a"
        },
        {
            "id": "6239d35a-edf5-4c72-ab19-0f70194275b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f4d08bcb-28f4-4f01-8e65-dc0ad26c8a4a"
        },
        {
            "id": "381c7f41-fd78-4bbd-bcce-990334d9666f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "f4d08bcb-28f4-4f01-8e65-dc0ad26c8a4a"
        },
        {
            "id": "8b0c8ea1-c614-49e4-816f-d1840e12505e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f4d08bcb-28f4-4f01-8e65-dc0ad26c8a4a"
        },
        {
            "id": "8ffc586c-54d2-4231-95c0-d78f1224c7cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "f4d08bcb-28f4-4f01-8e65-dc0ad26c8a4a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "d594ad90-4395-4845-a650-cce94970244f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "3e57fc06-10c4-4446-ac2b-5b22a2821455",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "57a28299-3f08-4c4c-b02b-63153f9f07d4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "4f0b6c8c-e763-400f-9556-3221bfd599d9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}