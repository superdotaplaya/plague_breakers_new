/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 6308A08D
/// @DnDArgument : "x" "100"
/// @DnDArgument : "y" "200"
/// @DnDArgument : "caption" ""Lead Developer: SuperDotaPlaya""
draw_text(100, 200, string("Lead Developer: SuperDotaPlaya") + "");

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 0CB1772B
/// @DnDArgument : "x" "100"
/// @DnDArgument : "y" "250"
/// @DnDArgument : "caption" ""Lead Artist (Sound and Art): Fish""
draw_text(100, 250, string("Lead Artist (Sound and Art): Fish") + "");

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 7F860052
/// @DnDArgument : "x" "100"
/// @DnDArgument : "y" "300"
/// @DnDArgument : "caption" ""Lead Artist (Art): H3rsh3yb3ar""
draw_text(100, 300, string("Lead Artist (Art): H3rsh3yb3ar") + "");

/// @DnDAction : YoYo Games.Drawing.Set_Alignment
/// @DnDVersion : 1.1
/// @DnDHash : 5CA4E31A
draw_set_halign(fa_left);
draw_set_valign(fa_top);

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 5E8E521B
/// @DnDArgument : "x" "100"
/// @DnDArgument : "y" "350"
/// @DnDArgument : "caption" ""http://www.ludicarts.com/free-videogame-asset-desert/""
draw_text(100, 350, string("http://www.ludicarts.com/free-videogame-asset-desert/") + "");

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 3B01A9EB
/// @DnDArgument : "x" "100"
/// @DnDArgument : "y" "400"
/// @DnDArgument : "caption" ""We edited this to make it a bit more pixelated so it fit our theme better""
draw_text(100, 400, string("We edited this to make it a bit more pixelated so it fit our theme better") + "");