{
    "id": "be793010-f844-49b1-b89c-110f1bce9af7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_smoke_enemy",
    "eventList": [
        {
            "id": "c0e82bd2-7cdc-44c0-9deb-119936a67a51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "be793010-f844-49b1-b89c-110f1bce9af7"
        },
        {
            "id": "d99ece2a-4942-4a2f-a922-b0da5d03c94d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "be793010-f844-49b1-b89c-110f1bce9af7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e22497ba-920f-43fa-9f25-7ed63435e55d",
    "visible": true
}