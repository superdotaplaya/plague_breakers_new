/// @description HTTP Event Success
request_result_data_parsed = json_decode(request_result_data);
if(request_result_data_parsed == -1)
{
	event_user(eGameOnPromiseEvent.FAILURE);
	return;
}
	
status = eGameOnPromiseStatus.SUCCESS;

// Parse player tokens
oGameOnClient.player_token = request_result_data_parsed[? "playerToken"];
oGameOnClient.player_id = request_result_data_parsed[? "externalPlayerId"];
oGameOnClient.state = eGameOnState.REGISTERED;
gameon_util_session_data_save();

gameon_util_log("Successfully registered player. ID: " + string(oGameOnClient.player_id) + ". Token: " + string(oGameOnClient.player_token));

// Authenticate the player immediately after registering if requested. 
// Otherwise, trigger the on success callback now.
if(auth_on_success)
{
	gameon_util_log("Authenticating registered player..");
	var authPromise = gameon_player_auth(oGameOnClient.player_token, callback_success, callback_failed);
	authPromise.delegate_instance = self.delegate_instance;
	authPromise.delegate_event_success = self.delegate_event_success;
	authPromise.delegate_event_failure = self.delegate_event_failure;
}
else
{
	// Call success user event on delegate
	if(delegate_event_success >= 0 && !is_undefined(delegate_instance) && instance_exists(delegate_instance))
	{
		var eventIdx = delegate_event_success;
		var currentPromise = self;
		with(delegate_instance) 
		{ 
			delegate_current_promise = currentPromise;
			event_user(eventIdx);
			delegate_current_promise = undefined;

		}
	}
	
	// Call success callback
	if(is_undefined(callback_success) == false)
		script_execute(callback_success, self);

}