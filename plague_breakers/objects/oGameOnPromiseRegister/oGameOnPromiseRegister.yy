{
    "id": "cf9bb21c-57bb-40a7-883d-339667f43063",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameOnPromiseRegister",
    "eventList": [
        {
            "id": "ea983579-54f2-4c7f-ae0a-21d281703909",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "cf9bb21c-57bb-40a7-883d-339667f43063"
        },
        {
            "id": "45b5c28a-cacc-4b31-b29f-fed37e038252",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "cf9bb21c-57bb-40a7-883d-339667f43063"
        },
        {
            "id": "3bb8c87d-b776-4a43-b868-9a33bc8e3612",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cf9bb21c-57bb-40a7-883d-339667f43063"
        },
        {
            "id": "38b629c3-9653-4a3c-a7db-5d63981aa887",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "cf9bb21c-57bb-40a7-883d-339667f43063"
        },
        {
            "id": "a880adee-1b78-403b-a1fa-d3f7b5388ddb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 15,
            "eventtype": 7,
            "m_owner": "cf9bb21c-57bb-40a7-883d-339667f43063"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6d6eb90d-22ce-42f0-b45c-d4b59286039c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}