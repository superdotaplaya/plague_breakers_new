{
    "id": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_orange_tower",
    "eventList": [
        {
            "id": "d6cd1d33-12cf-4a37-81ed-30244bd8c969",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "c40f9e2e-019d-4c2b-88f9-fca83776a903",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "36891afd-fab6-4e4c-ad2a-79f9acb2b738",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "b7546cc5-8064-4747-89ac-e6febb167f13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "718d7a29-936f-4537-91fe-d0c5466e5eee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "41e108bb-d825-49f3-9808-8590bcb890ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "92399fc0-8055-431c-9640-665587105aa7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "6da8f1cd-86d1-47b6-9719-6e3f4125b5a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "ad35aff5-ac1b-4d58-b563-5b4ac80dd1f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "9cc4d931-9667-425f-830e-e63bcd29b58d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "5532c962-51b4-4e10-868d-fca5dfe2ec41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "d30538ed-87be-455e-90f4-45abf8e750b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "33df5457-6377-419e-86c1-066bed7d49ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "83d3a17c-e910-4ae2-b5b3-85a1afc5f251",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "f0875ffd-fd8e-428c-aabc-956eb91215a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4e7204c-bd0b-46b6-b2a1-b15035274049",
    "visible": true
}