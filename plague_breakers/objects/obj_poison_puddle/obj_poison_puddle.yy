{
    "id": "d07717ff-28ac-413a-9058-f35867a62618",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_poison_puddle",
    "eventList": [
        {
            "id": "fe51c6fc-8256-4ae5-8cdf-d7e2f4967eed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d07717ff-28ac-413a-9058-f35867a62618"
        },
        {
            "id": "0b18d221-f7c2-418c-97ec-93b31f5f6c75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d07717ff-28ac-413a-9058-f35867a62618"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7e33d25b-48a8-4127-8cc6-b0829cb4b5ac",
    "visible": true
}