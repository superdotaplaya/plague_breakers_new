{
    "id": "d1853bf4-a72e-4cd2-9e46-d6356598a588",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shop_arrow",
    "eventList": [
        {
            "id": "79804982-99f5-4ca6-8229-b1fc5609ac7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d1853bf4-a72e-4cd2-9e46-d6356598a588"
        },
        {
            "id": "622a4218-d29d-4ef6-a5de-e97ac0b0a4da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d1853bf4-a72e-4cd2-9e46-d6356598a588"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "be334386-c050-4fe3-a460-006da0e54d45",
    "visible": true
}