{
    "id": "1301dd57-5cb0-4f6b-b2d9-ff4ca676fcc5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_rebind_shop7",
    "eventList": [
        {
            "id": "99fb8d0a-ccf8-451c-9d33-ab510696a071",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1301dd57-5cb0-4f6b-b2d9-ff4ca676fcc5"
        },
        {
            "id": "8454c7b8-c7c7-45ca-aa99-aecd9254e2cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1301dd57-5cb0-4f6b-b2d9-ff4ca676fcc5"
        },
        {
            "id": "4519ad86-7a5f-42e8-a244-ebf9b30f01f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "1301dd57-5cb0-4f6b-b2d9-ff4ca676fcc5"
        },
        {
            "id": "15445562-b5fb-491b-8aaa-23b3416fc0e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "1301dd57-5cb0-4f6b-b2d9-ff4ca676fcc5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a832f2b-2273-4cc3-b553-8c06b7dc617f",
    "visible": true
}