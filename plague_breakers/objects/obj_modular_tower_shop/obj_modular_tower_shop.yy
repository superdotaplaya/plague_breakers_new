{
    "id": "87bfa26d-fea3-43b3-919d-d3eb5fe3dfd7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_modular_tower_shop",
    "eventList": [
        {
            "id": "295de854-b7a1-4ec6-9631-b473d3fedca0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "87bfa26d-fea3-43b3-919d-d3eb5fe3dfd7"
        },
        {
            "id": "ad7dbfba-d477-4f8b-a853-83d557697744",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "87bfa26d-fea3-43b3-919d-d3eb5fe3dfd7"
        },
        {
            "id": "b6354a1d-ea6c-43ad-8466-23d235e0bf8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "87bfa26d-fea3-43b3-919d-d3eb5fe3dfd7"
        },
        {
            "id": "67c51c94-1c16-4829-bc4a-a7fd7d5fb94d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "87bfa26d-fea3-43b3-919d-d3eb5fe3dfd7"
        },
        {
            "id": "c58da2ec-6a84-4dcc-966f-3abef11768e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "87bfa26d-fea3-43b3-919d-d3eb5fe3dfd7"
        },
        {
            "id": "37b0f4b1-40e7-480c-9b3c-c70f7e7882f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "87bfa26d-fea3-43b3-919d-d3eb5fe3dfd7"
        },
        {
            "id": "ab4c9007-0b88-4158-ac74-bb00c9fb31c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "87bfa26d-fea3-43b3-919d-d3eb5fe3dfd7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "692933ab-3c13-4a12-b13e-3265730f1324",
    "visible": true
}