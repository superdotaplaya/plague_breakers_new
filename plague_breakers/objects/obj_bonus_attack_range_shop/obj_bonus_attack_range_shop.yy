{
    "id": "01c1534f-2f34-4584-8541-73d76910b682",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bonus_attack_range_shop",
    "eventList": [
        {
            "id": "3960d3e2-cc58-4363-ab15-c66e7a931e4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "01c1534f-2f34-4584-8541-73d76910b682"
        },
        {
            "id": "71ce3adf-a711-423f-a50f-d87feceb5ad4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "01c1534f-2f34-4584-8541-73d76910b682"
        },
        {
            "id": "703ad2dd-6e9c-4cb4-ab6d-14682150998c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "01c1534f-2f34-4584-8541-73d76910b682"
        },
        {
            "id": "d367275c-6561-461b-bd58-e232804340ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "01c1534f-2f34-4584-8541-73d76910b682"
        },
        {
            "id": "64db2548-4139-4df3-a752-cd6d613b78f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "01c1534f-2f34-4584-8541-73d76910b682"
        },
        {
            "id": "56fe113d-c994-4e76-96ae-9e4a1945a2f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "01c1534f-2f34-4584-8541-73d76910b682"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "86e1ef81-744a-4a49-b005-72f7f36bddf5",
    "visible": true
}