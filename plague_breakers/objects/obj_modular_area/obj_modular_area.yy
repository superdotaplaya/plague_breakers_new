{
    "id": "bdf1ed79-e8a5-4a31-86f4-90e162d41bee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_modular_area",
    "eventList": [
        {
            "id": "8eeee7f5-0ce7-4344-8099-791dffdb48a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bdf1ed79-e8a5-4a31-86f4-90e162d41bee"
        },
        {
            "id": "a7a8a3cb-9787-4ca4-a987-c35977d51277",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bdf1ed79-e8a5-4a31-86f4-90e162d41bee"
        },
        {
            "id": "27a1f327-c26d-4230-b9e7-330466070efb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "bdf1ed79-e8a5-4a31-86f4-90e162d41bee"
        },
        {
            "id": "0ab3b1f5-2403-4ffb-a747-160ad91cbf36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bdf1ed79-e8a5-4a31-86f4-90e162d41bee"
        },
        {
            "id": "49dbb8d3-6f9a-4ad9-8adb-e046d431323f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e79f9a45-e3f7-4d5b-be0d-7022e4227488",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bdf1ed79-e8a5-4a31-86f4-90e162d41bee"
        },
        {
            "id": "a847b9c6-cfad-4bb5-a02b-4676242be868",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d61b52f9-1fcc-4338-8c5f-3fe842626af3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bdf1ed79-e8a5-4a31-86f4-90e162d41bee"
        },
        {
            "id": "66e37a6f-8c33-4bbc-beb2-566c6369babd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "95f15ea7-e8f3-487a-b143-3a45457f6eb6",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bdf1ed79-e8a5-4a31-86f4-90e162d41bee"
        },
        {
            "id": "4eb35a52-82c8-4abd-bb4d-7a7402fa61ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "345fca6a-7fab-4f58-ac30-804473824c5a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bdf1ed79-e8a5-4a31-86f4-90e162d41bee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "51d95f11-61b4-46fa-8ebf-1779ada7b4e4",
    "visible": true
}