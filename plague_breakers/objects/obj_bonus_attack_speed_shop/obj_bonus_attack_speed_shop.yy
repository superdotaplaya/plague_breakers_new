{
    "id": "9025d293-f46e-44b7-b332-8687eeb1c7fc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bonus_attack_speed_shop",
    "eventList": [
        {
            "id": "bd5c8a7c-789a-40f7-98d4-292369732463",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "9025d293-f46e-44b7-b332-8687eeb1c7fc"
        },
        {
            "id": "ee38f088-466f-4d67-bd8e-68379d13de51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9025d293-f46e-44b7-b332-8687eeb1c7fc"
        },
        {
            "id": "ece4e266-76bf-4712-8b6e-08d4168cf41f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9025d293-f46e-44b7-b332-8687eeb1c7fc"
        },
        {
            "id": "098c1abc-0762-499f-ae0c-711e20d681ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "9025d293-f46e-44b7-b332-8687eeb1c7fc"
        },
        {
            "id": "e54a2be7-8619-49d6-b481-85719e8065b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "9025d293-f46e-44b7-b332-8687eeb1c7fc"
        },
        {
            "id": "98f6d898-ab7f-4a02-830b-600899402b02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9025d293-f46e-44b7-b332-8687eeb1c7fc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cd319e63-fd52-4717-92e8-dfc9f73a289b",
    "visible": true
}