{
    "id": "912518e6-9fba-4a49-acf7-8e3663a73252",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_rebind_quests",
    "eventList": [
        {
            "id": "18ad6d07-c6a9-44d4-9045-c48a50a561f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "912518e6-9fba-4a49-acf7-8e3663a73252"
        },
        {
            "id": "f7769c0c-69f6-4ad1-8a4b-8755afe3b917",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "912518e6-9fba-4a49-acf7-8e3663a73252"
        },
        {
            "id": "221a20d4-5209-4f62-8420-923e6e5efa48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "912518e6-9fba-4a49-acf7-8e3663a73252"
        },
        {
            "id": "fd182f76-3bd0-4f4f-8bf0-f31d73a5b7ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "912518e6-9fba-4a49-acf7-8e3663a73252"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a832f2b-2273-4cc3-b553-8c06b7dc617f",
    "visible": true
}