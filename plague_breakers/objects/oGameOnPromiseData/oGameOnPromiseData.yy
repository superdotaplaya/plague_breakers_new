{
    "id": "85de35c9-f3d3-4eed-a65d-c6f0d4f34e37",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameOnPromiseData",
    "eventList": [
        {
            "id": "d0454417-a180-44e9-a5a3-a714caa2e6d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "85de35c9-f3d3-4eed-a65d-c6f0d4f34e37"
        },
        {
            "id": "bba27b09-6b1b-40b5-80f5-a06655db37a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "85de35c9-f3d3-4eed-a65d-c6f0d4f34e37"
        },
        {
            "id": "08bda1d9-9cad-4a02-9341-300c2b0b78e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 15,
            "eventtype": 7,
            "m_owner": "85de35c9-f3d3-4eed-a65d-c6f0d4f34e37"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6d6eb90d-22ce-42f0-b45c-d4b59286039c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}