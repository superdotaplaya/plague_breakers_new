/// @description Request Success
var requestResultJSON = json_decode(request_result_data);

// If we have a list key for this promise response data, try parsing it as a list response
if(!is_undefined(request_result_list_key))
{
	request_result_data_parsed = ds_list_create();
	gameon_util_data_list_parse(requestResultJSON, request_result_data_parsed, 
								request_result_data_type, request_result_list_key);
}
else
{
	request_result_data_parsed = gameon_util_object_create(request_result_data_type);
	gameon_util_data_parse(request_result_data_parsed, requestResultJSON);
}

ds_map_destroy(requestResultJSON);
event_inherited();