/// @description Clean up response data
if(!is_undefined(request_result_data_parsed) && 
	ds_exists(request_result_data_parsed, ds_type_list) && 
	!is_undefined(request_result_list_key))
{
	// Clean up data instances in our result data list
	var entryCount = ds_list_size(request_result_data_parsed);
	for(var entryIdx = 0; entryIdx < entryCount; ++entryIdx)
	{
		if(gameon_util_object_exists(request_result_data_parsed[| entryIdx]))
			gameon_util_object_destroy(request_result_data_parsed[| entryIdx]);
	}
	
	// Destroy the list itself
	ds_list_destroy(request_result_data_parsed);
	request_result_data_parsed = undefined;
}

event_inherited();