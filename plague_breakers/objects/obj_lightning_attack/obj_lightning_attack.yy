{
    "id": "14af6861-f011-4f09-808a-273707bdbcc7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lightning_attack",
    "eventList": [
        {
            "id": "55cc04bc-f6bb-4e5a-9bd1-5868fcefb745",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "14af6861-f011-4f09-808a-273707bdbcc7"
        },
        {
            "id": "20a15f91-85c1-421c-8363-b7e4e3b19ab2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "14af6861-f011-4f09-808a-273707bdbcc7"
        },
        {
            "id": "63d1dbfe-cea1-484a-aa91-6d679abf1795",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "14af6861-f011-4f09-808a-273707bdbcc7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e29dc5b6-03d6-4bfb-b56c-2623d37140c5",
    "visible": true
}