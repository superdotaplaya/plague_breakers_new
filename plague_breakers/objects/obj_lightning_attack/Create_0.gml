/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 654095A1
/// @DnDArgument : "code" "target = instance_nearest(x,y,obj_enemy)$(13_10)// Determines how many times the projectile can bounce$(13_10)hits = 0$(13_10)damage = variable_instance_get(instance_nearest(x,y,obj_tesla_tower), "damage")$(13_10)target2 = noone$(13_10)last_hit = instance_nearest(x,y,obj_enemy)$(13_10)// Rescales the sptie to half the size$(13_10)image_xscale = .5$(13_10)image_yscale = .5$(13_10)move_towards_point(target.x,target.y,10)$(13_10)"
target = instance_nearest(x,y,obj_enemy)
// Determines how many times the projectile can bounce
hits = 0
damage = variable_instance_get(instance_nearest(x,y,obj_tesla_tower), "damage")
target2 = noone
last_hit = instance_nearest(x,y,obj_enemy)
// Rescales the sptie to half the size
image_xscale = .5
image_yscale = .5
move_towards_point(target.x,target.y,10)