/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 29CAC0D2
/// @DnDArgument : "code" "//Checks to see if the projectile has bounced 3 times$(13_10)if hits <= 3 and instance_nearest(x,y,obj_enemy) = target then$(13_10){$(13_10)last_hit = target$(13_10)// Finds a random nearby enemy to bounce to$(13_10)rand_enemy = irandom_range(2,3)$(13_10)target = instance_nth_nearest(x,y,obj_enemy,rand_enemy)$(13_10)move_towards_point(target.x,target.y,10)$(13_10)// adds 1 to the hit count$(13_10)hits += 1$(13_10)// removes the hp from the enemy$(13_10)enemy_hp = variable_instance_get(instance_nearest(x,y,obj_enemy),"hp")$(13_10)variable_instance_set(instance_nearest(x,y,obj_enemy),"hp", (enemy_hp - damage))$(13_10)}$(13_10)else$(13_10){$(13_10)	$(13_10)}$(13_10)// If the projectile did hit 3 times it gets destroyed$(13_10)if hits = 3 then$(13_10){$(13_10)instance_destroy()$(13_10)}"
//Checks to see if the projectile has bounced 3 times
if hits <= 3 and instance_nearest(x,y,obj_enemy) = target then
{
last_hit = target
// Finds a random nearby enemy to bounce to
rand_enemy = irandom_range(2,3)
target = instance_nth_nearest(x,y,obj_enemy,rand_enemy)
move_towards_point(target.x,target.y,10)
// adds 1 to the hit count
hits += 1
// removes the hp from the enemy
enemy_hp = variable_instance_get(instance_nearest(x,y,obj_enemy),"hp")
variable_instance_set(instance_nearest(x,y,obj_enemy),"hp", (enemy_hp - damage))
}
else
{
	
}
// If the projectile did hit 3 times it gets destroyed
if hits = 3 then
{
instance_destroy()
}