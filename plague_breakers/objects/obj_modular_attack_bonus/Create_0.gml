/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 6A248C9F
/// @DnDArgument : "code" "target = instance_nearest(x,y,obj_enemy)$(13_10)randx = irandom_range(-300,300)$(13_10)randy = irandom_range(-300,300)$(13_10)move_towards_point(target.x+randx,target.y+randy,20)$(13_10)alarm_set(0,100)$(13_10)attacker = instance_nearest(x,y,obj_green_tower)$(13_10)damage = variable_instance_get(instance_nearest(x,y,obj_modular_tower),"damage")$(13_10)"
target = instance_nearest(x,y,obj_enemy)
randx = irandom_range(-300,300)
randy = irandom_range(-300,300)
move_towards_point(target.x+randx,target.y+randy,20)
alarm_set(0,100)
attacker = instance_nearest(x,y,obj_green_tower)
damage = variable_instance_get(instance_nearest(x,y,obj_modular_tower),"damage")