{
    "id": "2ee197c4-6fe8-4126-b1e6-713f72886e75",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_modular_attack_bonus",
    "eventList": [
        {
            "id": "01c29510-31b7-4a04-be58-cecf0a925f82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2ee197c4-6fe8-4126-b1e6-713f72886e75"
        },
        {
            "id": "8559d8ca-95b4-4d28-845f-38938864a7ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2ee197c4-6fe8-4126-b1e6-713f72886e75"
        },
        {
            "id": "d1751082-bc94-4249-bfd7-7411864de07b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2ee197c4-6fe8-4126-b1e6-713f72886e75"
        },
        {
            "id": "7afbe764-5776-4d58-9281-ef90e42b9498",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2ee197c4-6fe8-4126-b1e6-713f72886e75"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "253138d9-82ea-4fef-9b55-02494aead4bd",
    "visible": true
}