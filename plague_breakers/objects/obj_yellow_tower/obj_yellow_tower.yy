{
    "id": "12a615a1-fa0f-4167-95d1-7acdee951786",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_yellow_tower",
    "eventList": [
        {
            "id": "a75d26bb-2f91-4f01-a501-8b34cc8ac2d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "12a615a1-fa0f-4167-95d1-7acdee951786"
        },
        {
            "id": "c15e124d-0979-480c-a291-94ec770316b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "12a615a1-fa0f-4167-95d1-7acdee951786"
        },
        {
            "id": "f744ff56-96af-4935-8b33-71c78ed988d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "12a615a1-fa0f-4167-95d1-7acdee951786"
        },
        {
            "id": "d1aa2496-fee8-4747-805a-7e5a1af00e3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "12a615a1-fa0f-4167-95d1-7acdee951786"
        },
        {
            "id": "da65b88a-ecca-480d-bb7e-656c9876ac57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "12a615a1-fa0f-4167-95d1-7acdee951786"
        },
        {
            "id": "679b06c7-2977-42b2-b6df-b2e317b38552",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "12a615a1-fa0f-4167-95d1-7acdee951786"
        },
        {
            "id": "84f601e4-c037-4f1b-9476-b0c66ef74ff0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "12a615a1-fa0f-4167-95d1-7acdee951786"
        },
        {
            "id": "2070449b-1c3e-4390-b141-7426b8f4af0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "12a615a1-fa0f-4167-95d1-7acdee951786"
        },
        {
            "id": "8ec414b4-5c30-463d-85fd-78af3acf1432",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "12a615a1-fa0f-4167-95d1-7acdee951786"
        },
        {
            "id": "bd317d13-d1fb-4c3e-adbf-aabd3c6c2898",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "12a615a1-fa0f-4167-95d1-7acdee951786"
        },
        {
            "id": "4dcae89d-f525-492e-8f6b-18643fa6c384",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "12a615a1-fa0f-4167-95d1-7acdee951786"
        },
        {
            "id": "89530906-f128-4ade-ad1b-e5e394284fca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "12a615a1-fa0f-4167-95d1-7acdee951786"
        },
        {
            "id": "b68483d2-26ef-44a4-8849-56f909b0ce9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "12a615a1-fa0f-4167-95d1-7acdee951786"
        },
        {
            "id": "c17f2ed1-862f-4c10-a62a-9d928b902918",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "f0875ffd-fd8e-428c-aabc-956eb91215a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "12a615a1-fa0f-4167-95d1-7acdee951786"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c82849b0-f7ad-4a5d-aadd-977a6138ee59",
    "visible": true
}