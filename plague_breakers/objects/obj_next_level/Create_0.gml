/// @DnDAction : YoYo Games.Instances.Sprite_Scale
/// @DnDVersion : 1
/// @DnDHash : 14BA3BDD
/// @DnDArgument : "xscale" ".5"
/// @DnDArgument : "yscale" ".5"
image_xscale = .5;
image_yscale = .5;

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 62F84368
/// @DnDArgument : "code" "instance_destroy(instance_nearest(x,y,obj_speedup))$(13_10)instance_destroy(instance_nearest(x,y,obj_speeddown))$(13_10)if global.autostart = true then $(13_10){$(13_10)alarm_set(0,300)	$(13_10)}"
instance_destroy(instance_nearest(x,y,obj_speedup))
instance_destroy(instance_nearest(x,y,obj_speeddown))
if global.autostart = true then 
{
alarm_set(0,300)	
}