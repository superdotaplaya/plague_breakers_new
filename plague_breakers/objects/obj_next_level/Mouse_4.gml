/// @DnDAction : YoYo Games.Instances.Set_Alarm
/// @DnDVersion : 1
/// @DnDHash : 44EC48A3
/// @DnDApplyTo : ab303306-eb35-4318-8ad1-e68f47107931
/// @DnDArgument : "steps" "rate"
with(obj_enemy_spawner) {
alarm_set(0, rate);

}

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 269926ED
/// @DnDArgument : "code" "instance_activate_layer("Instances_1")$(13_10)if global.speedup = true then$(13_10){$(13_10)instance_create_layer(x,y,"shop_icons",obj_speeddown)$(13_10)}$(13_10)if global.speedup = false then$(13_10){$(13_10)	instance_create_layer(x,y,"shop_icons",obj_speedup)$(13_10)	$(13_10)}$(13_10)global.repair_mode = false$(13_10)instance_destroy()"
instance_activate_layer("Instances_1")
if global.speedup = true then
{
instance_create_layer(x,y,"shop_icons",obj_speeddown)
}
if global.speedup = false then
{
	instance_create_layer(x,y,"shop_icons",obj_speedup)
	
}
global.repair_mode = false
instance_destroy()