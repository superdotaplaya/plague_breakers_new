{
    "id": "aebbf5ff-b177-45c8-a235-8b4b05766816",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_next_level",
    "eventList": [
        {
            "id": "b8ecb42e-819f-472e-b023-358436452f2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aebbf5ff-b177-45c8-a235-8b4b05766816"
        },
        {
            "id": "bc7b10e6-3127-4c99-9947-91d3261cc90b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "aebbf5ff-b177-45c8-a235-8b4b05766816"
        },
        {
            "id": "7224ede0-0131-4d05-af65-486eddb11d68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "aebbf5ff-b177-45c8-a235-8b4b05766816"
        },
        {
            "id": "cac222bc-3234-41c5-8a4a-581808e2a585",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "aebbf5ff-b177-45c8-a235-8b4b05766816"
        },
        {
            "id": "bc5544dc-b147-46c1-ab44-e0ee1e8094fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "aebbf5ff-b177-45c8-a235-8b4b05766816"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1795b121-2cd7-4e63-a7bf-3fc0cde4db8c",
    "visible": true
}