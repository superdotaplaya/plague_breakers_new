/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 5764FB16
/// @DnDArgument : "code" "// adds the wrench sprite to the top rioght corener to show its in repair mode$(13_10)if global.repair_mode = true then$(13_10){$(13_10)draw_sprite_stretched(spr_wrench,0,2300,60,64,64)$(13_10)}$(13_10)// Draws text to tell the player they cant afford the item/tower$(13_10)if global.not_enough_gold = true then$(13_10){$(13_10)draw_set_color(c_red)	$(13_10)draw_text((window_get_width()/2)+150,100,"Not Enough Gold")$(13_10)} $(13_10)// Sets the progress bar for fortify to red if not fully charges$(13_10)if global.fortify_progress <= 99 then$(13_10){$(13_10)draw_healthbar_circular(130,1320,64,90,(global.fortify_progress/100)*100,spr_healthbar)$(13_10)} else $(13_10)// Sets progress bar to green if fully charged and can be used$(13_10)if global.fortify_progress >= 100 then$(13_10){$(13_10)draw_healthbar_circular(130,1320,64,90,100,spr_healthbar_green)	$(13_10)}"
// adds the wrench sprite to the top rioght corener to show its in repair mode
if global.repair_mode = true then
{
draw_sprite_stretched(spr_wrench,0,2300,60,64,64)
}
// Draws text to tell the player they cant afford the item/tower
if global.not_enough_gold = true then
{
draw_set_color(c_red)	
draw_text((window_get_width()/2)+150,100,"Not Enough Gold")
} 
// Sets the progress bar for fortify to red if not fully charges
if global.fortify_progress <= 99 then
{
draw_healthbar_circular(130,1320,64,90,(global.fortify_progress/100)*100,spr_healthbar)
} else 
// Sets progress bar to green if fully charged and can be used
if global.fortify_progress >= 100 then
{
draw_healthbar_circular(130,1320,64,90,100,spr_healthbar_green)	
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 6B4737FA
/// @DnDArgument : "var" "global.ui"
/// @DnDArgument : "value" "true"
if(global.ui == true)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Color
	/// @DnDVersion : 1
	/// @DnDHash : 373B2837
	/// @DnDParent : 6B4737FA
	draw_set_colour($FFFFFFFF & $ffffff);
	var l373B2837_0=($FFFFFFFF >> 24);
	draw_set_alpha(l373B2837_0 / $ff);

	/// @DnDAction : YoYo Games.Drawing.Draw_Value
	/// @DnDVersion : 1
	/// @DnDHash : 71DAD389
	/// @DnDParent : 6B4737FA
	/// @DnDArgument : "x" "obj_ui_info.x + 170"
	/// @DnDArgument : "y" "obj_ui_info.y + 50"
	/// @DnDArgument : "caption" """"
	/// @DnDArgument : "var" "string(global.gold)"
	draw_text(obj_ui_info.x + 170, obj_ui_info.y + 50, string("") + string(string(global.gold)));

	/// @DnDAction : YoYo Games.Drawing.Draw_Value
	/// @DnDVersion : 1
	/// @DnDHash : 141D5255
	/// @DnDParent : 6B4737FA
	/// @DnDArgument : "x" "obj_ui_info.x + 170"
	/// @DnDArgument : "y" "obj_ui_info.y + 5"
	/// @DnDArgument : "caption" """"
	/// @DnDArgument : "var" ""                      " + string(key_name_convert(global.infoHotkey)) + " to close""
	draw_text(obj_ui_info.x + 170, obj_ui_info.y + 5, string("") + string("                      " + string(key_name_convert(global.infoHotkey)) + " to close"));

	/// @DnDAction : YoYo Games.Drawing.Set_Color
	/// @DnDVersion : 1
	/// @DnDHash : 5E2051F0
	/// @DnDParent : 6B4737FA
	/// @DnDArgument : "color" "$FF0800FF"
	draw_set_colour($FF0800FF & $ffffff);
	var l5E2051F0_0=($FF0800FF >> 24);
	draw_set_alpha(l5E2051F0_0 / $ff);

	/// @DnDAction : YoYo Games.Drawing.Draw_Value
	/// @DnDVersion : 1
	/// @DnDHash : 7F6785D8
	/// @DnDParent : 6B4737FA
	/// @DnDArgument : "x" "obj_ui_info.x + 90"
	/// @DnDArgument : "y" "obj_ui_info.y + 150"
	/// @DnDArgument : "caption" """"
	/// @DnDArgument : "var" "global.hp"
	draw_text(obj_ui_info.x + 90, obj_ui_info.y + 150, string("") + string(global.hp));

	/// @DnDAction : YoYo Games.Drawing.Set_Color
	/// @DnDVersion : 1
	/// @DnDHash : 08A316D0
	/// @DnDParent : 6B4737FA
	draw_set_colour($FFFFFFFF & $ffffff);
	var l08A316D0_0=($FFFFFFFF >> 24);
	draw_set_alpha(l08A316D0_0 / $ff);

	/// @DnDAction : YoYo Games.Drawing.Draw_Value
	/// @DnDVersion : 1
	/// @DnDHash : 165E19D9
	/// @DnDParent : 6B4737FA
	/// @DnDArgument : "x" "obj_ui_info.x + 20"
	/// @DnDArgument : "y" "obj_ui_info.y + 225"
	/// @DnDArgument : "caption" ""Wave: ""
	/// @DnDArgument : "var" "global.currentlevel"
	draw_text(obj_ui_info.x + 20, obj_ui_info.y + 225, string("Wave: ") + string(global.currentlevel));
}