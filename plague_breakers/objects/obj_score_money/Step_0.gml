/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 3CA2EA0A
/// @DnDArgument : "code" "global.gold = round(global.gold)$(13_10)// Opens a twitter page for the player to share their score when they lose$(13_10)if global.hp <= 0 then$(13_10){$(13_10)		fast_file_key_crypt("_amazon_gameon_session.dat","_amazon_gameon_session.dat",true,"work")$(13_10)file_delete("autosave.dat")$(13_10)twit_post("I made it to wave " + string(global.currentlevel) + ". What level can you make it too? Go download it here: https://superdotaplaya.itch.io/siege-defenders A unique and interesting tower defense game with damageable towers and highground elements!") 	$(13_10)room_goto(0)$(13_10)}$(13_10)// Makes sure you can only repair towers between waves$(13_10)if instance_exists(obj_next_level) then$(13_10){$(13_10)global.repair_possible = true	$(13_10)}$(13_10)else$(13_10){$(13_10)global.repair_possible = false	$(13_10)}$(13_10)// handles the fortify progrss bar and fortifying$(13_10)if global.fortify_progress >= 100 then$(13_10){$(13_10)global.canfortify = true$(13_10)global.fortify_progress = 100$(13_10)} else$(13_10){$(13_10)global.canfortify = false	$(13_10)}$(13_10)"
global.gold = round(global.gold)
// Opens a twitter page for the player to share their score when they lose
if global.hp <= 0 then
{
		fast_file_key_crypt("_amazon_gameon_session.dat","_amazon_gameon_session.dat",true,"work")
file_delete("autosave.dat")
twit_post("I made it to wave " + string(global.currentlevel) + ". What level can you make it too? Go download it here: https://superdotaplaya.itch.io/siege-defenders A unique and interesting tower defense game with damageable towers and highground elements!") 	
room_goto(0)
}
// Makes sure you can only repair towers between waves
if instance_exists(obj_next_level) then
{
global.repair_possible = true	
}
else
{
global.repair_possible = false	
}
// handles the fortify progrss bar and fortifying
if global.fortify_progress >= 100 then
{
global.canfortify = true
global.fortify_progress = 100
} else
{
global.canfortify = false	
}