/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 0EB97398
/// @DnDArgument : "var" "keyboard_lastchar"
/// @DnDArgument : "value" "chr(global.backKey)"
if(keyboard_lastchar == chr(global.backKey))
{
	/// @DnDAction : YoYo Games.Instances.Color_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 3354756F
	/// @DnDApplyTo : 7b4a9b34-6545-4240-869f-e66525bd0fcc
	/// @DnDParent : 0EB97398
	with(obj_grid) {
	image_blend = $FFFFFFFF & $ffffff;
	image_alpha = ($FFFFFFFF >> 24) / $ff;
	}

	/// @DnDAction : YoYo Games.Common.Function_Call
	/// @DnDVersion : 1
	/// @DnDHash : 0C762BA7
	/// @DnDParent : 0EB97398
	/// @DnDArgument : "function" "instance_destroy_shop"
	instance_destroy_shop();

	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 1D9DAB84
	/// @DnDParent : 0EB97398
	/// @DnDArgument : "code" "/// @description Execute Code$(13_10)global.shop_open = false$(13_10)instance_activate_layer("Instances")$(13_10)instance_destroy(obj_tooltips)"
	/// @description Execute Code
	global.shop_open = false
	instance_activate_layer("Instances")
	instance_destroy(obj_tooltips)

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 30B82690
	/// @DnDParent : 0EB97398
	/// @DnDArgument : "var" "global.shop_open"
	/// @DnDArgument : "value" "true"
	if(global.shop_open == true)
	{
		/// @DnDAction : YoYo Games.Paths.Start_Path
		/// @DnDVersion : 1.1
		/// @DnDHash : 72529373
		/// @DnDApplyTo : 37f57748-c862-493a-bbbd-c8ffdf8f8d60
		/// @DnDParent : 30B82690
		/// @DnDArgument : "path" "shop_close"
		/// @DnDArgument : "speed" "40"
		/// @DnDArgument : "relative" "true"
		/// @DnDSaveInfo : "path" "82e069ee-3c69-4d32-95f0-56a32a932d1f"
		with(obj_shop_menu) path_start(shop_close, 40, path_action_stop, true);
	
		/// @DnDAction : YoYo Games.Common.Set_Global
		/// @DnDVersion : 1
		/// @DnDHash : 025DC73E
		/// @DnDParent : 30B82690
		/// @DnDArgument : "value" "false"
		/// @DnDArgument : "var" "shop_open"
		global.shop_open = false;
	}
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 5ED6806D
/// @DnDArgument : "var" "keyboard_lastchar"
/// @DnDArgument : "value" "chr(global.trapsHotkey)"
if(keyboard_lastchar == chr(global.trapsHotkey))
{
	/// @DnDAction : YoYo Games.Common.Function_Call
	/// @DnDVersion : 1
	/// @DnDHash : 19E93008
	/// @DnDParent : 5ED6806D
	/// @DnDArgument : "function" "instance_destroy_shop"
	instance_destroy_shop();

	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 7235629E
	/// @DnDParent : 5ED6806D
	/// @DnDArgument : "code" "instance_create_layer(0,0,"shop_icons",obj_mud_buy)$(13_10)instance_create_layer(0,0,"shop_icons",obj_wall_buy)$(13_10)instance_create_layer(0,0,"shop_icons",obj_coin_vacuum_buy)$(13_10)instance_activate_layer("Instances")$(13_10)instance_destroy(obj_tooltips)"
	instance_create_layer(0,0,"shop_icons",obj_mud_buy)
	instance_create_layer(0,0,"shop_icons",obj_wall_buy)
	instance_create_layer(0,0,"shop_icons",obj_coin_vacuum_buy)
	instance_activate_layer("Instances")
	instance_destroy(obj_tooltips)

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 768B8779
	/// @DnDParent : 5ED6806D
	/// @DnDArgument : "var" "global.shop_open"
	/// @DnDArgument : "value" "false"
	if(global.shop_open == false)
	{
		/// @DnDAction : YoYo Games.Common.Set_Global
		/// @DnDVersion : 1
		/// @DnDHash : 73DB4C96
		/// @DnDParent : 768B8779
		/// @DnDArgument : "value" "true"
		/// @DnDArgument : "var" "shop_open"
		global.shop_open = true;
	}

	/// @DnDAction : YoYo Games.Paths.Start_Path
	/// @DnDVersion : 1.1
	/// @DnDHash : 7D722D12
	/// @DnDApplyTo : 37f57748-c862-493a-bbbd-c8ffdf8f8d60
	/// @DnDParent : 5ED6806D
	/// @DnDArgument : "path" "shop"
	/// @DnDArgument : "speed" "40"
	/// @DnDArgument : "relative" "true"
	/// @DnDSaveInfo : "path" "51c200a6-a5a1-4aee-9b09-55296bbe01a1"
	with(obj_shop_menu) path_start(shop, 40, path_action_stop, true);
}

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 4441A793
/// @DnDArgument : "code" "if keyboard_lastchar = chr(global.fortifyHotkey) then$(13_10){$(13_10)if (global.fortify = false and global.canfortify = true) then$(13_10){$(13_10)alarm_set(1,900)$(13_10)global.fortify = true$(13_10)global.canfortify = false$(13_10)global.fortify_progress -= 100$(13_10)alarm_set(2,3000)$(13_10)}}$(13_10)$(13_10)if keyboard_lastchar = chr(global.repairHotkey)$(13_10){$(13_10)if global.repair_mode = false and global.repair_possible = true then$(13_10){$(13_10)global.repair_mode = true$(13_10)} else$(13_10){$(13_10)global.repair_mode = false	$(13_10)}$(13_10)}$(13_10)"
if keyboard_lastchar = chr(global.fortifyHotkey) then
{
if (global.fortify = false and global.canfortify = true) then
{
alarm_set(1,900)
global.fortify = true
global.canfortify = false
global.fortify_progress -= 100
alarm_set(2,3000)
}}

if keyboard_lastchar = chr(global.repairHotkey)
{
if global.repair_mode = false and global.repair_possible = true then
{
global.repair_mode = true
} else
{
global.repair_mode = false	
}
}