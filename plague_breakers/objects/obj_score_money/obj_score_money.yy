{
    "id": "bd506929-690c-4d0b-bcc7-76f64dfd6fdb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_score_money",
    "eventList": [
        {
            "id": "66f5f86e-a3f5-439f-b2be-0c37efa3f6dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bd506929-690c-4d0b-bcc7-76f64dfd6fdb"
        },
        {
            "id": "771e3a57-68a5-4a48-89fe-eff98df8a759",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "bd506929-690c-4d0b-bcc7-76f64dfd6fdb"
        },
        {
            "id": "566303bb-e276-41ce-b939-5c46bdccd0d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bd506929-690c-4d0b-bcc7-76f64dfd6fdb"
        },
        {
            "id": "9b0dbb61-42de-40fd-af6c-8e9430e3038e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "bd506929-690c-4d0b-bcc7-76f64dfd6fdb"
        },
        {
            "id": "21aa4dac-d26a-4d52-8250-aa4c042f19eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "bd506929-690c-4d0b-bcc7-76f64dfd6fdb"
        },
        {
            "id": "5bc8ab4b-0844-4a7c-b03d-9c23540e13b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "bd506929-690c-4d0b-bcc7-76f64dfd6fdb"
        },
        {
            "id": "a6667884-ce0c-4e40-8e22-53c001c50fca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "bd506929-690c-4d0b-bcc7-76f64dfd6fdb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}