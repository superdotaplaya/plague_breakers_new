{
    "id": "37f57748-c862-493a-bbbd-c8ffdf8f8d60",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shop_menu",
    "eventList": [
        {
            "id": "dcc0acd6-e94f-44ab-b5db-cdffc3a33d07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "37f57748-c862-493a-bbbd-c8ffdf8f8d60"
        },
        {
            "id": "27d7cb9e-bbc6-4477-ac48-474a526fc349",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "37f57748-c862-493a-bbbd-c8ffdf8f8d60"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4a5219a4-3109-4c9c-a443-17df7cdb9908",
    "visible": true
}