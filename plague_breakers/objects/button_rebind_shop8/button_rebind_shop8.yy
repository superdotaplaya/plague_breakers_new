{
    "id": "ec18d984-5a1c-4af4-8ccc-64e622d45b72",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_rebind_shop8",
    "eventList": [
        {
            "id": "d2098d13-fe96-425d-b4be-1cb2651201e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ec18d984-5a1c-4af4-8ccc-64e622d45b72"
        },
        {
            "id": "cbaafc21-d74f-4be8-9bfc-a88124c9ab89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ec18d984-5a1c-4af4-8ccc-64e622d45b72"
        },
        {
            "id": "8f6a32dd-2279-4154-8640-466e79504dd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ec18d984-5a1c-4af4-8ccc-64e622d45b72"
        },
        {
            "id": "1789e73d-b35e-4616-82cd-a47164219b83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "ec18d984-5a1c-4af4-8ccc-64e622d45b72"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a832f2b-2273-4cc3-b553-8c06b7dc617f",
    "visible": true
}