/// @description Parse JSON
event_inherited();

playerScore =	json_raw_data[? "score"];
teamScore =		json_raw_data[? "teamScore"];