{
    "id": "8c6657eb-ba1a-4bb3-a87b-942defa24304",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cannon_tower",
    "eventList": [
        {
            "id": "23597a09-b31b-4833-9843-e99dd073e287",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8c6657eb-ba1a-4bb3-a87b-942defa24304"
        },
        {
            "id": "8f0f4319-73c0-433f-b541-97c9de683550",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8c6657eb-ba1a-4bb3-a87b-942defa24304"
        },
        {
            "id": "8bce7700-cedc-49d7-9cd9-797d991afafa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "8c6657eb-ba1a-4bb3-a87b-942defa24304"
        },
        {
            "id": "12583d8c-bb2b-4939-9a5f-e36e99e8306b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8c6657eb-ba1a-4bb3-a87b-942defa24304"
        },
        {
            "id": "974ecdc1-7302-4a05-b213-3a6e495be5ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "8c6657eb-ba1a-4bb3-a87b-942defa24304"
        },
        {
            "id": "3f2be284-66db-497a-bafc-fa1c9e143de9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8c6657eb-ba1a-4bb3-a87b-942defa24304"
        },
        {
            "id": "ec23abe2-f09e-4d9d-9b50-9e6852d3b370",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8c6657eb-ba1a-4bb3-a87b-942defa24304"
        },
        {
            "id": "4d426c17-4d04-4260-9491-554732f6bd3a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "8c6657eb-ba1a-4bb3-a87b-942defa24304"
        },
        {
            "id": "5bb8fc4a-351e-45e9-a122-d933c912f3bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "8c6657eb-ba1a-4bb3-a87b-942defa24304"
        },
        {
            "id": "dedce081-4c41-420a-8b69-e167aaa27c6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8c6657eb-ba1a-4bb3-a87b-942defa24304"
        },
        {
            "id": "1807de29-fa6e-4758-a3b8-d0cc797c5765",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "f0875ffd-fd8e-428c-aabc-956eb91215a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8c6657eb-ba1a-4bb3-a87b-942defa24304"
        },
        {
            "id": "5495741e-219b-4bd3-9019-38ca31df0e93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "8c6657eb-ba1a-4bb3-a87b-942defa24304"
        },
        {
            "id": "b7ebde5f-302c-4f77-b532-db5862ccbf8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "8c6657eb-ba1a-4bb3-a87b-942defa24304"
        },
        {
            "id": "194e2c0e-2213-457c-904e-7f2689d46f47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "8c6657eb-ba1a-4bb3-a87b-942defa24304"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "22b2a54a-e74f-4bcc-b8e1-19fa267438ec",
    "visible": true
}