/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 1AA3B465
/// @DnDArgument : "code" "if highground = true then$(13_10){$(13_10)	instance_deactivate_layer("TOP_GRID")$(13_10)	instance_deactivate_layer("Instances")$(13_10)	instance_activate_layer("Instances")$(13_10)	instance_activate_layer("TOP_GRID")$(13_10)	$(13_10)$(13_10)instance_create_layer(x,y,"TOP_GRID",obj_high_grid)$(13_10)} else$(13_10){$(13_10)	instance_deactivate_layer("TOP_GRID")$(13_10)	instance_deactivate_layer("Instances")$(13_10)	instance_activate_layer("Instances")$(13_10)	instance_activate_layer("TOP_GRID")$(13_10)instance_create_layer(x,y,"instances_towers",obj_return_enemies)$(13_10)	instance_create_layer(x,y,"TOP_GRID",obj_grid)$(13_10)}"
if highground = true then
{
	instance_deactivate_layer("TOP_GRID")
	instance_deactivate_layer("Instances")
	instance_activate_layer("Instances")
	instance_activate_layer("TOP_GRID")
	

instance_create_layer(x,y,"TOP_GRID",obj_high_grid)
} else
{
	instance_deactivate_layer("TOP_GRID")
	instance_deactivate_layer("Instances")
	instance_activate_layer("Instances")
	instance_activate_layer("TOP_GRID")
instance_create_layer(x,y,"instances_towers",obj_return_enemies)
	instance_create_layer(x,y,"TOP_GRID",obj_grid)
}

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 14ADA19D
/// @DnDApplyTo : be61b32a-8c4e-4b4d-9925-6965634dbb17
with(obj_green_damage_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 453115BB
/// @DnDApplyTo : d2a5ac99-b5d0-40cd-95c5-11df01d2059d
with(obj_green_attack_range_up) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 6CCAF2FA
/// @DnDApplyTo : 8ae6c06b-2c3b-4787-b574-78c7112cddf2
with(obj_blue_damage_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 45985F4F
/// @DnDApplyTo : ad3c7a93-2d91-49e2-8273-cf85eb89e048
with(obj_blue_range_up) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 6582C0EE
/// @DnDApplyTo : 2631dbca-5d33-4648-bc49-3198bc4211d8
with(obj_red_damage_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 3DF1FC66
/// @DnDApplyTo : b0acdc6d-c837-4818-b5c6-27ddb536fadc
with(obj_red_range_up) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 1E6B46F7
/// @DnDApplyTo : ff372800-7810-49bb-898f-a7defb5cc613
with(obj_tower_3_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 32953765
/// @DnDApplyTo : 95206be0-6e6c-4734-8b6c-0cf228a2d3bc
with(obj_yellow_damage_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 691C618A
/// @DnDApplyTo : b1324da9-7d47-4d53-9da2-8d713b110ca5
with(obj_yellow_range_up) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 3FED576D
/// @DnDApplyTo : 2631dbca-5d33-4648-bc49-3198bc4211d8
with(obj_red_damage_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 6B95094E
/// @DnDApplyTo : b0acdc6d-c837-4818-b5c6-27ddb536fadc
with(obj_red_range_up) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 7352FDAA
/// @DnDApplyTo : 1839b936-ae8c-45b5-b836-3f28b8a336e8
with(obj_white_damage_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 53C78A44
/// @DnDApplyTo : 2260b290-3999-45c1-8c9e-8777dba2f23a
with(obj_white_range_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 14FA4F4C
/// @DnDApplyTo : a550b399-af22-4b2c-8d4f-698f20706196
with(obj_orange_damage_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 1851DDC0
/// @DnDApplyTo : 5cab794b-de08-4f2e-84e5-abed593dd509
with(obj_poison_damage_up1) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 7197EA26
/// @DnDApplyTo : 8aea4293-de50-4063-9f50-d75be1378031
with(obj_tower_1_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 39F935FD
/// @DnDApplyTo : 073fec17-ba80-4dec-a3c8-288b74078b2d
with(obj_tower_2_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 641843DD
/// @DnDApplyTo : ff372800-7810-49bb-898f-a7defb5cc613
with(obj_tower_3_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 287FBB8A
/// @DnDApplyTo : 43c1b6e1-4e5c-402d-9b7f-54483b5eb297
with(obj_tower_4_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 5C136B5C
/// @DnDApplyTo : 16d11c02-1a7d-4f1e-8cf3-b39df52c2b8b
with(obj_tower_5_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 6D443C29
/// @DnDApplyTo : b44ce0f6-e314-48d5-9a11-f7e037ea1145
with(obj_tower_6_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 569419A9
/// @DnDApplyTo : 32183b7a-d784-471e-939b-c846cb1d5088
with(obj_tower_poison_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 72FDCC1A
/// @DnDApplyTo : 71b2a270-606b-4041-bf6a-7f7b00d49850
with(obj_mud_buy) instance_destroy();