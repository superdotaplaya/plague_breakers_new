/// @DnDAction : YoYo Games.Instances.Sprite_Scale
/// @DnDVersion : 1
/// @DnDHash : 12C10DAE
/// @DnDArgument : "xscale" ".25"
/// @DnDArgument : "yscale" ".25"
image_xscale = .25;
image_yscale = .25;

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 7FCB5D69
/// @DnDArgument : "code" "alarm_set(0,-1)$(13_10)value = 1$(13_10)rand_time = irandom(10)$(13_10)new = true$(13_10)can_be_collected = true$(13_10)if !instance_exists(obj_gold_ui)$(13_10)then$(13_10){$(13_10)instance_create_layer(obj_ui_info.x + 80,obj_ui_info.y + 60, "shop_icons",obj_gold_ui)	$(13_10)}$(13_10)alarm_set(2,5)$(13_10)"
alarm_set(0,-1)
value = 1
rand_time = irandom(10)
new = true
can_be_collected = true
if !instance_exists(obj_gold_ui)
then
{
instance_create_layer(obj_ui_info.x + 80,obj_ui_info.y + 60, "shop_icons",obj_gold_ui)	
}
alarm_set(2,5)