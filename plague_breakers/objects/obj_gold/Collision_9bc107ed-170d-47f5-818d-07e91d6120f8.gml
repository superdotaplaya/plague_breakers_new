/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 4D58B8D6
/// @DnDArgument : "code" "//Checkes to make sure the player has not already been credited for collecting the coin$(13_10)if can_be_collected = true then$(13_10){$(13_10)prev_collected = variable_instance_get(other,"collected")$(13_10)variable_instance_set(other,"collected", prev_collected + value)$(13_10)audio_play_sound(snd_coin_pickup,1,0)$(13_10)instance_destroy()$(13_10)} else$(13_10){$(13_10)instance_destroy()	$(13_10)}$(13_10)"
//Checkes to make sure the player has not already been credited for collecting the coin
if can_be_collected = true then
{
prev_collected = variable_instance_get(other,"collected")
variable_instance_set(other,"collected", prev_collected + value)
audio_play_sound(snd_coin_pickup,1,0)
instance_destroy()
} else
{
instance_destroy()	
}