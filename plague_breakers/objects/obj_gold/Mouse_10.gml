/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 7998286F
/// @DnDArgument : "code" "// Handles gold collection manually$(13_10)if alarm_get(0) = -1 then$(13_10){$(13_10)	// sets it so the coin can no longer be collected to prevent duplication$(13_10)can_be_collected = false$(13_10)// Adds the gold coins value to the players gold$(13_10)global.gold += value$(13_10)// moves the coin towards the ui elements for animation$(13_10)move_towards_point(obj_gold_ui.x,obj_gold_ui.y,12)$(13_10)// alarms for hadling deletionof the coin if needed$(13_10)alarm_set(0,600)$(13_10)alarm_set(1,500)$(13_10)audio_play_sound(snd_coin_pickup,1,0)$(13_10)}"
// Handles gold collection manually
if alarm_get(0) = -1 then
{
	// sets it so the coin can no longer be collected to prevent duplication
can_be_collected = false
// Adds the gold coins value to the players gold
global.gold += value
// moves the coin towards the ui elements for animation
move_towards_point(obj_gold_ui.x,obj_gold_ui.y,12)
// alarms for hadling deletionof the coin if needed
alarm_set(0,600)
alarm_set(1,500)
audio_play_sound(snd_coin_pickup,1,0)
}