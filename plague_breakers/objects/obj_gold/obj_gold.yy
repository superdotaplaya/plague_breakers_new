{
    "id": "721454be-8a92-444d-8eaf-873df8cbf18d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gold",
    "eventList": [
        {
            "id": "f19d825a-8d8c-43ba-971f-351563f4efe9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "721454be-8a92-444d-8eaf-873df8cbf18d"
        },
        {
            "id": "53a1ef56-f3d2-4a30-8ddc-6bc2bcc99dc9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "721454be-8a92-444d-8eaf-873df8cbf18d"
        },
        {
            "id": "4f692a76-aab3-4c3e-8065-090d04ed0bd3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "c670709f-0786-4f76-b666-b70debe1bf31",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "721454be-8a92-444d-8eaf-873df8cbf18d"
        },
        {
            "id": "3ccf2579-a197-4848-b972-f8cca87ef8de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "721454be-8a92-444d-8eaf-873df8cbf18d"
        },
        {
            "id": "9bc107ed-170d-47f5-818d-07e91d6120f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "b3cb2dcd-08ed-4097-9aa8-dcbc23463fe2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "721454be-8a92-444d-8eaf-873df8cbf18d"
        },
        {
            "id": "f661d89f-1206-4722-af01-9b7ef7769c3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "721454be-8a92-444d-8eaf-873df8cbf18d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "721454be-8a92-444d-8eaf-873df8cbf18d"
        },
        {
            "id": "e428026e-b3d6-4c1c-a83b-00bbe306739c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "721454be-8a92-444d-8eaf-873df8cbf18d"
        },
        {
            "id": "db84a984-d113-45fd-83d5-2d99ca7207cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "721454be-8a92-444d-8eaf-873df8cbf18d"
        },
        {
            "id": "a25986a1-8fec-4d97-b6b6-91b82d6620cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "721454be-8a92-444d-8eaf-873df8cbf18d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b1ac8a00-419f-4e13-b41d-288459b9a5ce",
    "visible": true
}