/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 105FF686
/// @DnDArgument : "code" "// Handles incrementing the value of coins by adding to the older instance to aavoid issues$(13_10)if id < other.id and other.can_be_collected = true and can_be_collected = true then$(13_10){$(13_10)value += other.value$(13_10)instance_destroy(other)$(13_10)}"
// Handles incrementing the value of coins by adding to the older instance to aavoid issues
if id < other.id and other.can_be_collected = true and can_be_collected = true then
{
value += other.value
instance_destroy(other)
}