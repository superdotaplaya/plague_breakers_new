{
    "id": "279cef27-326c-4697-9da1-c75704465aaa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_speedup",
    "eventList": [
        {
            "id": "2218476c-1bb7-4de0-b9ed-d2e4530d12d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "279cef27-326c-4697-9da1-c75704465aaa"
        },
        {
            "id": "7a767cdc-2b89-4996-b8f7-e9e272382451",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "279cef27-326c-4697-9da1-c75704465aaa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "98749d97-f760-4ccc-b56a-8e09e874f23b",
    "visible": true
}