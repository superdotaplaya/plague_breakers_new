/// @description Parse JSON
event_inherited();

rankFrom =		json_raw_data[? "rankFrom"];
rankTo =		json_raw_data[? "rankTo"];
title =			json_raw_data[? "title"];
description =	json_raw_data[? "description"];
imageUrl =		json_raw_data[? "imageUrl"];

ds_list_copy(prizeIds, json_raw_data[? "prizeIds"]);