{
    "id": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blue_tower",
    "eventList": [
        {
            "id": "2ca844d5-112d-4270-9429-93e9f4a4e137",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        },
        {
            "id": "88f3406f-04f0-487e-aaef-12c4244ccc0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        },
        {
            "id": "519dda2b-ba62-4ff7-8cce-76050e03e897",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        },
        {
            "id": "e2ef1f31-8778-4a7c-b082-8c6ddd061e67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        },
        {
            "id": "bd7dc8c2-3878-4a8b-98e4-a0a0b096cb38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        },
        {
            "id": "95006988-6f08-49fb-9f06-4fdb0f35e18a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        },
        {
            "id": "98bca056-f233-4db7-b64b-29c2079e0b12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        },
        {
            "id": "96368680-a263-41ef-a4bc-938a8136a34e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        },
        {
            "id": "de73256c-4063-4554-a8f2-22b4d0ffdf65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        },
        {
            "id": "76109762-a0a0-453f-9b4d-c97981d3c2c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        },
        {
            "id": "3849509b-e91a-49eb-a98d-0f86e86df5e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        },
        {
            "id": "1e2f7496-bb8d-4eae-bb85-423d446d793d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        },
        {
            "id": "b5ce85d7-dff7-42f7-9945-415b56a3e3fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        },
        {
            "id": "25c01d6a-d43d-4f45-b1df-d0ec9df67e7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "f0875ffd-fd8e-428c-aabc-956eb91215a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "72079f4f-9558-4696-8b39-21e6a15d0714",
    "visible": true
}