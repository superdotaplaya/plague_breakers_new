/// @DnDAction : YoYo Games.Common.Function_Call
/// @DnDVersion : 1
/// @DnDHash : 57F9B3C5
/// @DnDArgument : "function" "instance_destroy_shop"
instance_destroy_shop();

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 5620798F
/// @DnDArgument : "code" "global.upgrade_current = "green"$(13_10)global.current_tower = layer_instance_get_instance(self)$(13_10)instance_create_layer(95,830,"shop_icons",obj_blue_damage_up1)$(13_10)instance_create_layer(95,830,"shop_icons",obj_blue_range_up)$(13_10)global.x = x$(13_10)global.y = y$(13_10)global.current_tower = instance_nearest(global.x,global.y,obj_blue_tower)$(13_10)$(13_10)"
global.upgrade_current = "green"
global.current_tower = layer_instance_get_instance(self)
instance_create_layer(95,830,"shop_icons",obj_blue_damage_up1)
instance_create_layer(95,830,"shop_icons",obj_blue_range_up)
global.x = x
global.y = y
global.current_tower = instance_nearest(global.x,global.y,obj_blue_tower)

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 641F4202
/// @DnDArgument : "var" "global.shop_open"
/// @DnDArgument : "value" "false"
if(global.shop_open == false)
{
	/// @DnDAction : YoYo Games.Common.Set_Global
	/// @DnDVersion : 1
	/// @DnDHash : 69E5B7F0
	/// @DnDParent : 641F4202
	/// @DnDArgument : "value" "true"
	/// @DnDArgument : "var" "shop_open"
	global.shop_open = true;

	/// @DnDAction : YoYo Games.Paths.Start_Path
	/// @DnDVersion : 1.1
	/// @DnDHash : 6AA63047
	/// @DnDApplyTo : 37f57748-c862-493a-bbbd-c8ffdf8f8d60
	/// @DnDParent : 641F4202
	/// @DnDArgument : "path" "shop"
	/// @DnDArgument : "speed" "40"
	/// @DnDArgument : "relative" "true"
	/// @DnDSaveInfo : "path" "51c200a6-a5a1-4aee-9b09-55296bbe01a1"
	with(obj_shop_menu) path_start(shop, 40, path_action_stop, true);

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 75E0C99C
	/// @DnDParent : 641F4202
	/// @DnDArgument : "var" "global.ui"
	/// @DnDArgument : "value" "false"
	if(global.ui == false)
	{
		/// @DnDAction : YoYo Games.Paths.Start_Path
		/// @DnDVersion : 1.1
		/// @DnDHash : 195E7F89
		/// @DnDApplyTo : dce01893-52fc-4398-ad95-8c88eef129f0
		/// @DnDParent : 75E0C99C
		/// @DnDArgument : "path" "ui_info_open"
		/// @DnDArgument : "speed" "40"
		/// @DnDArgument : "relative" "true"
		/// @DnDSaveInfo : "path" "9dcfed44-8f1f-4d49-ada5-ce48f3e2d95d"
		with(obj_ui_info) path_start(ui_info_open, 40, path_action_stop, true);
	
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 19ADBC5B
		/// @DnDParent : 75E0C99C
		/// @DnDArgument : "expr" "true"
		/// @DnDArgument : "var" "global.ui"
		global.ui = true;
	}
}