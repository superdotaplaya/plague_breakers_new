{
    "id": "b4d0b336-2558-4772-9e30-d4d7df57d081",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enter_name_options",
    "eventList": [
        {
            "id": "91aaf9b1-303d-4ed6-a309-e262328a6856",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b4d0b336-2558-4772-9e30-d4d7df57d081"
        },
        {
            "id": "e88363f8-077b-41b1-a9ff-8910b37f2abd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b4d0b336-2558-4772-9e30-d4d7df57d081"
        },
        {
            "id": "f0674245-675c-4e86-a525-92971f906fa8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "b4d0b336-2558-4772-9e30-d4d7df57d081"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e7ce6e11-c388-4c41-b619-9ba8ec6982d7",
    "visible": true
}