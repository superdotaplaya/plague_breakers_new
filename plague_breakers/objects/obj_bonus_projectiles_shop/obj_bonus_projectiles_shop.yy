{
    "id": "88c7627e-1428-4a10-929e-fb111232adfe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bonus_projectiles_shop",
    "eventList": [
        {
            "id": "63e73053-55dc-4d6e-b971-6600a64c8267",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "88c7627e-1428-4a10-929e-fb111232adfe"
        },
        {
            "id": "c9e6c34c-b371-4df9-936a-92c4a655ce6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "88c7627e-1428-4a10-929e-fb111232adfe"
        },
        {
            "id": "631ab6ac-81e5-440c-8f96-43f7c293e007",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "88c7627e-1428-4a10-929e-fb111232adfe"
        },
        {
            "id": "1a0750cb-feb6-4a61-bda2-a7f773a8d068",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "88c7627e-1428-4a10-929e-fb111232adfe"
        },
        {
            "id": "d979cfb5-9b31-4422-9492-375a5098e3b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "88c7627e-1428-4a10-929e-fb111232adfe"
        },
        {
            "id": "0705858f-00b2-49f1-95e8-42f635986c0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "88c7627e-1428-4a10-929e-fb111232adfe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "07f495b3-fe21-4ae5-8ec5-f0def477b4f5",
    "visible": true
}