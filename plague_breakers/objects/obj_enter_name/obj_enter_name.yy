{
    "id": "8362d382-521f-4ee4-8264-d59d10053f66",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enter_name",
    "eventList": [
        {
            "id": "81b08af8-060e-4351-9953-3ff8a9485e43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8362d382-521f-4ee4-8264-d59d10053f66"
        },
        {
            "id": "12f167aa-f1c7-448d-bca4-e75375733272",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8362d382-521f-4ee4-8264-d59d10053f66"
        },
        {
            "id": "e4ae3ee9-398b-4c63-9e2b-8cde8e8fef63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "8362d382-521f-4ee4-8264-d59d10053f66"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e7ce6e11-c388-4c41-b619-9ba8ec6982d7",
    "visible": true
}