/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 34C96867
/// @DnDArgument : "code" "/// @description Execute Code$(13_10)$(13_10)if keyboard_lastchar = chr(global.infoHotkey) then$(13_10)$(13_10){$(13_10)if global.ui = false and global.can_tab = true then$(13_10){$(13_10)	// Start Following Path$(13_10)	path_start(ui_info_open, 50, path_action_stop, true);$(13_10)$(13_10)	// Assign Variable$(13_10)	alarm_set(0,40)$(13_10)	global.can_tab = false$(13_10)	global.ui = true;$(13_10)}$(13_10)if global.ui = true and global.can_tab = true then$(13_10){$(13_10)	// Start Following Path$(13_10)	path_start(ui_info_close, 50, path_action_stop, true);$(13_10)$(13_10)	// Assign Variable$(13_10)	alarm_set(0,40)$(13_10)	global.can_tab = false$(13_10)	global.ui = false;$(13_10)}$(13_10)}"
/// @description Execute Code

if keyboard_lastchar = chr(global.infoHotkey) then

{
if global.ui = false and global.can_tab = true then
{
	// Start Following Path
	path_start(ui_info_open, 50, path_action_stop, true);

	// Assign Variable
	alarm_set(0,40)
	global.can_tab = false
	global.ui = true;
}
if global.ui = true and global.can_tab = true then
{
	// Start Following Path
	path_start(ui_info_close, 50, path_action_stop, true);

	// Assign Variable
	alarm_set(0,40)
	global.can_tab = false
	global.ui = false;
}
}