{
    "id": "dce01893-52fc-4398-ad95-8c88eef129f0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ui_info",
    "eventList": [
        {
            "id": "2fda0255-2266-44e1-8073-d00118120447",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dce01893-52fc-4398-ad95-8c88eef129f0"
        },
        {
            "id": "e88e0e36-4d7c-4a4b-af24-4a992fe63ce6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "dce01893-52fc-4398-ad95-8c88eef129f0"
        },
        {
            "id": "f61cc5bb-3951-4845-be47-7d0924da8800",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "dce01893-52fc-4398-ad95-8c88eef129f0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d8d1cf77-18a1-4e11-be66-1a9006f6bc4d",
    "visible": true
}