{
    "id": "95f15ea7-e8f3-487a-b143-3a45457f6eb6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_modular_attack_range",
    "eventList": [
        {
            "id": "7d4f0539-4568-494d-b613-1fee13d3301e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "95f15ea7-e8f3-487a-b143-3a45457f6eb6"
        },
        {
            "id": "a88cf111-f5f3-489b-be24-bb5323b97859",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "95f15ea7-e8f3-487a-b143-3a45457f6eb6"
        },
        {
            "id": "32c224a8-7639-4009-8381-191693972551",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "95f15ea7-e8f3-487a-b143-3a45457f6eb6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "86e1ef81-744a-4a49-b005-72f7f36bddf5",
    "visible": true
}