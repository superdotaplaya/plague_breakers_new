{
    "id": "a7dd93f0-3576-4688-825d-2e9abb886a8d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hp_potion",
    "eventList": [
        {
            "id": "7324017f-98e6-4c6d-aa43-37b4b6d96ec8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a7dd93f0-3576-4688-825d-2e9abb886a8d"
        },
        {
            "id": "95e9a2c4-32f6-4461-89c3-53bb95573b15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a7dd93f0-3576-4688-825d-2e9abb886a8d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7c783422-a340-4215-9a7e-fb978e7a3e3b",
    "visible": true
}