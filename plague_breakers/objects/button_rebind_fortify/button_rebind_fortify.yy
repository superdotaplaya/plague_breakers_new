{
    "id": "0add03fa-eb36-4a07-95b9-b60168a2f6f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_rebind_fortify",
    "eventList": [
        {
            "id": "298c45be-e6dc-4ac5-9dac-d52740a59d43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0add03fa-eb36-4a07-95b9-b60168a2f6f1"
        },
        {
            "id": "d6e5dc39-8a8e-4b8e-b944-36ec757051b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0add03fa-eb36-4a07-95b9-b60168a2f6f1"
        },
        {
            "id": "d85753f1-f69a-4924-81ae-df35319865d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "0add03fa-eb36-4a07-95b9-b60168a2f6f1"
        },
        {
            "id": "e02ea485-2645-40d4-a698-53db283713bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "0add03fa-eb36-4a07-95b9-b60168a2f6f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a832f2b-2273-4cc3-b553-8c06b7dc617f",
    "visible": true
}