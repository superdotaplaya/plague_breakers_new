{
    "id": "917d34ca-80aa-417f-83d3-99b44da049e3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tesla_tower",
    "eventList": [
        {
            "id": "06151d16-ac0c-49f5-af6c-91fc43b9394e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "917d34ca-80aa-417f-83d3-99b44da049e3"
        },
        {
            "id": "74853482-6f00-4689-a751-2aa427855398",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "917d34ca-80aa-417f-83d3-99b44da049e3"
        },
        {
            "id": "a1d6e25f-cdab-4774-ba4a-0418c927d5c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "917d34ca-80aa-417f-83d3-99b44da049e3"
        },
        {
            "id": "3f53f352-a48b-4ce9-b2eb-f4a5a5606d75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "917d34ca-80aa-417f-83d3-99b44da049e3"
        },
        {
            "id": "416b94a4-4168-453a-a458-37f038450e92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "917d34ca-80aa-417f-83d3-99b44da049e3"
        },
        {
            "id": "e91292d4-d2fa-4f55-af3a-08beb9d124c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "917d34ca-80aa-417f-83d3-99b44da049e3"
        },
        {
            "id": "cfb8212f-090f-4e79-b6cd-df6c1cb88a8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "917d34ca-80aa-417f-83d3-99b44da049e3"
        },
        {
            "id": "596dc67e-7845-4c07-b23d-48cd8c1eced2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "917d34ca-80aa-417f-83d3-99b44da049e3"
        },
        {
            "id": "d6ec533f-5bca-41d4-893d-e050b257ac20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "917d34ca-80aa-417f-83d3-99b44da049e3"
        },
        {
            "id": "dbf87138-54be-496a-ab5c-d9d6e4ad2fb0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "917d34ca-80aa-417f-83d3-99b44da049e3"
        },
        {
            "id": "c6b62209-58b9-498e-8bd0-65e596ad3143",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "f0875ffd-fd8e-428c-aabc-956eb91215a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "917d34ca-80aa-417f-83d3-99b44da049e3"
        },
        {
            "id": "1e6d4382-d89b-44a5-9efb-b0862e6625bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "917d34ca-80aa-417f-83d3-99b44da049e3"
        },
        {
            "id": "e1a65e95-4772-498b-8209-ce2e32845517",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "917d34ca-80aa-417f-83d3-99b44da049e3"
        },
        {
            "id": "fefb459c-2344-4b2f-91cf-dbd29f21e449",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "917d34ca-80aa-417f-83d3-99b44da049e3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "45cfa927-ae76-40bc-80ed-369d8ccc3501",
    "visible": true
}