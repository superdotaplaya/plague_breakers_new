{
    "id": "4da6d479-44e6-4b9f-b2ba-edaacb08e4d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_modular_attack",
    "eventList": [
        {
            "id": "3ee9a30d-212c-4ffa-9bda-66ef741835e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4da6d479-44e6-4b9f-b2ba-edaacb08e4d3"
        },
        {
            "id": "3ae49fab-339a-40f4-9d8c-819f53d62d3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4da6d479-44e6-4b9f-b2ba-edaacb08e4d3"
        },
        {
            "id": "cecac2ae-6955-4c84-8ee5-6f9312892cf1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4da6d479-44e6-4b9f-b2ba-edaacb08e4d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "253138d9-82ea-4fef-9b55-02494aead4bd",
    "visible": true
}