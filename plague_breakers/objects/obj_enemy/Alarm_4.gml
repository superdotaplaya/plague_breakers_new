/// @DnDAction : YoYo Games.Paths.Get_Path_Position
/// @DnDVersion : 1
/// @DnDHash : 2B65E1EE
/// @DnDArgument : "target" "continue_path"
continue_path = path_position;

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 1615BDE6
/// @DnDArgument : "code" "i = irandom(99)$(13_10)last_posx = x$(13_10)$(13_10)if i < 21 then$(13_10){$(13_10)target_tower = min(distance1,distance2,distance3,distance4,distance5,distance6,distance7,distance8,distance9,distance10)$(13_10)} else target_tower = noone$(13_10)$(13_10)$(13_10)$(13_10)$(13_10)if target_tower = distance1 and instance_exists(obj_green_tower) then$(13_10){$(13_10)$(13_10)	target = instance_nearest(x,y,obj_green_tower)$(13_10)	if variable_instance_get(target,"highground") != true$(13_10)	{$(13_10)		path_end()$(13_10)	move_towards_point(target.x,target.y,4)$(13_10)	}$(13_10)} else$(13_10)if target_tower = distance2 and instance_exists(obj_poison_tower) then$(13_10){$(13_10)target = instance_nearest(x,y,obj_poison_tower)$(13_10)	if variable_instance_get(target,"highground") != true$(13_10)	{$(13_10)		path_end()$(13_10)	move_towards_point(target.x,target.y,4)$(13_10)	}$(13_10)} else$(13_10)if target_tower = distance3 and instance_exists(obj_red_tower) then$(13_10){$(13_10)$(13_10)	target = instance_nearest(x,y,obj_red_tower)$(13_10)	if variable_instance_get(target,"highground") != true$(13_10)	{$(13_10)		path_end()$(13_10)	move_towards_point(target.x,target.y,4)$(13_10)	}$(13_10)} else$(13_10)if target_tower = distance4 and instance_exists(obj_yellow_tower)then$(13_10){$(13_10)$(13_10)	target = instance_nearest(x,y,obj_yellow_tower)$(13_10)	if variable_instance_get(target,"highground") != true$(13_10)	{$(13_10)		path_end()$(13_10)	move_towards_point(target.x,target.y,4)$(13_10)	}$(13_10)} else$(13_10)if target_tower = distance5 and instance_exists(obj_blue_tower) then$(13_10){$(13_10)$(13_10)	target = instance_nearest(x,y,obj_blue_tower)$(13_10)	if variable_instance_get(target,"highground") != true$(13_10)	{$(13_10)		path_end()$(13_10)	move_towards_point(target.x,target.y,4)$(13_10)	}$(13_10)} else$(13_10)if target_tower = distance6 and instance_exists(obj_white_tower) then$(13_10){$(13_10)	$(13_10)	target = instance_nearest(x,y,obj_white_tower)$(13_10)	if variable_instance_get(target,"highground") != true$(13_10)	{$(13_10)		path_end()$(13_10)	move_towards_point(target.x,target.y,4)$(13_10)	}$(13_10)}$(13_10) else$(13_10)if target_tower = distance7 and instance_exists(obj_orange_tower) then$(13_10){$(13_10)	target = instance_nearest(x,y,obj_orange_tower)$(13_10)	if variable_instance_get(target,"highground") != true$(13_10)	{$(13_10)		path_end()$(13_10)	move_towards_point(target.x,target.y,4)$(13_10)	}$(13_10)}$(13_10)if target_tower = distance8 and instance_exists(obj_cannon_tower) then$(13_10){$(13_10)	target = instance_nearest(x,y,obj_cannon_tower)$(13_10)	if variable_instance_get(target,"highground") != true$(13_10)	{$(13_10)		path_end()$(13_10)	move_towards_point(target.x,target.y,4)$(13_10)	}$(13_10)}$(13_10)if target_tower = distance9 and instance_exists(obj_tesla_tower) then$(13_10){$(13_10)	target = instance_nearest(x,y,obj_tesla_tower)$(13_10)	if variable_instance_get(target,"highground") != true$(13_10)	{$(13_10)		path_end()$(13_10)	move_towards_point(target.x,target.y,4)$(13_10)	}$(13_10)}$(13_10)if target_tower = distance10 and instance_exists(obj_modular_tower) then$(13_10){$(13_10)	target = instance_nearest(x,y,obj_modular_tower)$(13_10)	if variable_instance_get(target,"highground") != true$(13_10)	{$(13_10)		path_end()$(13_10)	move_towards_point(target.x,target.y,4)$(13_10)	}$(13_10)}$(13_10)$(13_10)$(13_10)$(13_10)alarm_set(4,600)"
i = irandom(99)
last_posx = x

if i < 21 then
{
target_tower = min(distance1,distance2,distance3,distance4,distance5,distance6,distance7,distance8,distance9,distance10)
} else target_tower = noone




if target_tower = distance1 and instance_exists(obj_green_tower) then
{

	target = instance_nearest(x,y,obj_green_tower)
	if variable_instance_get(target,"highground") != true
	{
		path_end()
	move_towards_point(target.x,target.y,4)
	}
} else
if target_tower = distance2 and instance_exists(obj_poison_tower) then
{
target = instance_nearest(x,y,obj_poison_tower)
	if variable_instance_get(target,"highground") != true
	{
		path_end()
	move_towards_point(target.x,target.y,4)
	}
} else
if target_tower = distance3 and instance_exists(obj_red_tower) then
{

	target = instance_nearest(x,y,obj_red_tower)
	if variable_instance_get(target,"highground") != true
	{
		path_end()
	move_towards_point(target.x,target.y,4)
	}
} else
if target_tower = distance4 and instance_exists(obj_yellow_tower)then
{

	target = instance_nearest(x,y,obj_yellow_tower)
	if variable_instance_get(target,"highground") != true
	{
		path_end()
	move_towards_point(target.x,target.y,4)
	}
} else
if target_tower = distance5 and instance_exists(obj_blue_tower) then
{

	target = instance_nearest(x,y,obj_blue_tower)
	if variable_instance_get(target,"highground") != true
	{
		path_end()
	move_towards_point(target.x,target.y,4)
	}
} else
if target_tower = distance6 and instance_exists(obj_white_tower) then
{
	
	target = instance_nearest(x,y,obj_white_tower)
	if variable_instance_get(target,"highground") != true
	{
		path_end()
	move_towards_point(target.x,target.y,4)
	}
}
 else
if target_tower = distance7 and instance_exists(obj_orange_tower) then
{
	target = instance_nearest(x,y,obj_orange_tower)
	if variable_instance_get(target,"highground") != true
	{
		path_end()
	move_towards_point(target.x,target.y,4)
	}
}
if target_tower = distance8 and instance_exists(obj_cannon_tower) then
{
	target = instance_nearest(x,y,obj_cannon_tower)
	if variable_instance_get(target,"highground") != true
	{
		path_end()
	move_towards_point(target.x,target.y,4)
	}
}
if target_tower = distance9 and instance_exists(obj_tesla_tower) then
{
	target = instance_nearest(x,y,obj_tesla_tower)
	if variable_instance_get(target,"highground") != true
	{
		path_end()
	move_towards_point(target.x,target.y,4)
	}
}
if target_tower = distance10 and instance_exists(obj_modular_tower) then
{
	target = instance_nearest(x,y,obj_modular_tower)
	if variable_instance_get(target,"highground") != true
	{
		path_end()
	move_towards_point(target.x,target.y,4)
	}
}



alarm_set(4,600)