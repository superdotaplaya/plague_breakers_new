/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 33391242
/// @DnDArgument : "code" "if type = 6 and variable_instance_get(instance_nearest(x,y,obj_gold),"can_be_collected") = true then$(13_10){$(13_10)instance_destroy(instance_nearest(x,y,obj_gold))$(13_10)coin_value = variable_instance_get(other,"value")$(13_10)stolen += coin_value$(13_10)}"
if type = 6 and variable_instance_get(instance_nearest(x,y,obj_gold),"can_be_collected") = true then
{
instance_destroy(instance_nearest(x,y,obj_gold))
coin_value = variable_instance_get(other,"value")
stolen += coin_value
}