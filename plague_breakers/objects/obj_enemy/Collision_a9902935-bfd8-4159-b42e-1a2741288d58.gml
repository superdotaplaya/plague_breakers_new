/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 180E62AF
/// @DnDArgument : "code" "hp -= variable_instance_get(instance_nearest(x,y,obj_poison_puddle),"damage")$(13_10)alarm_set(6,30)$(13_10)if hp <= 0 then$(13_10){$(13_10)drop_gold(value,stolen,20,-20,20,-20)$(13_10)global.killed_by_poison += 1$(13_10)}"
hp -= variable_instance_get(instance_nearest(x,y,obj_poison_puddle),"damage")
alarm_set(6,30)
if hp <= 0 then
{
drop_gold(value,stolen,20,-20,20,-20)
global.killed_by_poison += 1
}

/// @DnDAction : YoYo Games.Instances.Color_Sprite
/// @DnDVersion : 1
/// @DnDHash : 12524C9D
/// @DnDArgument : "colour" "$FF0A0AFF"
image_blend = $FF0A0AFF & $ffffff;
image_alpha = ($FF0A0AFF >> 24) / $ff;

/// @DnDAction : YoYo Games.Instances.Set_Alarm
/// @DnDVersion : 1
/// @DnDHash : 1080EAA5
/// @DnDArgument : "steps" "200"
/// @DnDArgument : "alarm" "2"
alarm_set(2, 200);

/// @DnDAction : YoYo Games.Paths.Path_Speed
/// @DnDVersion : 1
/// @DnDHash : 253D3355
/// @DnDArgument : "speed" "spd/3"
path_speed = spd/3;