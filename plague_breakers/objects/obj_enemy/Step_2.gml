/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 4C667192
/// @DnDArgument : "code" ""


/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 3A9F55D8
/// @DnDArgument : "var" "returning"
/// @DnDArgument : "value" "true"
if(returning == true)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 2B8E2BFA
	/// @DnDParent : 3A9F55D8
	/// @DnDArgument : "expr" "false"
	/// @DnDArgument : "var" "returning"
	returning = false;

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4C9A96E7
	/// @DnDParent : 3A9F55D8
	/// @DnDArgument : "var" "x"
	/// @DnDArgument : "value" "last_posx + 20"
	if(x == last_posx + 20)
	{
		/// @DnDAction : YoYo Games.Common.If_Variable
		/// @DnDVersion : 1
		/// @DnDHash : 476C157D
		/// @DnDParent : 4C9A96E7
		/// @DnDArgument : "var" "y +- 20"
		/// @DnDArgument : "value" "last_posy"
		if(y +- 20 == last_posy)
		{
			/// @DnDAction : YoYo Games.Paths.Start_Path
			/// @DnDVersion : 1.1
			/// @DnDHash : 730762EB
			/// @DnDParent : 476C157D
			/// @DnDArgument : "path" "desert"
			/// @DnDArgument : "speed" "spd"
			/// @DnDArgument : "relative" "true"
			/// @DnDSaveInfo : "path" "37c696c9-9191-4e4e-81ab-e7ac309cb552"
			path_start(desert, spd, path_action_stop, true);
		
			/// @DnDAction : YoYo Games.Paths.Path_Position
			/// @DnDVersion : 1
			/// @DnDHash : 466E1DB6
			/// @DnDParent : 476C157D
			/// @DnDArgument : "position" "continue_path"
			/// @DnDArgument : "position_relative" "1"
			path_position += continue_path;
		}
	}
}