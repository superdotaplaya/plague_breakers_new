/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 21E8A646
/// @DnDArgument : "code" "if justspawned = false then$(13_10){$(13_10)global.hp -= 1$(13_10)killedbyuser = false$(13_10)audio_play_sound(snd_enemy_base_hit,1,0)$(13_10)global.daily_level = 0$(13_10)log("[CURRENT LIVES]   Player has lost 1 life from an enemy, Remaining health: " + string(global.hp))$(13_10)instance_destroy()$(13_10)}"
if justspawned = false then
{
global.hp -= 1
killedbyuser = false
audio_play_sound(snd_enemy_base_hit,1,0)
global.daily_level = 0
log("[CURRENT LIVES]   Player has lost 1 life from an enemy, Remaining health: " + string(global.hp))
instance_destroy()
}