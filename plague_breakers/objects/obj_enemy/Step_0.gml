/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 67C97939
/// @DnDArgument : "code" "image_angle = direction$(13_10)distance1 = 99999999$(13_10)distance2 = 99999999$(13_10)distance3 = 99999999$(13_10)distance4 = 99999999$(13_10)distance5 = 99999999$(13_10)distance6 = 99999999$(13_10)distance7 = 99999999$(13_10)distance8 = 99999999$(13_10)distance9 = 99999999$(13_10)distance10 = 99999999$(13_10)if hp > max_hp then$(13_10){$(13_10)hp = max_hp	$(13_10)}$(13_10)if instance_exists(obj_green_tower) $(13_10){$(13_10)distance1 = distance_to_object(obj_green_tower)	$(13_10)}$(13_10)if instance_exists(obj_poison_tower) $(13_10){$(13_10)distance2 = distance_to_object(obj_poison_tower)	$(13_10)}$(13_10)if instance_exists(obj_red_tower) $(13_10){$(13_10)distance3 = distance_to_object(obj_red_tower)	$(13_10)}$(13_10)if instance_exists(obj_yellow_tower) $(13_10){$(13_10)distance4 = distance_to_object(obj_yellow_tower)	$(13_10)}$(13_10)if instance_exists(obj_blue_tower) $(13_10){$(13_10)distance5 = distance_to_object(obj_blue_tower)	$(13_10)}$(13_10)if instance_exists(obj_white_tower) $(13_10){$(13_10)distance6 = distance_to_object(obj_white_tower)	$(13_10)}$(13_10)if instance_exists(obj_orange_tower) $(13_10){$(13_10)distance7 = distance_to_object(obj_orange_tower)	$(13_10)}$(13_10)if instance_exists(obj_cannon_tower) $(13_10){$(13_10)distance8 = distance_to_object(obj_cannon_tower)	$(13_10)}$(13_10)if instance_exists(obj_tesla_tower) $(13_10){$(13_10)distance9 = distance_to_object(obj_tesla_tower)$(13_10)}$(13_10)if instance_exists(obj_modular_tower) $(13_10){$(13_10)distance10 = distance_to_object(obj_modular_tower)$(13_10)}$(13_10)$(13_10)round(hp)$(13_10)$(13_10)"
image_angle = direction
distance1 = 99999999
distance2 = 99999999
distance3 = 99999999
distance4 = 99999999
distance5 = 99999999
distance6 = 99999999
distance7 = 99999999
distance8 = 99999999
distance9 = 99999999
distance10 = 99999999
if hp > max_hp then
{
hp = max_hp	
}
if instance_exists(obj_green_tower) 
{
distance1 = distance_to_object(obj_green_tower)	
}
if instance_exists(obj_poison_tower) 
{
distance2 = distance_to_object(obj_poison_tower)	
}
if instance_exists(obj_red_tower) 
{
distance3 = distance_to_object(obj_red_tower)	
}
if instance_exists(obj_yellow_tower) 
{
distance4 = distance_to_object(obj_yellow_tower)	
}
if instance_exists(obj_blue_tower) 
{
distance5 = distance_to_object(obj_blue_tower)	
}
if instance_exists(obj_white_tower) 
{
distance6 = distance_to_object(obj_white_tower)	
}
if instance_exists(obj_orange_tower) 
{
distance7 = distance_to_object(obj_orange_tower)	
}
if instance_exists(obj_cannon_tower) 
{
distance8 = distance_to_object(obj_cannon_tower)	
}
if instance_exists(obj_tesla_tower) 
{
distance9 = distance_to_object(obj_tesla_tower)
}
if instance_exists(obj_modular_tower) 
{
distance10 = distance_to_object(obj_modular_tower)
}

round(hp)