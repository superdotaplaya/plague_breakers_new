/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 6A7FAC4A
/// @DnDArgument : "code" "hp -= variable_instance_get(instance_nearest(global.x,global.y,obj_yellow_attack),"damage")$(13_10)alarm_set(6,30)$(13_10)instance_destroy(other)$(13_10)if hp <= 0 then$(13_10){$(13_10)drop_gold(value,stolen,20,-20,20,-20)$(13_10)}"
hp -= variable_instance_get(instance_nearest(global.x,global.y,obj_yellow_attack),"damage")
alarm_set(6,30)
instance_destroy(other)
if hp <= 0 then
{
drop_gold(value,stolen,20,-20,20,-20)
}

/// @DnDAction : YoYo Games.Instances.Color_Sprite
/// @DnDVersion : 1
/// @DnDHash : 7FE30444
/// @DnDArgument : "colour" "$FF0A0AFF"
image_blend = $FF0A0AFF & $ffffff;
image_alpha = ($FF0A0AFF >> 24) / $ff;