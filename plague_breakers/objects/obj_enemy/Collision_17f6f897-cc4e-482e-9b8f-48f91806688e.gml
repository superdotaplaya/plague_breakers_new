/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 5C92D238
/// @DnDArgument : "code" "hp -= variable_instance_get(instance_nearest(x,y,obj_cannon_ball_explosion),"explosion_damage")$(13_10)alarm_set(6,30)$(13_10)if hp <= 0 then$(13_10){$(13_10)drop_gold(value,stolen,20,-20,20,-20)$(13_10)}"
hp -= variable_instance_get(instance_nearest(x,y,obj_cannon_ball_explosion),"explosion_damage")
alarm_set(6,30)
if hp <= 0 then
{
drop_gold(value,stolen,20,-20,20,-20)
}

/// @DnDAction : YoYo Games.Instances.Color_Sprite
/// @DnDVersion : 1
/// @DnDHash : 0CE42E86
/// @DnDArgument : "colour" "$FF0A0AFF"
image_blend = $FF0A0AFF & $ffffff;
image_alpha = ($FF0A0AFF >> 24) / $ff;