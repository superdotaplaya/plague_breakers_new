/// @DnDAction : YoYo Games.Paths.Start_Path
/// @DnDVersion : 1.1
/// @DnDHash : 15D55940
/// @DnDArgument : "path" "global.path"
/// @DnDArgument : "speed" "spd"
/// @DnDArgument : "relative" "true"
path_start(global.path, spd, path_action_stop, true);

/// @DnDAction : YoYo Games.Paths.Path_Position
/// @DnDVersion : 1
/// @DnDHash : 3B9DD2DF
/// @DnDArgument : "position" "continue_path"
path_position = continue_path;