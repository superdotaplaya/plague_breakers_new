{
    "id": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "b37fe945-fb58-4c64-968c-0668aaf99525",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "8cfc1f6f-e63c-469a-839e-1ec20c99f059",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "387ba0ed-9914-41b6-bc90-374cfff90c80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "f628a420-3771-4f8a-b863-cbc688c1d5bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "4343e1d3-0e54-446d-a179-4d5eb21f72b8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "6755fbc4-c707-407c-b4de-9a8a3e1ca498",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "5a39ad46-1804-4ad1-951d-bd9239e0b8cd",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "cf120d5f-f7cd-42aa-ba85-5fae7bd299a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e2b49a6d-6db2-4ec5-a186-b25158aff264",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "afbdd560-c1a9-41c6-bc31-6ef2cd0b2e76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "f9f14a6f-b432-4b2e-afd9-92d8085cc2fb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "09e4c043-528e-4334-97a0-86ded74e290a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "937a7c44-9bb2-4462-be16-0706945e428a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "e55cf0e2-b9eb-4c47-a77d-20475c2b76e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "bac4d5f9-44f3-4434-aa73-829bda86ce59",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "20f24a0a-66e0-4985-aa61-c5c06eee4598",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "a5618c59-9263-4868-885b-1ac700a1e9e9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "a1b07936-878a-4a49-9b74-53f83b950cb0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "113faadd-2ec4-468a-9d20-75913c9647da",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "758e2b66-5758-446d-a4ec-04bba9337e19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "721454be-8a92-444d-8eaf-873df8cbf18d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "98473ef7-8d1b-4ab4-8eed-52250b740b10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "104329c7-9234-4c2a-9c65-98c4f664cb05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "041aae4b-7dfb-441c-936a-050548c13828",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "0f5624bd-e061-454c-8f9b-d79150ca5cfd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "7437a726-b58e-47cb-a206-3eddff28a26a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "022e41d8-c6fe-4d5d-ba5e-c0d09a6855fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "95c36c99-22b2-4162-af9b-7dd339d76541",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "65ccd4cc-a824-4d65-80b2-434c72ea9dd6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "61e83a7c-2645-45b3-946e-115275820f23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "aa46c6a4-f1c9-435f-9838-c1c0c80a40dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "0c6eaf9f-ca87-4055-855b-4224f662fc43",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "e4b08293-6906-4e36-b1ce-803fc7b15bea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "0d4bf462-0cbf-4eae-bddf-eb9d2b4a2d5c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "a9902935-bfd8-4159-b42e-1a2741288d58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d07717ff-28ac-413a-9058-f35867a62618",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "ec6e93bc-db60-4ef7-b96e-c6e4edd5a43c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "71d6636b-a580-4e5b-ab93-0b37da493b52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "0708b27d-ec4b-4491-8914-cf7279b99b45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "298f9251-b5be-4ec9-90fc-1da37b6baf2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "30082ecf-a284-473a-8176-7fe44ce08047",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "e75409f0-aa6e-4f51-9b88-37898327ff33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "14af6861-f011-4f09-808a-273707bdbcc7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "1b1003d3-8404-4f8c-8619-b00484b2fc94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 2,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "17f6f897-cc4e-482e-9b8f-48f91806688e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "ce267fd0-2724-4fa5-89cf-234d987b68ad",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "c04784cb-00ca-44b6-8491-0d925d063f12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "4da6d479-44e6-4b9f-b2ba-edaacb08e4d3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "0df6d31c-00af-4dbe-96a2-278978a61f8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 6,
            "eventtype": 2,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "4f513ed1-cd19-4ae7-9c73-f55db65866f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 2,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "71970af0-4845-4772-9a2a-00d2b5559430",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "0822c473-24d9-49e7-bcdc-e9f08543582d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 2,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "ac337f2b-859b-4985-b672-dfd73c173e7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "a24a6cde-9798-4b86-b3f8-d1531ebdd16a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "2c31ea89-a803-492d-882f-9febc71831d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cae81964-d4ac-454f-a5a2-c281915d634f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "770227e6-74ca-42aa-9e62-7163301dec9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "917d34ca-80aa-417f-83d3-99b44da049e3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "0f082fb8-5bf9-4d2a-a77d-bfd3ef612109",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "8c6657eb-ba1a-4bb3-a87b-942defa24304",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "e2214cc4-5550-4012-aa52-b845e47c317d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "8f474088-e4dc-410b-947c-2b9e909cbb62",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "da6c2602-062d-436a-80a6-d78599231f18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "8740a74a-b10a-40bc-bda8-835fa2b522c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "12a615a1-fa0f-4167-95d1-7acdee951786",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "79a09216-fc16-4fad-bee5-a8b6d965dc6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "7cb5f25c-b718-452e-85f3-a20821ad615a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "5c37c902-777c-43b5-a437-9dca52f80486",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "4a45e47f-8340-43fe-9a82-fb609a0dd57a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "4306fcea-ac48-4917-8579-20936f7b315f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "6805e61e-ad9e-4afe-a331-68c08d156c9c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "5f62f35c-94be-4676-a069-f92b227f804c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "2ee197c4-6fe8-4126-b1e6-713f72886e75",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bd7ec843-ded7-4fcc-99c6-e7a58292f42f",
    "visible": true
}