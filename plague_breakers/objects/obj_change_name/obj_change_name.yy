{
    "id": "cf10f37b-8067-43bf-b50f-c0e0e0192c30",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_change_name",
    "eventList": [
        {
            "id": "1ef9341a-91cf-48e9-8ceb-d67ceede9a8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "cf10f37b-8067-43bf-b50f-c0e0e0192c30"
        },
        {
            "id": "47963bf0-3e94-4118-a5dc-ff655aaddbb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cf10f37b-8067-43bf-b50f-c0e0e0192c30"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d4feecde-dd78-4d82-b0e6-b3075e17c982",
    "visible": true
}