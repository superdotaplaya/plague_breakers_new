{
    "id": "2631dbca-5d33-4648-bc49-3198bc4211d8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_red_damage_up1",
    "eventList": [
        {
            "id": "b814293d-daf7-4e1e-9322-fa4fed678733",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2631dbca-5d33-4648-bc49-3198bc4211d8"
        },
        {
            "id": "e0304e94-08d5-47c9-baa0-bc421143fa79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2631dbca-5d33-4648-bc49-3198bc4211d8"
        },
        {
            "id": "42a743d7-76a8-4ca3-b6c1-3d0814050154",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "2631dbca-5d33-4648-bc49-3198bc4211d8"
        },
        {
            "id": "24e47185-a497-4934-921b-906b5ce91c9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "2631dbca-5d33-4648-bc49-3198bc4211d8"
        },
        {
            "id": "fa9460b9-02a2-4563-8f16-0d8dd4782201",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "2631dbca-5d33-4648-bc49-3198bc4211d8"
        },
        {
            "id": "5182cfc6-351e-445c-b376-df879fb96eea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2631dbca-5d33-4648-bc49-3198bc4211d8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "afd048dc-5f11-4a66-a632-1d454265b957",
    "visible": true
}