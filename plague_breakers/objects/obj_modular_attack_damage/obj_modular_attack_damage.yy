{
    "id": "d61b52f9-1fcc-4338-8c5f-3fe842626af3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_modular_attack_damage",
    "eventList": [
        {
            "id": "350c6697-fc2a-469c-b577-6e6c25801d60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d61b52f9-1fcc-4338-8c5f-3fe842626af3"
        },
        {
            "id": "9ec5740a-8ee3-4c00-8fe5-58641f8e8004",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "d61b52f9-1fcc-4338-8c5f-3fe842626af3"
        },
        {
            "id": "bbbdef7e-4ec6-4942-a105-f1a968ff8ddb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "d61b52f9-1fcc-4338-8c5f-3fe842626af3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7db01435-55ce-4634-a108-3c17341a58ce",
    "visible": true
}