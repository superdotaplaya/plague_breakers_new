{
    "id": "2260b290-3999-45c1-8c9e-8777dba2f23a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_white_range_up1",
    "eventList": [
        {
            "id": "7e600027-35e4-4395-9fa1-e7391a0e94d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2260b290-3999-45c1-8c9e-8777dba2f23a"
        },
        {
            "id": "ac065c08-20c2-4686-b77e-764207835235",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2260b290-3999-45c1-8c9e-8777dba2f23a"
        },
        {
            "id": "e4ebc95d-e398-4ed0-ad13-36ab2d68b609",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "2260b290-3999-45c1-8c9e-8777dba2f23a"
        },
        {
            "id": "32e9c47b-8482-45ae-9338-c0b80e36e4ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "2260b290-3999-45c1-8c9e-8777dba2f23a"
        },
        {
            "id": "b4d071af-3c11-42c5-bc32-d67ad67daabc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "2260b290-3999-45c1-8c9e-8777dba2f23a"
        },
        {
            "id": "a1635baa-4f80-4719-8535-c00a1720baaa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2260b290-3999-45c1-8c9e-8777dba2f23a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "92de3f75-7cf1-48b2-ae14-43cffb5901c3",
    "visible": true
}