/// @DnDAction : YoYo Games.Drawing.Draw_Self
/// @DnDVersion : 1
/// @DnDHash : 728355C9
draw_self();

/// @DnDAction : YoYo Games.Drawing.Set_Color
/// @DnDVersion : 1
/// @DnDHash : 56DAE4C1
draw_set_colour($FFFFFFFF & $ffffff);
var l56DAE4C1_0=($FFFFFFFF >> 24);
draw_set_alpha(l56DAE4C1_0 / $ff);

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 3F4188B2
/// @DnDArgument : "x" "-75"
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "115"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "caption" ""Cost: ""
/// @DnDArgument : "var" "cost"
draw_text(x + -75, y + 115, string("Cost: ") + string(cost));

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 04B24F30
/// @DnDArgument : "var" "hover"
/// @DnDArgument : "value" "true"
if(hover == true)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 1776B13F
	/// @DnDParent : 04B24F30
	/// @DnDArgument : "alpha" ".5"
	draw_set_alpha(.5);

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 7DA2A548
	/// @DnDParent : 04B24F30
	/// @DnDArgument : "var" "global.highground"
	/// @DnDArgument : "value" "false"
	if(global.highground == false)
	{
		/// @DnDAction : YoYo Games.Drawing.Draw_Gradient_Ellipse
		/// @DnDVersion : 1
		/// @DnDHash : 6FD972D4
		/// @DnDParent : 7DA2A548
		/// @DnDArgument : "x1" "global.x -300"
		/// @DnDArgument : "y1" "global.y -300"
		/// @DnDArgument : "x2" "global.x + 300"
		/// @DnDArgument : "y2" "global.y + 300"
		/// @DnDArgument : "col1" "$FFCCCCCC"
		/// @DnDArgument : "col2" "$FFFFFFFF"
		/// @DnDArgument : "fill" "1"
		draw_ellipse_colour(global.x -300, global.y -300, global.x + 300, global.y + 300, $FFCCCCCC & $FFFFFF, $FFFFFFFF & $FFFFFF, 0);
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 3B4DC1F4
	/// @DnDParent : 04B24F30
	else
	{
		/// @DnDAction : YoYo Games.Drawing.Draw_Gradient_Ellipse
		/// @DnDVersion : 1
		/// @DnDHash : 06B15B20
		/// @DnDParent : 3B4DC1F4
		/// @DnDArgument : "x1" "global.x -600"
		/// @DnDArgument : "y1" "global.y -600"
		/// @DnDArgument : "x2" "global.x + 600"
		/// @DnDArgument : "y2" "global.y + 600"
		/// @DnDArgument : "col1" "$FFCCCCCC"
		/// @DnDArgument : "col2" "$FFFFFFFF"
		/// @DnDArgument : "fill" "1"
		draw_ellipse_colour(global.x -600, global.y -600, global.x + 600, global.y + 600, $FFCCCCCC & $FFFFFF, $FFFFFFFF & $FFFFFF, 0);
	}

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 507BC984
	/// @DnDParent : 04B24F30
	/// @DnDArgument : "expr" ""shoots a laser piercing through enemies dealing damage the longer it hits them""
	/// @DnDArgument : "var" "global.tooltip"
	global.tooltip = "shoots a laser piercing through enemies dealing damage the longer it hits them";
}