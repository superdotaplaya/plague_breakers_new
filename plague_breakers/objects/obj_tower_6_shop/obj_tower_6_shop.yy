{
    "id": "b44ce0f6-e314-48d5-9a11-f7e037ea1145",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tower_6_shop",
    "eventList": [
        {
            "id": "2d0106a4-1856-4a0a-8273-566a5b91666b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b44ce0f6-e314-48d5-9a11-f7e037ea1145"
        },
        {
            "id": "7d3bc924-3f4a-4e30-8c5b-508133f82e08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b44ce0f6-e314-48d5-9a11-f7e037ea1145"
        },
        {
            "id": "5c2ada06-0285-422a-a0b1-9f683c3b1967",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b44ce0f6-e314-48d5-9a11-f7e037ea1145"
        },
        {
            "id": "a028f3c5-4df3-4ffd-8a36-377a5a76822b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "b44ce0f6-e314-48d5-9a11-f7e037ea1145"
        },
        {
            "id": "d46e2c10-0324-4bae-a2d1-13d87dfbfd0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "b44ce0f6-e314-48d5-9a11-f7e037ea1145"
        },
        {
            "id": "0332222c-1271-4b04-bd58-505b41316ac0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b44ce0f6-e314-48d5-9a11-f7e037ea1145"
        },
        {
            "id": "f83461ae-ff7e-4d42-9a4d-d84052837ec1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "b44ce0f6-e314-48d5-9a11-f7e037ea1145"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "92de3f75-7cf1-48b2-ae14-43cffb5901c3",
    "visible": true
}