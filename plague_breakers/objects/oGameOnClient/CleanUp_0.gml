if(ds_exists(http_headers_default, ds_type_map))
	ds_map_destroy(http_headers_default);
	
if(ds_exists(promises, ds_type_list))
	ds_list_destroy(promises);
	
http_headers_default = undefined;
promises = undefined;