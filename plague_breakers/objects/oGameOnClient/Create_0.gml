// CLIENT STATE
enum eGameOnState
{
	INITIALIZED = 0,
	REGISTRATION,
	REGISTERED,
	REGISTRATION_FAILED,
	AUTHENTICATING,
	AUTHENTICATION_FAILED,
	AUTHENTICATED
};

// SECURITY TYPES
enum eGameOnSecurity
{
	STANDARD = 0,
	ADVANCED
};

// AUTH TYPES
enum eGameOnAuth
{
	NONE = 0,
	LOAD_EXISTING,
	REGISTER_NEW
};

// MEMBERS
game_api_key = undefined;
game_public_key = undefined;
security_type = undefined;
state = undefined;

player_id = undefined;
player_token = undefined;

player_name = undefined;

session_id = undefined;
session_exp = undefined;
session_api_key = undefined;

client_public_key = undefined;
client_private_key = undefined;

http_headers_default = undefined;

promises = ds_list_create();