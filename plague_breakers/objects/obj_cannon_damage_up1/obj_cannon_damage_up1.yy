{
    "id": "a8694151-7dc4-4081-8915-13b0fb5a9396",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cannon_damage_up1",
    "eventList": [
        {
            "id": "ebf8c840-85f2-4a06-b76f-7a1ef7af3f01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a8694151-7dc4-4081-8915-13b0fb5a9396"
        },
        {
            "id": "55df57c7-d45d-4286-b15b-ff89c8f05b3b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a8694151-7dc4-4081-8915-13b0fb5a9396"
        },
        {
            "id": "86123e82-4776-441d-b1f1-84bdb9878df6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "a8694151-7dc4-4081-8915-13b0fb5a9396"
        },
        {
            "id": "1d16f8fe-026e-44ba-9fc7-eba5318d5481",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "a8694151-7dc4-4081-8915-13b0fb5a9396"
        },
        {
            "id": "e0b8a320-c51d-4d36-bd6f-c4ec3533a35e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a8694151-7dc4-4081-8915-13b0fb5a9396"
        },
        {
            "id": "21d4cef6-1feb-4cdd-844a-118705ee9f15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a8694151-7dc4-4081-8915-13b0fb5a9396"
        },
        {
            "id": "352c3e9c-2010-46fd-89e8-c23bb1d20555",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "a8694151-7dc4-4081-8915-13b0fb5a9396"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "22b2a54a-e74f-4bcc-b8e1-19fa267438ec",
    "visible": true
}