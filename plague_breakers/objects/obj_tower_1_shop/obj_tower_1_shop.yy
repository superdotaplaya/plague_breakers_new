{
    "id": "8aea4293-de50-4063-9f50-d75be1378031",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tower_1_shop",
    "eventList": [
        {
            "id": "5ab113dc-975d-4ce0-b0c8-d0bd2d9fc503",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8aea4293-de50-4063-9f50-d75be1378031"
        },
        {
            "id": "362c9f1f-9d6a-4d23-a5b6-adf31626f540",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8aea4293-de50-4063-9f50-d75be1378031"
        },
        {
            "id": "ad543dec-9097-4530-a6e6-fe14757af86d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8aea4293-de50-4063-9f50-d75be1378031"
        },
        {
            "id": "664c8516-7ca8-4bd5-83bf-e0b3081cacb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "8aea4293-de50-4063-9f50-d75be1378031"
        },
        {
            "id": "958c5d0b-9307-4d1f-af3b-edf84ecbbab9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8aea4293-de50-4063-9f50-d75be1378031"
        },
        {
            "id": "7e80c72e-2046-40a3-8cc3-c53c3a64a181",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "8aea4293-de50-4063-9f50-d75be1378031"
        },
        {
            "id": "b177c07e-5c83-4b88-af3c-1675d93427e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "8aea4293-de50-4063-9f50-d75be1378031"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ff17cdf8-f43a-45c6-8068-c71366134d73",
    "visible": true
}