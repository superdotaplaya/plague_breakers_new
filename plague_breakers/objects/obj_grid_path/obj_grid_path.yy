{
    "id": "60a4c4af-a30a-4faa-9f98-a262d166f75e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_grid_path",
    "eventList": [
        
    ],
    "maskSpriteId": "59da29b9-0888-4671-90c0-5d7f78cf715a",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}