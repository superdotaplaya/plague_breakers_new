{
    "id": "ce267fd0-2724-4fa5-89cf-234d987b68ad",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cannon_ball_explosion",
    "eventList": [
        {
            "id": "d309c0dc-8472-4358-bae9-94bb73825852",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ce267fd0-2724-4fa5-89cf-234d987b68ad"
        },
        {
            "id": "6d487433-ba73-4b84-94fb-e09a2a2c0160",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ce267fd0-2724-4fa5-89cf-234d987b68ad"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f3f49e62-29bf-4389-aae4-b22d528fccb0",
    "visible": true
}