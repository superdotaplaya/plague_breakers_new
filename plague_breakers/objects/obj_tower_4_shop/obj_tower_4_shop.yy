{
    "id": "43c1b6e1-4e5c-402d-9b7f-54483b5eb297",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tower_4_shop",
    "eventList": [
        {
            "id": "575c02e1-f0d7-49a1-b4ff-94186651d600",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "43c1b6e1-4e5c-402d-9b7f-54483b5eb297"
        },
        {
            "id": "c79e34a2-66e0-49df-b319-4977c0b73e59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "43c1b6e1-4e5c-402d-9b7f-54483b5eb297"
        },
        {
            "id": "1878df2d-ccfd-4e1f-8341-f6a14c5ee019",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "43c1b6e1-4e5c-402d-9b7f-54483b5eb297"
        },
        {
            "id": "ebde6791-4f54-474f-a6f3-a4f332d9a2fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "43c1b6e1-4e5c-402d-9b7f-54483b5eb297"
        },
        {
            "id": "d10f8439-a76e-4552-bbc3-669270ad9079",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "43c1b6e1-4e5c-402d-9b7f-54483b5eb297"
        },
        {
            "id": "578bb40e-6e98-4e20-82d4-0049d67c1a29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "43c1b6e1-4e5c-402d-9b7f-54483b5eb297"
        },
        {
            "id": "36881ed3-28fe-4cea-bc02-d7b48e7d3791",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "43c1b6e1-4e5c-402d-9b7f-54483b5eb297"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4e7204c-bd0b-46b6-b2a1-b15035274049",
    "visible": true
}