{
    "id": "2b7eafba-df0e-49f6-9c87-a040b705859f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_rebind_shop1",
    "eventList": [
        {
            "id": "4918b247-d0ec-421e-9f41-65e47b9d0b18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2b7eafba-df0e-49f6-9c87-a040b705859f"
        },
        {
            "id": "821ba95f-7965-4a91-94e5-bb7370d2ffa9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2b7eafba-df0e-49f6-9c87-a040b705859f"
        },
        {
            "id": "1fde7a1c-e23b-4938-a9fb-4142455ec2e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "2b7eafba-df0e-49f6-9c87-a040b705859f"
        },
        {
            "id": "880f2588-bc49-442a-9ff8-7fc7b8d6d25e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "2b7eafba-df0e-49f6-9c87-a040b705859f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a832f2b-2273-4cc3-b553-8c06b7dc617f",
    "visible": true
}