{
    "id": "d8224659-55ce-4fd1-9bde-dbe9b4082526",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_rebind_repair",
    "eventList": [
        {
            "id": "457dd659-6c08-4595-9188-6d4c7f73ef91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d8224659-55ce-4fd1-9bde-dbe9b4082526"
        },
        {
            "id": "1e5f73bc-ba7d-4e91-8776-6f079662fbd6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d8224659-55ce-4fd1-9bde-dbe9b4082526"
        },
        {
            "id": "a9d368d5-4887-4b4f-b35b-c63151eb7430",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d8224659-55ce-4fd1-9bde-dbe9b4082526"
        },
        {
            "id": "fa08dbaf-06f3-49b6-90e1-2bf68b6339a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "d8224659-55ce-4fd1-9bde-dbe9b4082526"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a832f2b-2273-4cc3-b553-8c06b7dc617f",
    "visible": true
}