{
    "id": "4fe43015-ec45-4fb7-b7dc-143a56562fe5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shop_arrow_back",
    "eventList": [
        {
            "id": "3e332d38-d615-4ad3-8181-e338761bcad4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4fe43015-ec45-4fb7-b7dc-143a56562fe5"
        },
        {
            "id": "f7482dd0-1028-471c-9327-9b8afc4b2470",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "4fe43015-ec45-4fb7-b7dc-143a56562fe5"
        },
        {
            "id": "014a10dd-5c75-498a-9878-9df540cd614f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4fe43015-ec45-4fb7-b7dc-143a56562fe5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "be334386-c050-4fe3-a460-006da0e54d45",
    "visible": true
}