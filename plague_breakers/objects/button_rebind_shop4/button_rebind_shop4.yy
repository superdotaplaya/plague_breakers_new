{
    "id": "8eb60ee8-54df-4bd6-84f7-650043f89a44",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_rebind_shop4",
    "eventList": [
        {
            "id": "2779e90a-381a-44fb-a76d-1679522f0bd6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8eb60ee8-54df-4bd6-84f7-650043f89a44"
        },
        {
            "id": "f370adec-bf28-4b3f-8fcd-11be2e08cd37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8eb60ee8-54df-4bd6-84f7-650043f89a44"
        },
        {
            "id": "5615a895-6239-434d-a83b-2a55decaae76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8eb60ee8-54df-4bd6-84f7-650043f89a44"
        },
        {
            "id": "a2246b58-ddc4-41fe-a7e4-3295177b402b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "8eb60ee8-54df-4bd6-84f7-650043f89a44"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a832f2b-2273-4cc3-b553-8c06b7dc617f",
    "visible": true
}