/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 41123EDF
/// @DnDArgument : "code" "alarm_set(0,30)$(13_10)if healing = false then$(13_10){$(13_10)if instance_exists(obj_green_tower) then$(13_10){$(13_10)	total_green = instance_number(obj_green_tower)$(13_10)	for(g = 0; g <= total_green; g++)$(13_10)		{$(13_10)			{$(13_10)			green_tower_id[g] = instance_nth_nearest(x,y,obj_green_tower,g)$(13_10)			}$(13_10)show_debug_message(green_tower_id[g])$(13_10)		}$(13_10)}$(13_10)if instance_exists(obj_blue_tower) then$(13_10){$(13_10)	total_blue = instance_number(obj_blue_tower)$(13_10)	for(b = 0; b <= total_blue; b++)$(13_10)		{$(13_10)			{$(13_10)			blue_tower_id[b] = instance_nth_nearest(x,y,obj_blue_tower,b)$(13_10)			}$(13_10)show_debug_message(blue_tower_id[b])$(13_10)		}$(13_10)}$(13_10)if instance_exists(obj_tesla_tower) then$(13_10){$(13_10)	total_tesla = instance_number(obj_tesla_tower)$(13_10)	for(t = 0; t <= total_tesla; t++)$(13_10)		{$(13_10)			{$(13_10)			tesla_tower_id[t] = instance_nth_nearest(x,y,obj_tesla_tower,t)$(13_10)			}$(13_10)show_debug_message(tesla_tower_id[t])$(13_10)		}$(13_10)}$(13_10)if instance_exists(obj_cannon_tower) then$(13_10){$(13_10)	total_cannon = instance_number(obj_cannon_tower)$(13_10)	for(c = 0; c <= total_cannon; c++)$(13_10)		{$(13_10)			{$(13_10)			cannon_tower_id[c] = instance_nth_nearest(x,y,obj_cannon_tower,c)$(13_10)			}$(13_10)show_debug_message(cannon_tower_id[c])$(13_10)		}$(13_10)}$(13_10)if instance_exists(obj_poison_tower) then$(13_10){$(13_10)	total_poison = instance_number(obj_poison_tower)$(13_10)	for(p = 0; p <= total_poison; p++)$(13_10)		{$(13_10)			{$(13_10)			poison_tower_id[p] = instance_nth_nearest(x,y,obj_poison_tower,p)$(13_10)			}$(13_10)show_debug_message(poison_tower_id[p])$(13_10)		}$(13_10)}$(13_10)if instance_exists(obj_red_tower) then$(13_10){$(13_10)	total_red = instance_number(obj_red_tower)$(13_10)	for(r = 0; r <= total_red; r++)$(13_10)		{$(13_10)			{$(13_10)			red_tower_id[r] = instance_nth_nearest(x,y,obj_red_tower,r)$(13_10)			}$(13_10)show_debug_message(red_tower_id[r])$(13_10)		}$(13_10)}$(13_10)if instance_exists(obj_yellow_tower) then$(13_10){$(13_10)	total_yellow = instance_number(obj_yellow_tower)$(13_10)	for(yellow = 0; yellow <= total_yellow; yellow++)$(13_10)		{$(13_10)			{$(13_10)			yellow_tower_id[yellow] = instance_nth_nearest(x,y,obj_yellow_tower,yellow)$(13_10)			}$(13_10)show_debug_message(yellow_tower_id[yellow])$(13_10)		}$(13_10)}$(13_10)if instance_exists(obj_white_tower) then$(13_10){$(13_10)	total_white = instance_number(obj_white_tower)$(13_10)	for(w = 0; w <= total_white; w++)$(13_10)		{$(13_10)			{$(13_10)			white_tower_id[w] = instance_nth_nearest(x,y,obj_white_tower,w)$(13_10)			}$(13_10)show_debug_message(white_tower_id[w])$(13_10)		}$(13_10)}$(13_10)if instance_exists(obj_orange_tower) then$(13_10){$(13_10)	total_orange = instance_number(obj_orange_tower)$(13_10)	for(o = 0; o <= total_orange; o++)$(13_10)		{$(13_10)			{$(13_10)			orange_tower_id[o] = instance_nth_nearest(x,y,obj_orange_tower,o)$(13_10)			}$(13_10)show_debug_message(orange_tower_id[o])$(13_10)		}$(13_10)}$(13_10)if instance_exists(obj_modular_tower) then$(13_10){$(13_10)	total_modular = instance_number(obj_modular_tower)$(13_10)	for(m = 0; m <= total_modular; m++)$(13_10)		{$(13_10)			{$(13_10)			modular_tower_id[m] = instance_nth_nearest(x,y,obj_modular_tower,m)$(13_10)			}$(13_10)show_debug_message(modular_tower_id[m])$(13_10)		}$(13_10)}$(13_10)$(13_10)if instance_exists(obj_green_tower) then$(13_10){$(13_10)		total_green = instance_number(obj_green_tower)$(13_10)	for(g = 0; g <= total_green; g++)$(13_10)	{$(13_10)	hp = variable_instance_get(green_tower_id[g],"hp")$(13_10)	show_debug_message(hp)$(13_10)		if hp < 100 then$(13_10)		{$(13_10)		healing = true$(13_10)		healing_tower = green_tower_id[g]$(13_10)		move_towards_point(green_tower_id[g].x,green_tower_id[g].y,4) break	$(13_10)		}$(13_10)	}$(13_10)}$(13_10)if instance_exists(obj_tesla_tower) then$(13_10){$(13_10)		total_tesla = instance_number(obj_tesla_tower)$(13_10)	for(t = 0; t <= total_tesla; t++)$(13_10)	{$(13_10)	hp = variable_instance_get(tesla_tower_id[t],"hp")$(13_10)	show_debug_message(hp)$(13_10)		if hp < 100 then$(13_10)		{$(13_10)		healing = true$(13_10)		healing_tower = tesla_tower_id[t]$(13_10)		move_towards_point(tesla_tower_id[t].x,tesla_tower_id[t].y,4) break	$(13_10)		}$(13_10)	}$(13_10)}$(13_10)if instance_exists(obj_cannon_tower) then$(13_10){$(13_10)		total_cannon = instance_number(obj_cannon_tower)$(13_10)	for(c = 0; c <= total_cannon; c++)$(13_10)	{$(13_10)	hp = variable_instance_get(cannon_tower_id[c],"hp")$(13_10)	show_debug_message(hp)$(13_10)		if hp < 100 then$(13_10)		{$(13_10)		healing = true$(13_10)		healing_tower = cannon_tower_id[c]$(13_10)		move_towards_point(cannon_tower_id[c].x,cannon_tower_id[c].y,4) break	$(13_10)		}$(13_10)	}$(13_10)}$(13_10)if instance_exists(obj_poison_tower) then$(13_10){$(13_10)		total_poison = instance_number(obj_poison_tower)$(13_10)	for(p = 0; p <= total_poison; p++)$(13_10)	{$(13_10)	hp = variable_instance_get(poison_tower_id[p],"hp")$(13_10)	show_debug_message(hp)$(13_10)		if hp < 100 then$(13_10)		{$(13_10)		healing = true$(13_10)		healing_tower = poison_tower_id[p]$(13_10)		move_towards_point(poison_tower_id[p].x,poison_tower_id[p].y,4) break	$(13_10)		}$(13_10)	}$(13_10)}$(13_10)if instance_exists(obj_red_tower) then$(13_10){$(13_10)		total_red = instance_number(obj_red_tower)$(13_10)	for(r = 0; r <= total_red; r++)$(13_10)	{$(13_10)	hp = variable_instance_get(red_tower_id[r],"hp")$(13_10)	show_debug_message(hp)$(13_10)		if hp < 100 then$(13_10)		{$(13_10)		healing = true$(13_10)		healing_tower = tesla_tower_id[r]$(13_10)		move_towards_point(red_tower_id[r].x,red_tower_id[r].y,4) break	$(13_10)		}$(13_10)	}$(13_10)}$(13_10)if instance_exists(obj_yellow_tower) then$(13_10){$(13_10)		total_red = instance_number(obj_red_tower)$(13_10)	for(yellow = 0; yellow <= total_yellow; yellow++)$(13_10)	{$(13_10)	hp = variable_instance_get(yellow_tower_id[yellow],"hp")$(13_10)	show_debug_message(hp)$(13_10)		if hp < 100 then$(13_10)		{$(13_10)		healing = true$(13_10)		healing_tower = yellow_tower_id[yellow]$(13_10)		move_towards_point(yellow_tower_id[yellow].x,yellow_tower_id[yellow].y,4) break	$(13_10)		}$(13_10)	}$(13_10)}$(13_10)if instance_exists(obj_blue_tower) then$(13_10){$(13_10)		total_blue = instance_number(obj_blue_tower)$(13_10)	for(b = 0; b <= total_blue; b++)$(13_10)	{$(13_10)	hp = variable_instance_get(blue_tower_id[b],"hp")$(13_10)	show_debug_message(hp)$(13_10)		if hp < 100 then$(13_10)		{$(13_10)		healing = true$(13_10)		healing_tower = blue_tower_id[b]$(13_10)		move_towards_point(blue_tower_id[b].x,blue_tower_id[b].y,4) break	$(13_10)		}$(13_10)	}$(13_10)}$(13_10)if instance_exists(obj_white_tower) then$(13_10){$(13_10)		total_white = instance_number(obj_white_tower)$(13_10)	for(w = 0; w <= total_white; w++)$(13_10)	{$(13_10)	hp = variable_instance_get(white_tower_id[w],"hp")$(13_10)	show_debug_message(hp)$(13_10)		if hp < 100 then$(13_10)		{$(13_10)		healing = true$(13_10)		healing_tower = white_tower_id[w]$(13_10)		move_towards_point(white_tower_id[w].x,white_tower_id[w].y,4) break	$(13_10)		}$(13_10)	}$(13_10)}$(13_10)if instance_exists(obj_orange_tower) then$(13_10){$(13_10)		total_orange = instance_number(obj_orange_tower)$(13_10)	for(o = 0; o <= total_orange; o++)$(13_10)	{$(13_10)	hp = variable_instance_get(orange_tower_id[o],"hp")$(13_10)	show_debug_message(hp)$(13_10)		if hp < 100 then$(13_10)		{$(13_10)		healing = true$(13_10)		healing_tower = orange_tower_id[o]$(13_10)		move_towards_point(orange_tower_id[o].x,orange_tower_id[o].y,4) break	$(13_10)		}$(13_10)	}$(13_10)}$(13_10)if instance_exists(obj_modular_tower) then$(13_10){$(13_10)		total_modular = instance_number(obj_modular_tower)$(13_10)	{$(13_10)	hp = variable_instance_get(modular_tower_id[m],"hp")$(13_10)	show_debug_message(hp)$(13_10)		if hp < 100 then$(13_10)		{$(13_10)		healing = true$(13_10)		healing_tower = modular_tower_id[m]$(13_10)		move_towards_point(modular_tower_id[m].x,modular_tower_id[m].y,4)$(13_10)		}$(13_10)	}$(13_10)}$(13_10)}"
alarm_set(0,30)
if healing = false then
{
if instance_exists(obj_green_tower) then
{
	total_green = instance_number(obj_green_tower)
	for(g = 0; g <= total_green; g++)
		{
			{
			green_tower_id[g] = instance_nth_nearest(x,y,obj_green_tower,g)
			}
show_debug_message(green_tower_id[g])
		}
}
if instance_exists(obj_blue_tower) then
{
	total_blue = instance_number(obj_blue_tower)
	for(b = 0; b <= total_blue; b++)
		{
			{
			blue_tower_id[b] = instance_nth_nearest(x,y,obj_blue_tower,b)
			}
show_debug_message(blue_tower_id[b])
		}
}
if instance_exists(obj_tesla_tower) then
{
	total_tesla = instance_number(obj_tesla_tower)
	for(t = 0; t <= total_tesla; t++)
		{
			{
			tesla_tower_id[t] = instance_nth_nearest(x,y,obj_tesla_tower,t)
			}
show_debug_message(tesla_tower_id[t])
		}
}
if instance_exists(obj_cannon_tower) then
{
	total_cannon = instance_number(obj_cannon_tower)
	for(c = 0; c <= total_cannon; c++)
		{
			{
			cannon_tower_id[c] = instance_nth_nearest(x,y,obj_cannon_tower,c)
			}
show_debug_message(cannon_tower_id[c])
		}
}
if instance_exists(obj_poison_tower) then
{
	total_poison = instance_number(obj_poison_tower)
	for(p = 0; p <= total_poison; p++)
		{
			{
			poison_tower_id[p] = instance_nth_nearest(x,y,obj_poison_tower,p)
			}
show_debug_message(poison_tower_id[p])
		}
}
if instance_exists(obj_red_tower) then
{
	total_red = instance_number(obj_red_tower)
	for(r = 0; r <= total_red; r++)
		{
			{
			red_tower_id[r] = instance_nth_nearest(x,y,obj_red_tower,r)
			}
show_debug_message(red_tower_id[r])
		}
}
if instance_exists(obj_yellow_tower) then
{
	total_yellow = instance_number(obj_yellow_tower)
	for(yellow = 0; yellow <= total_yellow; yellow++)
		{
			{
			yellow_tower_id[yellow] = instance_nth_nearest(x,y,obj_yellow_tower,yellow)
			}
show_debug_message(yellow_tower_id[yellow])
		}
}
if instance_exists(obj_white_tower) then
{
	total_white = instance_number(obj_white_tower)
	for(w = 0; w <= total_white; w++)
		{
			{
			white_tower_id[w] = instance_nth_nearest(x,y,obj_white_tower,w)
			}
show_debug_message(white_tower_id[w])
		}
}
if instance_exists(obj_orange_tower) then
{
	total_orange = instance_number(obj_orange_tower)
	for(o = 0; o <= total_orange; o++)
		{
			{
			orange_tower_id[o] = instance_nth_nearest(x,y,obj_orange_tower,o)
			}
show_debug_message(orange_tower_id[o])
		}
}
if instance_exists(obj_modular_tower) then
{
	total_modular = instance_number(obj_modular_tower)
	for(m = 0; m <= total_modular; m++)
		{
			{
			modular_tower_id[m] = instance_nth_nearest(x,y,obj_modular_tower,m)
			}
show_debug_message(modular_tower_id[m])
		}
}

if instance_exists(obj_green_tower) then
{
		total_green = instance_number(obj_green_tower)
	for(g = 0; g <= total_green; g++)
	{
	hp = variable_instance_get(green_tower_id[g],"hp")
	show_debug_message(hp)
		if hp < 100 then
		{
		healing = true
		healing_tower = green_tower_id[g]
		move_towards_point(green_tower_id[g].x,green_tower_id[g].y,4) break	
		}
	}
}
if instance_exists(obj_tesla_tower) then
{
		total_tesla = instance_number(obj_tesla_tower)
	for(t = 0; t <= total_tesla; t++)
	{
	hp = variable_instance_get(tesla_tower_id[t],"hp")
	show_debug_message(hp)
		if hp < 100 then
		{
		healing = true
		healing_tower = tesla_tower_id[t]
		move_towards_point(tesla_tower_id[t].x,tesla_tower_id[t].y,4) break	
		}
	}
}
if instance_exists(obj_cannon_tower) then
{
		total_cannon = instance_number(obj_cannon_tower)
	for(c = 0; c <= total_cannon; c++)
	{
	hp = variable_instance_get(cannon_tower_id[c],"hp")
	show_debug_message(hp)
		if hp < 100 then
		{
		healing = true
		healing_tower = cannon_tower_id[c]
		move_towards_point(cannon_tower_id[c].x,cannon_tower_id[c].y,4) break	
		}
	}
}
if instance_exists(obj_poison_tower) then
{
		total_poison = instance_number(obj_poison_tower)
	for(p = 0; p <= total_poison; p++)
	{
	hp = variable_instance_get(poison_tower_id[p],"hp")
	show_debug_message(hp)
		if hp < 100 then
		{
		healing = true
		healing_tower = poison_tower_id[p]
		move_towards_point(poison_tower_id[p].x,poison_tower_id[p].y,4) break	
		}
	}
}
if instance_exists(obj_red_tower) then
{
		total_red = instance_number(obj_red_tower)
	for(r = 0; r <= total_red; r++)
	{
	hp = variable_instance_get(red_tower_id[r],"hp")
	show_debug_message(hp)
		if hp < 100 then
		{
		healing = true
		healing_tower = tesla_tower_id[r]
		move_towards_point(red_tower_id[r].x,red_tower_id[r].y,4) break	
		}
	}
}
if instance_exists(obj_yellow_tower) then
{
		total_red = instance_number(obj_red_tower)
	for(yellow = 0; yellow <= total_yellow; yellow++)
	{
	hp = variable_instance_get(yellow_tower_id[yellow],"hp")
	show_debug_message(hp)
		if hp < 100 then
		{
		healing = true
		healing_tower = yellow_tower_id[yellow]
		move_towards_point(yellow_tower_id[yellow].x,yellow_tower_id[yellow].y,4) break	
		}
	}
}
if instance_exists(obj_blue_tower) then
{
		total_blue = instance_number(obj_blue_tower)
	for(b = 0; b <= total_blue; b++)
	{
	hp = variable_instance_get(blue_tower_id[b],"hp")
	show_debug_message(hp)
		if hp < 100 then
		{
		healing = true
		healing_tower = blue_tower_id[b]
		move_towards_point(blue_tower_id[b].x,blue_tower_id[b].y,4) break	
		}
	}
}
if instance_exists(obj_white_tower) then
{
		total_white = instance_number(obj_white_tower)
	for(w = 0; w <= total_white; w++)
	{
	hp = variable_instance_get(white_tower_id[w],"hp")
	show_debug_message(hp)
		if hp < 100 then
		{
		healing = true
		healing_tower = white_tower_id[w]
		move_towards_point(white_tower_id[w].x,white_tower_id[w].y,4) break	
		}
	}
}
if instance_exists(obj_orange_tower) then
{
		total_orange = instance_number(obj_orange_tower)
	for(o = 0; o <= total_orange; o++)
	{
	hp = variable_instance_get(orange_tower_id[o],"hp")
	show_debug_message(hp)
		if hp < 100 then
		{
		healing = true
		healing_tower = orange_tower_id[o]
		move_towards_point(orange_tower_id[o].x,orange_tower_id[o].y,4) break	
		}
	}
}
if instance_exists(obj_modular_tower) then
{
		total_modular = instance_number(obj_modular_tower)
	{
	hp = variable_instance_get(modular_tower_id[m],"hp")
	show_debug_message(hp)
		if hp < 100 then
		{
		healing = true
		healing_tower = modular_tower_id[m]
		move_towards_point(modular_tower_id[m].x,modular_tower_id[m].y,4)
		}
	}
}
}