{
    "id": "54572a24-33e8-43aa-ad36-10856a453cf5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_repair_unit",
    "eventList": [
        {
            "id": "58bf5e3a-7368-4666-85f3-6774f528ff5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "54572a24-33e8-43aa-ad36-10856a453cf5"
        },
        {
            "id": "d5ac834d-7494-489d-af90-578fed194a32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "54572a24-33e8-43aa-ad36-10856a453cf5"
        },
        {
            "id": "e52800d1-9989-41d3-aeba-d6ae338d8bb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "54572a24-33e8-43aa-ad36-10856a453cf5"
        },
        {
            "id": "a3d24d1d-8d4f-4e16-b1e9-0787f85b1161",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cae81964-d4ac-454f-a5a2-c281915d634f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "54572a24-33e8-43aa-ad36-10856a453cf5"
        },
        {
            "id": "7bf4e094-3b03-4959-a21f-58689d01e554",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "917d34ca-80aa-417f-83d3-99b44da049e3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "54572a24-33e8-43aa-ad36-10856a453cf5"
        },
        {
            "id": "592e2ae3-b569-4b2e-8b8e-2c06952c19d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "8c6657eb-ba1a-4bb3-a87b-942defa24304",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "54572a24-33e8-43aa-ad36-10856a453cf5"
        },
        {
            "id": "c2ce5f3f-8dcd-4762-bdf2-c7d35758f396",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "8f474088-e4dc-410b-947c-2b9e909cbb62",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "54572a24-33e8-43aa-ad36-10856a453cf5"
        },
        {
            "id": "8207baf8-dec1-4f07-b9cf-4dfc7f2167ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "54572a24-33e8-43aa-ad36-10856a453cf5"
        },
        {
            "id": "44a04ced-25a1-4520-babc-37ae95c55d23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "12a615a1-fa0f-4167-95d1-7acdee951786",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "54572a24-33e8-43aa-ad36-10856a453cf5"
        },
        {
            "id": "ef0d7694-3b10-4d8e-8c0a-72d1cd6eb568",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "54572a24-33e8-43aa-ad36-10856a453cf5"
        },
        {
            "id": "12270953-4ee6-4be2-9de4-d63c0a71ca76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "5c37c902-777c-43b5-a437-9dca52f80486",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "54572a24-33e8-43aa-ad36-10856a453cf5"
        },
        {
            "id": "41c4d142-25d4-4728-88eb-904cce549fc5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "54572a24-33e8-43aa-ad36-10856a453cf5"
        },
        {
            "id": "60045153-5fb5-451d-b736-a0b83946a277",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "6805e61e-ad9e-4afe-a331-68c08d156c9c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "54572a24-33e8-43aa-ad36-10856a453cf5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7e2d22d2-2ee3-47d6-b267-f07eb76b5fe7",
    "visible": true
}