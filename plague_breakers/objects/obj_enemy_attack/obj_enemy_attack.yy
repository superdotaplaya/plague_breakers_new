{
    "id": "f0875ffd-fd8e-428c-aabc-956eb91215a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_attack",
    "eventList": [
        {
            "id": "3de4d69a-b230-4b08-9bc1-fbd9c469881a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f0875ffd-fd8e-428c-aabc-956eb91215a5"
        },
        {
            "id": "20eac708-af10-45da-863c-d68f2519c631",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f0875ffd-fd8e-428c-aabc-956eb91215a5"
        },
        {
            "id": "9738bdd2-1e21-400a-9610-c32c90f1d712",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "cae81964-d4ac-454f-a5a2-c281915d634f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f0875ffd-fd8e-428c-aabc-956eb91215a5"
        }
    ],
    "maskSpriteId": "8024de4a-5f56-4836-9a65-fd17fd8bed4b",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cd280ec0-cafa-4c27-a6f0-46d60d1e9c3a",
    "visible": true
}