/// @description Request Success
if(is_undefined(request_result_data_key))
{
	gameon_util_log("GameOn generic number promise object has no valid request data key set. Request result cannot be parsed.", true, undefined);
	event_user(eGameOnPromiseEvent.FAILURE);
	return;
}

var responseJSON = json_decode(request_result_data);
if(!ds_exists(responseJSON, ds_type_map) || !ds_map_exists(responseJSON, request_result_data_key))
{
	gameon_util_log("Failed to parse response data.", true, undefined);
	event_user(eGameOnPromiseEvent.FAILURE);
	return;
}

request_result_data_parsed = responseJSON[? request_result_data_key];
ds_map_destroy(responseJSON);

event_inherited();