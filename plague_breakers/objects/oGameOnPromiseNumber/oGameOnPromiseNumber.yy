{
    "id": "1e0a7c1e-5f3e-49c6-b3da-484d8eea66b7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameOnPromiseNumber",
    "eventList": [
        {
            "id": "be6f69d0-1ca6-4f53-9b49-07513b9a6e47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "1e0a7c1e-5f3e-49c6-b3da-484d8eea66b7"
        },
        {
            "id": "9764a2b7-d313-4189-9ea7-e5eb8dce6b85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1e0a7c1e-5f3e-49c6-b3da-484d8eea66b7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6d6eb90d-22ce-42f0-b45c-d4b59286039c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}