{
    "id": "f9f14a6f-b432-4b2e-afd9-92d8085cc2fb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_red_attack_sub",
    "eventList": [
        {
            "id": "41ba5e9d-fb67-4431-844e-91a382485a3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f9f14a6f-b432-4b2e-afd9-92d8085cc2fb"
        },
        {
            "id": "2de1f7e8-fbef-4c43-9b11-ce88893c2829",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f9f14a6f-b432-4b2e-afd9-92d8085cc2fb"
        },
        {
            "id": "ff085892-3bbd-4011-839a-71bfa1057e8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f9f14a6f-b432-4b2e-afd9-92d8085cc2fb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ad00aabc-3afc-4848-8f18-53f3606a4cc4",
    "visible": true
}