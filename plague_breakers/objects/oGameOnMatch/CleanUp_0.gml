if(gameon_util_object_exists(tournament))
	gameon_util_object_destroy(tournament);

if(!is_undefined(awardedPrizes) && ds_exists(awardedPrizes, ds_type_list))
	ds_list_destroy(awardedPrizes);

tournament = undefined;
awardedPrizes = undefined;

event_inherited();