event_inherited();

// Parse root fields
attemptsRemaining =			json_raw_data[? "attemptsRemaining"];
matchId =					json_raw_data[? "matchId"];

lastScore =					json_raw_data[? "lastScore"];
lastScoreDate =				json_raw_data[? "lastScore"];
scoreOverall =				json_raw_data[? "score"];
scoreOverallDate =			json_raw_data[? "scoreDate"];

prizeBundleClaimStatus =	json_raw_data[? "prizeBundleClaimStatus"];

teamId =					json_raw_data[? "teamId"];

// Parse awarded prizes
var awardedPrizesJSON = json_raw_data[? "awardedPrizes"];
if(!is_undefined(awardedPrizesJSON) && ds_exists(awardedPrizesJSON, ds_type_list))
{
	awardedPrizes = ds_list_create();
	gameon_util_data_list_parse(json_raw_data, awardedPrizes, oGameOnPrizeAwardItem, "awardedPrizes");
}

// Some match endpoints will return tournament data for the match nested in a "tournamentDetails" node
// while others will return it as part of the root match node. We'll parse both into a separate tournament object.
tournament = gameon_util_object_create(oGameOnTournament);
if(ds_map_exists(json_raw_data, "tournamentDetails"))
	gameon_util_data_parse(tournament, json_raw_data[? "tournamentDetails"]);
else
	gameon_util_data_parse(tournament, json_raw_data);