event_inherited();

attemptsRemaining = undefined;
matchId = undefined;

lastScore =	undefined;
lastScoreDate =	undefined;
scoreOverall = undefined;
scoreOverallDate = undefined;

tournament = undefined;
awardedPrizes = undefined;
prizeBundleClaimStatus = undefined;

teamId = undefined;