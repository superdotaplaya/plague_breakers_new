/// @description Parse JSON
event_inherited();

prizeId =		json_raw_data[? "prizeId"];
title =			json_raw_data[? "title"];
description =	json_raw_data[? "description"];
imageUrl =		json_raw_data[? "imageUrl"];
prizeInfoType = json_raw_data[? "prizeInfoType"];
dateCreated =	json_raw_data[? "dateCreated"];