{
    "id": "23ff3f04-e27a-44a3-90bd-5a938e14be03",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameOnPrize",
    "eventList": [
        {
            "id": "9f9ab1d3-a10f-419d-8bf1-6f78564e0032",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "23ff3f04-e27a-44a3-90bd-5a938e14be03"
        },
        {
            "id": "e6f61310-2737-4d37-a233-370742c63a24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "23ff3f04-e27a-44a3-90bd-5a938e14be03"
        },
        {
            "id": "918da6ab-5589-48ce-bc55-163318c1b1f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "23ff3f04-e27a-44a3-90bd-5a938e14be03"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0330c072-566e-4a20-bbb9-c6edf5a161cf",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}