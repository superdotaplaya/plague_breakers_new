{
    "id": "24df6b46-cd3a-4825-9a96-832716290936",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_source_desert_tiles",
    "eventList": [
        {
            "id": "9b07d68e-caa1-4aae-a251-b4158949abfb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "24df6b46-cd3a-4825-9a96-832716290936"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b0ded13b-636c-4074-a05a-9df3ee3a67d1",
    "visible": true
}