/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 59957205
/// @DnDArgument : "code" "draw_set_font(fnt_main)$(13_10)global.hotkeyDisplay = global.questsHotkey;$(13_10)if global.questmode = true then$(13_10){$(13_10)current_room = room$(13_10)ini_open("quests.ini")$(13_10)$(13_10)if current_room = map_002 then$(13_10){$(13_10)draw_text(x-50,y-50,"Quests (" + string(key_name_convert(global.hotkeyDisplay)) + " to close)")$(13_10)draw_text(x,y,quest_desc_1)$(13_10)$(13_10)if quest_desc_1 = "Kill 50 enemies"$(13_10){$(13_10)draw_text(x+350,y,string(enemies_killed) + "/50")$(13_10)draw_sprite_stretched(spr_shield,0,x+500,y-53,102,125)$(13_10)draw_set_color(c_yellow)$(13_10)draw_text(x+530,y-23,"10")$(13_10)draw_set_color(c_white)$(13_10)if enemies_killed >= 50 then$(13_10){$(13_10)premium_add(10)$(13_10)ini_write_string("Quest 1","quest","completed")$(13_10)} $(13_10)}$(13_10)else$(13_10)if quest_desc_1 = "Survive 30 rounds on any map without losing a life" then$(13_10){$(13_10)	draw_text(x+1100,y,string(global.daily_level) + "/30")$(13_10)	draw_sprite_stretched(spr_shield,0,x+1250,y-53,102,125)$(13_10)draw_set_color(c_yellow)$(13_10)draw_text(x+1280,y-23,"10")$(13_10)draw_set_color(c_white)$(13_10)}$(13_10)if quest_desc_1 = "Kill a thief that is carrying more than 10 gold" then$(13_10){$(13_10)	draw_text(x+950,y,"0/1")$(13_10)		draw_sprite_stretched(spr_shield,0,x+1100,y-53,102,125)$(13_10)draw_set_color(c_yellow)$(13_10)draw_text(x+1130,y-23,"10")$(13_10)draw_set_color(c_white)$(13_10)}$(13_10)if quest_desc_1 = "Repair a tower for 50 hp" then$(13_10){$(13_10)	draw_text(x+600,y,string(global.tower_healed) + "/50")$(13_10)		draw_sprite_stretched(spr_shield,0,x+750,y-53,102,125)$(13_10)draw_set_color(c_yellow)$(13_10)draw_text(x+780,y-23,"10")$(13_10)draw_set_color(c_white)$(13_10)}$(13_10)if quest_desc_1 = "Kill 20 enemies with the poison tower" then$(13_10){$(13_10)	draw_text(x+800,y,string(global.killed_by_poison) + "/20")$(13_10)			draw_sprite_stretched(spr_shield,0,x+950,y-53,102,125)$(13_10)draw_set_color(c_yellow)$(13_10)draw_text(x+980,y-23,"10")$(13_10)draw_set_color(c_white)$(13_10)}$(13_10)}$(13_10)}"
draw_set_font(fnt_main)
global.hotkeyDisplay = global.questsHotkey;
if global.questmode = true then
{
current_room = room
ini_open("quests.ini")

if current_room = map_002 then
{
draw_text(x-50,y-50,"Quests (" + string(key_name_convert(global.hotkeyDisplay)) + " to close)")
draw_text(x,y,quest_desc_1)

if quest_desc_1 = "Kill 50 enemies"
{
draw_text(x+350,y,string(enemies_killed) + "/50")
draw_sprite_stretched(spr_shield,0,x+500,y-53,102,125)
draw_set_color(c_yellow)
draw_text(x+530,y-23,"10")
draw_set_color(c_white)
if enemies_killed >= 50 then
{
premium_add(10)
ini_write_string("Quest 1","quest","completed")
} 
}
else
if quest_desc_1 = "Survive 30 rounds on any map without losing a life" then
{
	draw_text(x+1100,y,string(global.daily_level) + "/30")
	draw_sprite_stretched(spr_shield,0,x+1250,y-53,102,125)
draw_set_color(c_yellow)
draw_text(x+1280,y-23,"10")
draw_set_color(c_white)
}
if quest_desc_1 = "Kill a thief that is carrying more than 10 gold" then
{
	draw_text(x+950,y,"0/1")
		draw_sprite_stretched(spr_shield,0,x+1100,y-53,102,125)
draw_set_color(c_yellow)
draw_text(x+1130,y-23,"10")
draw_set_color(c_white)
}
if quest_desc_1 = "Repair a tower for 50 hp" then
{
	draw_text(x+600,y,string(global.tower_healed) + "/50")
		draw_sprite_stretched(spr_shield,0,x+750,y-53,102,125)
draw_set_color(c_yellow)
draw_text(x+780,y-23,"10")
draw_set_color(c_white)
}
if quest_desc_1 = "Kill 20 enemies with the poison tower" then
{
	draw_text(x+800,y,string(global.killed_by_poison) + "/20")
			draw_sprite_stretched(spr_shield,0,x+950,y-53,102,125)
draw_set_color(c_yellow)
draw_text(x+980,y-23,"10")
draw_set_color(c_white)
}
}
}