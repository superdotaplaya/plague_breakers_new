/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 4E9C913C
/// @DnDArgument : "code" "ini_open("quests.ini")$(13_10)if (ini_read_real("Date","Last Date Assigned",0)) != current_day then$(13_10){$(13_10)    ini_open("quests.ini")$(13_10)    date_assigned = date_get_day(date_current_datetime())$(13_10)    ini_write_real("Date","Last Date Assigned", date_assigned)$(13_10)    quests_assign()$(13_10)    ini_close()$(13_10)}$(13_10)ini_open("quests.ini")$(13_10)quest_desc_1 = ini_read_string("Quest 1","quest"," ")$(13_10)quest_progress_1 = ini_read_real("Quest 1","Progress",0)$(13_10)quest_desc_2 = ini_read_string("Quest 2","quest"," ")$(13_10)quest_progress_2= ini_read_real("Quest 2","Progress",0)$(13_10)if quest_desc_1 = "Survive 30 rounds on any map without losing a life" then$(13_10){$(13_10)    if global.daily_level = 30 then$(13_10)    {$(13_10)		ini_open("quests.ini")$(13_10)        premium_add(10)$(13_10)        ini_write_string("Quest 1","quest","completed")$(13_10)    }$(13_10)}$(13_10)if quest_desc_1 = "Kill 20 enemies with the poison tower" and global.killed_by_poison >= 20 then$(13_10){$(13_10)    premium_add(10)$(13_10)    ini_write_string("Quest 1","quest","completed")$(13_10)    global.killed_by_poison = 0$(13_10)}$(13_10)$(13_10)$(13_10)"
ini_open("quests.ini")
if (ini_read_real("Date","Last Date Assigned",0)) != current_day then
{
    ini_open("quests.ini")
    date_assigned = date_get_day(date_current_datetime())
    ini_write_real("Date","Last Date Assigned", date_assigned)
    quests_assign()
    ini_close()
}
ini_open("quests.ini")
quest_desc_1 = ini_read_string("Quest 1","quest"," ")
quest_progress_1 = ini_read_real("Quest 1","Progress",0)
quest_desc_2 = ini_read_string("Quest 2","quest"," ")
quest_progress_2= ini_read_real("Quest 2","Progress",0)
if quest_desc_1 = "Survive 30 rounds on any map without losing a life" then
{
    if global.daily_level = 30 then
    {
		ini_open("quests.ini")
        premium_add(10)
        ini_write_string("Quest 1","quest","completed")
    }
}
if quest_desc_1 = "Kill 20 enemies with the poison tower" and global.killed_by_poison >= 20 then
{
    premium_add(10)
    ini_write_string("Quest 1","quest","completed")
    global.killed_by_poison = 0
}