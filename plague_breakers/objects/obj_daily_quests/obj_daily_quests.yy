{
    "id": "61bab87b-3114-4dce-886b-2103410de7af",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_daily_quests",
    "eventList": [
        {
            "id": "ee39ac5e-0f48-4979-89f0-ddb3c8b83c69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "61bab87b-3114-4dce-886b-2103410de7af"
        },
        {
            "id": "50318cbf-52b0-47e9-b4b0-8c7624a2faa6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "61bab87b-3114-4dce-886b-2103410de7af"
        },
        {
            "id": "d1f3db9d-aa1f-47ca-abdc-9253b5913725",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "61bab87b-3114-4dce-886b-2103410de7af"
        },
        {
            "id": "26a79ad1-ac23-4481-a7c5-f120db287335",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "61bab87b-3114-4dce-886b-2103410de7af"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}