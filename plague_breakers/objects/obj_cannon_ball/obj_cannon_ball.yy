{
    "id": "bc437458-0bcb-4797-ad25-4e62e9e4ef6e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cannon_ball",
    "eventList": [
        {
            "id": "f0c4530a-b0ab-4123-9bfb-186146f88216",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bc437458-0bcb-4797-ad25-4e62e9e4ef6e"
        },
        {
            "id": "5adbec6f-d2f5-4187-abbb-3dfa7896d287",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bc437458-0bcb-4797-ad25-4e62e9e4ef6e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "75cdcd5a-4765-4935-96b3-08cfad664e70",
    "visible": true
}