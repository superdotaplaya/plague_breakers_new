/// @DnDAction : YoYo Games.Drawing.Draw_Self
/// @DnDVersion : 1
/// @DnDHash : 728355C9
draw_self();

/// @DnDAction : YoYo Games.Drawing.Set_Color
/// @DnDVersion : 1
/// @DnDHash : 0AC85743
draw_set_colour($FFFFFFFF & $ffffff);
var l0AC85743_0=($FFFFFFFF >> 24);
draw_set_alpha(l0AC85743_0 / $ff);

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 17173C0A
/// @DnDArgument : "x" "-75"
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "115"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "caption" ""Cost: ""
/// @DnDArgument : "var" "cost"
draw_text(x + -75, y + 115, string("Cost: ") + string(cost));

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 04B24F30
/// @DnDArgument : "var" "hover"
/// @DnDArgument : "value" "true"
if(hover == true)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 1776B13F
	/// @DnDParent : 04B24F30
	/// @DnDArgument : "alpha" ".5"
	draw_set_alpha(.5);

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4E073ED8
	/// @DnDParent : 04B24F30
	/// @DnDArgument : "var" "global.highground"
	/// @DnDArgument : "value" "false"
	if(global.highground == false)
	{
		/// @DnDAction : YoYo Games.Drawing.Draw_Gradient_Ellipse
		/// @DnDVersion : 1
		/// @DnDHash : 6FD972D4
		/// @DnDParent : 4E073ED8
		/// @DnDArgument : "x1" "global.x -110"
		/// @DnDArgument : "y1" "global.y -110"
		/// @DnDArgument : "x2" "global.x + 110"
		/// @DnDArgument : "y2" "global.y + 110"
		/// @DnDArgument : "col1" "$FFFFFFFF"
		/// @DnDArgument : "col2" "$FFB3B3B3"
		/// @DnDArgument : "fill" "1"
		draw_ellipse_colour(global.x -110, global.y -110, global.x + 110, global.y + 110, $FFFFFFFF & $FFFFFF, $FFB3B3B3 & $FFFFFF, 0);
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 0568E408
	/// @DnDParent : 04B24F30
	else
	{
		/// @DnDAction : YoYo Games.Drawing.Draw_Gradient_Ellipse
		/// @DnDVersion : 1
		/// @DnDHash : 51355A5E
		/// @DnDParent : 0568E408
		/// @DnDArgument : "x1" "global.x -360"
		/// @DnDArgument : "y1" "global.y -360"
		/// @DnDArgument : "x2" "global.x + 360"
		/// @DnDArgument : "y2" "global.y + 360"
		/// @DnDArgument : "col1" "$FFFFFFFF"
		/// @DnDArgument : "col2" "$FFB3B3B3"
		/// @DnDArgument : "fill" "1"
		draw_ellipse_colour(global.x -360, global.y -360, global.x + 360, global.y + 360, $FFFFFFFF & $FFFFFF, $FFB3B3B3 & $FFFFFF, 0);
	}

	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 13D69EE3
	/// @DnDParent : 04B24F30
	draw_set_alpha(1);

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 68B7D12B
	/// @DnDParent : 04B24F30
	/// @DnDArgument : "expr" ""Fires one main shot and 2 sub shots in a  random direction for half damage""
	/// @DnDArgument : "var" "global.tooltip"
	global.tooltip = "Fires one main shot and 2 sub shots in a  random direction for half damage";
}