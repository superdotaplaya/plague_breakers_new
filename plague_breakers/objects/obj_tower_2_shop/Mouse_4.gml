/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 55D6F4FD
/// @DnDArgument : "var" "global.gold"
/// @DnDArgument : "op" "4"
/// @DnDArgument : "value" "cost"
if(global.gold >= cost)
{
	/// @DnDAction : YoYo Games.Common.Set_Global
	/// @DnDVersion : 1
	/// @DnDHash : 1C088A1B
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "value" "-cost"
	/// @DnDArgument : "value_relative" "1"
	/// @DnDArgument : "var" "gold"
	global.gold += -cost;

	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 0180A620
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "xpos" "global.x"
	/// @DnDArgument : "ypos" "global.y"
	/// @DnDArgument : "objectid" "obj_red_tower"
	/// @DnDArgument : "layer" ""Instances_towers""
	/// @DnDSaveInfo : "objectid" "2ecc4cc9-4703-49e9-ae88-96e60fd2a8e2"
	instance_create_layer(global.x, global.y, "Instances_towers", obj_red_tower);

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4BF30A3A
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "var" "global.highground"
	/// @DnDArgument : "value" "true"
	if(global.highground == true)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 28BA223B
		/// @DnDInput : 2
		/// @DnDApplyTo : instance_nearest(global.x,global.y,obj_red_tower)
		/// @DnDParent : 4BF30A3A
		/// @DnDArgument : "expr" "range + 250"
		/// @DnDArgument : "expr_1" "true"
		/// @DnDArgument : "var" "range"
		/// @DnDArgument : "var_1" "highground"
		with(instance_nearest(global.x,global.y,obj_red_tower)) {
		range = range + 250;
		highground = true;
		
		}
	}

	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 19FCD6C0
	/// @DnDParent : 55D6F4FD
	draw_set_alpha(1);

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 05DE1BDC
	/// @DnDApplyTo : 073fec17-ba80-4dec-a3c8-288b74078b2d
	/// @DnDParent : 55D6F4FD
	with(obj_tower_2_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 25EEA9B5
	/// @DnDApplyTo : 8aea4293-de50-4063-9f50-d75be1378031
	/// @DnDParent : 55D6F4FD
	with(obj_tower_1_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 1CA51B70
	/// @DnDApplyTo : ff372800-7810-49bb-898f-a7defb5cc613
	/// @DnDParent : 55D6F4FD
	with(obj_tower_3_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 06A51664
	/// @DnDApplyTo : 43c1b6e1-4e5c-402d-9b7f-54483b5eb297
	/// @DnDParent : 55D6F4FD
	with(obj_tower_4_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 49CD5E15
	/// @DnDApplyTo : 90a0bd5f-c90d-4cee-8bb7-72c2fd9704c9
	/// @DnDParent : 55D6F4FD
	with(obj_tesla_tower_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 72ACDBCB
	/// @DnDApplyTo : b69a5d5d-926a-4cc5-b306-6b0be9c55b19
	/// @DnDParent : 55D6F4FD
	with(obj_cannon_tower_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 48A8C41B
	/// @DnDApplyTo : 16d11c02-1a7d-4f1e-8cf3-b39df52c2b8b
	/// @DnDParent : 55D6F4FD
	with(obj_tower_5_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 3944D048
	/// @DnDApplyTo : 32183b7a-d784-471e-939b-c846cb1d5088
	/// @DnDParent : 55D6F4FD
	with(obj_tower_poison_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Paths.Start_Path
	/// @DnDVersion : 1.1
	/// @DnDHash : 4CB932A7
	/// @DnDApplyTo : 37f57748-c862-493a-bbbd-c8ffdf8f8d60
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "path" "shop_close"
	/// @DnDArgument : "speed" "20"
	/// @DnDArgument : "relative" "true"
	/// @DnDSaveInfo : "path" "82e069ee-3c69-4d32-95f0-56a32a932d1f"
	with(obj_shop_menu) path_start(shop_close, 20, path_action_stop, true);

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 31C7940E
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "expr" "false"
	/// @DnDArgument : "var" "global.shop_open"
	global.shop_open = false;

	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 74E37F4F
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "code" "instance_activate_layer("Instances")$(13_10)instance_destroy(obj_tooltips)$(13_10)audio_play_sound(snd_buy,1,0)"
	instance_activate_layer("Instances")
	instance_destroy(obj_tooltips)
	audio_play_sound(snd_buy,1,0)
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 1C3DA4A7
/// @DnDArgument : "var" "global.ui"
/// @DnDArgument : "value" "true"
if(global.ui == true)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 084E0FC5
	/// @DnDParent : 1C3DA4A7
	/// @DnDArgument : "expr" "false"
	/// @DnDArgument : "var" "global.ui"
	global.ui = false;

	/// @DnDAction : YoYo Games.Paths.Start_Path
	/// @DnDVersion : 1.1
	/// @DnDHash : 26374188
	/// @DnDApplyTo : dce01893-52fc-4398-ad95-8c88eef129f0
	/// @DnDParent : 1C3DA4A7
	/// @DnDArgument : "path" "ui_info_close"
	/// @DnDArgument : "speed" "50"
	/// @DnDArgument : "relative" "true"
	/// @DnDSaveInfo : "path" "3c7e4a30-d0dc-4a7c-af55-33616da99290"
	with(obj_ui_info) path_start(ui_info_close, 50, path_action_stop, true);
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 3D2DCB19
else
{
	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 464A36A5
	/// @DnDParent : 3D2DCB19
	/// @DnDArgument : "code" "audio_play_sound(snd_not_enough_gold,1,0)$(13_10)global.not_enough_gold = true$(13_10)"
	audio_play_sound(snd_not_enough_gold,1,0)
	global.not_enough_gold = true

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 04278766
	/// @DnDApplyTo : bd506929-690c-4d0b-bcc7-76f64dfd6fdb
	/// @DnDParent : 3D2DCB19
	/// @DnDArgument : "steps" "600"
	with(obj_score_money) {
	alarm_set(0, 600);
	
	}
}