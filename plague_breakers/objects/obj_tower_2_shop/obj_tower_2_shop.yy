{
    "id": "073fec17-ba80-4dec-a3c8-288b74078b2d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tower_2_shop",
    "eventList": [
        {
            "id": "c426f44a-ec39-4d98-bab2-6ea49928d90d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "073fec17-ba80-4dec-a3c8-288b74078b2d"
        },
        {
            "id": "649de12b-cf70-4468-a699-e10c937e88d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "073fec17-ba80-4dec-a3c8-288b74078b2d"
        },
        {
            "id": "a821093f-3fad-429c-a394-88978d3f531b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "073fec17-ba80-4dec-a3c8-288b74078b2d"
        },
        {
            "id": "42bcd684-25c0-470d-b29e-1fa6d4e73ab3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "073fec17-ba80-4dec-a3c8-288b74078b2d"
        },
        {
            "id": "885eda9f-74d1-4c17-8979-0d8216d818a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "073fec17-ba80-4dec-a3c8-288b74078b2d"
        },
        {
            "id": "2c540ba4-9c27-4787-b903-d596e640b27b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "073fec17-ba80-4dec-a3c8-288b74078b2d"
        },
        {
            "id": "9c299182-e52d-44e0-8a6c-e62766a8399e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "073fec17-ba80-4dec-a3c8-288b74078b2d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "afd048dc-5f11-4a66-a632-1d454265b957",
    "visible": true
}