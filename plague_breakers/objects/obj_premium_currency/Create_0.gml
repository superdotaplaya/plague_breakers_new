/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 48E3C55E
/// @DnDArgument : "code" "// Creates the ini file for setting defualt bindings$(13_10)log("Loading bindings")$(13_10)global.bindings_loaded = false$(13_10)if !file_exists("Bindings.ini") then$(13_10){$(13_10)global.questsHotkey = 113;$(13_10)global.hotkeyDisplay = "q";$(13_10)global.infoHotkey = 9;$(13_10)global.repairHotkey = 114;$(13_10)global.fortifyHotkey = 102;$(13_10)global.trapsHotkey = 116;$(13_10)global.shop1Hotkey = 49;$(13_10)global.shop2Hotkey = 50;$(13_10)global.shop3Hotkey = 51;$(13_10)global.shop4Hotkey = 52;$(13_10)global.shop5Hotkey = 53;$(13_10)global.shop6Hotkey = 54;$(13_10)global.shop7Hotkey = 55;$(13_10)global.shop8Hotkey = 56;$(13_10)global.shop9Hotkey = 57;$(13_10)$(13_10)ini_open("Bindings.ini")$(13_10)ini_write_real("Bindings","Quests", global.questsHotkey)$(13_10)ini_write_real("Bindings","Info", global.infoHotkey)$(13_10)ini_write_real("Bindings","Repair", global.repairHotkey)$(13_10)ini_write_real("Bindings","Fortify", global.fortifyHotkey)$(13_10)ini_write_real("Bindings","Traps", global.trapsHotkey)$(13_10)ini_write_real("Bindings","shop1", global.shop1Hotkey)$(13_10)ini_write_real("Bindings","shop2", global.shop2Hotkey)$(13_10)ini_write_real("Bindings","shop3", global.shop3Hotkey)$(13_10)ini_write_real("Bindings","shop4", global.shop4Hotkey)$(13_10)ini_write_real("Bindings","shop5", global.shop5Hotkey)$(13_10)ini_write_real("Bindings","shop6", global.shop6Hotkey)$(13_10)ini_write_real("Bindings","shop7", global.shop7Hotkey)$(13_10)ini_write_real("Bindings","shop8", global.shop8Hotkey)$(13_10)ini_write_real("Bindings","shop9", global.shop9Hotkey)$(13_10)ini_close()$(13_10)log("[BINDINGS]  Default Bindings have been set")$(13_10)} else$(13_10)// Handles loading the ini file for setting bindings to what the player set them to$(13_10){$(13_10)ini_open("Bindings.ini")$(13_10)global.questsHotkey = ini_read_real("Bindings","Quests",113)$(13_10)global.infoHotkey = ini_read_real("Bindings","Info",9)$(13_10)global.repairHotkey = ini_read_real("Bindings","Repair",114)$(13_10)global.fortifyHotkey = ini_read_real("Bindings","Fortify",102)$(13_10)global.trapsHotkey = ini_read_real("Bindings","Traps",116)$(13_10)global.shop1Hotkey = ini_read_real("Bindings","shop1",49)$(13_10)global.shop2Hotkey = ini_read_real("Bindings","shop2",50)$(13_10)global.shop3Hotkey = ini_read_real("Bindings","shop3",51)$(13_10)global.shop4Hotkey = ini_read_real("Bindings","shop4",52)$(13_10)global.shop5Hotkey = ini_read_real("Bindings","shop5",53)$(13_10)global.shop6Hotkey = ini_read_real("Bindings","shop6",54)$(13_10)global.shop7Hotkey = ini_read_real("Bindings","shop7",55)$(13_10)global.shop8Hotkey = ini_read_real("Bindings","shop8",56)$(13_10)global.shop9Hotkey = ini_read_real("Bindings","shop9",57)$(13_10)ini_close()$(13_10)global.bindings_loaded = true$(13_10)}$(13_10)global.infoDisplay = string(key_name_convert(global.infoHotkey))$(13_10)global.repairDisplay = string(key_name_convert(global.repairHotkey))$(13_10)global.fortifyDisplay = string(key_name_convert(global.fortifyHotkey))$(13_10)global.trapsDisplay = string(key_name_convert(global.trapsHotkey))$(13_10)global.shop1Display = string(key_name_convert(global.shop1Hotkey))$(13_10)global.shop2Display = string(key_name_convert(global.shop2Hotkey))$(13_10)global.shop3Display = string(key_name_convert(global.shop3Hotkey))$(13_10)global.shop4Display = string(key_name_convert(global.shop4Hotkey))$(13_10)global.shop5Display = string(key_name_convert(global.shop5Hotkey))$(13_10)global.shop6Display = string(key_name_convert(global.shop6Hotkey))$(13_10)global.shop7Display = string(key_name_convert(global.shop7Hotkey))$(13_10)global.shop8Display = string(key_name_convert(global.shop8Hotkey))$(13_10)global.shop9Display = string(key_name_convert(global.shop9Hotkey))$(13_10)global.bindings_loaded = true$(13_10)log("[BINDINGS]  Bindings succesfully loaded")$(13_10)$(13_10)// Handles version checking and setting to update the version change both global.versionCurrent and the version number inside the line below!!!$(13_10)log("checking current version")$(13_10)if !ds_map_exists(global.map,"Version") then$(13_10){$(13_10)ds_map_add(global.map,"Version",1.0)$(13_10)}$(13_10)global.versionCurrent = 1.0$(13_10)log("Current version is: " + string(global.versionCurrent))$(13_10)"
// Creates the ini file for setting defualt bindings
log("Loading bindings")
global.bindings_loaded = false
if !file_exists("Bindings.ini") then
{
global.questsHotkey = 113;
global.hotkeyDisplay = "q";
global.infoHotkey = 9;
global.repairHotkey = 114;
global.fortifyHotkey = 102;
global.trapsHotkey = 116;
global.shop1Hotkey = 49;
global.shop2Hotkey = 50;
global.shop3Hotkey = 51;
global.shop4Hotkey = 52;
global.shop5Hotkey = 53;
global.shop6Hotkey = 54;
global.shop7Hotkey = 55;
global.shop8Hotkey = 56;
global.shop9Hotkey = 57;

ini_open("Bindings.ini")
ini_write_real("Bindings","Quests", global.questsHotkey)
ini_write_real("Bindings","Info", global.infoHotkey)
ini_write_real("Bindings","Repair", global.repairHotkey)
ini_write_real("Bindings","Fortify", global.fortifyHotkey)
ini_write_real("Bindings","Traps", global.trapsHotkey)
ini_write_real("Bindings","shop1", global.shop1Hotkey)
ini_write_real("Bindings","shop2", global.shop2Hotkey)
ini_write_real("Bindings","shop3", global.shop3Hotkey)
ini_write_real("Bindings","shop4", global.shop4Hotkey)
ini_write_real("Bindings","shop5", global.shop5Hotkey)
ini_write_real("Bindings","shop6", global.shop6Hotkey)
ini_write_real("Bindings","shop7", global.shop7Hotkey)
ini_write_real("Bindings","shop8", global.shop8Hotkey)
ini_write_real("Bindings","shop9", global.shop9Hotkey)
ini_close()
log("[BINDINGS]  Default Bindings have been set")
} else
// Handles loading the ini file for setting bindings to what the player set them to
{
ini_open("Bindings.ini")
global.questsHotkey = ini_read_real("Bindings","Quests",113)
global.infoHotkey = ini_read_real("Bindings","Info",9)
global.repairHotkey = ini_read_real("Bindings","Repair",114)
global.fortifyHotkey = ini_read_real("Bindings","Fortify",102)
global.trapsHotkey = ini_read_real("Bindings","Traps",116)
global.shop1Hotkey = ini_read_real("Bindings","shop1",49)
global.shop2Hotkey = ini_read_real("Bindings","shop2",50)
global.shop3Hotkey = ini_read_real("Bindings","shop3",51)
global.shop4Hotkey = ini_read_real("Bindings","shop4",52)
global.shop5Hotkey = ini_read_real("Bindings","shop5",53)
global.shop6Hotkey = ini_read_real("Bindings","shop6",54)
global.shop7Hotkey = ini_read_real("Bindings","shop7",55)
global.shop8Hotkey = ini_read_real("Bindings","shop8",56)
global.shop9Hotkey = ini_read_real("Bindings","shop9",57)
ini_close()
global.bindings_loaded = true
}
global.infoDisplay = string(key_name_convert(global.infoHotkey))
global.repairDisplay = string(key_name_convert(global.repairHotkey))
global.fortifyDisplay = string(key_name_convert(global.fortifyHotkey))
global.trapsDisplay = string(key_name_convert(global.trapsHotkey))
global.shop1Display = string(key_name_convert(global.shop1Hotkey))
global.shop2Display = string(key_name_convert(global.shop2Hotkey))
global.shop3Display = string(key_name_convert(global.shop3Hotkey))
global.shop4Display = string(key_name_convert(global.shop4Hotkey))
global.shop5Display = string(key_name_convert(global.shop5Hotkey))
global.shop6Display = string(key_name_convert(global.shop6Hotkey))
global.shop7Display = string(key_name_convert(global.shop7Hotkey))
global.shop8Display = string(key_name_convert(global.shop8Hotkey))
global.shop9Display = string(key_name_convert(global.shop9Hotkey))
global.bindings_loaded = true
log("[BINDINGS]  Bindings succesfully loaded")

// Handles version checking and setting to update the version change both global.versionCurrent and the version number inside the line below!!!
log("checking current version")
if !ds_map_exists(global.map,"Version") then
{
ds_map_add(global.map,"Version",1.0)
}
global.versionCurrent = 1.0
log("Current version is: " + string(global.versionCurrent))