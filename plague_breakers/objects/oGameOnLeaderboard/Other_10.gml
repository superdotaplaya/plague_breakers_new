/// @description Request Success
// Inherit the parent event
event_inherited();

// Parse URL for the next set of leaderboard entries for the current query
next = json_raw_data[? "next"];

// Parse current team data (if this is a team tournament leaderboard)
var currentTeamJSON = json_raw_data[? "currentTeam"];
if(!is_undefined(currentTeamJSON))
{
	isTeam = true;
	
	currentTeam = gameon_util_object_create(oGameOnLeaderboardItem);
	gameon_util_data_parse(currentTeam, currentTeamJSON);
}
else
{
	isTeam = false;
}


// Parse current player data
var currentPlayerJSON = json_raw_data[? "currentPlayer"];
if(!is_undefined(currentPlayerJSON))
{
	currentPlayer = gameon_util_object_create(oGameOnLeaderboardItem);
	gameon_util_data_parse(currentPlayer, currentPlayerJSON);
	currentPlayer.isCurrentPlayer = true;
}

// Parse neighbor data
var neighborJSON = json_raw_data[? "neighbors"];
if(!is_undefined(neighborJSON))
{
	neighbors = ds_list_create();
	gameon_util_data_list_parse(json_raw_data, neighbors, oGameOnLeaderboardItem, "neighbors");
}

// Parse leaderboard data
var leaderboardJSON = json_raw_data[? "leaderboard"];
if(!is_undefined(leaderboardJSON))
{
	leaderboard = ds_list_create();
	gameon_util_data_list_parse(json_raw_data, leaderboard, oGameOnLeaderboardItem, "leaderboard");
}