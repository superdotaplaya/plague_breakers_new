if(!is_undefined(neighbors) && ds_exists(neighbors, ds_type_list))
	ds_list_destroy(neighbors);
	
if(!is_undefined(leaderboard) && ds_exists(leaderboard, ds_type_list))
	ds_list_destroy(leaderboard);
	
if(gameon_util_object_exists(currentPlayer))
	gameon_util_object_destroy(currentPlayer);
	
neighbors = undefined;
leaderboard = undefined;
currentPlayer = undefined;

event_inherited();

