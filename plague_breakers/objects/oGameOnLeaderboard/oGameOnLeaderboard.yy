{
    "id": "7e3cc37a-e05e-4df8-ad3d-48bef9113ce3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameOnLeaderboard",
    "eventList": [
        {
            "id": "d3ccb565-c4bd-44d9-845f-e8366ebe89b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7e3cc37a-e05e-4df8-ad3d-48bef9113ce3"
        },
        {
            "id": "86dfa772-9b5e-4b57-ba55-c93bfabb3708",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "7e3cc37a-e05e-4df8-ad3d-48bef9113ce3"
        },
        {
            "id": "3e5b737f-b51f-4685-9fb5-24aadacdcf51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "7e3cc37a-e05e-4df8-ad3d-48bef9113ce3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0330c072-566e-4a20-bbb9-c6edf5a161cf",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}