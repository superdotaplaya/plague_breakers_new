{
    "id": "0a1f5046-1c43-4f3f-97d0-de01927695fa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameOnPromisePlayerName",
    "eventList": [
        {
            "id": "90ee5862-fbaa-43e4-83a3-0c80cede38e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0a1f5046-1c43-4f3f-97d0-de01927695fa"
        },
        {
            "id": "508d2ab1-7b33-45a0-a63d-dd031f1012c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "0a1f5046-1c43-4f3f-97d0-de01927695fa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6d6eb90d-22ce-42f0-b45c-d4b59286039c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}