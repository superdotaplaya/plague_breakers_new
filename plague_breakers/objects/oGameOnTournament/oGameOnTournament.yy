{
    "id": "5af66163-1a94-45b7-a05d-9a25e10671d4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameOnTournament",
    "eventList": [
        {
            "id": "017a2dcd-bd42-4da9-aa75-e46413dcf1e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5af66163-1a94-45b7-a05d-9a25e10671d4"
        },
        {
            "id": "bb594a80-92c9-4d34-98a4-1f073826911c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "5af66163-1a94-45b7-a05d-9a25e10671d4"
        },
        {
            "id": "dbb328cc-dcc4-4f7c-a7ec-e10900b8b419",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5af66163-1a94-45b7-a05d-9a25e10671d4"
        },
        {
            "id": "67eb102d-2f4d-46c0-b58d-c661399da6a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "5af66163-1a94-45b7-a05d-9a25e10671d4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0330c072-566e-4a20-bbb9-c6edf5a161cf",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}