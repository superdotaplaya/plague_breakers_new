event_inherited();

tournamentId =				json_raw_data[? "tournamentId"];
title =						json_raw_data[? "title"];
subtitle =					json_raw_data[? "subtitle"];
winType =					json_raw_data[? "winType"];
dateStart =					json_raw_data[? "dateStart"];
dateEnd =					json_raw_data[? "dateEnd"];
playersPerMatch =			json_raw_data[? "playersPerMatch"];
playerAttemptsPerMatch =	json_raw_data[? "playerAttemptsPerMatch"];
playersEntered =			json_raw_data[? "playersEntered"];
metadata =					json_raw_data[? "metadata"];
matchesPerPlayer =			json_raw_data[? "matchesPerPlayer"];
description =				json_raw_data[? "description"];
canEnter =					json_raw_data[? "canEnter"];
hasAccessKey =				json_raw_data[? "hasAccessKey"];
accessKey =					json_raw_data[? "accessKey"];
scoreType =					json_raw_data[? "scoreType"];
tournamentState =			json_raw_data[? "tournamentState"];
creatorPlayerName =			json_raw_data[? "creatorPlayerName"];
creatorExternalPlayerId =	json_raw_data[? "creatorExternalPlayerId"];
imageUrl =					json_raw_data[? "imageUrl"];
matchId =					json_raw_data[? "matchId"];
participantType	=			json_raw_data[? "participantType"];
teamSizeMin =				json_raw_data[? "teamSizeMin"];
teamSizeMax =				json_raw_data[? "teamSizeMax"];
gameon_tournament_enter(tournamentId)
// Parse country codes
var countryCodesJSON = json_raw_data[? "countryCodes"];
if(!is_undefined(countryCodesJSON) && ds_exists(countryCodesJSON, ds_type_list))
{
	countryCodes = ds_list_create();
	ds_list_copy(countryCodes, countryCodesJSON);
}

// Parse eligible team IDs
var eligibleTeamIdsJSON = json_raw_data[? "eligibleTeamIds"];
if(!is_undefined(eligibleTeamIdsJSON) && ds_exists(eligibleTeamIdsJSON, ds_type_list))
{
	eligibleTeamIds = ds_list_create();
	ds_list_copy(eligibleTeamIds, eligibleTeamIdsJSON);
}

// Parse prize bundles
var prizeBundlesJSON = json_raw_data[? "prizeBundles"];
if(!is_undefined(prizeBundlesJSON) && ds_exists(prizeBundlesJSON, ds_type_list))
{
	prizeBundles = ds_list_create();
	gameon_util_data_list_parse(json_raw_data, prizeBundles, oGameOnPrizeBundle, "prizeBundles");
}