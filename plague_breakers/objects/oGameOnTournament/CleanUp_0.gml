if(!is_undefined(prizeBundles) && ds_exists(prizeBundles, ds_type_list))
	ds_list_destroy(prizeBundles);
	
if(!is_undefined(countryCodes) && ds_exists(countryCodes, ds_type_list))
	ds_list_destroy(countryCodes);
	
if(!is_undefined(eligibleTeamIds) && ds_exists(eligibleTeamIds, ds_type_list))
	ds_list_destroy(eligibleTeamIds);
	
prizeBundles = undefined;
countryCodes = undefined;
eligibleTeamIds = undefined;

event_inherited();