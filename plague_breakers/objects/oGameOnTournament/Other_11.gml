// Inherit the parent event
event_inherited();

// Populate JSON data with member fields
if(!is_undefined(title))					json_raw_data[? "title"] = title;
if(!is_undefined(subtitle))					json_raw_data[? "subtitle"] = subtitle;
if(!is_undefined(metadata))					json_raw_data[? "metadata"] = metadata;
if(!is_undefined(description))				json_raw_data[? "description"] = description;
if(!is_undefined(hasAccessKey))				json_raw_data[? "generateAccessKey"] = bool(hasAccessKey);
if(!is_undefined(dateStart))				json_raw_data[? "dateStart"] = int64(dateStart);
if(!is_undefined(dateEnd))					json_raw_data[? "dateEnd"] = int64(dateEnd);
if(!is_undefined(playersPerMatch))			json_raw_data[? "playersPerMatch"] = int64(playersPerMatch);
if(!is_undefined(playerAttemptsPerMatch))	json_raw_data[? "playerAttemptsPerMatch"] = int64(playerAttemptsPerMatch);