/// @DnDAction : YoYo Games.Drawing.Draw_Self
/// @DnDVersion : 1
/// @DnDHash : 728355C9
draw_self();

/// @DnDAction : YoYo Games.Drawing.Set_Color
/// @DnDVersion : 1
/// @DnDHash : 41D6C52A
draw_set_colour($FFFFFFFF & $ffffff);
var l41D6C52A_0=($FFFFFFFF >> 24);
draw_set_alpha(l41D6C52A_0 / $ff);

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 0E8C4780
/// @DnDArgument : "x" "-75"
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "115"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "caption" ""Cost: ""
/// @DnDArgument : "var" "cost"
draw_text(x + -75, y + 115, string("Cost: ") + string(cost));

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 04B24F30
/// @DnDArgument : "var" "hover"
/// @DnDArgument : "value" "true"
if(hover == true)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 1776B13F
	/// @DnDParent : 04B24F30
	/// @DnDArgument : "alpha" ".5"
	draw_set_alpha(.5);

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 3A15B461
	/// @DnDParent : 04B24F30
	/// @DnDArgument : "var" "global.highground"
	/// @DnDArgument : "value" "false"
	if(global.highground == false)
	{
		/// @DnDAction : YoYo Games.Drawing.Draw_Gradient_Ellipse
		/// @DnDVersion : 1
		/// @DnDHash : 39F350C4
		/// @DnDParent : 3A15B461
		/// @DnDArgument : "x1" "global.x - 400"
		/// @DnDArgument : "y1" "global.y - 400"
		/// @DnDArgument : "x2" "global.x + 400"
		/// @DnDArgument : "y2" "global.y + 400"
		/// @DnDArgument : "col1" "$FFFFFFFF"
		/// @DnDArgument : "col2" "$FFB3B3B3"
		/// @DnDArgument : "fill" "1"
		draw_ellipse_colour(global.x - 400, global.y - 400, global.x + 400, global.y + 400, $FFFFFFFF & $FFFFFF, $FFB3B3B3 & $FFFFFF, 0);
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 58657D43
	/// @DnDParent : 04B24F30
	else
	{
		/// @DnDAction : YoYo Games.Drawing.Draw_Gradient_Ellipse
		/// @DnDVersion : 1
		/// @DnDHash : 27C4DB70
		/// @DnDParent : 58657D43
		/// @DnDArgument : "x1" "global.x - 700"
		/// @DnDArgument : "y1" "global.y - 700"
		/// @DnDArgument : "x2" "global.x + 700"
		/// @DnDArgument : "y2" "global.y + 700"
		/// @DnDArgument : "col1" "$FFFFFFFF"
		/// @DnDArgument : "col2" "$FFB3B3B3"
		/// @DnDArgument : "fill" "1"
		draw_ellipse_colour(global.x - 700, global.y - 700, global.x + 700, global.y + 700, $FFFFFFFF & $FFFFFF, $FFB3B3B3 & $FFFFFF, 0);
	}

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 7EA55AA0
	/// @DnDParent : 04B24F30
	/// @DnDArgument : "expr" ""Shoots very slowly, but at great range and for a lot of damage""
	/// @DnDArgument : "var" "global.tooltip"
	global.tooltip = "Shoots very slowly, but at great range and for a lot of damage";
}