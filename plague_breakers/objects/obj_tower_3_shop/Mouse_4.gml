/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 55D6F4FD
/// @DnDArgument : "var" "global.gold"
/// @DnDArgument : "op" "4"
/// @DnDArgument : "value" "cost"
if(global.gold >= cost)
{
	/// @DnDAction : YoYo Games.Common.Set_Global
	/// @DnDVersion : 1
	/// @DnDHash : 1C088A1B
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "value" "-cost"
	/// @DnDArgument : "value_relative" "1"
	/// @DnDArgument : "var" "gold"
	global.gold += -cost;

	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 0180A620
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "xpos" "global.x"
	/// @DnDArgument : "ypos" "global.y"
	/// @DnDArgument : "objectid" "obj_blue_tower"
	/// @DnDArgument : "layer" ""Instances_towers""
	/// @DnDSaveInfo : "objectid" "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
	instance_create_layer(global.x, global.y, "Instances_towers", obj_blue_tower);

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 5C298EC6
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "var" "global.highground"
	/// @DnDArgument : "value" "true"
	if(global.highground == true)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 6A59EF74
		/// @DnDInput : 2
		/// @DnDApplyTo : instance_nearest(global.x,global.y,obj_blue_tower)
		/// @DnDParent : 5C298EC6
		/// @DnDArgument : "expr" "range + 300"
		/// @DnDArgument : "expr_1" "true"
		/// @DnDArgument : "var" "range"
		/// @DnDArgument : "var_1" "highground"
		with(instance_nearest(global.x,global.y,obj_blue_tower)) {
		range = range + 300;
		highground = true;
		
		}
	}

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 05DE1BDC
	/// @DnDApplyTo : 073fec17-ba80-4dec-a3c8-288b74078b2d
	/// @DnDParent : 55D6F4FD
	with(obj_tower_2_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 25EEA9B5
	/// @DnDApplyTo : 8aea4293-de50-4063-9f50-d75be1378031
	/// @DnDParent : 55D6F4FD
	with(obj_tower_1_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 221553EC
	/// @DnDApplyTo : b69a5d5d-926a-4cc5-b306-6b0be9c55b19
	/// @DnDParent : 55D6F4FD
	with(obj_cannon_tower_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 76564644
	/// @DnDApplyTo : 90a0bd5f-c90d-4cee-8bb7-72c2fd9704c9
	/// @DnDParent : 55D6F4FD
	with(obj_tesla_tower_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 06092FA4
	/// @DnDApplyTo : 43c1b6e1-4e5c-402d-9b7f-54483b5eb297
	/// @DnDParent : 55D6F4FD
	with(obj_tower_4_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 401B7A00
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "steps" "20"
	alarm_set(0, 20);

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 3BA28E29
	/// @DnDApplyTo : 16d11c02-1a7d-4f1e-8cf3-b39df52c2b8b
	/// @DnDParent : 55D6F4FD
	with(obj_tower_5_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 12E547D6
	/// @DnDApplyTo : 32183b7a-d784-471e-939b-c846cb1d5088
	/// @DnDParent : 55D6F4FD
	with(obj_tower_poison_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Paths.Start_Path
	/// @DnDVersion : 1.1
	/// @DnDHash : 3B423B84
	/// @DnDApplyTo : 37f57748-c862-493a-bbbd-c8ffdf8f8d60
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "path" "shop_close"
	/// @DnDArgument : "speed" "20"
	/// @DnDArgument : "relative" "true"
	/// @DnDSaveInfo : "path" "82e069ee-3c69-4d32-95f0-56a32a932d1f"
	with(obj_shop_menu) path_start(shop_close, 20, path_action_stop, true);

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 6C6CCD24
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "expr" "false"
	/// @DnDArgument : "var" "global.shop_open"
	global.shop_open = false;

	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 36BE3E2E
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "code" "instance_activate_layer("Instances")$(13_10)instance_destroy(obj_tooltips)$(13_10)audio_play_sound(snd_buy,1,0)"
	instance_activate_layer("Instances")
	instance_destroy(obj_tooltips)
	audio_play_sound(snd_buy,1,0)

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 2DB616B4
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "var" "global.ui"
	/// @DnDArgument : "value" "true"
	if(global.ui == true)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 35536F9F
		/// @DnDParent : 2DB616B4
		/// @DnDArgument : "expr" "false"
		/// @DnDArgument : "var" "global.ui"
		global.ui = false;
	
		/// @DnDAction : YoYo Games.Paths.Start_Path
		/// @DnDVersion : 1.1
		/// @DnDHash : 6E9A5261
		/// @DnDApplyTo : dce01893-52fc-4398-ad95-8c88eef129f0
		/// @DnDParent : 2DB616B4
		/// @DnDArgument : "path" "ui_info_close"
		/// @DnDArgument : "speed" "50"
		/// @DnDArgument : "relative" "true"
		/// @DnDSaveInfo : "path" "3c7e4a30-d0dc-4a7c-af55-33616da99290"
		with(obj_ui_info) path_start(ui_info_close, 50, path_action_stop, true);
	}
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 02404407
else
{
	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 7B274B98
	/// @DnDParent : 02404407
	/// @DnDArgument : "code" "audio_play_sound(snd_not_enough_gold,1,0)$(13_10)global.not_enough_gold = true$(13_10)"
	audio_play_sound(snd_not_enough_gold,1,0)
	global.not_enough_gold = true

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 32174F8D
	/// @DnDApplyTo : bd506929-690c-4d0b-bcc7-76f64dfd6fdb
	/// @DnDParent : 02404407
	/// @DnDArgument : "steps" "600"
	with(obj_score_money) {
	alarm_set(0, 600);
	
	}
}