{
    "id": "a24a6cde-9798-4b86-b3f8-d1531ebdd16a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hp_splash",
    "eventList": [
        {
            "id": "008b8f89-ecef-4253-b7c8-b4d955e473e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a24a6cde-9798-4b86-b3f8-d1531ebdd16a"
        },
        {
            "id": "2ced11ba-04e2-404a-8ea2-ce09687a27b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a24a6cde-9798-4b86-b3f8-d1531ebdd16a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8ede8fa8-0b2e-4578-bcca-df67edf75c6b",
    "visible": true
}