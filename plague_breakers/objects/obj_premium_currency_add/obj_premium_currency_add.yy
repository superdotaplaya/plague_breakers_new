{
    "id": "323be0b2-9099-487d-b037-a6017e32cc4f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_premium_currency_add",
    "eventList": [
        {
            "id": "4d6220a3-ba51-4a1a-abb4-22e41779e77a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "323be0b2-9099-487d-b037-a6017e32cc4f"
        },
        {
            "id": "5eb563dc-68e5-4da0-891c-40736d115ab6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "323be0b2-9099-487d-b037-a6017e32cc4f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c9445489-393e-46b3-a8f8-f2e643423d54",
    "visible": true
}