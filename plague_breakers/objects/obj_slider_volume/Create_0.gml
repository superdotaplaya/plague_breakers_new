/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 2E33937E
/// @DnDArgument : "code" "volume1 = audio_sound_get_gain(snd_song_001)$(13_10)volume2 = audio_sound_get_gain(snd_song_001_LOOP)$(13_10)vol_start = audio_sound_get_gain(snd_song_001)$(13_10)image_xscale = 2$(13_10)image_yscale = 2$(13_10)if !file_exists("music_volume.ini") then$(13_10){$(13_10)ini_open("music_volume.ini")$(13_10)start = x$(13_10)ini_write_real("Music Volume","Volume",x)$(13_10)ini_write_real("Music Volume","Volume_max",x)$(13_10)}$(13_10)else$(13_10){$(13_10)ini_open("music_volume.ini")$(13_10)start = ini_read_real("Music Volume","Volume_m",x)$(13_10)x = ini_read_real("Music Volume","Volume",400)	$(13_10)}$(13_10)$(13_10)maxvol = 400$(13_10)minvol = 0$(13_10)held = false"
volume1 = audio_sound_get_gain(snd_song_001)
volume2 = audio_sound_get_gain(snd_song_001_LOOP)
vol_start = audio_sound_get_gain(snd_song_001)
image_xscale = 2
image_yscale = 2
if !file_exists("music_volume.ini") then
{
ini_open("music_volume.ini")
start = x
ini_write_real("Music Volume","Volume",x)
ini_write_real("Music Volume","Volume_max",x)
}
else
{
ini_open("music_volume.ini")
start = ini_read_real("Music Volume","Volume_m",x)
x = ini_read_real("Music Volume","Volume",400)	
}

maxvol = 400
minvol = 0
held = false