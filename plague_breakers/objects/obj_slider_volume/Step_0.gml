/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 6CDCEA6D
/// @DnDArgument : "code" "//checks to see if the user is actively clicked on the slider bar$(13_10)if held = true and room = option then$(13_10){$(13_10)x = mouse_x$(13_10)y = y$(13_10)}$(13_10)$(13_10)$(13_10)// Ensures the slider wont leave the boundary for the slider$(13_10)if x >= start then$(13_10){$(13_10)x = start$(13_10)}$(13_10)if x <= start - 400 then$(13_10){$(13_10)x = start - 400$(13_10)}$(13_10)$(13_10)vol_change = (start - x)$(13_10)$(13_10)"
//checks to see if the user is actively clicked on the slider bar
if held = true and room = option then
{
x = mouse_x
y = y
}


// Ensures the slider wont leave the boundary for the slider
if x >= start then
{
x = start
}
if x <= start - 400 then
{
x = start - 400
}

vol_change = (start - x)

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 1012D769
/// @DnDArgument : "sound" "snd_song_001"
/// @DnDArgument : "volume" "vol_start - (vol_change/400)"
/// @DnDSaveInfo : "sound" "74394b18-9603-4db2-99d6-82c499677b8a"
audio_sound_gain(snd_song_001, vol_start - (vol_change/400), 0);

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 34D816B5
/// @DnDArgument : "sound" "snd_song_001_LOOP"
/// @DnDArgument : "volume" "vol_start - (vol_change/400)"
/// @DnDSaveInfo : "sound" "d6477212-dc94-449c-998f-fe175b2dfce2"
audio_sound_gain(snd_song_001_LOOP, vol_start - (vol_change/400), 0);