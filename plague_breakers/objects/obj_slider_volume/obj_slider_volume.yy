{
    "id": "9abea9e2-a6c2-4ec7-92f6-cae830fe307f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_slider_volume",
    "eventList": [
        {
            "id": "fed13a2b-a5d6-47e8-a0a5-3e50d5db559a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9abea9e2-a6c2-4ec7-92f6-cae830fe307f"
        },
        {
            "id": "078a387e-bb18-4749-bfff-69297a97c340",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9abea9e2-a6c2-4ec7-92f6-cae830fe307f"
        },
        {
            "id": "1bbd63c0-6851-4bcd-a405-2c1a93ca5ae2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "9abea9e2-a6c2-4ec7-92f6-cae830fe307f"
        },
        {
            "id": "c6e42aaf-aaa0-471a-ae51-9dae5612cad3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "9abea9e2-a6c2-4ec7-92f6-cae830fe307f"
        },
        {
            "id": "6d5e3db8-804b-401d-ba6d-71f03a521ed9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "9abea9e2-a6c2-4ec7-92f6-cae830fe307f"
        },
        {
            "id": "bb9eff8d-83c6-44da-bad9-2f122a05d5af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9abea9e2-a6c2-4ec7-92f6-cae830fe307f"
        },
        {
            "id": "c0953ab7-fd88-40db-8622-12c006a1b33e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "9abea9e2-a6c2-4ec7-92f6-cae830fe307f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2bb70071-0834-45b3-9d76-856304ab5d8f",
    "visible": true
}