{
    "id": "ea242d1e-e9d2-4188-b206-ddad57c8abfd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "button_rebind_traps",
    "eventList": [
        {
            "id": "b3829b33-f48b-47ed-831e-622671f5a4a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ea242d1e-e9d2-4188-b206-ddad57c8abfd"
        },
        {
            "id": "9abda616-0e11-4697-990b-fa9e464bf6ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ea242d1e-e9d2-4188-b206-ddad57c8abfd"
        },
        {
            "id": "0577a23e-e6c1-412d-bd6c-7b5337d002bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ea242d1e-e9d2-4188-b206-ddad57c8abfd"
        },
        {
            "id": "0e519947-bdbb-4207-ae6c-ecfa5915b795",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "ea242d1e-e9d2-4188-b206-ddad57c8abfd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a832f2b-2273-4cc3-b553-8c06b7dc617f",
    "visible": true
}