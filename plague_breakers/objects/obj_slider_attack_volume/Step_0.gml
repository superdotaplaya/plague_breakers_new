/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 6CDCEA6D
/// @DnDArgument : "code" "//checks to see if the user is actively clicked on the slider bar$(13_10)if held = true and room = option then$(13_10){$(13_10)x = mouse_x$(13_10)y = y$(13_10)}$(13_10)$(13_10)$(13_10)// Ensures the slider wont leave the boundary for the slider$(13_10)if x >= start then$(13_10){$(13_10)x = start$(13_10)}$(13_10)if x <= start - 400 then$(13_10){$(13_10)x = start - 400$(13_10)}$(13_10)$(13_10)vol_change = (start - x)$(13_10)round(vol_change)$(13_10)"
//checks to see if the user is actively clicked on the slider bar
if held = true and room = option then
{
x = mouse_x
y = y
}


// Ensures the slider wont leave the boundary for the slider
if x >= start then
{
x = start
}
if x <= start - 400 then
{
x = start - 400
}

vol_change = (start - x)
round(vol_change)

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 1012D769
/// @DnDArgument : "sound" "snd_tower_attack_basic"
/// @DnDArgument : "volume" "vol_start - (vol_change/400)"
/// @DnDSaveInfo : "sound" "d9f3a062-2f70-43fb-b6a7-6120866779a6"
audio_sound_gain(snd_tower_attack_basic, vol_start - (vol_change/400), 0);

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 34D816B5
/// @DnDArgument : "sound" "snd_shotgun_tower"
/// @DnDArgument : "volume" "vol_start - (vol_change/400)"
/// @DnDSaveInfo : "sound" "88e64b07-354d-42f6-9427-40e9696ef2ba"
audio_sound_gain(snd_shotgun_tower, vol_start - (vol_change/400), 0);

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 41C5CE4C
/// @DnDArgument : "sound" "snd_sniper_shot"
/// @DnDArgument : "volume" "vol_start - (vol_change/400)"
/// @DnDSaveInfo : "sound" "d9d78670-3d5e-4f49-aa46-028c2c09959d"
audio_sound_gain(snd_sniper_shot, vol_start - (vol_change/400), 0);

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 558EDA28
/// @DnDArgument : "sound" "snd_aoe_tower_attack"
/// @DnDArgument : "volume" "vol_start - (vol_change/400)"
/// @DnDSaveInfo : "sound" "3e3220e7-8ea8-40eb-8cd2-f520025b1458"
audio_sound_gain(snd_aoe_tower_attack, vol_start - (vol_change/400), 0);

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 798AB698
/// @DnDArgument : "sound" "snd_machine_1"
/// @DnDArgument : "volume" "vol_start - (vol_change/400)"
/// @DnDSaveInfo : "sound" "bc09b28b-81bd-4e87-bd8f-952b2253944b"
audio_sound_gain(snd_machine_1, vol_start - (vol_change/400), 0);

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 485819F1
/// @DnDArgument : "sound" "snd_machine_2"
/// @DnDArgument : "volume" "vol_start - (vol_change/400)"
/// @DnDSaveInfo : "sound" "3b4c51e1-82ab-412e-a72d-3fd5d7b3a529"
audio_sound_gain(snd_machine_2, vol_start - (vol_change/400), 0);

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 53783973
/// @DnDArgument : "sound" "snd_machine_3"
/// @DnDArgument : "volume" "vol_start - (vol_change/400)"
/// @DnDSaveInfo : "sound" "bacbcb01-3f45-4ac2-a706-f8721eadaab0"
audio_sound_gain(snd_machine_3, vol_start - (vol_change/400), 0);

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 0B458AD9
/// @DnDArgument : "sound" "snd_machine_4"
/// @DnDArgument : "volume" "vol_start - (vol_change/400)"
/// @DnDSaveInfo : "sound" "a7262493-8018-4e50-add3-b495fcb6e261"
audio_sound_gain(snd_machine_4, vol_start - (vol_change/400), 0);

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 7C88E5CF
/// @DnDArgument : "sound" "snd_laser_attack"
/// @DnDArgument : "volume" "vol_start - (vol_change/400)"
/// @DnDSaveInfo : "sound" "fc3e1382-b645-477c-b8fc-09531ff7abd3"
audio_sound_gain(snd_laser_attack, vol_start - (vol_change/400), 0);

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 0CB8C52A
/// @DnDArgument : "sound" "snd_enemy_base_hit"
/// @DnDArgument : "volume" "vol_start - (vol_change/400)"
/// @DnDSaveInfo : "sound" "db10fee6-14f4-453b-ab3d-e1641571b920"
audio_sound_gain(snd_enemy_base_hit, vol_start - (vol_change/400), 0);

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 6D0FDDEC
/// @DnDArgument : "sound" "snd_coin_pickup"
/// @DnDArgument : "volume" "vol_start - (vol_change/400)"
/// @DnDSaveInfo : "sound" "996752fe-f0d2-467a-870f-106c9551c5b0"
audio_sound_gain(snd_coin_pickup, vol_start - (vol_change/400), 0);

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 6DF55B04
/// @DnDArgument : "sound" "snd_round_bonus"
/// @DnDArgument : "volume" "vol_start - (vol_change/400)"
/// @DnDSaveInfo : "sound" "cebcd0c9-0c1b-4624-a071-84e6158c5b2c"
audio_sound_gain(snd_round_bonus, vol_start - (vol_change/400), 0);

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 6708A847
/// @DnDArgument : "sound" "snd_mud_splat"
/// @DnDArgument : "volume" "vol_start - (vol_change/400)"
/// @DnDSaveInfo : "sound" "4294db0c-3019-4168-856e-6fab726b01f5"
audio_sound_gain(snd_mud_splat, vol_start - (vol_change/400), 0);

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 5B34D0B8
/// @DnDArgument : "sound" "snd_brick_wall"
/// @DnDArgument : "volume" "vol_start - (vol_change/400)"
/// @DnDSaveInfo : "sound" "7744ee90-419a-43ec-bf75-a172014a4314"
audio_sound_gain(snd_brick_wall, vol_start - (vol_change/400), 0);

/// @DnDAction : YoYo Games.Audio.Audio_Set_Volume
/// @DnDVersion : 1
/// @DnDHash : 0B4F21F8
/// @DnDArgument : "sound" "snd_buy"
/// @DnDArgument : "volume" "vol_start - (vol_change/400)"
/// @DnDSaveInfo : "sound" "54e32311-d6f8-4576-bcf0-0a3d933b9e33"
audio_sound_gain(snd_buy, vol_start - (vol_change/400), 0);