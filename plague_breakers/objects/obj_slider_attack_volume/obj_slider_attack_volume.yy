{
    "id": "22169555-9fce-4ce5-9600-2ad8ddf12d1b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_slider_attack_volume",
    "eventList": [
        {
            "id": "6da1b847-4fd6-443b-9c1e-673b9448b147",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "22169555-9fce-4ce5-9600-2ad8ddf12d1b"
        },
        {
            "id": "cbfed389-990a-45f9-88f3-742021867c2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "22169555-9fce-4ce5-9600-2ad8ddf12d1b"
        },
        {
            "id": "657bc0ae-cf69-40ea-80ad-9a01e9d05a97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "22169555-9fce-4ce5-9600-2ad8ddf12d1b"
        },
        {
            "id": "d07991ee-70f4-45ef-82ee-47b682c45db0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "22169555-9fce-4ce5-9600-2ad8ddf12d1b"
        },
        {
            "id": "5ca33f88-4cf9-4a8d-8d39-4c3589ed0c8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "22169555-9fce-4ce5-9600-2ad8ddf12d1b"
        },
        {
            "id": "d50dff9a-3550-4f10-9849-b74f65d819c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "22169555-9fce-4ce5-9600-2ad8ddf12d1b"
        },
        {
            "id": "eed5a6ae-41ef-4b37-b8cd-cc503bb298be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "22169555-9fce-4ce5-9600-2ad8ddf12d1b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2bb70071-0834-45b3-9d76-856304ab5d8f",
    "visible": true
}