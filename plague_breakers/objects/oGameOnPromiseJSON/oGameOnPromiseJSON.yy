{
    "id": "f531af47-242a-423e-8456-6acd2f90a11a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameOnPromiseJSON",
    "eventList": [
        {
            "id": "da09613d-96da-4bfe-9a09-e09a99803fb4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "f531af47-242a-423e-8456-6acd2f90a11a"
        },
        {
            "id": "fdccc300-fe9b-4363-ab57-3ffecbfccdc9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 15,
            "eventtype": 7,
            "m_owner": "f531af47-242a-423e-8456-6acd2f90a11a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6d6eb90d-22ce-42f0-b45c-d4b59286039c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": false,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}