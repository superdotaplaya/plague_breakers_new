/// @description HTTP Request Success
request_result_data_parsed = json_decode(request_result_data);
if(request_result_data_parsed == -1)
	gameon_util_log("Failed to parse request result data into JSON.", true);

event_inherited();