{
    "id": "7437a726-b58e-47cb-a206-3eddff28a26a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wall_basic",
    "eventList": [
        {
            "id": "19ff2715-e659-4147-9c8c-835f10e1530c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7437a726-b58e-47cb-a206-3eddff28a26a"
        },
        {
            "id": "de26aaba-225a-4269-8250-b852e2f71fc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7437a726-b58e-47cb-a206-3eddff28a26a"
        },
        {
            "id": "cad293f8-7a5d-4eb7-833f-9fd320aaa230",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "7437a726-b58e-47cb-a206-3eddff28a26a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "8790704c-191e-4222-95ed-89cd5354d33d",
    "visible": true
}