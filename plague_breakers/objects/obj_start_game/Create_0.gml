/// @DnDAction : YoYo Games.Instances.Sprite_Scale
/// @DnDVersion : 1
/// @DnDHash : 01418B1E
/// @DnDArgument : "xscale" ".5"
/// @DnDArgument : "yscale" ".5"
image_xscale = .5;
image_yscale = .5;

/// @DnDAction : YoYo Games.Audio.Audio_Get_Length
/// @DnDVersion : 1
/// @DnDHash : 1E7E0282
/// @DnDArgument : "var" "song_length"
/// @DnDArgument : "sound" "snd_song_001"
/// @DnDSaveInfo : "sound" "74394b18-9603-4db2-99d6-82c499677b8a"
song_length = audio_sound_length(snd_song_001);

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 796CC5DC
/// @DnDArgument : "code" ""