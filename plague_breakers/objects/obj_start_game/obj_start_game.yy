{
    "id": "8f8d0f15-a1e5-4d73-9471-487dfcd7fc56",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_start_game",
    "eventList": [
        {
            "id": "716e394a-5a3d-494c-a1ff-deab3f0a8a97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "8f8d0f15-a1e5-4d73-9471-487dfcd7fc56"
        },
        {
            "id": "6868922a-9174-44ae-af03-12d5c9bf736d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "8f8d0f15-a1e5-4d73-9471-487dfcd7fc56"
        },
        {
            "id": "b2e0ac34-53c0-4214-883b-b9b799713e8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8f8d0f15-a1e5-4d73-9471-487dfcd7fc56"
        },
        {
            "id": "26a78397-9019-4cb4-8b97-c3204795ac84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "8f8d0f15-a1e5-4d73-9471-487dfcd7fc56"
        },
        {
            "id": "1c4c6470-c623-4096-b42c-b845bfb37f43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8f8d0f15-a1e5-4d73-9471-487dfcd7fc56"
        },
        {
            "id": "6f306ae8-87c6-42b3-bbba-aef79cb6dfb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8f8d0f15-a1e5-4d73-9471-487dfcd7fc56"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "90850e67-fd74-4467-8e48-35a4afdcfdeb",
    "visible": true
}