/// @description Read JSON
event_inherited();

matchId =				json_raw_data[? "matchId"];
encryptedPrizeInfo =	json_raw_data[? "encryptedPrizeInfo"];
encryptedPrizeInfoV2 =	json_raw_data[? "encryptedPrizeInfoV2"];
prizeInfo =				json_raw_data[? "prizeInfo"];
prizeInfoType =			json_raw_data[? "prizeInfoType"];
status =				json_raw_data[? "status"];
dateOfExpiration =		json_raw_data[? "dateOfExpiration"];
description =			json_raw_data[? "description"];
imageUrl =				json_raw_data[? "imageUrl"];
title =					json_raw_data[? "title"];
