event_inherited();

matchId =				undefined;
encryptedPrizeInfo =	undefined;
encryptedPrizeInfoV2 =	undefined;
prizeInfo =				undefined;
prizeInfoType =			undefined;
status =				undefined;
dateOfExpiration =		undefined;
description =			undefined;
imageUrl =				undefined;
title =					undefined;