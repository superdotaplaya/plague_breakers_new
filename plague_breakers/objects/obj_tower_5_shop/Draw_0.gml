/// @DnDAction : YoYo Games.Drawing.Draw_Self
/// @DnDVersion : 1
/// @DnDHash : 728355C9
draw_self();

/// @DnDAction : YoYo Games.Drawing.Set_Color
/// @DnDVersion : 1
/// @DnDHash : 56DAE4C1
draw_set_colour($FFFFFFFF & $ffffff);
var l56DAE4C1_0=($FFFFFFFF >> 24);
draw_set_alpha(l56DAE4C1_0 / $ff);

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 421F36AC
/// @DnDArgument : "x" "-75"
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "115"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "caption" ""Cost: ""
/// @DnDArgument : "var" "cost"
draw_text(x + -75, y + 115, string("Cost: ") + string(cost));

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 04B24F30
/// @DnDArgument : "var" "hover"
/// @DnDArgument : "value" "true"
if(hover == true)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 1776B13F
	/// @DnDParent : 04B24F30
	/// @DnDArgument : "alpha" ".5"
	draw_set_alpha(.5);

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 4CECC07D
	/// @DnDParent : 04B24F30
	/// @DnDArgument : "var" "global.highground"
	/// @DnDArgument : "value" "false"
	if(global.highground == false)
	{
		/// @DnDAction : YoYo Games.Drawing.Draw_Gradient_Ellipse
		/// @DnDVersion : 1
		/// @DnDHash : 6FD972D4
		/// @DnDParent : 4CECC07D
		/// @DnDArgument : "x1" "global.x -110"
		/// @DnDArgument : "y1" "global.y -110"
		/// @DnDArgument : "x2" "global.x + 110"
		/// @DnDArgument : "y2" "global.y + 110"
		/// @DnDArgument : "col1" "$FFFFFFFF"
		/// @DnDArgument : "col2" "$FFB3B3B3"
		/// @DnDArgument : "fill" "1"
		draw_ellipse_colour(global.x -110, global.y -110, global.x + 110, global.y + 110, $FFFFFFFF & $FFFFFF, $FFB3B3B3 & $FFFFFF, 0);
	}

	/// @DnDAction : YoYo Games.Common.Else
	/// @DnDVersion : 1
	/// @DnDHash : 53EFB684
	/// @DnDParent : 04B24F30
	else
	{
		/// @DnDAction : YoYo Games.Drawing.Draw_Gradient_Ellipse
		/// @DnDVersion : 1
		/// @DnDHash : 6E26B45C
		/// @DnDParent : 53EFB684
		/// @DnDArgument : "x1" "global.x -310"
		/// @DnDArgument : "y1" "global.y -310"
		/// @DnDArgument : "x2" "global.x + 310"
		/// @DnDArgument : "y2" "global.y + 310"
		/// @DnDArgument : "col1" "$FFFFFFFF"
		/// @DnDArgument : "col2" "$FFB3B3B3"
		/// @DnDArgument : "fill" "1"
		draw_ellipse_colour(global.x -310, global.y -310, global.x + 310, global.y + 310, $FFFFFFFF & $FFFFFF, $FFB3B3B3 & $FFFFFF, 0);
	}

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 1CC7B6EE
	/// @DnDParent : 04B24F30
	/// @DnDArgument : "expr" ""shoots very fast, but deals little damage""
	/// @DnDArgument : "var" "global.tooltip"
	global.tooltip = "shoots very fast, but deals little damage";
}