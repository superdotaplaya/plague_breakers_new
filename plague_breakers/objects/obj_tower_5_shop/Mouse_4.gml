/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4B1286B8
/// @DnDArgument : "var" "global.gold"
/// @DnDArgument : "op" "4"
/// @DnDArgument : "value" "cost"
if(global.gold >= cost)
{
	/// @DnDAction : YoYo Games.Common.Set_Global
	/// @DnDVersion : 1
	/// @DnDHash : 5673953E
	/// @DnDParent : 4B1286B8
	/// @DnDArgument : "value" "-cost"
	/// @DnDArgument : "value_relative" "1"
	/// @DnDArgument : "var" "gold"
	global.gold += -cost;

	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 7B76976E
	/// @DnDParent : 4B1286B8
	/// @DnDArgument : "xpos" "global.x"
	/// @DnDArgument : "ypos" "global.y"
	/// @DnDArgument : "objectid" "obj_yellow_tower"
	/// @DnDArgument : "layer" ""Instances_towers""
	/// @DnDSaveInfo : "objectid" "12a615a1-fa0f-4167-95d1-7acdee951786"
	instance_create_layer(global.x, global.y, "Instances_towers", obj_yellow_tower);

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 1C6993C2
	/// @DnDParent : 4B1286B8
	/// @DnDArgument : "var" "global.highground"
	/// @DnDArgument : "value" "true"
	if(global.highground == true)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 4CC62A7E
		/// @DnDInput : 2
		/// @DnDApplyTo : instance_nearest(global.x,global.y,obj_yellow_tower)
		/// @DnDParent : 1C6993C2
		/// @DnDArgument : "expr" "range + 200"
		/// @DnDArgument : "expr_1" "true"
		/// @DnDArgument : "var" "range"
		/// @DnDArgument : "var_1" "highground"
		with(instance_nearest(global.x,global.y,obj_yellow_tower)) {
		range = range + 200;
		highground = true;
		
		}
	}

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 32B9357A
	/// @DnDApplyTo : 073fec17-ba80-4dec-a3c8-288b74078b2d
	/// @DnDParent : 4B1286B8
	with(obj_tower_2_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 493E9526
	/// @DnDApplyTo : 8aea4293-de50-4063-9f50-d75be1378031
	/// @DnDParent : 4B1286B8
	with(obj_tower_1_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 045EA590
	/// @DnDApplyTo : 90a0bd5f-c90d-4cee-8bb7-72c2fd9704c9
	/// @DnDParent : 4B1286B8
	with(obj_tesla_tower_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 717E10E3
	/// @DnDApplyTo : b69a5d5d-926a-4cc5-b306-6b0be9c55b19
	/// @DnDParent : 4B1286B8
	with(obj_cannon_tower_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 7A5CE46C
	/// @DnDApplyTo : ff372800-7810-49bb-898f-a7defb5cc613
	/// @DnDParent : 4B1286B8
	with(obj_tower_3_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 3018B23F
	/// @DnDParent : 4B1286B8
	draw_set_alpha(1);

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 62A27713
	/// @DnDApplyTo : 43c1b6e1-4e5c-402d-9b7f-54483b5eb297
	/// @DnDParent : 4B1286B8
	with(obj_tower_4_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 211E6D56
	/// @DnDApplyTo : 32183b7a-d784-471e-939b-c846cb1d5088
	/// @DnDParent : 4B1286B8
	with(obj_tower_poison_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 585DC6BF
	/// @DnDParent : 4B1286B8
	instance_destroy();

	/// @DnDAction : YoYo Games.Paths.Start_Path
	/// @DnDVersion : 1.1
	/// @DnDHash : 55ED44DD
	/// @DnDApplyTo : 37f57748-c862-493a-bbbd-c8ffdf8f8d60
	/// @DnDParent : 4B1286B8
	/// @DnDArgument : "path" "shop_close"
	/// @DnDArgument : "speed" "20"
	/// @DnDArgument : "relative" "true"
	/// @DnDSaveInfo : "path" "82e069ee-3c69-4d32-95f0-56a32a932d1f"
	with(obj_shop_menu) path_start(shop_close, 20, path_action_stop, true);

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 04C54D9F
	/// @DnDParent : 4B1286B8
	/// @DnDArgument : "expr" "false"
	/// @DnDArgument : "var" "global.shop_open"
	global.shop_open = false;

	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 29B777D7
	/// @DnDParent : 4B1286B8
	/// @DnDArgument : "code" "instance_activate_layer("Instances")$(13_10)instance_destroy(obj_tooltips)$(13_10)audio_play_sound(snd_buy,1,0)"
	instance_activate_layer("Instances")
	instance_destroy(obj_tooltips)
	audio_play_sound(snd_buy,1,0)

	/// @DnDAction : YoYo Games.Common.If_Variable
	/// @DnDVersion : 1
	/// @DnDHash : 689C970D
	/// @DnDParent : 4B1286B8
	/// @DnDArgument : "var" "global.ui"
	/// @DnDArgument : "value" "true"
	if(global.ui == true)
	{
		/// @DnDAction : YoYo Games.Common.Variable
		/// @DnDVersion : 1
		/// @DnDHash : 5586561A
		/// @DnDParent : 689C970D
		/// @DnDArgument : "expr" "false"
		/// @DnDArgument : "var" "global.ui"
		global.ui = false;
	
		/// @DnDAction : YoYo Games.Paths.Start_Path
		/// @DnDVersion : 1.1
		/// @DnDHash : 3F6689BF
		/// @DnDApplyTo : dce01893-52fc-4398-ad95-8c88eef129f0
		/// @DnDParent : 689C970D
		/// @DnDArgument : "path" "ui_info_close"
		/// @DnDArgument : "speed" "50"
		/// @DnDArgument : "relative" "true"
		/// @DnDSaveInfo : "path" "3c7e4a30-d0dc-4a7c-af55-33616da99290"
		with(obj_ui_info) path_start(ui_info_close, 50, path_action_stop, true);
	}
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 189D01CB
else
{
	/// @DnDAction : YoYo Games.Common.Execute_Code
	/// @DnDVersion : 1
	/// @DnDHash : 0C637B92
	/// @DnDParent : 189D01CB
	/// @DnDArgument : "code" "audio_play_sound(snd_not_enough_gold,1,0)$(13_10)global.not_enough_gold = true$(13_10)"
	audio_play_sound(snd_not_enough_gold,1,0)
	global.not_enough_gold = true

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 6DF6755B
	/// @DnDApplyTo : bd506929-690c-4d0b-bcc7-76f64dfd6fdb
	/// @DnDParent : 189D01CB
	/// @DnDArgument : "steps" "600"
	with(obj_score_money) {
	alarm_set(0, 600);
	
	}
}