event_inherited();

externalPlayerId = json_raw_data[? "externalPlayerId"];

// Parse failed prize IDs
var failedPrizeListJSON = json_raw_data[? "failedAwardedPrizeIds"];
if(!is_undefined(failedPrizeListJSON) && ds_exists(failedPrizeListJSON, ds_type_list))
{
	failedAwardedPrizeIds = ds_list_create();
	ds_list_copy(failedAwardedPrizeIds, failedPrizeListJSON);
}

// Load prize list
var prizeListJSON = json_raw_data[? "prizes"];
if(!is_undefined(prizeListJSON) && ds_exists(prizeListJSON, ds_type_list))
{
	prizes = ds_list_create();
	gameon_util_data_list_parse(json_raw_data, prizes, oGameOnPrizeClaimItem, "prizes");
}