if(!is_undefined(prizes) && ds_exists(prizes, ds_type_list))
{
	var listSize = ds_list_size(prizes);
	for(var listIdx = 0; listIdx < listSize; ++listIdx)
	{
		gameon_util_object_destroy(prizes[| listIdx]);
	}
	
	ds_list_destroy(prizes);
}
	
if(!is_undefined(failedAwardedPrizeIds) && ds_exists(failedAwardedPrizeIds, ds_type_list))
	ds_list_destroy(failedAwardedPrizeIds);
	
prizes = undefined;
failedAwardedPrizeIds = undefined;

event_inherited();

