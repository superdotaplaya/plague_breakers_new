{
    "id": "60ace261-04f6-4b4c-a0c6-4e7097c14949",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameOnPrizeClaim",
    "eventList": [
        {
            "id": "786ba46a-ea16-4d55-ad1b-96f831908b0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "60ace261-04f6-4b4c-a0c6-4e7097c14949"
        },
        {
            "id": "8990d5b5-a29b-4a43-b905-b3059b19fc85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "60ace261-04f6-4b4c-a0c6-4e7097c14949"
        },
        {
            "id": "7b48c87f-d0b3-455a-9d58-deb566092410",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "60ace261-04f6-4b4c-a0c6-4e7097c14949"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0330c072-566e-4a20-bbb9-c6edf5a161cf",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}