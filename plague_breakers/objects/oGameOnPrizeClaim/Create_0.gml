event_inherited();

enum eGameOnPrizeAwardMethod
{
	CLAIM = 0,
	FULFILL
};

externalPlayerId = undefined;
failedAwardedPrizeIds = undefined;
prizes = undefined;