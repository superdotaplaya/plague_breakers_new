/// @description HTTP Request Success
request_result_data_parsed = json_decode(request_result_data);
gameon_util_log("Player authenticated! " + json_encode(request_result_data_parsed));

// Update and save session data
oGameOnClient.session_id = request_result_data_parsed[? "sessionId"];
oGameOnClient.session_exp = request_result_data_parsed[? "sessionExpirationDate"];
oGameOnClient.session_api_key = request_result_data_parsed[? "sessionApiKey"];
gameon_util_session_data_save();

oGameOnClient.state = eGameOnState.AUTHENTICATED;
ds_map_add(oGameOnClient.http_headers_default, "Session-Id", oGameOnClient.session_id);
gameon_player_update(global.player_name)
gameon_tournament_query()

event_inherited();