/// @description Clean up request result data
if(!is_undefined(request_result_data_parsed) && ds_exists(request_result_data_parsed, ds_type_map))
	ds_map_destroy(request_result_data_parsed);

event_inherited();