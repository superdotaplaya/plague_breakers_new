{
    "id": "e0625a1f-d004-42a1-9d7b-974b3eb0f66e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameOnPromiseAuth",
    "eventList": [
        {
            "id": "87ed6d02-3cd5-4e21-902a-b8c85b726937",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "e0625a1f-d004-42a1-9d7b-974b3eb0f66e"
        },
        {
            "id": "8036ddba-c598-4727-bbee-9b121073f767",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "e0625a1f-d004-42a1-9d7b-974b3eb0f66e"
        },
        {
            "id": "f3b47dfb-5e29-49a2-853f-b9e25ff6f583",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 15,
            "eventtype": 7,
            "m_owner": "e0625a1f-d004-42a1-9d7b-974b3eb0f66e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6d6eb90d-22ce-42f0-b45c-d4b59286039c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}