{
    "id": "4da2c3fe-60c1-4b4b-ac4f-9870d2b85391",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shop_x_button",
    "eventList": [
        {
            "id": "6e024f3c-9e26-4f2b-9796-e91b7c57fd30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4da2c3fe-60c1-4b4b-ac4f-9870d2b85391"
        },
        {
            "id": "ef7fe5ac-48e3-4b94-8075-5643812be34b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "4da2c3fe-60c1-4b4b-ac4f-9870d2b85391"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e1e0b95f-ad8a-4e3b-adfd-d2dc213ad78b",
    "visible": true
}