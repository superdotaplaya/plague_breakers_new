/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 4F0E2CA6
/// @DnDArgument : "code" "instance_destroy_shop()$(13_10)global.shop_open = false$(13_10)instance_activate_layer("Instances")"
instance_destroy_shop()
global.shop_open = false
instance_activate_layer("Instances")

/// @DnDAction : YoYo Games.Paths.Start_Path
/// @DnDVersion : 1.1
/// @DnDHash : 7DC555CE
/// @DnDApplyTo : 37f57748-c862-493a-bbbd-c8ffdf8f8d60
/// @DnDArgument : "path" "shop_close"
/// @DnDArgument : "speed" "50"
/// @DnDSaveInfo : "path" "82e069ee-3c69-4d32-95f0-56a32a932d1f"
with(obj_shop_menu) path_start(shop_close, 50, path_action_stop, false);