/// @description Request Success
event_inherited();

isTeam = ds_map_exists(json_raw_data, "teamId");;

externalPlayerId =		json_raw_data[? "externalPlayerId"];
playerName =			json_raw_data[? "playerName"];
isCurrentPlayer =		ds_map_exists(json_raw_data, "isCurrentPlayer") ? json_raw_data[? "isCurrentPlayer"] : false;

teamId =				json_raw_data[? "teamId"];
teamName =				json_raw_data[? "teamName"];
hasPlayerContributed =	ds_map_exists(json_raw_data, "hasPlayerContributed") ? json_raw_data[? "hasPlayerContributed"] : false;

rank =					json_raw_data[? "rank"];
leaderboardScore =		json_raw_data[? "score"];

global.leaderboardArray[real(rank)] = ("Rank:  " + string(json_raw_data[? "rank"]) + "   " + string_copy(json_raw_data[? "playerName"],1,15)+ "    Wave   " + string(json_raw_data[? "score"]))
	log("[GAMEON-LEADERBOARD]   " + string(global.leaderboardArray[real(rank)]))
