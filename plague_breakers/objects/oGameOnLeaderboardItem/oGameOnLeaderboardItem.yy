{
    "id": "3ed91c7e-6d2a-4519-a453-61ba058da219",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameOnLeaderboardItem",
    "eventList": [
        {
            "id": "55e80e3e-642d-463a-8cd6-dbda9a2a8abc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3ed91c7e-6d2a-4519-a453-61ba058da219"
        },
        {
            "id": "3d2fc5ad-51ba-48a3-9362-65aee5496c21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "3ed91c7e-6d2a-4519-a453-61ba058da219"
        },
        {
            "id": "67a00a2d-4c31-4b7d-a2d0-6435c613209c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3ed91c7e-6d2a-4519-a453-61ba058da219"
        },
        {
            "id": "f3eb31df-f95d-4b8b-b737-b6478d08bc62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 7,
            "m_owner": "3ed91c7e-6d2a-4519-a453-61ba058da219"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0330c072-566e-4a20-bbb9-c6edf5a161cf",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}