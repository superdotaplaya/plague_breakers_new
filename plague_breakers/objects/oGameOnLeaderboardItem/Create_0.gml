event_inherited();

isTeam =				false;

externalPlayerId =		undefined;
isCurrentPlayer =		undefined;
playerName =			undefined;

teamId =				undefined;
teamName =				undefined;
hasPlayerContributed =	undefined;

rank =					undefined;
leaderboardScore =		undefined;