{
    "id": "a24a2b35-b1ac-4ef1-90c0-dddd124b9c4e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tutorial",
    "eventList": [
        {
            "id": "b556bb45-c28b-43b1-ad98-1c644fa6f25f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "a24a2b35-b1ac-4ef1-90c0-dddd124b9c4e"
        },
        {
            "id": "d0903e84-f92b-466a-a6c3-f9c4bc453d03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "a24a2b35-b1ac-4ef1-90c0-dddd124b9c4e"
        },
        {
            "id": "b276c55f-60c5-4160-9b0c-aee6438c04d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a24a2b35-b1ac-4ef1-90c0-dddd124b9c4e"
        },
        {
            "id": "d20813ce-5d0b-4ac9-836d-820a7fd18aee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "a24a2b35-b1ac-4ef1-90c0-dddd124b9c4e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "44d0938e-a44c-42ae-a9ec-78fd21aecb46",
    "visible": true
}