{
    "id": "32183b7a-d784-471e-939b-c846cb1d5088",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tower_poison_shop",
    "eventList": [
        {
            "id": "dceb9087-ee8f-4975-83b5-9578b9d2602f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "32183b7a-d784-471e-939b-c846cb1d5088"
        },
        {
            "id": "ec6119d9-d8d3-4f27-8908-685355f0ab14",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "32183b7a-d784-471e-939b-c846cb1d5088"
        },
        {
            "id": "27e522c4-ba7c-486c-97bf-b111aca01340",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "32183b7a-d784-471e-939b-c846cb1d5088"
        },
        {
            "id": "de5780a5-1459-4d5d-bc12-bc27d372eb54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "32183b7a-d784-471e-939b-c846cb1d5088"
        },
        {
            "id": "8379d3ec-ffa0-4c79-b010-6b1ed6a43f49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "32183b7a-d784-471e-939b-c846cb1d5088"
        },
        {
            "id": "0180d615-6511-4499-b5b2-f8dbb893c03a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "32183b7a-d784-471e-939b-c846cb1d5088"
        },
        {
            "id": "510f2b90-4610-4e05-8808-99e6b0c45d92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 9,
            "m_owner": "32183b7a-d784-471e-939b-c846cb1d5088"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "92dc95d5-3f2b-416f-bc4b-84f7be27f74b",
    "visible": true
}