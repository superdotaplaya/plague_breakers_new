/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 4EEC039D
/// @DnDArgument : "code" "randx = irandom_range(-32,32)$(13_10)if distance_to_object(obj_enemy) <= range then$(13_10){$(13_10)instance_create_depth(x,y,0,obj_modular_attack)$(13_10)audio_play_sound(snd_tower_attack_basic,1,0)$(13_10)$(13_10)$(13_10)if bonus_projectiles != 0 then$(13_10){$(13_10)for ( i = 0; i < bonus_projectiles; i += 1)$(13_10)	{$(13_10)instance_create_layer(x+randx,y,"Instances_towers",obj_modular_attack_bonus)$(13_10)$(13_10)	}$(13_10)}$(13_10)}$(13_10)alarm_set(0,attack_speed)"
randx = irandom_range(-32,32)
if distance_to_object(obj_enemy) <= range then
{
instance_create_depth(x,y,0,obj_modular_attack)
audio_play_sound(snd_tower_attack_basic,1,0)


if bonus_projectiles != 0 then
{
for ( i = 0; i < bonus_projectiles; i += 1)
	{
instance_create_layer(x+randx,y,"Instances_towers",obj_modular_attack_bonus)

	}
}
}
alarm_set(0,attack_speed)