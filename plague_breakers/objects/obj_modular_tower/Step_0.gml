/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 3F5EF09D
/// @DnDArgument : "code" "target= instance_nearest(x,y,obj_enemy)$(13_10)if target != noone then$(13_10){$(13_10)	image_angle	= point_direction(x,y,target.x,target.y)-90$(13_10)}$(13_10)if target = noone$(13_10){$(13_10)	target = instance_nearest(x,y,all)	$(13_10)}$(13_10)$(13_10)if hp <= 0 then$(13_10){$(13_10)instance_destroy()	$(13_10)}$(13_10)ini_open("quests.ini")$(13_10)if ini_read_string("Quest 1","quest"," ") = "Upgrade a tower to level 3 in either its range or damage" and dmg_lvl = 3 then$(13_10){$(13_10)premium_add(10)$(13_10)ini_write_string("Quest 1","quest","Completed")$(13_10)}$(13_10)if ini_read_string("Quest 1","quest"," ") = "Upgrade a tower to level 3 in either its range or damage" and range_lvl = 3 then$(13_10){$(13_10)premium_add(10)$(13_10)ini_write_string("Quest 1","quest","Completed")$(13_10)}$(13_10)ini_close()$(13_10)bonus = variable_instance_get(instance_nearest(x,y,obj_modular_area),"attack_speed_bonus")$(13_10)attack_speed = attack_speed_base - bonus$(13_10)bonus_damage = variable_instance_get(instance_nearest(x,y,obj_modular_area),"attack_damage_bonus")$(13_10)$(13_10)damage = damage_base + bonus_damage$(13_10)$(13_10)bonus_range = variable_instance_get(instance_nearest(x,y,obj_modular_area),"attack_range_bonus")$(13_10)$(13_10)range = base_range + bonus_range$(13_10)$(13_10)bonus_projectiles = variable_instance_get(instance_nearest(x,y,obj_modular_area),"bonus_projectiles")$(13_10)"
target= instance_nearest(x,y,obj_enemy)
if target != noone then
{
	image_angle	= point_direction(x,y,target.x,target.y)-90
}
if target = noone
{
	target = instance_nearest(x,y,all)	
}

if hp <= 0 then
{
instance_destroy()	
}
ini_open("quests.ini")
if ini_read_string("Quest 1","quest"," ") = "Upgrade a tower to level 3 in either its range or damage" and dmg_lvl = 3 then
{
premium_add(10)
ini_write_string("Quest 1","quest","Completed")
}
if ini_read_string("Quest 1","quest"," ") = "Upgrade a tower to level 3 in either its range or damage" and range_lvl = 3 then
{
premium_add(10)
ini_write_string("Quest 1","quest","Completed")
}
ini_close()
bonus = variable_instance_get(instance_nearest(x,y,obj_modular_area),"attack_speed_bonus")
attack_speed = attack_speed_base - bonus
bonus_damage = variable_instance_get(instance_nearest(x,y,obj_modular_area),"attack_damage_bonus")

damage = damage_base + bonus_damage

bonus_range = variable_instance_get(instance_nearest(x,y,obj_modular_area),"attack_range_bonus")

range = base_range + bonus_range

bonus_projectiles = variable_instance_get(instance_nearest(x,y,obj_modular_area),"bonus_projectiles")