{
    "id": "6805e61e-ad9e-4afe-a331-68c08d156c9c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_modular_tower",
    "eventList": [
        {
            "id": "86664932-9b99-4ded-a1cc-361fbccd51d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "6805e61e-ad9e-4afe-a331-68c08d156c9c"
        },
        {
            "id": "a1c88f7e-1dac-4431-b8ae-df34d7311db0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6805e61e-ad9e-4afe-a331-68c08d156c9c"
        },
        {
            "id": "d94e50e6-92b0-4a2c-96b2-515a2dcd263f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "6805e61e-ad9e-4afe-a331-68c08d156c9c"
        },
        {
            "id": "d708e25e-ddbb-47eb-9c24-bd2c36f38fff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6805e61e-ad9e-4afe-a331-68c08d156c9c"
        },
        {
            "id": "5a488d04-0ccc-4151-8aff-a0107e613352",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "6805e61e-ad9e-4afe-a331-68c08d156c9c"
        },
        {
            "id": "8b6fa828-b38e-404a-a349-f1605d313276",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "6805e61e-ad9e-4afe-a331-68c08d156c9c"
        },
        {
            "id": "e375e9e2-4687-48c0-b334-c77c194b4849",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6805e61e-ad9e-4afe-a331-68c08d156c9c"
        },
        {
            "id": "605d0322-6389-436b-945d-d0417fa08b7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "6805e61e-ad9e-4afe-a331-68c08d156c9c"
        },
        {
            "id": "6733f35e-37b2-4dff-8485-3098bdcdf823",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "6805e61e-ad9e-4afe-a331-68c08d156c9c"
        },
        {
            "id": "a9be2fb8-38d3-41b1-8f4c-a0b097a7bed2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6805e61e-ad9e-4afe-a331-68c08d156c9c"
        },
        {
            "id": "a2ad4797-c10e-4b17-a57e-fd17af2f6c22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "f0875ffd-fd8e-428c-aabc-956eb91215a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6805e61e-ad9e-4afe-a331-68c08d156c9c"
        },
        {
            "id": "52c30f85-1e1e-469d-8ab6-bbb97a7048ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "6805e61e-ad9e-4afe-a331-68c08d156c9c"
        },
        {
            "id": "21d61984-7153-43fa-9d85-4dd5430ae37e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "6805e61e-ad9e-4afe-a331-68c08d156c9c"
        },
        {
            "id": "62225043-5d36-4600-858c-dbf1d9445965",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "6805e61e-ad9e-4afe-a331-68c08d156c9c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "692933ab-3c13-4a12-b13e-3265730f1324",
    "visible": true
}