{
    "id": "2ca34d38-de67-4caa-be0a-aac3f524ace4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bonus_attack_damage_shop",
    "eventList": [
        {
            "id": "df3005b6-610f-4fde-8ee4-e35e39437e08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "2ca34d38-de67-4caa-be0a-aac3f524ace4"
        },
        {
            "id": "294c03ca-b844-48c4-9f7c-2f9944770f5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2ca34d38-de67-4caa-be0a-aac3f524ace4"
        },
        {
            "id": "1a3ddbf2-1e87-42d2-9d08-7a2de3543cde",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2ca34d38-de67-4caa-be0a-aac3f524ace4"
        },
        {
            "id": "321ca7c4-e42c-4b6e-b331-3e26ff9e7747",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "2ca34d38-de67-4caa-be0a-aac3f524ace4"
        },
        {
            "id": "9648d1b6-56f6-4c0d-8df6-39fc32057aa3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "2ca34d38-de67-4caa-be0a-aac3f524ace4"
        },
        {
            "id": "3c843f44-b9ce-49d5-a3cc-b86c9a034a3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2ca34d38-de67-4caa-be0a-aac3f524ace4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7db01435-55ce-4634-a108-3c17341a58ce",
    "visible": true
}