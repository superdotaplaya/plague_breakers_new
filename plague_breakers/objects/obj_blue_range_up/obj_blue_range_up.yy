{
    "id": "ad3c7a93-2d91-49e2-8273-cf85eb89e048",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blue_range_up",
    "eventList": [
        {
            "id": "6b0ccb91-afb2-4628-9839-9b3cb4bb6476",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ad3c7a93-2d91-49e2-8273-cf85eb89e048"
        },
        {
            "id": "e55db7d9-f7e6-4b9a-91ce-ca7f577750a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ad3c7a93-2d91-49e2-8273-cf85eb89e048"
        },
        {
            "id": "696b71a5-91db-4ded-a9f0-8e8bcd606a72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "ad3c7a93-2d91-49e2-8273-cf85eb89e048"
        },
        {
            "id": "d7bc2dcd-61bd-4adc-9bf6-cac3e76ec552",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "ad3c7a93-2d91-49e2-8273-cf85eb89e048"
        },
        {
            "id": "89ff0024-47b9-4cb4-86e1-04313c414df5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ad3c7a93-2d91-49e2-8273-cf85eb89e048"
        },
        {
            "id": "3d56cd43-bf62-4a52-9f0e-92baf9665968",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ad3c7a93-2d91-49e2-8273-cf85eb89e048"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "72079f4f-9558-4696-8b39-21e6a15d0714",
    "visible": true
}