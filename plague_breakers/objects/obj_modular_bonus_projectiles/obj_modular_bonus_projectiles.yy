{
    "id": "345fca6a-7fab-4f58-ac30-804473824c5a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_modular_bonus_projectiles",
    "eventList": [
        {
            "id": "4f736717-e82c-489b-aae4-edce5e8701ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "345fca6a-7fab-4f58-ac30-804473824c5a"
        },
        {
            "id": "9fa32418-cdda-444e-8cc8-c08ea408a520",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "345fca6a-7fab-4f58-ac30-804473824c5a"
        },
        {
            "id": "dfa573fb-3966-4fce-8911-1fe5258c4046",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "345fca6a-7fab-4f58-ac30-804473824c5a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "07f495b3-fe21-4ae5-8ec5-f0def477b4f5",
    "visible": true
}