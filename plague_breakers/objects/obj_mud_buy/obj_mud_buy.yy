{
    "id": "71b2a270-606b-4041-bf6a-7f7b00d49850",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mud_buy",
    "eventList": [
        {
            "id": "1dc66ffa-155e-4192-84b1-bc7d364987c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "71b2a270-606b-4041-bf6a-7f7b00d49850"
        },
        {
            "id": "449fe55b-1fa1-4e07-b9d6-bd3b3d64d5ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "71b2a270-606b-4041-bf6a-7f7b00d49850"
        },
        {
            "id": "443402ec-3550-4a68-b7cc-7e00c002bbe6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "71b2a270-606b-4041-bf6a-7f7b00d49850"
        },
        {
            "id": "9af65e30-14bb-463c-b1b0-6242478cdd2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "71b2a270-606b-4041-bf6a-7f7b00d49850"
        },
        {
            "id": "d9112d16-9d22-4755-9d48-00d6e147cb26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "71b2a270-606b-4041-bf6a-7f7b00d49850"
        },
        {
            "id": "0b8758a7-a983-4dd9-a94d-6ab15181780d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "71b2a270-606b-4041-bf6a-7f7b00d49850"
        },
        {
            "id": "a396eff0-b6c2-4be9-8716-d0b00c808c16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "71b2a270-606b-4041-bf6a-7f7b00d49850"
        },
        {
            "id": "7cde1721-6e60-4849-ab46-b504f200dafb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "71b2a270-606b-4041-bf6a-7f7b00d49850"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e564aebc-39ea-457e-9d53-6cba7b2083f1",
    "visible": true
}