/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 0336DFF6
/// @DnDArgument : "code" "if global.gold >= cost then$(13_10){$(13_10)instance_create_layer(x,y,"Instances_towers",obj_mud)$(13_10)global.gold -= cost$(13_10)held = false$(13_10)audio_play_sound(snd_mud_splat,1,0)$(13_10)}$(13_10)instance_create_layer(1560,1300,"shop_icons",obj_mud_buy)$(13_10)instance_destroy(obj_tooltips)$(13_10)instance_destroy()$(13_10)audio_play_sound(snd_buy,1,0)"
if global.gold >= cost then
{
instance_create_layer(x,y,"Instances_towers",obj_mud)
global.gold -= cost
held = false
audio_play_sound(snd_mud_splat,1,0)
}
instance_create_layer(1560,1300,"shop_icons",obj_mud_buy)
instance_destroy(obj_tooltips)
instance_destroy()
audio_play_sound(snd_buy,1,0)