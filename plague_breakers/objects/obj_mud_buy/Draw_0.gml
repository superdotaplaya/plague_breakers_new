/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 5F475DFA
/// @DnDArgument : "code" "draw_self()$(13_10)draw_set_color(c_white)"
draw_self()
draw_set_color(c_white)

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 1F89E5FC
/// @DnDArgument : "var" "held"
/// @DnDArgument : "value" "false"
if(held == false)
{
	/// @DnDAction : YoYo Games.Drawing.Draw_Value
	/// @DnDVersion : 1
	/// @DnDHash : 06CF3803
	/// @DnDParent : 1F89E5FC
	/// @DnDArgument : "x" "-75"
	/// @DnDArgument : "x_relative" "1"
	/// @DnDArgument : "y" "115"
	/// @DnDArgument : "y_relative" "1"
	/// @DnDArgument : "caption" ""Cost: ""
	/// @DnDArgument : "var" "cost"
	draw_text(x + -75, y + 115, string("Cost: ") + string(cost));
}