/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 5A3312EE
/// @DnDArgument : "expr" ""Slows enemies while they are on top if, lasts for 10 seconds""
/// @DnDArgument : "var" "global.tooltip"
global.tooltip = "Slows enemies while they are on top if, lasts for 10 seconds";

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 51FD86FE
/// @DnDArgument : "code" "image_xscale = 2.2$(13_10)image_yscale = 2.2$(13_10)instance_create_layer(x-160,y-450,"shop_icons",obj_tooltips)"
image_xscale = 2.2
image_yscale = 2.2
instance_create_layer(x-160,y-450,"shop_icons",obj_tooltips)